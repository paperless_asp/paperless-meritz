package net.ib.paperless;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaperlessMeritzApplicationTests {

	@Test
	public void contextLoads() {
	}

}
