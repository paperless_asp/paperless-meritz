$(document).ready(function() {

	//현재 페이지가 /main이면 이전버튼 삭제
	var isPrevView = false;
	var uri = location.href;
	var prevBtn = $('.btn_page_prev');
	prevBtn.css("visibility","hidden");

	if(uri.indexOf("/main") == -1){
		isPrevView = true;
		prevBtn.css("visibility","visible");
	}

	
	$('.drop').click(function(){
		var $this = $(this);
		$this.toggleClass('on');
		$this.parent().next('.drop_cont').toggle();
	});
	
	$('.drop + td>.progress').click(function(){ 
		$(this).parent().siblings('.drop').trigger('click');
	});

	//layer_close
	$('.layer_close').click(function(){
		var $this = $(this);
		$this.parent().parent().hide();
	});
	

	$('.fastapp_btn').click(function(){
		$(this).toggleClass('on');
		$('.fastapp_contents').toggle();
		$('.fast_mask').toggle();
	});
	
	$('.btn_page_prev').click(function(){
		if(isPrevView){
			if(uri.indexOf("/search/customer_agree") != -1){
				self.close();
			}
			else{
				location.href=history.back();
			}
		}
	});
	
	
	showAlert = function (msg, header, callback) {
	      var mydiv = $("<div id='mydiv'> </div>");
	      mydiv.alertBox({
	          message: msg,
	          header: header,
	          callback: callback
	      });

	  },

	  
	  $.widget("MY.alertBox", {
	      options: {
	          message: "",
	          header: "",
	          callback: ''
	      },
		
	      _create: function () {
	          var self = this;
	          self.callback = self.options.callback;

	          self.container = $(".alert-messagebox");
	          var header = self.container.find(".alert-header");
	          header.html(self.options.header);

	          var message = self.container.find(".alert-message");
	          message.html(self.options.message);

	          var closeButton = self.container.find("button.modal-close-button");
	          closeButton.click(function () {
	              self.close();
	          });

	          self.show();
	      },

	      show: function () {
	          var self = this;
	          self.container.modal({
	              maxWidth: 500
	          });
	      },

	      close: function () {
	          if (this.callback != null) {
	              this.callback();
	              $.modal.close();
	              return;
	          }
	          $.modal.close();

	      },

	      destroy: function () {
	          $.Widget.prototype.destroy.call(this);
	      }

	  });
});