<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>굿페이퍼</title>
	<%@ include file="/WEB-INF/views/common/head.jsp"%>
</head>
<body class="bg">
<div id="wrap">

	<!-- header -->
	<%@ include file="/WEB-INF/views/common/head.jsp"%>
	<div id="container">
		<div class="contents">
			<div class="contbox first">
				<span class="ico_style05"></span>
				<p class="cont_txt type03">
					<span class="txt_style01 ex_bold"></span>
					페이지를 <span class="txt_color02">찾을 수 없습니다.</span>
				</p>
				<p class="txt_style02">화면을 장시간 사용하지 않으셨거나,<br>고객님 휴대폰의 인터넷 사용이 원활하지 않을 수 있습니다.<br>처음화면으로 이동 후 다시 시작해 주세요.</p>
			</div>
			<div class="btn_wrap">
				<button type="button" class="btn_type10 bg_color11" onClick="window.history.back()">
					<span class="txt">이전화면으로 이동</span>
					<span class="btn_icon btn_icon5"></span>
				</button>
			</div>
		</div>
	</div>
</div>

</body>
</html>