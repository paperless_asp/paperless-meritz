<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="/WEB-INF/views/common/head.jsp"%>
<head>
<script type="text/javascript">
	$(document).ready(function(){	
		stepMenuControl(3);	
		
		//뒤로가기막기
		history.pushState(null, null, location.href);
		window.onpopstate = function(event) {	
			history.go(1);
		};
		
		//초기화 param3
		if('${map.param3}' == 'Y'){
			$('#div_confirm_btn').show();
		}else{
			$('#div_sign_btn').show();
			$('.sig_btn').removeClass("on");
		}
		
		/***********************************약관 동의***********************************************/
		
		$('.all_agree').click(function(){
			if ($('.all_agree').hasClass("on")) {
				$('.check1').prop("checked","");
				$('.check2').prop("checked","");
				$('.check3').prop("checked","");
				$('.check3_1').prop("checked","");
				$('.all_agree').removeClass("on");	
				$('.sig_btn').addClass("on");
				$('#csinfyn01').val('0');
				$('#csinfyn02').val('0');
				$('#csinfyn03').val('0');
				$('#csinfyn04').val('0');
				agreeSelect4("");
			} else {
				$('.check1').prop("checked","checked");
				$('.check2').prop("checked","checked");
				$('.check3').prop("checked","checked");
				$('.check3_1').prop("checked","checked");
				$('.all_agree').addClass("on");		
				$('.sig_btn').removeClass("on");
				$('#csinfyn01').val('1');
				$('#csinfyn02').val('1');
				$('#csinfyn03').val('1');
				$('#csinfyn04').val('1');
				agreeSelect4("checked");
			}
		});
		
		//1,2,3 개인신용정보필수적 수집 이용에 관한 체크박스
		$('.check1').change(function(){
			setAgreeCheckValue($(this),'1');
			isNextBtnEnable();
			isAgreeBtnEnable();
		});
		
		$('.check2').change(function(){
			setAgreeCheckValue($(this),'2');
			isNextBtnEnable();
			isAgreeBtnEnable();
		});
		
		$('.check3').change(function(){
			setAgreeCheckValue($(this),'3');
			isNextBtnEnable();
			isAgreeBtnEnable();
		});
		
		$('.check3_1').change(function(){
			setAgreeCheckValue($(this),'3_1');
			isAgreeBtnEnable();
			if(!$(".check3_1").is(":checked")){
				agreeSelect4("");
			}else{
				agreeSelect4("checked");
			}
		});
		
		//4. 개인신용정보선택적 수집 이용 동의 체크박스
		$('.chk3_1_1').change(function(){
			setAgreeSelectBoxValue($(this),'2');
			isSelect4CheckEnable();
			isAgreeBtnEnable();
			if($(this).is(":checked")){
				$('#csinfyn04').val('1');
			}
			if(!$(".chk3_1_1").is(":checked") &&  !$(".chk3_1_2").is(":checked") && !$(".chk3_1_3").is(":checked") && !$(".chk3_1_4").is(":checked")) {
				$('#csinfyn04').val('0');
			}
		});
		$('.chk3_1_2').change(function(){
			setAgreeSelectBoxValue($(this),'3');
			isSelect4CheckEnable();
			isAgreeBtnEnable();
			if($(this).is(":checked")){
				$('#csinfyn04').val('1');
			}
			if(!$(".chk3_1_1").is(":checked") &&  !$(".chk3_1_2").is(":checked") && !$(".chk3_1_3").is(":checked") && !$(".chk3_1_4").is(":checked")) {
				$('#csinfyn04').val('0');
			}
		});
		$('.chk3_1_3').change(function(){
			setAgreeSelectBoxValue($(this),'4');
			isSelect4CheckEnable();
			isAgreeBtnEnable();
			if($(this).is(":checked")){
				$('#csinfyn04').val('1');
			}
			if(!$(".chk3_1_1").is(":checked") &&  !$(".chk3_1_2").is(":checked") && !$(".chk3_1_3").is(":checked") && !$(".chk3_1_4").is(":checked")) {
				$('#csinfyn04').val('0');
			}
		});
		$('.chk3_1_4').change(function(){
			setAgreeSelectBoxValue($(this),'5');
			isSelect4CheckEnable();
			isAgreeBtnEnable();
			if($(this).is(":checked")){
				$('#csinfyn04').val('1');
			}
			if(!$(".chk3_1_1").is(":checked") &&  !$(".chk3_1_2").is(":checked") && !$(".chk3_1_3").is(":checked") && !$(".chk3_1_4").is(":checked")) {
				$('#csinfyn04').val('0');
			}
		});
		
		//취약금융소비자확인서 - 동의여부
		$('.check4').click(function(){
			$('.check4').prop("checked","checked");
			$('.check5').prop("checked","");
		});
		$('.check5').click(function(){
			$('.check4').prop("checked","");
			$('.check5').prop("checked","checked");
		});
		
		//취약금융소비자확인서 - 해당사항
		$('.check6').click(function(){
			$('.check6').prop("checked","checked");
			$('.check7').prop("checked","");
			$('.check8').prop("checked","");
			$('.check9').prop("checked","");
		});
		$('.check7').click(function(){
			$('.check6').prop("checked","");
			$('.check7').prop("checked","checked");
			$('.check8').prop("checked","");
			$('.check9').prop("checked","");
		});
		$('.check8').click(function(){
			$('.check6').prop("checked","");
			$('.check7').prop("checked","");
			$('.check8').prop("checked","checked");
			$('.check9').prop("checked","");
		});
		$('.check9').click(function(){
			$('.check6').prop("checked","");
			$('.check7').prop("checked","");
			$('.check8').prop("checked","");
			$('.check9').prop("checked","checked");
		});

		
		$('.sig_btn').click(function(){
			if($(this).attr('class') == "btn_type10 bg_color11 sig_btn on")
			{
				return false;	
			}
	    	
			//개인 신용정보 약관 param3 가 Y,N 으로 구분
			if('${map.param3}' == 'Y'){
				getUserData('Y');
			}else{
				getUserData('N');
			}
	  
		});
		
		$('.confirm_btn').click(function(){
			$('.seconds').hide();
			$('#div_confirm_btn').hide();
			$('#div_sign_btn').show();
			$('.first').show();
		});
	});
	
//----------------------------------------------------------function----------------------------------------------

	function getUserData(param3) {
		//취약금융소비자확인서 
		var vfnYn, vfnInfo;

		$(":checkbox[name='vfnYn']:checked").each(function(pi,po){
			vfnYn = po.value;
		});
		$(":checkbox[name='vfnInfo']:checked").each(function(pi,po){
			vfnInfo = po.value;
		});
		
		//파람 셋팅
		$('#param3').val('${map.param3}');
		
		if(param3 == 'Y'){
			var csinfyn01 = $('#csinfyn01').val();
			var csinfyn02 = $('#csinfyn02').val();
			var csinfyn03 = $('#csinfyn03').val();
			var csinfyn04 = $('#csinfyn04').val();
			
			var usmktmd02 = $('#usmktmd02').val();
			var usmktmd03 = $('#usmktmd03').val();
			var usmktmd04 = $('#usmktmd04').val();
			var usmktmd05 = $('#usmktmd05').val();
			
			$.ajax({
				url: "/ajax/eformUserData?progress_id=${map.seq}",
				global: false,
				type: "GET",
				data: {vfnYn : vfnYn, vfnInfo : vfnInfo, param3 : param3,
					csinfyn01 : csinfyn01, csinfyn02 : csinfyn02, csinfyn03 : csinfyn03, csinfyn04 : csinfyn04, 
					usmktmd02 : usmktmd02, usmktmd03 : usmktmd03, usmktmd04 : usmktmd04, usmktmd05 : usmktmd05
					},
// 				async: false,
				success: function (msg) {
					if (!msg) {
    	    			showAlert('먼저 상담사와 전화 상담 진행 후 상담사가 자료를 입력해야만\n전자서식 내용 확인 / 전자 서명 단계를 진행하실 수 있습니다.\n이미 전화 상담을 진행 하셨다면 상담사에게 진행상태 확인을 요청해 주세요.', '알림', function(){  window.history.back();});
					} else if(!msg.sendOmap0003Result){
						showAlert('먼저 상담사와 전화 상담 진행 후 상담사가 자료를 입력해야만\n전자서식 내용 확인 / 전자 서명 단계를 진행하실 수 있습니다.\n이미 전화 상담을 진행 하셨다면 상담사에게 진행상태 확인을 요청해 주세요.', '알림', function(){  window.history.back();});
					} else {
						var rawJsonData = JSON.stringify(msg);		
						$("#userdata").val(rawJsonData);
						
						$("#vfnYn").val(vfnYn);
						$("#vfnInfo").val(vfnInfo);
						$("#ct_code").val(msg.ct_code);
						$("#eform_id").val(msg.eform_id);
						
						$("#eformData").submit();
					}
				},beforeSend : function(){
					showLoading(false);
				},complete:function(){
					closeLoading();
				}
			});
		}else{
			$.ajax({
				url: "/ajax/eformUserData?progress_id=${map.seq}",
				global: false,
				type: "GET",
				data: {vfnYn : vfnYn, vfnInfo : vfnInfo},
// 				async: false,
				success: function (msg) {
					if (!msg) {
    	    			showAlert('먼저 상담사와 전화 상담 진행 후 상담사가 자료를 입력해야만\n전자서식 내용 확인 / 전자 서명 단계를 진행하실 수 있습니다.\n이미 전화 상담을 진행 하셨다면 상담사에게 진행상태 확인을 요청해 주세요.', '알림', function(){  window.history.back();});
					} else if(!msg.sendOmap0003Result){
						showAlert('먼저 상담사와 전화 상담 진행 후 상담사가 자료를 입력해야만\n전자서식 내용 확인 / 전자 서명 단계를 진행하실 수 있습니다.\n이미 전화 상담을 진행 하셨다면 상담사에게 진행상태 확인을 요청해 주세요.', '알림', function(){  window.history.back();});
					} else {
						var rawJsonData = JSON.stringify(msg);		
						$("#userdata").val(rawJsonData);
						
						$("#vfnYn").val(vfnYn);
						$("#vfnInfo").val(vfnInfo);
						$("#ct_code").val(msg.ct_code);
						$("#eform_id").val(msg.eform_id);
						
						$("#eformData").submit();
					}
				},beforeSend : function(){
					showLoading(false);
				},complete:function(){
					closeLoading();
				}
			});
		}
	}
	
	function isNextBtnEnable() {
		if(!$(".check1").is(":checked") ||  !$(".check2").is(":checked") || !$(".check3").is(":checked")) {
			$('.sig_btn').addClass("on");
			return;
		}
		if ($(".check1").is(":checked") &&  $(".check2").is(":checked") && $(".check3").is(":checked")) {
			$('.sig_btn').removeClass("on");
			return;
		}
	}
	
	function isAgreeBtnEnable() {
		if(!$(".check1").is(":checked") ||  !$(".check2").is(":checked") || !$(".check3").is(":checked") || !$(".check3_1").is(":checked")) {
			$('.all_agree').removeClass("on");
			return;
		}
		if ($(".check1").is(":checked") &&  $(".check2").is(":checked") && $(".check3").is(":checked")&& $(".check3_1").is(":checked")) {
			$('.all_agree').addClass("on");
			return;
		}
	}
	
	function isSelect4CheckEnable() {
		if($(".chk3_1_1").is(":checked") &&  $(".chk3_1_2").is(":checked") && $(".chk3_1_3").is(":checked") && $(".chk3_1_4").is(":checked")) {
			$('.check3_1').prop("checked","checked");
			$('#usmktmd01').val("Y");
			return;
		}else{
			$('.check3_1').prop("checked","");
			$('#usmktmd01').val("N");
			return;
		}
	}
	
	//약관 내용 보고난 후 동의 버튼 눌렀을때 function
	function agreeClk(type) {
		if(type == '3_1'){
			agreeSelect4('checked');
		}
		$('#a_check' + type).addClass("bg_color2");	
		$('.check'+ type).prop("checked","checked");
		isNextBtnEnable();
		isAgreeBtnEnable();
		
		setAgreeCheckValue($('.check'+ type),type);
	}
	
	function agreeSelect4(check){
		$('.chk3_1_1').prop("checked",check);
		$('.chk3_1_2').prop("checked",check);
		$('.chk3_1_3').prop("checked",check);
		$('.chk3_1_4').prop("checked",check);
		if(check == 'checked'){
			$('#usmktmd01').val("Y");
			$('#usmktmd02').val("Y");
			$('#usmktmd03').val("Y");
			$('#usmktmd04').val("Y");
			$('#usmktmd05').val("Y");
		}else{
			$('#usmktmd01').val("N");
			$('#usmktmd02').val("N");
			$('#usmktmd03').val("N");
			$('#usmktmd04').val("N");
			$('#usmktmd05').val("N");
		}
		
	}
	
	function setAgreeCheckValue(target, value){
		//value가 3_1 은 선택적동의 전문이 04임
		if(value == '3_1'){
			value = '4';
		}
		if(target.is(":checked")){
			//체크가 되었다면
			$('#csinfyn0'+value).val('1');
		}else{
			//체크 해제되었다면
			$('#csinfyn0'+value).val('0');
		}
	}
	
	function setAgreeSelectBoxValue(target, value){
		if(target.is(":checked")){
			//체크가 되었다면
			$('#usmktmd0'+value).val('Y');
		}else{
			//체크 해제되었다면
			$('#usmktmd0'+value).val('N');
		}
	}
	
	</script>
	<style type="text/css">
	.btn_type1 {
	   height: 26px;
	   padding: 4px 12px 5px;
	}
	</style>
</head>
<body>
<div id="accessibility">
	<a href="#container">본문 바로가기</a>
</div>
<div id="wrap">
	<header id="header">
		<h1 class="h1_tit">메리츠캐피탈 전자약정서비스</h1>
	</header>
	
	<div id="container">
		<%@ include file="/WEB-INF/views/common/step_menu.jsp"%>
		<%@ include file="/WEB-INF/views/common/agree.jsp"%>
		<div class="contents">
			<div class="contbox first" style="display:none;">
				<h2 class="cont_tit">약관 동의</h2>
				<div class="agree_box">
					<div class="header">
						<div class="tit_wrap">
							<span class="tit">1. 개인(신용)정보의 필수적인 수집·이용에 관한 사항</span>
							<br/>
							<span style="margin: 10px 0px 0px 10px;font-weight: normal;">고유식별 정보의 필수적인 수집·이용에 관한 동의</span>
							<br/>
							<span class="link_wrap">
								<a id="a_check1" href="#" data-modal="modal:open" data-href="#pop_alert_a" class="btn_type1"><span>보기</span></a>
							</span>
						</div>							
						<span class="check01">
							<input type="checkbox" name="privateAgreeBox" id="prd_chk3_1" class="check1">
							<label for="prd_chk3_1"><span class="hidden">동의</span></label>
						</span>
					</div>
				</div>
				<div class="agree_box">
					<div class="header">
						<div class="tit_wrap">
							<span class="tit">2. 개인(신용)정보 조회에 관한 사항</span>
							<br/>
							<span style="margin: 10px 0px 0px 10px;font-weight: normal;">개인(신용)정보 조회에 관한 동의</span>
							<br/>
							<span class="link_wrap">
								<a id="a_check2" href="#" data-modal="modal:open" data-href="#pop_alert_b" class="btn_type1"><span>보기</span></a>
							</span>
						</div>							
						<span class="check01">
							<input type="checkbox" name="privateAgreeBox" id="prd_chk3_2" class="check2">
							<label for="prd_chk3_2"><span class="hidden">동의</span></label>
						</span>
					</div>
				</div>
				<div class="agree_box">
					<div class="header">
						<div class="tit_wrap">
							<span class="tit">3. 개인(신용)정보의 필수적인 제공에 관한 사항</span>
							<br/>
							<span style="margin: 10px 0px 0px 10px;font-weight: normal;">고유식별정보의 필수적 수집ㆍ이용에 대한 동의</span>
							<br/>
							<span class="link_wrap">
								<a id="a_check3" href="#" data-modal="modal:open" data-href="#pop_alert_c" class="btn_type1"><span>보기</span></a>
							</span>
						</div>							
						<span class="check01">
							<input type="checkbox" name="privateAgreeBox" id="prd_chk3_3" class="check3">
							<label for="prd_chk3_3"><span class="hidden">동의</span></label>
						</span>
					</div>
				</div>
				<div class="agree_box">
					<div class="header">
						<div class="tit_wrap">
							<span class="tit">4. 개인(신용)정보의 선택적인 수집•이용에 관한 사항</span>
							<br/>
							<span style="margin: 10px 0px 0px 10px;font-weight: normal;">금융상품 안내 및 이용권유를 위한 수집 이용 동의</span>
							<br/>
							<span class="link_wrap">
								<a id="a_check3_1" href="#" data-modal="modal:open" data-href="#pop_alert_d" class="btn_type1"><span>보기</span></a>
							</span>
							<br/>
							<div class="agree_box" style="border-bottom: 0px;margin-left: 10px;">
								<div class="header">
									<span class="check01">
										<input type="checkbox" name="selectAgreeBox" id="prd_chk3_1_1" class="chk3_1_1" value="">
										<label for="prd_chk3_1_1"><span class="vt_m" style="margin: 5px 0 0 3px;font-size:11px;">전화</span></label>							
									</span>
									<span class="check01" style="margin-left: 7px;">
										<input type="checkbox" name="selectAgreeBox" id="prd_chk3_1_2" class="chk3_1_2" value="">
										<label for="prd_chk3_1_2"><span class="vt_m" style="margin: 5px 0 0 3px;font-size:11px;">SMS</span></label>
									</span>
									<span class="check01" style="margin-left: 7px;">
										<input type="checkbox" name="selectAgreeBox" id="prd_chk3_1_3" class="chk3_1_3" value="">
										<label for="prd_chk3_1_3"><span class="vt_m" style="margin: 5px 0 0 3px;font-size:11px;">서면</span></label>
									</span>
									<span class="check01" style="margin-left: 7px;">
										<input type="checkbox" name="selectAgreeBox" id="prd_chk3_1_4" class="chk3_1_4" value="">
										<label for="prd_chk3_1_4"><span class="vt_m" style="margin: 5px 0 0 3px;font-size:11px;">이메일</span></label>
									</span>
								</div>
							</div>
						</div>							
						<span class="check01">
							<input type="checkbox" name="privateAgreeBox" id="prd_chk3_4_1" class="check3_1">
							<label for="prd_chk3_4_1"><span class="hidden">동의</span></label>
						</span>
					</div>
				</div>
				<div class="btn_wrap">
					<button type="button" class="btn_agree1 all_agree">
						<span class="btn_icon btn_icon1"></span>
						<span class="txt">일괄 동의합니다</span>
					</button>
				</div>
			</div>
			<div class="contbox seconds">
				<div class="agree_box">
					<div class="header">
						<div class="tit_wrap">
							<span class="tit">취약금융소비자확인서</span>
						</div>							
						<span class="check01">
							<input type="checkbox" name="vfnYn" id="prd_chk3_4" class="check4" value="Y">
							<label for="prd_chk3_4" style="right: 5px;"><span class="vt_m">동의</span></label>
							<input type="checkbox" name="vfnYn" id="prd_chk3_5" class="check5" checked="checked" value="N">
							<label for="prd_chk3_5"><span class="vt_m" style="right: 3px;">동의 안함</span></label>
						</span>
					</div>
				</div>
				<div class="agree_box" style="border-bottom: 0px;">
					<div class="header">
						<span class="check01">
							<input type="checkbox" name="vfnInfo" id="prd_chk3_6" class="check6" checked="checked" value="ED080001">
							<label for="prd_chk3_6"><span class="vt_m" style="right: 3px;">해당사항없음</span></label>							
						</span>
						<span class="check01">
							<input type="checkbox" name="vfnInfo" id="prd_chk3_7" class="check7" value="ED080002">
							<label for="prd_chk3_7"><span class="vt_m" style="right: 3px;">고령자(65세이상)</span></label>
						</span>
					</div>
				</div>
				<div class="agree_box" >
					<div class="header">
						<span class="check01">
							<input type="checkbox" name="vfnInfo" id="prd_chk3_8" class="check8" value="ED080003">
							<label for="prd_chk3_8"><span class="vt_m">은퇴자</span></label>
						</span>
						<span class="check01">
							<input type="checkbox" name="vfnInfo" id="prd_chk3_9" class="check9" value="ED080004">
							<label for="prd_chk3_9"><span class="vt_m">주부</span></label>
						</span>
					</div>
				</div>
			</div>
			<div class="btn_wrap" id="div_confirm_btn" style="display:none;">
				<button type="button" class="btn_type10 bg_color11 confirm_btn">
					<span class="txt">확인</span>
					<span class="btn_icon btn_icon6"></span>
				</button>
			</div>
			<div class="btn_wrap" id="div_sign_btn" style="display:none;">
				<button type="button" class="btn_type10 bg_color11 sig_btn on">
					<span class="txt">전자서명</span>
					<span class="btn_icon btn_icon6"></span>
				</button>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/common/footer.jsp"%>
</div>
<form id="eformData" name="eformData" action="/eform/eform.jsp"  method="post" target="eform">
	<input type="hidden" id="userdata" name="userdata" value=""/>
	<input type="hidden" id="progress_id" name="progress_id" value="${map.seq}"/>
	<input type="hidden" id="vfnYn" name="vfnYn" value=""/>
	<input type="hidden" id="vfnInfo" name="vfnInfo" value=""/>
	<input type="hidden" id="ct_code" name="ct_code" value=""/>
	<input type="hidden" id="eform_id" name="eform_id" value=""/>
	
	<input type="hidden" id="param3" name="param3" value=""/>
	<input type="hidden" id="csinfyn01" name="csinfyn01" value="0"/>
	<input type="hidden" id="csinfyn02" name="csinfyn02" value="0"/>
	<input type="hidden" id="csinfyn03" name="csinfyn03" value="0"/>
	<input type="hidden" id="csinfyn04" name="csinfyn04" value="0"/>
	<input type="hidden" id="usmktmd01" name="usmktmd01" value="N"/>
	<input type="hidden" id="usmktmd02" name="usmktmd02" value="N"/>
	<input type="hidden" id="usmktmd03" name="usmktmd03" value="N"/>
	<input type="hidden" id="usmktmd04" name="usmktmd04" value="N"/>
	<input type="hidden" id="usmktmd05" name="usmktmd05" value="N"/>
</form>
</body>
</html>