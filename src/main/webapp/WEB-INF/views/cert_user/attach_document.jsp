<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<!DOCTYPE html>
<html lang="ko">
<%@ include file="/WEB-INF/views/common/head.jsp"%>
<c:set var="pEntity" value="${map.seq }"/>
<c:set var="efEntityList" value="${map.attachList }"/>
<script type="text/javascript">
	$(document).ready(function(){
		stepMenuControl(4);
		
		//뒤로가기막기
		history.pushState(null, null, location.href);
		window.onpopstate = function(event) {	
			history.go(1);
		};
		 
		/*var os = "";
    	if(navigator.userAgent.toLocaleLowerCase().search("iphone") > -1){
    		os = "ios";
    	}else if (navigator.userAgent.toLocaleLowerCase().search("android") > -1){
    		os = "android";
    	}else{
    		os = "other";
    	}
    	
    	 //첨부서류 사진촬영 실행
    	$(".attach_yn").bind("click",function(){
    		if (os != "ios" && os != "android") {
    			showAlert('모바일 전용 서비스입니다. \n휴대폰의 모바일 브라우저에서만 동작합니다.', '알림', function(){  });
    			return;
    		}
  		 	executeApp();
    	});
    	
    	//앱 실행
    	function executeApp() {
    		var url;    		
    		if (os == "ios") {
    			url = "goodPaper://progress_id=${map.seq}";
    		} else {
    			url = "gp://runbyweb?progress_id=${map.seq}";
    		}
               newWindow = window.open(url);
    	}
    	
    	//스토어 이동
    	function goAppStoreOrPlayStore() {
    		var storeURL ="";
    		if (os == "ios") {
    			storeURL = "https://itunes.apple.com/kr";
    		} else {
    			storeURL = "https://play.google.com/store/apps/details?id=test.svctech.ib.net.paperless_cam";
    		}
    		window.open(storeURL);
    	} */
    	
		if (!('url' in window) && ('webkitURL' in window)) {
	           window.URL = window.webkitURL;
		}
    	
    <c:choose>
        <c:when test="${!empty efEntityList}">
			<c:forEach items="${efEntityList}" var="list">
			    $('#${list.id}').change(function(e){
			    	$('#img_${list.id}').attr('src', URL.createObjectURL(e.target.files[0]));
			    	changeFile('${list.id}');
			    });
			</c:forEach>			
	    </c:when>	
     </c:choose>
    	
    	//완료 버튼 실행
    	$('.nextStep').click(function(){
    		$.ajax({
				url: "/ajax/isProgressStatus?progress_id=${map.seq}",
				global: false,
				type: "POST",
				async: false,
				success: function (msg) {
					console.log(msg.message);
					if(msg.message != 5)
					{
		    			showAlert("첨부서류 사진촬영이 완료되지 않았습니다.", '알림', function(){  });
					}
					else
					{
						location.href = "/acceptCertification?progress_id=${map.seq}";
					}
					
				}
			})
    	});
	});
	
	function changeFile(fileId) {
//		var imgSize;
//        if (typeof ($(fileId)[0].files) != "undefined") {
//        	imgSize = parseFloat($("input[name=" +fileId+ "]")[0].files[0].size / 1024).toFixed(2);
//        }      
        
		uploadFile(fileId);	
        $("#imgDiv_" + fileId).show();	// 촬영사진 VIEW
		if ($("#attach_" + fileId).hasClass("bg_color3")) {		// 버튼 비활성배경 설정
			$("#attach_" + fileId).removeClass("bg_color3");
			$("#attach_" + fileId).addClass("bg_color10");
		}        

//		showAlert(imgSize + "KB 의 이미지를 업로드 합니다.", '알림', function(){	});
	}
	
	
	function openCamera(id) {
		var inputId = '#' + id;
		$(inputId).click();		
	}
	
	
	function uploadFile(fileId) {
		var formData = new FormData(); 
		formData.append("progress_id", "${pEntity}"); 
		formData.append("attach_id", fileId); 
		formData.append("attach", $("input[name=" +fileId+ "]")[0].files[0]);
		formData.append("attach_cnt", ${fn:length(efEntityList)} );
		
		var button_name = $("button[id=attach_"+fileId+"]")[0].innerText.trim();
		var button_name_length = button_name.length;
		
		var pic_name = button_name.substr(0, button_name_length - 5);
		
		$.ajax({ 
			url: '/attach/upload', 
			data: formData, 
			processData: false, 
			contentType: false, 
			type: 'POST', 
			enctype: 'multipart/form-data',
			cache: false,
			success: function(data){
				alert(pic_name +" 업로드 완료");
			},error:function(data){
				alert(pic_name + " 업로드 실패");
			},beforeSend : function(){
				showLoading(false);
			},complete:function(){
				closeLoading();
			}
		});
	}
</script>
<body>
<div id="accessibility">
	<a href="#container">본문 바로가기</a>
</div>
<div id="wrap">
	<header id="header">
		<h1 class="h1_tit">메리츠캐피탈 전자약정서비스</h1>
	</header>
	
	<div id="container">
		<%@ include file="/WEB-INF/views/common/step_menu.jsp"%>
		<div class="contents">
			<div class="contbox first">
				<p class="txt_type1 txt_c">
					<span class="bold">
					금융사고 예방 및 대출 진행을 위해<br>최소한의 첨부서류를 받고 있으며,<br><span class="txt_color01 ex_bold">사진 촬영을 이용하여 쉽게 제출하실 수 있습니다.</span></span>
				</p>
				<!-- <div class="btn_wrap">
					<button type="button" class="btn_type2 bg_color3 attach_yn">
						<span class="btn_icon btn_icon3"></span>
						<span class="txt">첨부서류 사진촬영</span>
					</button>
				</div> -->
				<c:choose>
			        <c:when test="${!empty efEntityList}">
						<c:forEach items="${efEntityList}" var="list">				
							<div class="btn_wrap">
								<input type="file" accept="image/*" style="display:none;" id="${list.id}" name="${list.id}" capture>
								<c:choose>
								<c:when test="${list.upload_yn eq '1'}">
									<button id="attach_${list.id}" type="button" class="btn_type2 bg_color10 attach_yn" onClick="javascript:openCamera('${list.id}');">
										<span class="btn_icon btn_icon3"></span>
										<span class="txt">${list.name} 사진촬영</span>
									</button>									
								</c:when>
								<c:otherwise>
									<button id="attach_${list.id}" type="button" class="btn_type2 bg_color3 attach_yn" onClick="javascript:openCamera('${list.id}');">
										<span class="btn_icon btn_icon3"></span>
										<span class="txt">${list.name} 사진촬영</span>
									</button>
								</c:otherwise>
								</c:choose>
							</div>
							<div class="btn_wrap" id="imgDiv_${list.id}" style="display:none;">
								<span class="txt" align="center"><img id="img_${list.id}" style="width:97%;" /></span>
								&nbsp;<br>
							</div>							
						</c:forEach>			
			         </c:when>		
			         <c:otherwise>
						<div class="btn_wrap">
							<span class="txt">필요 서류 내역이 존재하지 않습니다.</span>
						</div>
				     </c:otherwise>	
			     </c:choose>	
			</div>
			<%-- <div class="contbox">
				<h2 class="cont_tit">필요서류내역</h2>
				<div class="form_type2 b_line02">
					<div class="txt_row">
						<div class="header">
							<span class="label">서류명</span>
						</div>
						<div class="cont">
							<div class="box">
								<ul class="list_style01">
									<c:forEach var="item" items="${map.attachList}">
									     <li>${item.name}</li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div> --%>
			<div class="btn_wrap">
				<button type="button" class="btn_type10 bg_color11 nextStep">
					<span class="txt">완료</span>
					<span class="btn_icon btn_icon5"></span>
				</button>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/common/footer.jsp"%>
</div>
</body>
</html>