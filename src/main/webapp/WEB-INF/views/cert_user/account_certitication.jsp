<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="/WEB-INF/views/common/head.jsp"%>
<script type="text/javascript">
	$(document).ready(function(){
		stepMenuControl(2);
// 		if("${map.step}" == "2"){
// 			showAuthCodeBox();
// 		}

		//뒤로가기막기
		history.pushState(null, null, location.href);
		window.onpopstate = function(event) {	
			history.go(1);
		};
	
		$(".send_accout_info").click(function(){
			//on버튼 한번 눌렀으니 안눌리게 분기처리
			if($(this).attr('class') == ".send_accout_info on")
			{
				return false;	
			}
			
			//버튼 한번 눌렀으니 눌림버튼 on
			$('.send_accout_info').addClass("on");
			
			var account_bank_name = $(".bank_select_box option:selected").text();
			var account_holder_number = $('.account_holder_number').val();
			var account_holder_code = $('.bank_select_box').val();
			var ssn_number = $('.ssn_number').val();
			var gender = $('.gender').val();
			var username = $('.username').val();
			
			if(username == ""){
				showAlert("이름을 입력해 주세요.", '알림', function(){  });
				return;
			}
			/* if(gender == "0")
			{
				showAlert("성별을 선택해 주세요.", '알림', function(){  });
				return;
			} */
			if(account_holder_code == '00')
			{
				showAlert("입금하실 은행을 선택해 주세요.", '알림', function(){  });
				return;
			}
			
			if(account_holder_number.length == 0)
			{
				showAlert('계좌번호를 입력해 주세요.', '알림', function(){  });
				return;
			}
			if(ssn_number.length != 6)
			{
				showAlert('생년월일 6자리를 입력해 주세요.', '알림', function(){  });
				return;
			}
			
			var method="POST";
			var requestUrl="/ajax/accountCheck";
			var params = {
					"account_holder_number"	: account_holder_number,
					"account_holder_code"	: account_holder_code,
					"ssn_number"			: ssn_number,
					"gender"				: '',
					"account_bank_name"		: account_bank_name,
					"username"				: username,
					"progress_id" 			: "${map.seq}"
			};
			var getType="json";
			var contType="application/json; charset=UTF-8";
			
			$.ajax({
				url: requestUrl,
				type: method,
				data: JSON.stringify( params),
				dataType: getType,
				contentType : contType,
				cache: false,
				success: function(result) {	
					showAlert(result.rsp_msg, '알림', function(){ closeLoading(); });
					
					if(result.rsp_code == "1"){
						showAuthCodeBox();
					}else if(result.rsp_code == "2"){
						disAccountCheckBox();
					}else{
						$('.send_accout_info').removeClass("on");						
					}
				},
				fail: function() {
					showAlert('서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요', '알림', function(){ closeLoading(); });
				},beforeSend : function(){
					showLoading(false);
				}
			});
			
		})
		
		$('.account_auth_code_confirm').click(function(){
			var auth_code = $('.account_auth_code').val();
			if(auth_code.length != 4){
				showAlert('인증코드 4자리를 입력해 주세요.', '알림', function(){
					$('.account_auth_code').focus();
					return;});
			}else{
				var method="POST";
				var requestUrl="/ajax/account_authcode_check";
				var params = {
						"auth_code"				: auth_code,
						"progress_id" 			: "${map.seq}"
				};
				var getType="json";
				var contType="application/json; charset=UTF-8";
				
				$.ajax({
					url: requestUrl,
					type: method,
					data: JSON.stringify( params),
					dataType: getType,
					contentType : contType,
					cache: false,
					success: function(result) {	
						showAlert(result.rsp_msg, '알림', function(){ 
							if(result.rsp_code == "1"){
								location.href = "/acceptCertification?progress_id=${map.seq}";
							}else if(result.rsp_code == "2"){
								$('.account_auth_code').val('')
							}else if(result.rsp_code == "3"){
								location.reload();
							}
						});
						
					},
					fail: function() {
						showAlert('서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요', '알림', function(){ });
					}
				});
			}
		});
	});
	function showAuthCodeBox(){
		$('.confirm_box').show();
		disAccountCheckBox();
	}
	function disAccountCheckBox(){
		$('.account_holder_number').attr('disabled', 'disabled');
		$('.bank_select_box').attr('disabled', 'disabled');
		$('.ssn_number').attr('disabled', 'disabled');
		$('.gender').attr('disabled', 'disabled');
		$('.username').attr('disabled', 'disabled');
		$('.send_accout_info').attr('disabled', 'disabled');
		$('.send_accout_info').addClass("on");
	}
</script>
<body>
<div id="accessibility">
	<a href="#container">본문 바로가기</a>
</div>
<div id="wrap">
	<header id="header">
		<h1 class="h1_tit">메리츠캐피탈 전자약정서비스</h1>
	</header>
	
	<div id="container">
		<%@ include file="/WEB-INF/views/common/step_menu.jsp"%>
		<div class="contents">
			<div class="cont_txt_wrap">
				<h2 class="h2_tit">기 보유하고 계신 계좌를 통해 <span class="txt_color02">본인확인</span>을 진행합니다.</h2>
			</div>
			<div class="contbox first">
				<h2 class="cont_tit">기 보유계좌 본인확인절차</h2>
				<div class="list_wrap">
					<ul class="list_style02">
						<li><span class="num">1.</span>기 보유하신 계좌를 등록합니다.</li>
						<li><span class="num">2.</span>등록하신 계좌로 1원을 입금해 드립니다.</li>
						<li><span class="num">3.</span>입금자명에 기재된 인증번호를 입력합니다.</li>
					</ul>
				</div>
			</div>
			<div class="contbox">
				<div class="tit_wrap">
					<h2 class="cont_tit">보유 계좌번호 등록</h2>
					<p class="info txt_color02">본인 명의의 계좌만 가능</p>
				</div>
				<div class="form_type1">
					
					<div class="row">
						<div class="header">
							<span class="label">이름</span>
						</div>
						<div class="cont">
							<div class="box">
								<span class="input_wrap">
									<input type="text" id="pop_inp2_1" class="text username" placeholder="이름을 입력하세요.">
								</span>
							</div>
						</div
						>
					</div>
					
					<div class="row">
						<div class="header">
							<label class="label" for="pop_inp2_1">생년월일</label>
						</div>
						<div class="cont">
							<div class="box">
								<span class="input_wrap">
									<input type="text" id="pop_inp2_1" class="text ssn_number" placeholder="생년월일 6자리를 입력하세요.">
								</span>
							</div>
						</div>
					</div>	
					
					<!-- <div class="row"> 
						<div class="header">
							<label class="label" for="sel1_1">성별</label>
						</div>
						<div class="cont">
							<div class="box">
								<span class="select">
									<select id="sel1_1" class="color3 gender">
									<option value="0">성별을 선택하세요.</option>
									    <option value="1">남자</option>
									    <option value="2">여자</option>
									</select>
								</span>
							</div>
						</div>					
					</div>-->
					<div class="row">
						<div class="header">
							<label class="label" for="sel1_1">은행</label>
						</div>
						<div class="cont">
							<div class="box">
								<span class="select">
									<select id="sel1_1" class="color3 bank_select_box">
										<option value="00">입금할 은행을 선택하세요.</option>
									   	<c:forEach var="item" items="${map.bankList}">
										     <c:if test="${item.bank_status eq 'Y'}">
										    	 <option value="${item.bank_code_std}" <c:if test="${map.bank_code_std eq item.bank_code_std}">selected</c:if>>${item.bank_name}</option>
										     </c:if>
										</c:forEach> X
									</select>
								</span>
							</div>
						</div>					
					</div>
					<div class="row">
						<div class="header">
							<label class="label" for="pop_inp2_1">계좌번호</label>
						</div>
						<div class="cont">
							<div class="box">
								<span class="input_wrap">
									<input type="text" id="pop_inp2_1" class="text account_holder_number" placeholder="-없이 입력하세요">
								</span>
							</div>
						</div>
					</div>	
					<div class="btn_wrap">
						<button type="button" class="btn_type2 bg_color3 send_accout_info">
						<!-- <button type="button" class="btn_type2 bg_color3" data-modal="modal:open" data-href="#pop_alert_button"> -->
							<span class="btn_icon btn_icon2"></span>
							<span class="txt">본인확인요청</span>
						</button>
						<!-- modal pop -->
						<div id="pop_alert_button" class="modal" style="display:none;">
							<div class="inner">
								<div class="tit_wrap">
									<h1 class="h2_tit">계좌 실명인증 완료</h1>
								</div>
								<div class="txt_alert">
									<strong class="txt_type2 color1">등록하신 계좌의 실명이 정상적으로 확인되었습니다.</strong>
								</div>
								<div class="btn_wrap">
									<button type="button" class="btn_type4">
										<span class="btn_icon btn_sicon2"></span>
										<span class="txt">확인</span>
									</button>
								</div>
							</div>
						</div>					
						<!-- //modal pop -->
					</div>
				</div>
			</div>
			<div class="contbox confirm_box" style="display:none">
				<h2 class="cont_tit">인증번호 등록</h2>
				<div class="form_type1">
					<div class="row">
						<div class="header">
							<label class="label" for="pop_inp2_1">인증번호</label>
						</div>
						<div class="cont">
							<div class="box">
								<span class="input_wrap">
									<input type="text" id="pop_inp2_1" class="text account_auth_code" placeholder="4자리 입력하세요">
								</span>
							</div>
						</div>
					</div>	
				</div>
				<div class="btn_wrap">
				<button type="button" class="btn_type10 bg_color11 account_auth_code_confirm">
					<span class="txt">확인</span>
					<span class="btn_icon btn_icon7"></span>
				</button>
			</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/common/footer.jsp"%>
</div>
</body>
</html>