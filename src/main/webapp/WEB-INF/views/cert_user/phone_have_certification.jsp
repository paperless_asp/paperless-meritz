<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="/WEB-INF/views/common/head.jsp"%>
<body>
<style type="text/css">
.btn_icon_message {vertical-align:middle; background:url('/static/img/common/envelope.png') no-repeat;}
.btn_icon_message1 {width:25px; height:21px;}
</style>
<script type="text/javascript">
$(document).ready(function(){
	stepMenuControl(1);
	//인증요청 버튼 이벤트
	$("#acessButton").click(function(){
		if($(this).attr('class') == "btn_type2 bg_color3 on")
		{
			return false;	
		}
		$('#acessButton').addClass("on");
		var phone_num = $("#pop_inp2_1_phone_num").val();
		
		var method="POST";
		var requestUrl="/ajax/setHavePhoneAuth";
		var params = {
				"phone_num" : phone_num,
				"progress_id" : "${map.seq}"
		};
		var getType="json";
		var contType="application/json; charset=UTF-8";
		
		$.ajax({
			url: requestUrl,
			type: method,
			data: JSON.stringify( params),
			dataType: getType,
			contentType : contType,
			cache: false,
			success: function(result) {	
				if(result.code == 'F'){
					showAlert(result.text, '알림', function(){ });
					$('#acessButton').removeClass("on");
				}else{
					showAlert('해당 휴대폰 번호로 인증번호가 전송되었습니다.\n인증번호를 확인해 주세요.', '알림', function(){ });
// 					$("#pop_alert_modal").modal("show");
					$('#auth_num').show();
				}
			},
			fail: function() {
				showAlert('서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요', '알림', function(){ });

			}
		});
	});
	
	//인증번호 입력 후 확인버튼 이벤트
	$("#okButton").click(function(){		
		var auth_num = $("#pop_inp2_1_auth_num").val();
		var phone_num = $("#pop_inp2_1_phone_num").val();
		
		if(auth_num == ''){
			showAlert('인증번호를 입력하세요.', '알림', function(){ });
			return false;
		}
		
		if($(this).attr('class') == "btn_type10 bg_color11 on")
		{
			return false;	
		}
		
		var method="POST";
		var requestUrl="/ajax/isPhoneAuthNum";
		var params = {
				"auth_num" : auth_num,
				"phone_num" : phone_num,
				"progress_id" : "${map.seq}"
		};
		var getType="json";
		var contType="application/json; charset=UTF-8";
		
		$('#okButton').addClass("on");
		
		$.ajax({
			url: requestUrl,
			type: method,
			data: JSON.stringify( params),
			dataType: getType,
			contentType : contType,
			cache: false,
			success: function(result) {				
				// code 상태 : S = 인증 성공, N = 실패(3회성), F = 3회 오류(3회 초과) or 그 외에러
				if(result.code == "Y"){
					$("#okButton").hide();
// 					$("#nextButton").show();
					location.href = "/acceptCertification?progress_id=${map.seq}";
				}else if(result.code == "N"){
					showAlert(result.text, '알림', function(){ });
					$('#okButton').removeClass("on");
				}else if(result.code == "F"){
					showAlert(result.text, '알림', function(){ });
					$('#acessButton').removeClass("on");
					$('#pop_inp2_1_auth_num').val('');
					$('#auth_num').hide();
					$('#okButton').removeClass("on");
				}
			},
			fail: function() {
				showAlert('서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요', '알림', function(){ });

			}
		});
	});
	
	$(".nextStep").click(function(){
// 		location.href = "/acceptCertification?progress_id=${map.seq}";
	});
	
});
</script>
<div id="accessibility">
	<a href="#container">본문 바로가기</a>
</div>
<div id="wrap">
	<header id="header">
		<h1 class="h1_tit">메리츠캐피탈 전자약정서비스</h1>
	</header>
	
	<div id="container">
		<%@ include file="/WEB-INF/views/common/step_menu.jsp"%>
		<div class="contents">
			<div class="contbox first">
				<div class="form_type1">
					<div class="row">
						<div class="header">
							<label class="label" for="pop_inp2_1">휴대폰번호</label>
						</div>
						<div class="cont">
							<div class="box">
								<span class="input_wrap">
									<input type="text" id="pop_inp2_1_phone_num" class="text" placeholder="-없이 입력하세요">
								</span>
							</div>
						</div>
					</div>	
					<div class="btn_wrap style02">
						<button type="button" class="btn_type2 bg_color3" id="acessButton" data-modal="modal">
							<span class="btn_icon_message btn_icon_message1"></span>
							<span class="txt">인증요청</span>
						</button>

						<!-- modal pop 인증요청-->
						<div id="pop_alert_modal" class="modal" style="display:none;">
							<div class="inner">
								<div class="tit_wrap">
									<h1 class="h2_tit">인증요청</h1>
								</div>
								<div class="txt_alert">
									<strong class="txt_type2 color1">해당 휴대폰 번호로 인증번호가 전송되었습니다.<br>인증번호를 확인해 주세요.</strong>
								</div>
								<div class="btn_wrap">
									<button type="button" data-dismiss="modal" class="btn_type4" id="pop_alert_okButton">
										<span class="btn_icon btn_sicon2"></span>
										<span class="txt">확인</span>
									</button>
								</div>
							</div>
						</div>					
						<!-- //modal pop 인증요청 -->
					</div>
					<div class="row" style="display:none;" id="auth_num">
						<div class="header">
							<label class="label" for="pop_inp2_1">인증번호</label>
						</div>
						<div class="cont">
							<div class="box">
								<span class="input_wrap">
									<input type="text" id="pop_inp2_1_auth_num" class="text" placeholder="숫자4자리 입력하세요">
								</span>
							</div>
						</div>
					</div>	
				</div>
			</div>
			<div class="btn_wrap">
				<button id="okButton" type="button" class="btn_type10 bg_color11">
					<span class="txt">확인</span>
					<span class="btn_icon btn_icon7"></span>
				</button>
			</div>
			<div id="nextButton" class="btn_wrap" style="display:none;">
				<button type="button" class="btn_type10 bg_color11 nextStep">
					<span class="txt">다음</span>
					<span class="btn_icon btn_icon5"></span>
				</button>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/common/footer.jsp"%>
</div>
</body>
</html>