<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import ="java.util.*,java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="/WEB-INF/views/common/head.jsp"%>
<%@ include file="/static/deploy_domain.jsp"%>
<%
	Calendar today = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	String day = sdf.format(today.getTime());
	
	java.util.Random ran = new Random();
	//랜덤 문자 길이
	int numLength = 6;
	String randomStr = "";
	
	for (int i = 0; i < numLength; i++) {
	    //0 ~ 9 랜덤 숫자 생성
	    randomStr += ran.nextInt(10);
	}
	
	//reqNum은 최대 40byte 까지 사용 가능
	String reqNum = day + randomStr;
	String certDate=day;
	
	String id       = "SOQH001";                               // 본인실명확인 회원사 아이디
	//개발
	String srvNo    = "008001";   // 본인실명확인 서비스번호
	if(deploy_domain.indexOf("meritzcapital") > -1){
		//상용
		srvNo    = "009001";                            // 본인실명확인 서비스번호
	}
	
	String exVar    = "0000000000000000";                                       // 복호화용 임시필드
	//String retUrl   = request.getRequestURL().toString();                           // 본인실명확인 결과수신 URL
	//String retUrl   = "32http://www.goodpaper.co.kr/abc/search/step01";                           // 본인실명확인 결과수신 URL
	//String retUrl   = "32https://www.goodpaper.co.kr/sci/pcc_V3_popup_seed.jsp";                           // 본인실명확인 결과수신 URL
	String retUrl   = "32"+deploy_domain +"/sci/pcc_V3_popup_seed.jsp?progress_id="+request.getParameter("progress_id");

	String certGb	= "H";                           // 본인실명확인 본인확인 인증수단
	String addVar	= "hcheck=y";                           // 본인실명확인 추가 파라메터

	/**
	*
	* reqNum 값은 최종 결과값 복호화를 위한 SecuKey로 활용 되므로 중요합니다.
	* reqNum 은 본인 확인 요청시 항상 새로운 값으로 중복 되지 않게 생성 해야 합니다.
	* 쿠키 또는 Session및 기타 방법을 사용해서 reqNum 값을 
	* pcc_V3_result_seed.jsp에서 가져 올 수 있도록 해야 함.
	* 샘플을 위해서 쿠키를 사용한 것이므로 참고 하시길 바랍니다.
	*
	*/
	Cookie c = new Cookie("reqNum", reqNum);
	//c.setMaxAge(1800);  // <== 필요시 설정(초단위로 설정됩니다)
	response.addCookie(c);
	
	//01. 암호화 모듈 선언
	com.sci.v2.pcc.secu.SciSecuManager seed  = new com.sci.v2.pcc.secu.SciSecuManager();
	
	//02. 1차 암호화
	String encStr = "";
	String reqInfo      = id+"^"+srvNo+"^"+reqNum+"^"+certDate+"^"+certGb+"^"+addVar+"^"+exVar;  // 데이터 암호화
	encStr              = seed.getEncPublic(reqInfo);
	
	//03. 위변조 검증 값 생성
	com.sci.v2.pcc.secu.hmac.SciHmac hmac = new com.sci.v2.pcc.secu.hmac.SciHmac();
	String hmacMsg = hmac.HMacEncriptPublic(encStr);
	
	//03. 2차 암호화
	reqInfo  = seed.getEncPublic(encStr + "^" + hmacMsg + "^" + "0000000000000000");  //2차암호화
	
	
	// 변수 --------------------------------------------------------------------------------
	String retInfo		= "";																// 결과정보
	
	String name			= "";                                                               //성명
	String sex			= "";																//성별
	String birYMD		= "";																//생년월일
	String fgnGbn		= "";																//내외국인 구분값
	
	String di			= "";																//DI
	String ci1			= "";																//CI
	String ci2			= "";																//CI
	String civersion    = "";                                                               //CI Version
	
	String cellNo		= "";																// 핸드폰 번호
	String cellCorp		= "";																// 이동통신사
	
	String result		= "";  
	//복화화용 변수
	String encPara		= "";
	String encMsg		= "";
	String msgChk       = "N";  
	
	String resultMsg 	= "실패";
	
	//-----------------------------------------------------------------------------------------------------------------

    try{

        // Parameter 수신 --------------------------------------------------------------------
        retInfo = request.getParameter("retInfo");
        

        if (retInfo != null && retInfo.length() > 1) {
        	retInfo  = retInfo.trim();
	        //쿠키값 가져 오기
	        Cookie[] cookies = request.getCookies();
	        String cookiename = "";
	        String cookiereqNum = "";
	    	if(cookies!=null){
	    		for (int i = 0; i < cookies.length; i++){
	    			c = cookies[i];
	    			cookiename = c.getName();
	    			cookiereqNum = c.getValue();
	    			if(cookiename.compareTo("reqNum")==0) break;
	    			
	    			cookiereqNum = null;
	    		}
	    	}
	        
	        // 1. 암호화 모듈 (jar) Loading
	        com.sci.v2.pcc.secu.SciSecuManager sciSecuMg = new com.sci.v2.pcc.secu.SciSecuManager();
	        //쿠키에서 생성한 값을 Key로 생성 한다.
	        retInfo  = sciSecuMg.getDec(retInfo, cookiereqNum);
	
	        // 2.1차 파싱---------------------------------------------------------------
	        String[] aRetInfo1 = retInfo.split("\\^");
	
			encPara  = aRetInfo1[0];         //암호화된 통합 파라미터
	        encMsg   = aRetInfo1[1];    //암호화된 통합 파라미터의 Hash값
			
			String  encMsg2   = sciSecuMg.getMsg(encPara);
				// 3.위/변조 검증 ---------------------------------------------------------------
	        if(encMsg2.equals(encMsg)){
	            msgChk="Y";
	        }
	
			if(msgChk.equals("N")){
				//System.out.println("비정상 접근입니다.");
				throw new Exception("비정상 접근입니다.");
			} else {
		        // 복호화 및 위/변조 검증 ---------------------------------------------------------------
				retInfo  = sciSecuMg.getDec(encPara, cookiereqNum);
	
		        String[] aRetInfo = retInfo.split("\\^");
				
		        name		= aRetInfo[0];
				birYMD		= aRetInfo[1];
		        sex			= aRetInfo[2];        
		        fgnGbn		= aRetInfo[3];
		        di			= aRetInfo[4];
		        ci1			= aRetInfo[5];
		        ci2			= aRetInfo[6];
		        civersion	= aRetInfo[7];
		        reqNum		= aRetInfo[8];
		        result		= aRetInfo[9];
		        certGb		= aRetInfo[10];
				cellNo		= aRetInfo[11];
				cellCorp	= aRetInfo[12];
		        certDate	= aRetInfo[13];
				addVar		= aRetInfo[14];
				
				if(result.equals("Y")){
					resultMsg = "성공";
				}else if(result.equals("N")){
					resultMsg = "실패";
				}else{
					resultMsg = "3회 오류";
				}
				
			}
			%>
			<script type="text/javascript">
			
				var method="POST";
				var requestUrl="/ajax/setIdentityAuthInfo";
				var params = {
						"BIRFLDTXT" : "<%=birYMD%>",
						"SEXFLDMOW" : "<%=sex%>",
						"DOMFORKND" : "<%=fgnGbn%>",
						"PIDFLDKND" : "<%=cellCorp%>",
						"PIDFLDCLL" : "<%=cellNo%>",
						"AUTFLDDTM"	: "<%=certDate%>",
						"AUTFLDCDE" : "<%=result%>",
						"AUTFLDNAM" : "<%=resultMsg%>",
						"DUPFLDTXT" : "<%=di%>",
						"CONINFTXT" : "<%=ci1%>",
						"progress_id" : "${map.seq}"
				};
				
				var getType="json";
				var contType="application/json; charset=UTF-8";
				
				$.ajax({
					url: requestUrl,
					type: method,
					data: JSON.stringify( params),
					dataType: getType,
					contentType : contType,
					cache: false,
					success: function(result) {	
						if(result.result == true){
							alert('인증 되었습니다.');
							$(".name").val("<%=name%>");
							$(".phone").val("<%=cellNo%>");
// 							$('.next_btn_box').show();
							location.href = "/acceptCertification?progress_id=${map.seq}";
						}
						else{
							//resultCode:resultMessage - 00:성공 , 01:등록실패, 03:계약정보없음, 04:고객정보없음, 05:휴대전화번호상이, 06:유효기간만료
							var resultCode = result.resultType;
							
							if(resultCode == '03'){
								alert('계약정보가 없습니다.');
							}else if(resultCode == '04'){
								alert('고객정보가 없습니다.');
							}else if(resultCode == '05'){
								alert('등록된 고객님의 휴대전화번호가 상이합니다.');
							}else if(resultCode == '06'){
								alert('유효기간이 만료되었습니다. 상담사를 통해 확인해 주시기 바랍니다.');
							}else if(resultCode == '90'){
								alert('현재 진행상태가 아닙니다. 상담사를 통해 확인해 주시기 바랍니다.');
							}else{
								alert('본인인증에 실패 했습니다.');
							}
							<%
							name = "";
							cellNo = "";
							%>
						}
					},
					fail: function() {
						showAlert('서버와의 연결에 실패하였습니다\n잠시후 다시 시도해주세요', '알림', function(){ });
						<%
						name = "";
						cellNo = "";
						%>
					}
				});
			</script>
			<%				
			
        }
    }catch(Exception ex){
        //System.out.println("[pcc] Receive Error -"+ex.getMessage());
  }		
%>	
<script language=javascript>  
<!--
var PCC_window; 

function openPCCWindow(){ 
    var PCC_window = window.open('', 'PCCV3Window', 'width=430, height=560, resizable=1, scrollbars=no, status=0, titlebar=0, toolbar=0, left=300, top=200' );

    if(PCC_window == null){ 
		 alert(" ※ 윈도우 XP SP2 또는 인터넷 익스플로러 7 사용자일 경우에는 \n    화면 상단에 있는 팝업 차단 알림줄을 클릭하여 팝업을 허용해 주시기 바랍니다. \n\n※ MSN,야후,구글 팝업 차단 툴바가 설치된 경우 팝업허용을 해주시기 바랍니다.");
    }

    document.reqPCCForm.action = 'https://pcc.siren24.com/pcc_V3/jsp/pcc_V3_j10.jsp';
    document.reqPCCForm.target = 'PCCV3Window';
    document.reqPCCForm.submit();

	return true;
}	

//-->

</script>	
<script type="text/javascript">
$(document).ready(function(){
	stepMenuControl(1);
	$(".name, .phone").click(function(){
		if($(".name").val() == ''){
			openPCCWindow();
		}
	});
	$(".next_btn").click(function(){
// 		location.href = "/acceptCertification?progress_id=${map.seq}";
	});
});
</script>
<body>
<div id="accessibility">
	<a href="#container">본문 바로가기</a>
</div>
<div id="wrap">
	<header id="header">
		<h1 class="h1_tit">메리츠캐피탈 전자약정서비스</h1>
	</header>
	
	<div id="container">
		<%@ include file="/WEB-INF/views/common/step_menu.jsp"%>
		<form id="reqPCCForm" name="reqPCCForm" method="post" action="">
		<input type="hidden" name="reqInfo"     value = "<%=reqInfo%>">
		<input type="hidden" name="retUrl"      value = "<%=retUrl%>">	
		<input type="hidden" name="userKey"     value = "${map.userKey}">	
		<div class="contents">
			<div class="contbox">
				<h2 class="cont_tit">휴대폰 본인확인</h2>
				<div class="form_type1">
					<div class="row">
						<div class="header">
							<label class="label" for="inp1_1">이름</label>
						</div>
						<div class="cont">
							<div class="box">
								<span class="input_wrap">
									<input type="text" id="inp1_1" class="text name" value="<%=name%>" readonly>
								</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="header">
							<label class="label" for="inp2_1">휴대폰번호</label>
						</div>
						<div class="cont">
							<div class="box">
								<span class="input_wrap">
									<input type="text" id="inp2_1" class="text phone" value="<%=cellNo%>" readonly>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="btn_wrap next_btn_box" style="display:none;">
				<button type="button" class="btn_type10 bg_color11 next_btn">
					<span class="txt">다음</span>
					<span class="btn_icon btn_icon5"></span>
				</button>
			</div>
		</div>
		</form>
	</div>
	<%@ include file="/WEB-INF/views/common/footer.jsp"%>
</div>
</body>
</html>