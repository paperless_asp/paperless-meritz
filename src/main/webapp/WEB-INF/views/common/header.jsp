<div class="progress_bar_wrap">
	<ul class="progress_bar">
		<li class="complete" >
			<span class="step step1"></span>
			<span class="tit">개인인증</span>
		</li>
		<li class="current" >
			<span class="step step2"></span>
			<span class="tit">계좌인증</span>
		</li>
		<li>
			<span class="step step3"></span>
			<span class="tit">전자서명</span>
		</li>
		<li>
			<span class="step step4"></span>
			<span class="tit">서류첨부</span>
		</li>
	</ul>
</div>