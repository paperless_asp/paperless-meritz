<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<div class="progress_bar_wrap">
	<ul class="progress_bar">
		<li id="step1" >
			<span class="step step1"></span>
			<span class="tit">개인인증</span>
		</li>
		<li id="step2" >
			<span class="step step2"></span>
			<span class="tit">계좌인증</span>
		</li>
		<li id="step3" >
			<span class="step step3"></span>
			<span class="tit">전자서명</span>
		</li>
		<li id="step4" >
			<span class="step step4"></span>
			<span class="tit">서류첨부</span>
		</li>
	</ul>
</div>