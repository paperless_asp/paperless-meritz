<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<footer id="footer">
	<h2 class="hidden">하단 영역</h2>
	<div class="cs_center_wrap">
		<div class="cs_center">
			<h3 class="tit">고객센터</h3>
			<div class="txt_wrap">
				<span class="txt_tel">1588-9666</span>
				<span class="txt_weekday"><em>평일</em>09:00 ~ 18:00</span>
			</div>
			<div class="btn_link">
				<a href="tel:15889666" class="btn_tel"><span class="txt_hidden">고객센터 전화연결</span></a>
			</div>
		</div>
	</div>
	<address class="address">
	<img src="/static/img/common/footer_logo.png" style="margin-left: 15px;" align="left">
		<p class="txt" style="margin-left: 10px; float:left;" align="left">
						서울특별시 영등포구 국제금융로10 ThreeIFC 23층<br>
						대표이사 권태길<span class="bar"></span>사업자등록번호 : 107-87-67865<br>
						COPYRIGHTⓒMERITZCAPITAL CO., LTD. ALL RIGHTS RESERVED.</p>
	</address>
</footer>


<!-- 로딩 팝업 -->
<div id="pop_loading" class="modal pop_loading" style="display:none;">
	<div class="inner">
		<div class="gif_wrap">
			<img src="/static/img/common/img_loading.gif" class="lotate_img" alt="진행 중">
		</div>
		<p class="txt">처리 중입니다.</p>
		<p class="desc">* 절대 화면을 닫거나 이동하지 마세요.<br>(자동으로 닫힙니다 – 최장 30초)</p>
	</div>
</div>

<!-- modal pop -->
<div id="alertBox" class="alert-messagebox " style="display:none;">
	<div class="inner">
		<div class="tit_wrap">
			<h1 class="h2_tit alert-header">a태그 - 안내 팝업</h1>
		</div>
		<div class="txt_alert">
			<strong class="txt_type2 color1 alert-message">등록하신 계좌의 실명이 정상적으로 확인되었습니다.</strong>
		</div>
		<div class="btn_wrap">
			<button type="button" class="btn_type4 modal-close-button">
				<span class="btn_icon btn_sicon2"></span>
				<span class="txt">확인</span>
			</button>
		</div>
	</div>
</div>

<script>
var isLoading = false;
function showLoading(autoHide = true, time = 2000)
{
	if(isLoading == false)
	{
		var angle = 0;
		setInterval(function(){
		      angle+=35;
		     $(".lotate_img").rotate(angle);
		},100);
		isLoading = true;
	}
	$('#pop_loading').modal({
		clickClose: false
	});
	
	if(autoHide == true)
    	setTimeout(closeLoading, 2000); 
}

function closeLoading()
{
	$(".close-modal").click();
}

function stepMenuControl(type){
	if(type == 1){
		$("#step1").attr('class','current');
	}else if(type ==2){
		$("#step1").attr('class','complete');
		$("#step2").attr('class','current');
	}else if(type ==3){
		$("#step1").attr('class','complete');
		$("#step2").attr('class','complete');
		$("#step3").attr('class','current');
	}else if(type ==4){
		$("#step1").attr('class','complete');
		$("#step2").attr('class','complete');
		$("#step3").attr('class','complete');
		$("#step4").attr('class','current');
	}
}
</script>