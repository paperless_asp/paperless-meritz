<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<style>
/* progress_bar */
.progress_bar_wrap_nh {margin-bottom:1px; border:2px solid #ddd; background-color:#fafafa;}
.progress_bar_nh {display:flex; justify-content:center; align-items:center; text-align:center;}
.progress_bar_nh li:first-child {border-left:0;}
.progress_bar_nh li:first-child:before {display:none;}
.progress_bar_nh li {position:relative; width:100%; padding:11px 0; border-left:2px solid #dcdcdc; background-color:#fafafa;}
.progress_bar_nh li:first-child:after {display:none;} 

.progress_bar_nh .tit {display:block; margin-top:6px; font-weight:bold; font-size:11px; color:#9b9b9b;}
.progress_bar_nh .step {background:url('../../img/content/bg_progressbar_G.png') no-repeat 0 0; background-size:200px 78px;}
.progress_bar_nh .step.step1 {width:21px; height:21px; background-position:0 0;}
.progress_bar_nh .step.step2 {width:21px; height:21px; background-position:-53px 0;}
.progress_bar_nh .step.step3 {width:21px; height:21px; background-position:-104px 0;}
.progress_bar_nh .step.step4 {width:21px; height:21px; background-position:-155px 0;}

.progress_bar_nh li.current .tit {color:#19a591;}
.progress_bar_nh li.current .step.step1 {width:45px; height:21px; background-position:0 -56px;}
.progress_bar_nh li.current .step.step2 {width:45px; height:21px; background-position:-53px -56px;}
.progress_bar_nh li.current .step.step3 {width:45px; height:21px; background-position:-104px -56px;}
.progress_bar_nh li.current .step.step4 {width:45px; height:21px; background-position:-155px -56px;}

.progress_bar_nh li.complete {background:url('../../img/content/bg_pattern01.png') repeat; background-size:87px 65px;}
.progress_bar_nh li.complete .tit {color:#19a591;}
.progress_bar_nh li.complete .step.step1 {width:21px; height:21px; background-position:0 -29px;}
.progress_bar_nh li.complete .step.step2 {width:21px; height:21px; background-position:-53px -28px;}
.progress_bar_nh li.complete .step.step3 {width:21px; height:21px; background-position:-104px -28px;}
.progress_bar_nh li.complete .step.step4 {width:21px; height:21px; background-position:-155px -28px;}
</style>
<div id="bank_list" class="modal" style="display:none;">
	<div class="inner">
		<div class="tit_wrap">
			<h1 class="h2_tit">계좌를 선택하세요.</h1>
		</div>
	
		<br/>
		<div class="guide_wrap">
			<div class="tit_wrap">
				<h1 class="h2_tit">< 은행 ></h1>
			</div>
			<br/>
			
			
<c:forEach var="item" items="${map.majorBankList}" varStatus="status">			
	<c:if test="${status.index%4==0}">	
			<div class="progress_bar_wrap_nh">
				<ul class="progress_bar_nh">
	</c:if>	
					<li id="BANKCODE${item.bank_code}" onClick="javascript:bankselect('BANKCODE${item.bank_code}');">
						<span class="tit" id="BANKNAME${item.bank_code}">${item.bank_name}</span>
					</li>
	<c:if test="${status.index%4==3}">				
					
				</ul>
			</div>
	</c:if>
</c:forEach>
		</div>
		<br/>
		<div class="guide_wrap">
			<div class="tit_wrap">
				<h1 class="h2_tit">< 증권사 ></h1>
			</div>
			<br/>
			
<c:forEach var="item" items="${map.otherBankList}" varStatus="status">			
	<c:if test="${status.index%4==0}">	
			<div class="progress_bar_wrap_nh">
				<ul class="progress_bar_nh">
	</c:if>	
					<li id="BANKCODE${item.bank_code}" onClick="javascript:bankselect('BANKCODE${item.bank_code}');">
						<span class="tit" id="BANKNAME${item.bank_code}">${item.bank_name}</span>
					</li>
					
	<c:if test="${status.index%4==3}">				
					
				</ul>
			</div>
	</c:if>
</c:forEach>				
			
		</div>
	</div>
</div>
<SCRIPT language=JavaScript>
	function bankselect(id) {
		selectedBankCode = id.substr(8);
		selectedBankName =$("#BANKNAME" + selectedBankCode).text();
		$("#bankCode").val(selectedBankCode); 
		$(".seletedbankName").val(selectedBankName);
// 		showAlert(selectedBankName , '선택 계좌', function(){});
// 		enableAccountCheck();
		$.modal.close();
	}
</SCRIPT>