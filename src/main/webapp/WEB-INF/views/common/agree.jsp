<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<style>
/**
 * basic mobile layout style
 */
body {
  line-height: 1.4;
}

/* default point color - red */
/* default font color */
.mobile-header {
  padding-top: 13px;
  height: 37px;
}
.mobile-header-logo {
  margin: 0 auto;
  width: 90px;
}
.mobile-header-logo img {
  width: 100%;
}

.mobile-content {
  padding-bottom: 20px;
  background: #f9f9f9;
}
.mobile-content-heading-wrap {
  padding-top: 18px;
  height: 32px;
  background: #e01b22 url(../images/bg_mobile_heading.jpg) center;
}
.mobile-content-heading {
  color: #fff;
  text-align: center;
  font-weight: bold;
  font-size: 15px;
}

.mobile-step {
  padding-top: 18px;
  height: 32px;
  text-align: center;
  background: white url(../images/bg_step.png) no-repeat center center;
  background-size: auto 17px;
  border-bottom: 1px solid #E5E5E5;
  font-size: 15px;
}
.mobile-step-item {
  display: inline-block;
  color: #777777;
  font-weight: bold;
}
.mobile-step-item.first {
  margin-right: 72px;
}
.mobile-step-item.active {
  color: #e01b22;
}

.mobile-section-heading {
  margin: 0 20px;
  padding-top: 20px;
  color: #333333;
  font-size: 15px;
  font-weight: bold;
  line-height: 1.5;
}
.mobile-section-wrap {
  margin: 20px;
  margin-bottom: 0;
}

.mobile-agreement-all {
  margin-top: 25px;
  padding: 10px 20px;
  background: #ececec;
  border-top: 1px solid #e3e3e3;
  border-bottom: 1px solid #e3e3e3;
}
.mobile-agreement-all-check .mobile-form-label {
  color: #e01b22;
  font-weight: bold;
}
.mobile-agreement-all-check:after {
  content: '';
  display: block;
  clear: both;
}

.mobile-agreement-link {
  position: relative;
  display: block;
  padding: 8px 40px 8px 15px;
  line-height: 1.8;
  font-weight: bold;
  border: 1px solid #e2e2e2;
  border-radius: 4px;
  background: #fff;
}
.mobile-agreement-link:after {
  content: "";
  position: absolute;
  top: 50%;
  right: 9px;
  display: block;
  margin-top: -5px;
  width: 0;
  height: 0;
  border-top: 5px solid transparent;
  border-bottom: 5px solid transparent;
  border-left: 5px solid #555;
}

.mobile-auth {
  margin-top: 20px;
}
.mobile-auth:after {
  content: '';
  display: block;
  clear: both;
}
.mobile-auth-label {
  display: block;
  margin-bottom: 6px;
  color: #333333;
  font-weight: bold;
}
.mobile-auth .mobile-btn-em {
  margin-top: 10px;
}

.mobile-list-title {
  margin-bottom: 10px;
  font-weight: bold;
}

.mobile-list-all {
  color: #e01b22;
}

.mobile-list {
  margin-top: 5px;
  padding: 10px;
  border: 1px solid #e2e2e2;
  background: #ececec;
  border-radius: 5px;
}
.mobile-list:after {
  content: '';
  display: block;
  clear: both;
}
.mobile-list-name {
  margin-bottom: 10px;
  font-weight: bold;
  color: #333333;
}
.mobile-list-item {
  float: left;
}
.mobile-list-item .mobile-form-label {
  margin-right: 10px;
  padding-left: 25px;
}
.mobile-list-item:last-child .mobile-form-label {
  margin-right: 0;
}

.mobile-footer {
  padding: 10px 0;
  border-top: 1px solid #e5e5e5;
  line-height: 1.5;
  font-size: 12px;
  text-align: center;
  color: #777777;
}

.mobile-agreement-list {
  margin-top: 15px;
}
.mobile-agreement-list .mobile-agreement-link {
  margin-bottom: 3px;
}
.mobile-agreement-list .mobile-agreement-link .mobile-form-label {
  margin-top: -1px;
  margin-right: 0;
  padding-left: 25px;
  width: 0;
  text-indent: -9999px;
}
.mobile-agreement-list .mobile-agreement-link a {
  vertical-align: middle;
}

.mobile-complete {
  padding-top: 208px;
  text-align: center;
  background: url(../images/bg_mobile_complete.png) no-repeat center 108px;
}
.mobile-complete-heading {
  font-size: 15px;
  font-weight: bold;
  color: #e01b22;
}
.mobile-complete-msg {
  margin-top: 10px;
  margin-bottom: 80px;
  color: #333333;
  line-height: 1.5;
}

.mobile-tab {
  padding: 8px 6%;
  width: 88%;
  height: 34px;
  border-bottom: 1px solid #E5E5E5;
  font-size: 15px;
  background: #fff;
}
.mobile-tab-item {
  float: left;
  width: 33.3%;
  box-sizing: border-box;
}
.mobile-tab-item:first-child .mobile-tab-link {
  border-left: 1px solid #e01b22;
  border-radius: 4px 0 0 4px;
}
.mobile-tab-item:last-child .mobile-tab-link {
  border-radius: 0 4px 4px 0;
}
.mobile-tab-link {
  display: block;
  border: 1px solid #e01b22;
  border-left: 0;
  height: 32px;
  line-height: 32px;
  color: #e01b22;
  text-align: center;
  font-weight: bold;
}
.mobile-tab-link.active {
  color: #fff;
  background: #e01b22;
}
.mobile-tab-link span {
  position: relative;
  top: -2px;
}

.mobile-terms {
  padding: 20px;
  color: #333333;
  line-height: 1.5;
}
.mobile-terms-wrap {
  display: none;
}
.mobile-terms-wrap.active {
  display: block;
}
.mobile-terms-heading {
  font-size: 15px;
  font-weight: bold;
}
.mobile-terms-title {
  margin-top: 25px;
  margin-bottom: 5px;
  font-weight: bold;
}
.mobile-terms-heading + .mobile-terms-title {
  margin-top: 10px;
}
.mobile-terms-list {
  margin-left: 5px;
}
.mobile-terms-listitem {
  padding-left: 15px;
  text-indent: -15px;
  line-height: 1.5;
}
.mobile-terms-sublist {
  margin: 25px 0;
  margin-left: -15px;
}
.mobile-terms-subtitle {
  margin-top: 25px;
  padding-left: 20px;
  text-indent: -20px;
  font-size: 15px;
  font-weight: bold;
}
.mobile-terms-text {
  line-height: 1.4;
  margin: 10px 0;
}

.mobile-terms-sub {
  padding: 0 20px 20px 20px;
}
.mobile-terms-sub .mobile-terms-heading {
  margin-top: 25px;
  padding-left: 25px;
  text-indent: -25px;
}
.mobile-terms-sub .mobile-terms-heading:first-child {
  margin-top: 0px;
}
.mobile-terms-sub .mobile-terms-title {
  margin-top: 5px;
  margin-left: 5px;
  padding-left: 15px;
  text-indent: -15px;
}
.mobile-terms-sub .mobile-terms-text {
  margin-left: 25px;
}
.mobile-terms-sub .mobile-terms-list {
  margin-left: 15px;
}
.mobile-terms-sub .mobile-terms-listitem {
  padding-left: 10px;
  text-indent: -10px;
}

.mobile-terms-table {
  margin: 10px 0 20px 20px;
  border: 1px solid #e6e6e6;
  border-top: 0;
}
.mobile-terms-table-vt {
  width: 100%;
}
.mobile-terms-table-head {
  text-indent: 0;
  padding: 10px;
  background: #f0f0f0;
  font-weight: bold;
  border-top: 1px solid #e6e6e6;
  border-bottom: 1px solid #e6e6e6;
}
.mobile-terms-table-td {
  line-height: 1.4;
  padding: 8px 10px;
}
.mobile-terms-table-hr .mobile-terms-table-head {
  text-indent: 0;
  text-align: center;
  border-left: 1px solid #e6e6e6;
}
.mobile-terms-table-hr .mobile-terms-table-head:first-child {
  border-left: 0;
}
.mobile-terms-table-hr .mobile-terms-table-td {
  text-indent: 0;
  border-left: 1px solid #e6e6e6;
}
.mobile-terms-table-hr .mobile-terms-table-td:first-child {
  border-left: 0;
}

.mobile-terms-listitem .mobile-terms-title {
  padding-left: 0;
  text-indent: 0;
}
.mobile-terms-listitem .mobile-terms-table {
  margin-left: 0;
  margin-bottom: 20px;
}
.mobile-terms-listitem .mobile-terms-table-hr {
  width: 100%;
}

.mobile-terms-agree {
  font-weight: bold;
  margin-top: 20px;
  line-height: 1.4;
}

.mobile-terms-close {
  margin-bottom: -20px;
  padding: 30px 0 10px 0;
  text-align: center;
}
.mobile-terms-close .mobile-btn {
  display: inline-block;
  width: 82px;
}
/* default point color - red */
/* default font color */
/* default component */
.mobile-input-text, .mobile-input-select {
  -webkit-appearance: none;
  -moz-appearance: none;
  padding-left: 5px;
  border: 1px solid #e2e2e2;
  border-radius: 4px;
  background: #fff;
  width: 100%;
  height: 40px;
  box-sizing: border-box;
}
.mobile-input-text.input-phone-num, .input-phone-num.mobile-input-select {
  float: left;
  margin-left: 3%;
  width: 22%;
}

.mobile-input-select {
  position: relative;
  background: white url(../images/bg_selectbox.png) no-repeat right center;
}

.mobile-btn, .mobile-btn-em {
  display: block;
  padding-top: 9px;
  width: 100%;
  height: 40px;
  box-sizing: border-box;
  border: 1px solid #696969;
  border-radius: 4px;
  background: #777;
  color: #fff;
  font-weight: bold;
  font-size: 14px;
  text-align: center;
}

.mobile-btn-em {
  border: 1px solid #CD131B;
  background: #E01B22;
}

.mobile-confirm {
  margin-top: 20px;
  text-align: center;
}
.mobile-confirm:after {
  content: '';
  display: block;
  clear: both;
}
.mobile-confirm .mobile-btn, .mobile-confirm .mobile-btn-em,
.mobile-confirm .mobile-btn-em {
  float: left;
  margin-left: 2%;
  width: 49%;
}
.mobile-confirm .mobile-btn:first-child, .mobile-confirm .mobile-btn-em:first-child,
.mobile-confirm .mobile-btn-em:first-child {
  margin-left: 0;
}

.mobile-info-text {
  margin-top: 10px;
  line-height: 1.5;
  color: #777777;
}
.mobile-info-text.mobile-info-rights {
  margin: 25px 0 20px;
}

.mobile-form-title {
  float: left;
}
.mobile-form-fieldset {
  margin-top: 20px;
}
.mobile-form-block {
  clear: both;
  padding-top: 10px;
}
.mobile-form-block:after {
  content: '';
  display: block;
  clear: both;
}
.mobile-form-radio, .mobile-form-check {
  position: absolute;
  opacity: 0;
}
.mobile-form-label {
  position: relative;
  display: inline-block;
  margin: 0;
  margin-right: 20px;
  padding-left: 30px;
  min-height: 20px;
  line-height: 20px;
  vertical-align: middle;
}
.mobile-form-label.focus-in:before {
  border: 1px solid #e01b22;
}
.mobile-form-radio + label:before, .mobile-form-check + label:before {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 18px;
  height: 18px;
  border-radius: 100%;
  border: 1px solid #b9b9b9;
}
.mobile-form-radio + label:before {
  border-radius: 100%;
  border: 1px solid #b9b9b9;
  background: #fff;
}
.mobile-form-radio:checked + label:after {
  content: '';
  position: absolute;
  top: 4px;
  left: 4px;
  width: 12px;
  height: 12px;
  border-radius: 100%;
  background: #e01b22;
}
.mobile-form-check + label:before {
  border-radius: 4px;
  border: 1px solid #e2e2e2;
  background: #fff;
}
.mobile-form-check:checked + label:before {
  background: #e01b22 url(../images/bg_checked.png) no-repeat center center;
  border-radius: 4px;
  border: 1px solid #cd131b;
}
.mobile-form-float {
  float: left;
  margin-right: 10px;
  height: 24px;
  line-height: 24px;
}
.mobile-form-float .mobile-form-label {
  float: left;
  margin-right: 0;
  height: 24px;
  line-height: 24px;
}
.mobile-form-float .mobile-form-label:before {
  top: 2px;
}
.mobile-form-sex {
  float: left;
  padding-top: 10px;
}
.mobile-form-nation.mobile-input-select {
  float: right;
  width: 50%;
}

h2 + .mobile-form-fieldset {
  margin-top: 10px;
}

.mobile-auth-select-yy {
  float: left;
  width: 32%;
  box-sizing: border-box;
}
.mobile-auth-select-mm, .mobile-auth-select-dd {
  float: left;
  padding-left: 3px;
  width: 34%;
  box-sizing: border-box;
}
.mobile-auth-select-career {
  float: left;
  width: 25%;
  box-sizing: border-box;
}
.mobile-auth-phone-num {
  float: left;
  padding-left: 3px;
  width: 25%;
  box-sizing: border-box;
}

.mobile-auth-input {
  margin-top: 5px;
}
.mobile-auth-input:after {
  content: '';
  display: block;
  clear: both;
}
.mobile-auth-input .mobile-auth-limit {
  float: left;
  margin: 13px 0 0 20px;
  color: #e01b22;
  font-weight: bold;
}
.mobile-auth-input .mobile-auth-resend {
  float: right;
  width: 40%;
}
.mobile-credt-strong-under {
  font-size: 16px;
  line-height: 20px;
  text-decoration: underline;
  display: inline;
}
</style>
<div id="pop_alert_a" class="modal" style="display:none;">
	<div class="inner">
		<div class="tit_wrap">
			<h1 class="h2_tit">개인(신용)정보 필수적 동의서(조회)</h1>
		</div>
		<div></div>
			
<div class="mobile-content-wrap">
		<section class="mobile-terms">
			<h1 class="mobile-terms-heading">개인(신용)정보의 필수적인 수집•이용에 관한 사항</h1>
			<p class="mobile-terms-text">메리츠캐피탈과의 (금융)거래 및 상품서비스 안내 등을 위하여 메리츠캐피탈이 본인의 개인(신용)정보를 수집•이용하나거 조회 또는 제3자에게 제공하고자 하는 경우에는「개인정보 보호법」제15조, 제22조, 제24조,「신용정보의 이용 및 보호에 관한 법률」제32조, 제33조 및 제34조,「정보통신망 이용촉진 및 정보보호 등에 관한 법률」제22조, 24조의2항에 따라 본인의 동의를 얻어야 합니다. 이에 본인은 메리츠캐피탈이 아래의 내용과 같이 본인의 개인(신용)정보를 수집•이용 하는 것에 동의합니다.</p>
				
			<h2 class="mobile-terms-title">개인(신용)정보의 수집•이용 목적</h2> 
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem">① (금융)거래관계의 설정 및 유지(리스크관리모형개발 포함) 신청한 상품, 서비스 제공, 법령상 의무이행, 신용질서 문란행위 조사, 분쟁처리, 민원처리, 전화상담업무, 본인여부확인, 공증, 채권추심 등 </li>
				<li class="mobile-terms-listitem">② 상담 또는 새로운 상거래 신청 시 본인 여부 확인</li>
			</ul>
			
			<h2 class="mobile-terms-title">수집•이용할 개인(신용)정보의 내용</h2>
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem">① 개인식별정보(성명,<span class="mobile-credt-strong-under">주민(법인)등록번호</span>, 사업자번호, 주민등록증 발급일, 연락처(휴대폰, 자택, 직장), 주소(자택, 직장), 이메일, 직장명, 부서, 직위, 성별, 국적,<span class="mobile-credt-strong-under">운전면허번호, 여권번호, 외국인등록 번호</span>, 음성data</li>
				<li class="mobile-terms-listitem">② 신용거래정보(귀사 및 타 금융사의 본 거래 이전 및 이후의 대출, 보증, 담보제공, 신용카드, 할부금융 등 상거래 관련 거래의 종류, 기간 , 금액, 이용한도 등 거래 내용을 판단할 수 있는 정보)</li>
				<li class="mobile-terms-listitem">③ 신용도정보(신용등급, 신용조회기록, 채무재조정약정, 연체, 부도, 대위변제, 기타 신용질서 문란행위 관련 금액, 발생•해소 시기 등 신용도를 판단할 수 있는 정보)</li>
				<li class="mobile-terms-listitem">④ 신용능력정보(재산•채무•소득의 총액, 납세실적 등 신용거래능력을 판단할 수 있는 정보)</li>
				<li class="mobile-terms-listitem">⑤ 기타 공공기관보유정보(개인회생, 파산, 면책, 채무불이행자등재 등 법원의 재판, 결정 정보 각종 체납 정보 / 주민등록관련정보 / 사회보험•공공요금 관련 정보 등)</li>
				<li class="mobile-terms-listitem">⑥ 기타 본인의 신용을 판단할 수 있는 정보(신용등급, 신용조회, 채무재조정약정 등)</li>
				<li class="mobile-terms-listitem">⑦ 본 계약 이전 및 이후의 대출 세부내역, 본인의 보유차량에 관한 정보, 가족 등 관련인 정보, 자체 기준에 따라 책정한 신용도 평가내용</li>
				<li class="mobile-terms-listitem">⑧ 본인이 대표자로 있는 가맹점의 기업정보(사업자번호, 상호, 업종코드 등), 매출정보 등</li>
				<li class="mobile-terms-listitem">* 이하에서는 개인식별정보, 신용거래정보, 신용도정보, 신용능력정보, 공공기관정보 등에 해당하는 각각의 개별정보 명칭은 생략합니다</li>
			</ul>
			
			<h2 class="mobile-terms-title">개인(신용)정보의 보유•이용 기간</h2> 
			<p class="mobile-terms-text"><span class="mobile-credt-strong-under">거래종료(채권, 채무 관계 종료) 일로 부터 5년간 (단, 관련법령의 별도 규정이 명시되어 있는 경우 그 기간을 따름)</span></p>
			
			<h2 class="mobile-terms-title">고유식별정보의 필수적 수집•이용에 대한 동의</h2> 
			<p class="mobile-terms-text">본인은 귀사가 상기 목적으로 상기의 보유•이용 기간 동안 다음과 같은 본인의 고유 식별정보를 수집•이용 하는 것에 동의합니다.</p>
			
			<h2 class="mobile-terms-title">고유식별정보</h2>
			<p class="mobile-terms-text"><span class="mobile-credt-strong-under">주민등록번호, 운전면허번호, 여권번호, 외국인등록번호</span></p>
			
			<h2 class="mobile-terms-title">※  귀하는 동의를 거부할 권리가 있으나, 동의하지 않으실 경우 거래관계의 설정 또는 유지가 불가능할 수 있음을 알려드립니다.</h2>
		</section>
	</div>
<br/>
							<div class="btn_wrap" onClick="javascript:agreeClk('1');">
								<a href="#close-modal" data-modal="modal:close" style="display:inline;">
									<button type="button" class="btn_type4">
										<span class="btn_icon btn_sicon2"></span>
										<span class="txt">동의</span>
									</button>
								</a>
							</div>

	</div>
</div>









<div id="pop_alert_b" class="modal" style="display:none;">
	<div class="inner">
		<div class="tit_wrap">
			<h1 class="h2_tit">개인(신용)정보 필수적 동의서(조회)</h1>
		</div>
	
<div class="mobile-content-wrap">
		<section class="mobile-terms">
			<h1 class="mobile-terms-heading">개인(신용)정보 조회에 관한 사항</h1>
			<p class="mobile-terms-text">[신용정보의 이용 및 보호에 관한 법률] 제32조제2항 및 [개인정보보호법] 제24조에 따라 귀사가 아래와 같은 내용으로 신용조회회사, 신용정보집중기관으로부터 본인의 신용정보를 조회하거나, 공공기관을 통해 본인임을 확인하는 것에 대하여 동의합니다</p>
			
			<h2 class="mobile-terms-title">조회할 개인신용정보</h2>	
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem">&nbsp;&nbsp;&nbsp;개인식별정보, 신용거래정보, 신용도정보, 신용능력정보, 공공기관정보, 기타 본인의 신용을 판단 할 수 있는 정보 </li>
			</ul>
			
			<h2 class="mobile-terms-title">조회 목적</h2>	
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem">&nbsp;&nbsp;&nbsp;(금융)거래관계의 체결, 유지, 이행, 관리, 개선 등 </li>
			</ul>
			
			<h2 class="mobile-terms-title">조회 동의 효력기간</h2>	
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem">&nbsp;&nbsp;&nbsp;동의일로부터 본 계약/거래의 종료 시 또는 금융거래설정이 거절되었을 경우에는 그 시점까지 본인은 귀사가 상기 목적으로 상기의 보유•이용 기간 동안 다음과 같은 본인의 고유 식별정보를 처리하는 것에 동의합니다.</li>
				<li class="mobile-terms-listitem">※ 대출 가능여부 판단을 위한 신용조회 기록은 신용등급에 영향을 주지 않습니다. 다만, 대출이 실행된 이후에는 대출정보가 신용조회회사 및 타 금융회사 등에 제공될 수 있으며 이로 인해 고객님의 신용등급이 하락할 수도 있습니다.</li>
			</ul>
			
			<h2 class="mobile-terms-title">고유식별정보</h2>
			<p class="mobile-terms-text">&nbsp;&nbsp;&nbsp;<span class="mobile-credt-strong-under">주민등록번호, 운전면허번호, 여권번호, 외국인등록번호</span></p>
			
			<h2 class="mobile-terms-title">※  귀하는 동의를 거부할 권리가 있으나, 동의하지 않으실 경우 거래관계의 설정 또는 유지가 불가능할 수 있음을 알려드립니다.</h2>			
		</section>
	</div>
<br/>
							<div class="btn_wrap" onClick="javascript:agreeClk('2');">
								<a href="#close-modal" data-modal="modal:close" style="display:inline;">
									<button type="button" class="btn_type4">
										<span class="btn_icon btn_sicon2"></span>
										<span class="txt">동의</span>
									</button>
								</a>
							</div>

	</div>

</div>



<div id="pop_alert_c" class="modal" style="display:none;">
	<div class="inner">
		<div class="tit_wrap">
			<h1 class="h2_tit">개인(신용)정보 필수적 동의서(조회)</h1>
		</div>

<div class="mobile-content-wrap">
		<section class="mobile-terms">
			<h1 class="mobile-terms-heading">메리츠캐피탈주식회사 귀하</h1>
			<p class="mobile-terms-text">귀사와의 (금융)거래와 관련하여 귀사가 본인으로부터 취득한 개인(신용)정보는「개인정보 보호법」제17조, 제22조및 제24조,「신용정보의 이용 및 보호에 관한 법률」제32조, 제33조 및 제34조, 정보통신망 이용촉진 및 정보보호 등에 관한 법률」제24조의 2에따라 제3자에게 제공할 경우 본인의 사전동의를 얻어야 하는정보입니다. 이에 본인은 귀사가 본인의 개인(신용)정보를 아래와 같이 제3자에게 제공하는 것에 대해 동의합니다.</p>
			
            <h1 class="mobile-terms-heading">개인(신용)정보의 필수적인 제공에 관한 사항</h1>
			<h2 class="mobile-terms-title">(1) 신용정보집중기관 및 신용조회회사에 개인(신용)정보 제공</h2>
			<h2 class="mobile-terms-title">개인(신용)정보를 제공 받는 자</h2>	
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">① 신용정보집중기관(전국은행연합회)</span></li>
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">② 신용조회회사(나이스신용평가정보㈜, 코리아크레딧뷰로㈜ 등)</span></li>
			</ul>
			
            <h2 class="mobile-terms-title">개인(신용)정보 제공목적</h2>	
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">① 신용정보의 집중관리 및 활용 등 신용정보집중 기관의 업무</span></li>
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">② 본인의 신용도 평가, 실명확인 등 신용조회업무, 본 계약 및 본 계약 이전 발생 계약의 유지 또는 사후관리 등</span></li>
			</ul>			
			
			<h2 class="mobile-terms-title">제공대상 개인(신용)정보</h2>	
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem">개인식별정보, 신용거래정보, 신용도정보, 신용능력정보, 공공기관정보, 기타 본인의 신용을 판단할 수 있는 정보 </li>
			</ul>
			
			<h2 class="mobile-terms-title">제공받는 자의 개인(신용)정보 보유 및 이용기간</h2>	
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">신용정보의 이용 및 보호에 관한 법률 및 관련 규약에 근거한 기간까지 </span></li>
			</ul>
			
			
            <h2 class="mobile-terms-title">(2) 거래목적 달성을 위한 개인(신용)정보 제공</h2>
            <h2 class="mobile-terms-title">개인(신용)정보를 제공 받는 자</h2>
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">(자세한 업체명은 메리츠캐피탈㈜ 홈페이지를 통해 확인 하실 수 있으며, 변동되는 경우에도 홈페이지를 통해서 알려드립니다.)</span></li>
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">  - 대출업무 위탁 및 차량할부금융사 : 카피아오토플랜㈜, 한길모터스, ㈜아이오토에프엔 등</span></li>
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">  - 카드제휴 및 보험업체 : 메리츠화재해상보험(주), 서울보증보험 등, 삼성카드(주), 롯데카드(주), 하나SK카드(주) 등</span></li>
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">  - 채권추심 및 회수업체 : 중앙신용정보, 세일신용정보 등</span></li>
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">  - 당사 업무 대행업체 : 법무사 김장훈 사무소, 코리아크레딧뷰로(주), SKT, KT, LGUplus 등 통신사, SK엔카, SLK솔루션, 유림로지텍, 데이터존 등</span></li>
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">  - 정비위탁 및 업무지원업체 : ㈜카일이삼제스퍼, (주)카리안, (주)하이렌터카, ㈜하이리모서비스, AJ셀카 등</span></li>
				<li class="mobile-terms-listitem"><span class="mobile-credt-strong-under">  - 공공기관 및 과태료/범칙금관련 기관 또는 그 외 업체의 경우 당사 홈페이지 참고</span></li>
			</ul>
			
            <h2 class="mobile-terms-title">개인(신용)정보 제공목적</h2>	
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem">&nbsp;&nbsp;&nbsp;<span class="mobile-credt-strong-under">거래목적의 달성, 금융사고조사, 금융거래의 설정, 유지, 이행, 관리 등에 필요한 업무를 수행 및 범죄행위의 고소, 고발, 혹은 유사 기타 국가기관의 협조(자서서류대행, 공동마케팅 비용정산, 차량 등 담보물 관리/평가, 본인인증, 신청서 등 문서 관리(입력ㆍ스캔ㆍ보관ㆍ폐기). 우편물(명세서 등) 배송 및 반송, 전화상담, 전산처리 및 개발ㆍ유지ㆍ보수, 권원조사/권리조사, 신용조사/채권추심, 결제대금출금, 사은품ㆍ 판촉물 발송, 법조치 업무, 법률자문, 녹취록 작성, 녹취서버 운영, SMS발송, 자동차 공매ㆍ이전ㆍ등록, 자동차 대출 제휴카드 발급, 자동차 구입ㆍ리콜ㆍ금리정산, 자동차 할부(대출) 중개ㆍ알선, 자동차 유지관리, 사고대차지급, 자동차 리스 중개ㆍ알선, 자동차 탁송, 리스 반납 차량 처리, 자동차보험 및 사고관리, 과태료 및 범칙금 처리, 자동차정비, 잔가보장, 보험가입, 범죄행위 관련 수사ㆍ조사 협조, 금융사고방지 등을 위한 공공기관 자료제출)</span></li>
			</ul>			
			
			<h2 class="mobile-terms-title">제공, 조회할 개인(신용)정보 내용</h2>	
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem">필수적 수집, 이용에 동의한 개인(신용)정보</li>
			</ul>
			
			<h2 class="mobile-terms-title">제공받는 자의 개인(신용)정보 보유 및 이용기간</h2>	
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem">&nbsp;&nbsp;&nbsp;<span class="mobile-credt-strong-under">위탁계약 종료 시 또는 위탁업무 완료 시 (단, 관련법령의 별도 규정이 명시되어 있는 경우 그 기간을 따름)</span></li>
			</ul>
			
			<h2 class="mobile-terms-title">고유식별정보의 필수적 수집ㆍ이용에 대한 동의</h2>
			<p class="mobile-terms-text">본인은 귀사가 위의 제공을 위하여 상기의 보유•이용 기간 동안 다음과 같은 본인의 고유식별정보를 처리하는 것에 동의합니다.</p>
			
			<h2 class="mobile-terms-title">고유식별정보</h2>
			<p class="mobile-terms-text"><span class="mobile-credt-strong-under">주민등록번호, 운전면허번호, 여권번호, 외국인등록번호</span></p>
			
			<h2 class="mobile-terms-title">※  귀하는 동의를 거부할 권리가 있으나, 위 사항에 동의하지 않으실 경우 거래관계의 설정 또는 유지가 불가능할 수 있음을 알려드립니다.</h2>
			<h2 class="mobile-terms-title">※ 상기 내용이 변동되는 경우 인터넷 홈페이지 게시를 통해 그 내용을 안내해 드리며, 본 동의서는 이 계약이 성립되지 않는 경우 그 시점으로부터 효력을 상실합니다.</h2>
			<h2 class="mobile-terms-title">※ 본인은 본 동의서의 내용을 이해하였으며, 개인(신용)정보 수집•이용•제공•조회에 관한 개인정보 처리방침에 관하여 자세히 설명을 듣고 수령하였습니다.</h2>
		</section>
	</div>
<br/>
							<div class="btn_wrap" onClick="javascript:agreeClk('3');">
								<a href="#close-modal" data-modal="modal:close" style="display:inline;">
									<button type="button" class="btn_type4">
											<span class="btn_icon btn_sicon2"></span>
											<span class="txt">동의</span>
									</button>
								</a>
							</div>

	</div>

</div>







<div id="pop_alert_d" class="modal" style="display:none;">
	<div class="inner">
		<div class="tit_wrap">
			<h1 class="h2_tit">개인(신용)정보 선택적 동의서 (조회)</h1>
		</div>

<div class="mobile-content-wrap">
		<section class="mobile-terms">
			<p class="mobile-terms-text"><span class="mobile-credt-strong-under">귀하는 개인(신용)정보의 선택적인 수집, 이용에 대한 동의를 거부할 수 있습니다. 다만, 동의하지 않을 경우에는 상품•서비스 소개 및 권유, 사은행사(경품, 사은품 제공 등), 판촉행사(특판금리 등), 보험상품 안내 등 이용목적에 따른 혜택의 제한이 있을 수 있습니다. 그 밖의 금융거래와 관련된 불이익은 없습니다.</span></p>
			
			<h2 class="mobile-terms-heading">개인(신용)정보의 선택적인 수집•이용에 관한 사항</h2> 
			
			<h2 class="mobile-terms-title">(1) 금융상품 안내 및 이용권유를 위한 수집 이용</h2>
			<h2 class="mobile-terms-title">개인(신용)정보의 수집•이용 목적</h2>
			<ul class="mobile-terms-list">
				<li class="mobile-terms-listitem">① 상품•서비스 소개 및 권유, 사은행사, 판촉행사, 시장조사, 상품개발•연구 등</li>
				<li class="mobile-terms-listitem">② 보험상품과 연계된 제휴금융상품 공동마케팅, 부가서비스 제공 및 활용</li>
				<li class="mobile-terms-listitem">③ 「정보통신망 이용 촉진 및 정보보호 등에 관한 법률」제50조제2항 규정에 따른 전화 또는 FAX에 의한 영리목적의 광고성 정보전송</li>
			</ul>
			
			<h2 class="mobile-terms-title">수집•이용할 개인(신용)정보의 내용</h2>
			<p class="mobile-terms-text">필수적 수집 이용에 동의한 개인(신용)정보</p>
			
			<h2 class="mobile-terms-title">개인(신용)정보의 보유•이용기간</h2>
			<p class="mobile-terms-text">계약종료시까지 또는 마케팅 동의 철회시까지</p>
			
			<h2 class="mobile-terms-title">이용권유 방법</h2>
			<p class="mobile-terms-text">□ 전부동의 (□전화 □문자메세지 □서면 □이메일</p>
			<p class="mobile-terms-text">□ 동의하지 않음</p>
			
			<p class="mobile-terms-text"><span class="mobile-credt-strong-under">고객님께서 동의하시면 당사가 제공하는 유익한 서비스나 제휴혜택 등을 전화, 휴대폰 문자메시지, 서면, 이메일 등으로 받아보실 수 있습니다.</span></p>
			
			<h2 class="mobile-terms-title">※ 위 내용에 동의하셨더라도 신용정보의 이용 및 보호에 관한 법률에 따라 이용권유를 목적으로 하는 연락에 대한 중단을 언제든지 요청할 수 있습니다</h2>
			<h2 class="mobile-terms-title">대표전화 : 6309-3700 / 홈페이지 : www.meritzcapital.com)</h2>
			<h2 class="mobile-terms-title">※ 상품변경안내 등 상품 변경 안내 등 필수 고지사항은 상기 동의대상에서 제외됩니다.</h2>
		</section>
	</div>
<br/>
							<div class="btn_wrap" onClick="javascript:agreeClk('3_1');">
								<a href="#close-modal" data-modal="modal:close" style="display:inline;">
									<button type="button" class="btn_type4">
											<span class="btn_icon btn_sicon2"></span>
											<span class="txt">동의</span>
									</button>
								</a>
							</div>

	</div>

</div>