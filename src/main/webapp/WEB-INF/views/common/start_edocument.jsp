<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="/WEB-INF/views/common/head.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	isNextBtnEnable();
	
	//팝업체크
	var popupWin = window.open('about:blank', 'popupWin');
	var popupBoo = false;

	if(popupWin == null) {
// 		showAlert('팝업이 차단되어 있습니다. 팝업차단해제 후 진행해 주세요.', '알림', function(){  });
	} else {
		popupBoo = true;
		popupWin.close();
		if(navigator.userAgent.indexOf('NAVER') != -1){
			setTimeout(function(){
			        popupWin.close();
			},1000);
		}
	}
	
	//삼성 브라우저 체크
// 	var browserBoo = false;
	
// 	var clientInfo = navigator.userAgent;
// 	var index = clientInfo.indexOf('SamsungBrowser/');
// 	var samsungBrowserVersion = clientInfo.substring(index + 15,index + 16);
	
// 	if(index != -1 && samsungBrowserVersion >= 7){
// 		showAlert('최신 버전의 삼성 브라우져로 접속 시 전자서식 사용이 불가능할 수 있습니다. 크롬 등 다른 브라우져를 이용하여 접속 부탁드립니다.', '알림', function(){  });
// 	}else{
// 		browserBoo = true;
// 	}
	
	$(".nextStep").click(function(){
		if($(this).attr('class') == "btn_type10 bg_color11 nextStep on"){
			showAlert("상기 고지사항에 대한 동의가 필요합니다.", '알림', function(){  });
			return false;
		}
// 		if(!browserBoo){
// 			showAlert('최신 버전의 삼성 브라우져로 접속 시 전자서식 사용이 불가능할 수 있습니다. 크롬 등 다른 브라우져를 이용하여 접속 부탁드립니다.', '알림', function(){  });
// 			return false;
// 		}else{
			if(!popupBoo){
				showAlert('팝업이 차단되어 있습니다. 팝업차단해제 후 진행해 주세요.(새로고침 필요)', '알림', function(){  });
				return false;
			}else{
				location.href = $(location).attr('href') +"&notice=1";	
			}
// 		}
	});
	
	$('.check1').click(function(){
		isNextBtnEnable();
	});
	
	function isNextBtnEnable(){
		if($(".check1").is(":checked"))
		{
			$('.nextStep').removeClass("on");
		}
		else
		{
			$('.nextStep').addClass("on");
		}
	}
	
});
</script>
<style>
.customInnerText1{
    font-size: 10px;
    color: #9b9b9b;
    margin-left: 20px;
    }
</style>
<body>
<div id="accessibility">
	<a href="#container">본문 바로가기</a>
</div>
<div id="wrap">
	<header id="header">
		<h1 class="h1_tit">메리츠캐피탈 전자약정서비스</h1>
	</header>	
	<div id="container">
		<div class="contents">
		<p style="margin-left:10px;margin-top: 10px;">메리츠캐피탈 전자약정서비스를 이용해 주셔서 감사합니다.<br><br>
			당사 지점 방문 없이 언제, 어느 곳에서든지 스마트폰을 이용하여 편리하게 약정서를 체결하실 수 있습니다.<br>
		</p>
			<div class="contbox first">
				<h2 class="cont_tit">이용 절차</h2>
				<div class="form_type1 b_line">
					<div class="row">
					<span>
						1. 휴대폰 본인확인<br>
					    2. 본인계좌 인증 <br>
					    <p class="customInnerText1">
					     * 당사에서 고객님 계좌로 1원을 이체하며, 입금자명에 삽입된 인증숫자를 확인 후 계좌인증숫자를 입력하여 실명인증을 완료합니다.<br>
					    </p>
					    3. 약정서 확인 및 자필서명<br>
					    4. 약정서 전송 <br>
					</span>
					</div>
				</div>
			</div>
			<div class="contbox first">
				<h2 class="cont_tit">고객(민감)정보 처리 위탁 등에 관한 고지</h2>
				<div class="form_type1 b_line">
					<div class="row">
						<span class="txt">
							정보통신망 이용촉진 및 정보보호 등에 관한 법률 제25조에 따라 고지합니다.<br>
						    ■ 처리목적 : 개인정보 수집, 이용, 처리 등의 동의에 따라 수집된 고객정보를
<!-- 						     <p style="margin-left: 60px;"> -->
						    		기반으로 전자약정서 생성, 조회 등의 처리를 위한 업무 위탁<br>
<!-- 						    </p> -->
						    ■ 처리기간 : 대출취급 혹은 거절 시 까지 <br>
						    ■ 처리범위 <br>
						    <p style="margin-left: 20px;">
						    ① 성명, 고유식별정보, 금융거래계좌, 생년월일, 유선전화번호,
					    		휴대폰번호, 주소 등<br>
						    ② 상품종류, 거래조건(이자율, 만기, 담보 등), 거래일시, 금액 등
		   					    거래 설정 및 내역 정보 <br>
		   					</p>
						    ■ 처리자(수탁자) : 이니텍㈜<br>
					    </span>
					</div>
				</div>
			</div>
			<div class="agree_box">
					<div class="header">
						<span class="check01">
							<input type="checkbox" id="prd_chk3_1" class="check1">
							<label for="prd_chk3_1"><span class="vt_m">상기 고지사항을 동의합니다.</span></label>
						</span>
					</div>
				</div>
			<div class="btn_wrap">
				<button type="button" class="btn_type10 bg_color11 nextStep on">
					<span class="txt">다음</span>
					<span class="btn_icon btn_icon5"></span>
				</button>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/common/footer.jsp"%>
</div>
</body>
</html>
