<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="/WEB-INF/views/common/head.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
//     $(".bg_color11").hide();
    $(".bg_color11").click(function(){
            window.close();
            window.history.go(-(window.history.length - 1));
    });
    
    //뒤로가기막기
	history.pushState(null, null, location.href);
	window.onpopstate = function(event) {	
		history.go(1);
	};
  
});
</script>
<body>
<div id="accessibility">
	<a href="#container">본문 바로가기</a>
</div>
<div id="wrap">
	<header id="header">
		<h1 class="h1_tit">메리츠캐피탈 전자약정서비스</h1>
	</header>
	
	<div id="container">
		<div class="contents">
			<div class="contbox first">
				<span class="ico_style04"></span>
				<p class="cont_txt type03">
					<span class="txt_style01 ex_bold"></span>
					전자약정이<span class="txt_color02">완료</span>되었습니다.
				</p>
				<ul class="list_style01">
					<li class="color1">금융사고 예방을 위해 상담원을 통해 안내 후 입금을 진행해 드리고 있습니다.</li>
					<li class="color1">영업 외 시간 접수 건에 대해서는 익영업일 오전 9시부터 접수 순서대로 담당상담원을 통해 연락을 드립니다.</li>
					<li>영업시간 : 평일 09:00 ~ 18:00 (주말, 공휴일 휴무)</li>
				</ul>
			</div>
			<div class="btn_wrap">
				<button type="button" class="btn_type10 bg_color11">
					<span class="txt">완료</span>
					<span class="btn_icon btn_icon5"></span>
				</button>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/common/footer.jsp"%>
</div>
</body>
</html>
