<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="/WEB-INF/views/common/head.jsp"%>
<body>
<div id="accessibility">
	<a href="#container">본문 바로가기</a>
</div>
<div id="wrap">
	<header id="header">
		<h1 class="h1_tit">메리츠캐피탈 전자약정서비스</h1>
	</header>
	
	<div id="container">
		<div class="contents">
			<div class="contbox first">
				<span class="ico_style04"></span>
				<p class="cont_txt type03">
					<span class="txt_style01 ex_bold"></span>
					해당 URL은 <span class="txt_color02">만료</span>되어 사용할 수 없습니다.
				</p>
				<ul class="list_style01">
					<li class="color1">담당직원에게 문의 하신 후 이용해 주시기 바랍니다.</li>
					<li>영업시간 : 평일 09:00 ~ 18:00 (주말, 공휴일 휴무)</li>
				</ul>
			</div>
			<div class="btn_wrap">
				<button type="button" class="btn_type10 bg_color11">
					<span class="txt">완료</span>
					<span class="btn_icon btn_icon5"></span>
				</button>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/common/footer.jsp"%>
</div>
</body>
</html>
