<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="/WEB-INF/views/common/head.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	
	$(".nextStep").click(function(){
		location.href = "/phone_certification?progress_id=${map.seq}";
	});
});
</script>
<body>
<div id="accessibility">
	<a href="#container">본문 바로가기</a>
</div>
<div id="wrap">
	<header id="header">
		<h1 class="h1_tit">메리츠캐피탈 전자약정서비스</h1>
	</header>
	
	<div id="container">
		<div class="contents">
			<div class="contbox first">
				<h2 class="cont_tit">전자청약 서비스</h2>
				<div class="form_type1 b_line">
					<div class="row">
						<div class="header">
							<span class="label">계약번호</span>
						</div>
						<div class="cont">
							<div class="box">
								<span class="txt">${map.contract_code}</span>
							</div>
						</div>
					</div>
					<div class="txt_row">
						<div class="header">
							<span class="label">거래처코드</span>
						</div>
						<div class="cont">
							<div class="box">
								<span class="txt">${map.customer_code}</span>
							</div>
						</div>
					</div>
					<div class="txt_row">
						<div class="header">
							<span class="label">현재 단계</span>
						</div>
						<div class="cont">
							<div class="box">
								<span class="txt">${map.status_name}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="btn_wrap">
				<button type="button" class="btn_type10 bg_color11 nextStep">
					<span class="txt">다음</span>
					<span class="btn_icon btn_icon5"></span>
				</button>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/common/footer.jsp"%>
</div>
</body>
</html>