var agt = navigator.userAgent.toLowerCase();
var view_domain = document.domain;
is_major = parseInt(navigator.appVersion);
is_minor = parseFloat(navigator.appVersion);

is_moz = ((agt.indexOf('mozilla') != -1) && (agt.indexOf('spoofer') == -1)
		&& (agt.indexOf('compatible') == -1) && (agt.indexOf('opera') == -1)
		&& (agt.indexOf('webtv') == -1) && (agt.indexOf('hotjava') == -1));
is_moz2 = (is_moz && (is_major == 2));
is_moz3 = (is_moz && (is_major == 3));
is_moz4 = (is_moz && (is_major == 4));
is_gecko = (agt.indexOf('gecko') != -1);

/**
 * 브라우져가 firefox 일 경우 변수의 값
 * @type boolean
 * @global is_firefox
 */
is_firefox = (agt.indexOf('firefox') != -1);

is_nav2 = is_moz2;
is_nav3 = is_moz3;
is_nav4 = is_moz4;
is_nav4up = (is_nav4 || ((is_major >= 4) && (agt.indexOf("netscape") != -1)));
is_nav = (is_nav2 || is_nav3 || is_nav4);

is_mozilla = (is_moz && is_gecko);
is_mozilla1 = (is_moz && is_gecko && (agt.indexOf("rv:1") != -1));
/**
 * 브라우져가 인터넷 익스플러러일 경우 변수의 값
 * @type boolean
 * @global is_ie
 */
is_ie = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
is_ieVersion = 0;
is_ie6 = (is_ie && (is_major == 4) && (agt.indexOf("msie 6.") != -1));
if (is_ie6) {
	is_ieVersion = 6;
}
/**
 * 브라우져가 인터넷 익스플러러 7 버전일 경우 변수의 값
 * @type boolean
 * @global is_ie7
 */
is_ie7 = (is_ie && (is_major == 4) && (agt.indexOf("msie 7.") != -1));
if (is_ie7) {
	is_ie6 = false;
	is_ieVersion = 7;
}
/**
 * 브라우져가 인터넷 익스플러러 8 버전일 경우 변수의 값
 * @type boolean
 * @global is_ie8
 */
is_ie8 = (is_ie && (is_major == 4) && (agt.indexOf("msie 8.") != -1));
if (is_ie8) {
	is_ie6 = false;
	is_ie7 = false;
	is_ieVersion = 8;
}
/**
 * 브라우져가 인터넷 익스플러러 9 버전일 경우 변수의 값
 * @type boolean
 * @global is_ie9
 */
is_ie9 = (is_ie && (is_major == 5) && (agt.indexOf("msie 9.") != -1));
if (is_ie9) {
	is_ie6 = false;
	is_ie7 = false;
	is_ie8 = false;
	is_ieVersion = 9;
}
/**
 * 브라우져가 인터넷 익스플러러 10 버전일 경우 변수의 값
 * @type boolean
 * @global is_ie10
 */
is_ie10 = (is_ie && (is_major == 5) && (agt.indexOf("msie 10.") != -1));
if (is_ie10) {
	is_ie6 = false;
	is_ie7 = false;
	is_ie8 = false;
	is_ie9 = false;
	is_ieVersion = 10;
}
/**
 * 브라우져가 인터넷 익스플러러 11 버전일 경우 변수의 값
 * @type boolean
 * @global is_ie11
 */
is_ie11 = (is_gecko && !is_firefox && (is_major == 5) && (agt.indexOf("rv:11.") != -1));
if (is_ie11) {
	is_ie6 = false;
	is_ie7 = false;
	is_ie8 = false;
	is_ie9 = false;
	is_ie10 = false;
	is_ie = true;
	is_ieVersion = 11;
}
is_window = (agt.indexOf('windows') != -1);
/**
 * 브라우져가 chrome 일 경우 변수의 값
 * @type boolean
 * @global is_chrome
 */
is_chrome = !is_ie && (agt.indexOf("chrome") != -1) ? true : false;
is_chrome58 = false;
if (is_chrome) {
	var tempVer = agt.match(/chrome\/([0-9]+)\./g);
	if (null != tempVer) {
		if (Number(tempVer[0].substring(7)) > 57) {
			is_chrome58 = true;
		}
	}
}
/**
 * 브라우져가 edge 일 경우 변수의 값
 * @type boolean
 * @global is_edge
 */
is_edge = is_chrome && (agt.indexOf("edge") != -1);

/**
 * 브라우져가 opera 일 경우 변수의 값
 * @type boolean
 * @global is_opera
 */
is_opera = is_chrome && (agt.indexOf("opr/") != -1);
/**
 * 브라우져가 safari 일 경우 변수의 값
 * @type boolean
 * @global is_safari
 */
is_safari = (agt.indexOf('safari') != -1 && !is_chrome);
/**
 * 브라우져가 window safari 일 경우 변수의 값
 * @type boolean
 * @global is_window_safari
 */
is_window_safari = (is_safari && agt.indexOf('windows') != -1);

function ReportExeViewerLog(message) {
	if (typeof window.console != 'undefined') {
		window.console.log(message);
	}
};

function ReportExeViewerEventHandler(eventArgs) {
	
};

function HttpClient() {
	try {
		var objRequest = new XMLHttpRequest();
		// var objRequest = new XDomainRequest();
		if (null != objRequest) {
			this.m_objHttpRequest = objRequest;
		}
	} catch (e) {
		try {
			var objRequest = new ActiveXObject("Msxml2.XMLHTTP");
			if (null != objRequest) {
				this.m_objHttpRequest = objRequest;
			}
		} catch (e) {
			try {
				var objRequest = new ActiveXObject("Microsoft.XMLHTTP");
				if (null != objRequest) {
					this.m_objHttpRequest = objRequest;
				}
			} catch (e) {
				ReportExeViewerLog("XMLHttpRequest not supported");
			}
		}
	}
};

HttpClient.prototype.send = function(url, objData, bAsync, handleASyncHttpResponse) {
	if (bAsync) {
		this.m_objHttpRequest.onreadystatechange = handleASyncHttpResponse;
	} else {
		this.m_objHttpRequest.onreadystatechange = null;
	}
	
	if (-1 != url.indexOf("?")){
	 	var sendUrl = url + "&" + objData;
		this.m_objHttpRequest.open("GET", sendUrl, bAsync);
		this.m_objHttpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		//this.m_objHttpRequest.setRequestHeader("Content-Encoding", "gzip");
		this.m_objHttpRequest.send();
	} else {
		this.m_objHttpRequest.open("POST", url, bAsync);
		this.m_objHttpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		//this.m_objHttpRequest.setRequestHeader("Content-Encoding", "gzip");
		this.m_objHttpRequest.send(objData);
	}
	

	if (!bAsync) {
		if (4 == this.m_objHttpRequest.readyState) {
			if (200 == this.m_objHttpRequest.status) {
				return this.m_objHttpRequest.responseText;
			} else {
				ReportExeViewerLog(this.m_objHttpRequest.status);
			}
		}
		return this._handleSyncHttpResponse();
	}
};

var EXEC_METHOD_CONSTANTS = { 
	VIEWER : "Viewer",
	DIRECT_PRINT : "DirectPrint",
	DIRECT_EXPORT : "DirectExport"
};



/**
 * @class createReportExeViewer
 * @description 실제 ReportExeViewer 객체를 생성합니다.
 * @example createExeViewer(urlPath + "/ClipReport/report_server.jsp", oofData, document.getElementById("targetDiv"));
 * 
 * @param requestUri
 * @param oof
 * @param targetDiv
 */
function createReportExeViewer(requestUri, oofData, targetDiv) {
	var reportExeViewer = new ReportExeViewer(requestUri, oofData, targetDiv);
	return reportExeViewer;
};

/**
 * @class ReportExeViewer
 * @description createReportExeViewer 함수를 이용하여 만들어진 뷰어 객체입니다.<br>
 * 객체를 이용하여 뷰어를 띄우거나, 다이렉트 프린트, 다이렉트 저장 기능을 사용할 수 있습니다.<br>
 * 
 * @param requestUri
 * @param oofData
 * @param targetDiv
 */
function ReportExeViewer(requestUri, oofData, targetDiv) {
	this.m_requestUri = requestUri;
	this.m_oofData = oofData;
	this.m_targetDiv = targetDiv;
	this.m_isCrossDomain = false;
	
	this.m_downloadLink = null;
	
	this.m_isUseKeyExec = true;
	
	this.m_service = {
		url : "http://127.0.0.1",
		port : [22000, 33000, 11000],
		httpPort : [22000, 33000, 11000],
		httpsPort : [22443, 33443, 11443],
		intervalKey : null,
		disconnectCount : 0,
		set : false,
		portNumber : null
	};
	
	this.m_execMethod = EXEC_METHOD_CONSTANTS.VIEWER;
	
	this.m_print = {
		isShowDialog : true,
		printerName : "",
		totalPageNumber: 0,
		currentPageNumber : 0,
		option : {
			documentName : "Document",
			
			printRange : "AllPages",
			
			paperSource : "",
			pageOrientation : "Auto",
			fitToPaperSize : false,
			centralPrint : false,
			zoomRate : 100,
			
			userColorMode : "Auto",
			
			copies : 1,
			collate : false,
			
			locationX : 0,
			locationY : 0
		}
	};
	
	this.m_isDirectExport = false;
	this.m_exportOption = null;
	
	this.m_logger = {
		log : function (message) {
			if (typeof window.console != 'undefined') {
				window.console.log(message);
			}
		}
	};
};

ReportExeViewer.prototype.log = function(message) {
	this.m_logger.log(message);
};

/**
 * @description 크로스 도메인 설정. <br>
 * {기본값 false} 
 * @param isCrossDomain {Boolean}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setCrossDomain = function(isCrossDomain) {
	this.m_isCrossDomain = isCrossDomain;
};

/**
 * @description EXE 뷰어가 설치되어있지 않을경우 알림창에 표시할 EXE 뷰어 설치파일 링크 주소. <br>
 * @param downloadLink {String}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setDownloadLink = function (downloadLink) {
	this.m_downloadLink = downloadLink;
};

/**
 * @description 리포트서버에 리포트생성 요청을 하여 키를 받아 뷰어를 실행 할 것인지에 대한 설정. </br>
 * 설정값이 false 일 경우 뷰어를 실행후 뷰어에서 서버로 리포트를 요청하여 리포트를 생성합니다. <br>
 * { 기본값 true }
 * @param useKeyExec {Boolean}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setUseKeyExec = function (isUseKeyExec) {
	this.m_isUseKeyExec = isUseKeyExec;
};


/**
 * @description 프린터 인쇄시 인쇄창에 표시되는 문서명
 * {기본값 Document}
 * @param documentName {String}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setPrintDocumentName = function(documentName) {
	this.m_print.option.documentName = documentName;
};

/**
 * @description 프린트 인쇄 범위. <br>
 * 전체페이지 - AllPages <br>
 * 페이지 지정 - SomePages;1-6 <br>
 * 현재 페이지 - CurrentPage <br>
 * 일부분 - Selection;1,3,5-6 <br> 
 * {기본값 AllPages}
 * @param printRange {String}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setPrintRange = function(printRange) {
	this.m_print.option.printRange = printRange;
};

/**
 * @description 프린트 인쇄 트레이 정보. <br>
 * @example 총 페이지가 6페이지 일 경우 <br>
 * 트레이 정보 PaperSource <br>
 * 	- 1. 자동 선택 - AutomaticFeed-7 <br>
 * - 2. 다목적 용지함[트레이 이름] - ManualFeed-6[트레이 아이디] <br>
 * - 3. 용지함 1[트레이 이름] - Upper-1 <br>
 * 하나의 트레이로 쭉 인쇄할 경우 - 트레이 이름 "다목적 용지함" 또는 트레이 아이디 "6" <br>
 * 하나 이상의 트레이로 인쇄해야 할 경우<br>
 * "[1,2,3,2,3]"<br>
 * 1페이지는 자동 선택후 2페이지는 다목적 용지함 3페이지는 용지함 1 .. <br>
 * 마지막페이지는 설정하지 않았으므로 마지막으로 설정된 용지함 1 트레이로 설정됨 
 * <font style='color:orange'>트레이 정보는 http://localhost:활성화된 서비스 포트/printer/papersource</font>
 * <font style='color:orange'>서비스 포트 [22000, 33000, 11000] </font>
 * @param printPaperSource {String}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setPrintPaperSource = function(paperSource) {
	this.m_print.option.paperSource = paperSource;
};

/**
 * @description 인쇄 방향. <br>
 * Auto(페이지 설정대로) <br>
 * Landscape(가로) <br>
 * Portrait(세로) <br>
 * {기본값 Auto}
 * @param pageOrientation {String}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setPrintPageOrientation = function(pageOrientation) {
	this.m_print.option.pageOrientation = pageOrientation;
};

/**
 * @description 용즈 크기에 맞춤. <br>
 * {기본값 false}
 * @param fitToPaperSize {Boolean}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setPrintFitToPaperSize = function(fitToPaperSize) {
	this.m_print.option.fitToPaperSize = fitToPaperSize;
};

/**
 * @description 용지 중앙에 인쇄. <br>
 * {기본값 false}
 * 
 * @param {Boolean}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setPrintCentralPrint = function(centralPrint) {
	this.m_print.option.centralPrint = centralPrint;
};

/**
 * @description 인쇄 확대/축소 비율. <br>
 * {기본값 100}
 * 
 * @param zoomRate {Integer}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setPrintZoomRate = function(zoomRate) {
	this.m_print.option.zoomRate = zoomRate;
};

/**
 * @description 사용자 정의 컬러모드. <br>
 * Auto - 자동 <br>
 * Color - 컬러 <br>
 * Grayscale - 흑백 <br>
 * {기본값 Auto}
 * 
 * @param userColorMode {Boolean}
 */
ReportExeViewer.prototype.setPrintUserColorMode = function(userColorMode) {
	this.m_print.option.userColorMode = userColorMode;
};

/**
 * @description 인쇄 매수. <br>
 * {기본값 1}
 * 
 * @param copies {Boolean}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setPrintCopies = function(copies) {
	this.m_print.option.copies = copies;
};

/**
 * @description 한 부씩 인쇄. <br>
 * {기본값 false}
 * 
 * @param collate {Boolean}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setPrintCollate = function(collate) {
	this.m_print.option.collate = collate;
};

/**
 * @description 프린트 인쇄 시작위치. <br>
 * {기본값  locationX = 0, locationY = 0 }
 * 
 * @param locationX {Integer}
 * @param locationY {Integer}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.setPrintLocation = function(locationX, locationY) {
	this.m_print.option.locationX = locationX;
	this.m_print.option.locationY = locationY;
};


ReportExeViewer.prototype.newReportResponseValidation = function (data) {
	if (data.resValue.status) {
		return true;
	} else {
		if (data.resValue.event > 999) {
			this.log("newReportResponseValidation - 리포트서버 라이센스 불일치 [LE0" + data.event + "]");
			/*
			if(this.m_isViewer){
				this.paintDiv.innerHTML = this.m_languageMap.lic_error + ".!! (errorCode : LE0" + data.event +")";
			}
			mRe_ReportEventHandler(data.event);
			this.endDebugMeg(false);
			*/
		} else if (data.resValue.event < 13) {
			this.log("newReportResponseValidation - 리포트서버 인스톨 실패 [IE00" + data.event + "]");
			/*
			if(this.m_isViewer){
				this.paintDiv.innerHTML = this.m_languageMap.install_error + "!! (errorCode : IE00" + data.event +")";
			}
			mRe_ReportEventHandler(data.event);
			this.endDebugMeg(false);
			*/
		} else {
			this.log("newReportResponseValidation - 리포트서버에서 리포트 생성중 오류 발생. 서버 로그 확인 필요");
			/*
			if(this.m_isViewer){
				this.paintDiv.innerHTML = this.m_languageMap.report_error;
			}
			mRe_ReportEventHandler(30);
			*/
		}
		return false;
	}
};

ReportExeViewer.prototype.startExecResponse = function(data) {
	try {
		if (data.result) {
			if (this.m_execMethod === EXEC_METHOD_CONSTANTS.DIRECT_PRINT) {
				this.log('startExecResponse - 다이렉트 프린트를 성공적으로 호출하였습니다.');
			} else if (this.m_execMethod === EXEC_METHOD_CONSTANTS.DIRECT_EXPORT) {
				this.log("startExecResponse - 다이렉트 저장을 성공적으로 호출였습니다.");
			} else if (this.m_execMethod === EXEC_METHOD_CONSTANTS.VIEWER) {
				this.log('startExecResponse - 뷰어를 성공적으로 호출하였습니다.');
			}
		} else {
			var code = data.code;
			if (this.m_execMethod === EXEC_METHOD_CONSTANTS.DIRECT_PRINT) {
				this.log('startExecResponse - 다이렉트 프린트를 호출하는데 실패하였습니다. [코드번호 : ' + code + ']');
			} else if (this.m_execMethod === EXEC_METHOD_CONSTANTS.DIRECT_EXPORT) {
				
			} else if (this.m_execMethod === EXEC_METHOD_CONSTANTS.VIEWER) {
				this.log('startExecResponse - 뷰어를 호출하는데 실패하였습니다. [코드번호 : ' + code + ']');
			}
		}
	} catch (e) {
		this.m_service.disconnectCount++;
		this.log('startExecResponse - 다른 서비스 로컬서버가 존재하는 것 같습니다.');
	}
};

ReportExeViewer.prototype.notFoundService = function () {
	this.log('notFoundService - 서비스를 찾을 수 없습니다.');
};

ReportExeViewer.prototype.serviceExecute = function (data) {
	try {
		if (is_chrome58 || "http:" == location.protocol.toLocaleLowerCase()) {
			this.m_service.url = "http://127.0.0.1:";
			this.m_service.port = this.m_service.httpPort;
		} else {
			this.m_service.url = "https://127.0.0.1:";
			this.m_service.port = this.m_service.httpsPort;
		}
		
		this.log("serviceExecute - ExeViewer 호출하기 위하여 통신 준비중.");
		var serverCookieUrl = "";
		if (-1 != this.m_requestUri.indexOf("?")) {
			serverCookieUrl = this.m_requestUri + "&" ;
		} else {
			serverCookieUrl = this.m_requestUri + "?" ;
		}
		serverCookieUrl += "ClipType=currentCookie";
		
		var httpClient = new HttpClient();
		var result = httpClient.send(serverCookieUrl, "", false, null);
		
		// 막 생성된 세션일경우 쿠키값에 JSESSIOND=세션값이 내려오지 않으므로 다시 요청
		if (result != null || result.length == 0) {
			result = httpClient.send(serverCookieUrl, "", false, null);
		}
		this.log("serviceExecute - 현재 쿠기 정보");
		this.log(result);
		
		var option = null;
		
		// 브라우저가 위치한 모니터에서 뷰어를 뛰울수 있도록 포지션 위치 
		var screenPos = (window.screenX == 0) ? 0 : screen.width / window.screenX;
		
		if (this.m_isUseKeyExec) {
			option = {
					cookie : encodeURIComponent(result),
					requestUri : encodeURIComponent(this.m_requestUri),
					ClipID : "E01",
					uid : data.resValue.uid,
					screenPos : screenPos
			};
		} else {
			option = {
					cookie : encodeURIComponent(result),
					requestUri : encodeURIComponent(this.m_requestUri),
					ClipID : "E01",
					oof : encodeURIComponent(Base64.encode(this.m_oofData)),
					screenPos : screenPos
			};
		}
		
		if (this.m_execMethod === EXEC_METHOD_CONSTANTS.DIRECT_PRINT) {
			option.print = {
				showDialog : this.m_print.isShowDialog,
				printerName : this.m_print.printerName,
				totalPageNumber : this.m_print.totalPageNumber,
				currentPageNumber : this.m_print.currentPageNumber,
				option : this.m_print.option
			};
		} else if (this.m_execMethod === EXEC_METHOD_CONSTANTS.DIRECT_EXPORT) {
			option.exportFile = this.m_exportOption;
		}
		
		var dataValue = "option=" + JSON.stringify(option);
		
		var reportExeViewer = this;
		reportExeViewer.m_service.disconnectCount = 0;
		reportExeViewer.m_service.intervalKey = window.setInterval(function(){
			if(3 == reportExeViewer.m_service.disconnectCount){
				window.clearInterval(reportExeViewer.m_service.intervalKey);
				reportExeViewer.notFoundService();
				//ReportExeViewerEventHandler({ message : message, event : 103});
			}
		},1500);
	        if (window.XDomainRequest) {
        		var xdr0 = new XDomainRequest();
	            if (xdr0) {
	            	try{
	            		xdr0.onload = function () {
	            			reportExeViewer.startExecResponse(xdr0.responseText);
	 	                };
	 	                xdr0.onerror = function () {
	 	                	reportExeViewer.m_service.disconnectCount++;
	 	                };
	 	                xdr0.open('GET', reportExeViewer.m_service.url + reportExeViewer.m_service.port[0] + "?" + dataValue);
	 	                xdr0.send();
	            	} catch (xdr0_e) {
	            		ReportExeViewerLog(xdr0_e.message);
	            	}
	            }
	            var xdr1 = new XDomainRequest();
	            if (xdr1) {
	            	try {
	            		xdr1.onload = function () {
	            			reportExeViewer.startExecResponse(xdr1.responseText);
	            		};
	            		xdr1.onerror = function () {
	            			reportExeViewer.m_service.disconnectCount++;
	            		};
	            		xdr1.open('GET', reportExeViewer.m_service.url + reportExeViewer.m_service.port[1] + "?" + dataValue);
	            		xdr1.send();
	            	} catch (xdr1_e) {
	            		ReportExeViewerLog(xdr1_e.message);
	            	}
	            }
	            var xdr2 = new XDomainRequest();
	            if (xdr2) {
	            	try{
	            		xdr2.onload = function () {
	            			reportExeViewer.startExecResponse(xdr2.responseText);
	            		};
	            		xdr2.onerror = function () {
	            			reportExeViewer.m_service.disconnectCount++;
	            		};
	            		xdr2.open('GET', reportExeViewer.m_service.url + reportExeViewer.m_service.port[2] + "?" + dataValue);
	            		xdr2.send();
	            	}catch (xdr2_e) {
	            		ReportExeViewerLog(xdr2_e.message);
	            	}
	            }
	        } else {
				for (var index = 0; index < reportExeViewer.m_service.port.length; index++) {
					var portNumber = reportExeViewer.m_service.port[index];
					var serviceUrl = reportExeViewer.m_service.url + portNumber;
					$.ajax({
				        type: "POST",
				        url: serviceUrl,
				        crossDomain: true,
				        data: dataValue,
				        success: function (data) {
				        	reportExeViewer.startExecResponse(data);
				        },
				        error: function (err) {
				        	reportExeViewer.m_service.disconnectCount++;
				        }
				    });
				}
			}
		} catch (e) {
			ReportExeViewerLog(e.message);
		}
};

ReportExeViewer.prototype.requestServiceExecute = function (data) {
	if (this.m_isUseKeyExec) { // 키를 받아서 서비스에 뷰어 실행 요청
		var obj = JSON.parse(data);
		if (this.newReportResponseValidation(obj)) {
			this.serviceExecute(obj);
		} 
	} else { // OOF를 서비스로 넘겨서 뷰어 실행 요청 리포트 생성 요청부터 뷰어에서 함
		this.serviceExecute();
	}
};

ReportExeViewer.prototype.newReport = function () {
	this.log('newReport - 리포트 생성을 시작합니다.');
	
	if (this.m_oofData != null) {
		this.log(this.m_oofData);
		
		var oofData = Base64.encode(this.m_oofData);
		var clipData = { oof : encodeURIComponent(oofData) };
		
		var sendData = "ClipType=newReport&ClipData=" + JSON.stringify(clipData); 
		
		if (this.m_isUseKeyExec) {
			var reportExeViewer = this;
			try {
				if (this.m_isCrossDomain) {
					if (window.XDomainRequest) {
						var xdr = new XDomainRequest();
		                var url = this.m_requestUri + "?" + sendData;
		                if (xdr) {
		                    xdr.onload = function () {
		                    	var result = xdr.responseText;
		                    	reportExeViewer.requestServiceExecute(result);
		                    };
		                    
		                    xdr.onerror = function () {
		                    	var message = 'Internet Explorer 8 크로스도메인 오류 발생';
		                    	log("newReport - " + message);
		                    	
		                    	var obj = { message : message ,event : 40 };
		                    	ReportExeViewerEventHandler(obj);
		                    };
		                    
		                    xdr.open('GET', url);
		                    xdr.send();
		                }
					} else {
						$.ajax({
					        type: "POST",
					        url: this.m_requestUri,
					        crossDomain: true,
					        data: sendData,
					        success: function (data) {
					        	reportExeViewer.requestServiceExecute(data);
					        },
					        error: function (err) {
					        	var message = '서버 통신 오류 발생';
					        	
					        	reportExeViewer.log("newReport - " + message);
					        	reportExeViewer.log(err);
					        	
					        	var obj = { message : message ,event : 40 };
		                    	ReportExeViewerEventHandler(obj);
					        }
					    });
					}
				} else {
					var objHttpClient = new HttpClient();
					var result = objHttpClient.send(this.m_requestUri, sendData, false, null);
					reportExeViewer.requestServiceExecute(result);
				}
			} catch (e) {
				var message = e.message;
				reportExeViewer.log("newReport - " + message);
				
				var obj = { message : message ,event : 40 };
            	ReportExeViewerEventHandler(obj);
			}
		} 
	} else {
		this.log('newReport - OOF 가 정의되지 않았습니다.');
	}
};

/**
 * @description 뷰어 실행. <br>
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.view = function () {
	if (this.m_isUseKeyExec) {
		this.newReport();
	} else {
		this.serviceExecute();
	}
};

/**
 * @description 다이렉트 저장 실행. <br>
 * @param option { ExportOption }
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.directExport = function(option) {
	this.m_execMethod = EXEC_METHOD_CONSTANTS.DIRECT_EXPORT;
	
	this.m_exportOption = option;
	if (this.m_isUseKeyExec) {
		this.newReport();
	} else {
		this.requestServiceExecute();
	}
};

/**
 * @description 다이렉트 프린터 실행. <br>
 * @param isShowDialog {Boolean}
 * @param printerName {String}
 * @param totalPageNumber {Integer}
 * @param currentPageNumber {Integer}
 * @method ReportExeViewer
 */
ReportExeViewer.prototype.directPrint = function (isShowDialog, printerName, totalPageNumber, currentPageNumber) {
	this.m_execMethod = EXEC_METHOD_CONSTANTS.DIRECT_PRINT;
	
	this.m_print.isShowDialog = isShowDialog;
	this.m_print.printerName = printerName;
	this.m_print.totalPageNumber = totalPageNumber;
	this.m_print.currentPageNumber = currentPageNumber;
	
	if (this.m_isUseKeyExec) {
		this.newReport();
	} else {
		this.requestServiceExecute();
	}
};
