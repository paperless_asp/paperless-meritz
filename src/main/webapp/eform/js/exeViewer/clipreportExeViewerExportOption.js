/**
 * @class ExportOption 
 * @description Export 옵션 기본객체입니다. <br>
 * <font style='color:orange'>총 페이지와 현재페이지가 유효해야합니다.</font> <br>
 * 저장범위, 저장경로, 파일이름, 알림창, 저장완료후 실행여부 등을 설정합니다.
 * 
 * @param totalPageNumber 총 페이지
 * @param currentPageNumber 현재 페이지
 */
function ExportOption(totalPageNumber, currentPageNumber) {
	this.totalPageNumber = totalPageNumber;
	this.currentPageNumber = currentPageNumber;

	this.exportRange = "AllPages";
	this.directory = "";
	this.fileName = "Report";

	this.showDialog = false;
	this.notifyIcon = true;
	this.fileOpen = false;
};

/**
 * @description 저장범위를 설정. <br>  
 * 총 페이지가 6페이지 일떄 <br>
 * AllPages : 전체페이지, SomePages;1-6 : 1페이지부터 6페이지까지, CurrentPage : 현재페이지 <br>
 * {기본값 AllPages}
 * 
 * @param exportRange {String} <br>
 * @method ExportOption
 */
ExportOption.prototype.setExportRange = function(exportRange) {
	this.exportRange = exportRange;
};

/**
 * @description 파일을 저장할 디렉토리 경로 설정. <br>
 * {기본값 유저 다운로드폴더 C:\Users\유저네임\Downloads} <br>
 * <font style='color:orange'>해당 경로에 디렉토리가 없을경우 디렉토리 생성필요</font>
 * 
 * @param directory {String}
 * @method ExportOption
 */
ExportOption.prototype.setDirectory = function(directory) {
	this.directory = directory;
};

/**
 * @description 저장할 파일 이름. <br>
 * {기본값 - Report}
 * 
 * @param fileName {String}
 * @method ExportOption 
 */
ExportOption.prototype.setFileName = function(fileName) {
	this.fileName = fileName;
};

/**
 * @description 저장 팝업창 여부. <br>
 * {기본값 - false}
 * 
 * @param showDialog {Boolean}
 * @method ExportOption
 */
ExportOption.prototype.setShowDialog = function(showDialog) {
	this.showDialog = showDialog;
};

/**
 * @description 저장이 완료되었을때 작업표시줄에 알림아이콘 보여줄지 여부. <br>
 * {기본값 true} 
 * 
 * @param notifyIcon {Boolean}
 * @method ExportOption
 */
ExportOption.prototype.setNotifyIcon = function(notifyIcon) {
	this.notifyIcon = notifyIcon;
};

/**
 * @description 저장이 완료 되었을때 파일열기 여부. <br>
 * {기본값 false}
 * 
 * @param fileOpen {Boolean}
 * @method ExportOption
 */
ExportOption.prototype.setFileOpen = function(fileOpen) {
	this.fileOpen = fileOpen;
};

/**
 * @class ExcelOption
 * @description 엑셀(.xls) 저장 옵션 객체입니다. 엑셀(.xls) 저장옵션에 대한 설정을 합니다.<br>
 * (저장범위, 저장경로, 파일이름, 알림창, 저장완료후 실행여부 등을 설정 등은 ExportOption 객체를 보시기 바랍니다.)
 * 
 * @param totalPageNumber 총 페이지
 * @param currentPageNumber 현재 페이지
 */
function ExcelOption(totalPageNumber, currentPageNumber) {
	ExportOption.call(this, totalPageNumber, currentPageNumber);

	this.option = {
		exportType : "1",

		exportMethod : 1,
		mergeCell : true,
		mergeEmptyCell : false,
		splitCellAtPageSize : true,
		rightToLeft : false,
		fitToPageWhenPrinting : false,
		removeHyperlink : false,

		widthRate : 100,
		heightRate : 100,

		coordinateErrorLimit : 10,
		processGeneralFormat : 1,
		printingMagnification : 100,

		insertBackgroundImage : false
	};
};

ExcelOption.prototype = Object.create(ExportOption.prototype);
ExcelOption.prototype.constructor = ExportOption;

/**
 * @description 엑셀(.xls) 저장 방법을 지정합니다. <br>
 * 페이지 마다 - 1 <br>
 * 하나의 시트 - 2 <br>
 * 하나의 시트 페이지 영역 무시 - 3 <br> 
 * 리포트 마다 - 4 <br>
 * 리포트 마다 페이지 영역 무시 - 5 <br>
 * {기본값 페이지마다}
 * 
 * @param exportMethod {Integer}
 * @method ExcelOption
 */
ExcelOption.prototype.setExportMethod = function(exportMethod) {
	if (exportMethod >= 1 && exportMethod <= 5) {
		this.option.exportMethod = exportMethod;	
	}
};

/**
 * @description 셀 합치기. <br> 
 * {기본값 true}
 * 
 * @param mergeCell {Boolean}
 * @method ExcelOption
 */
ExcelOption.prototype.setMergeCell = function(mergeCell) {
	this.option.mergeCell = mergeCell;
};

/**
 * @description 빈 셀 합치기. <br>
 * {기본값 false}
 * 
 * @param mergeEmptyCell {Boolean}
 * @method ExcelOption
 */
ExcelOption.prototype.setMergeEmptyCell = function(mergeEmptyCell) {
	this.option.mergeEmptyCell = mergeEmptyCell;
};

/**
 * @description 페이지 영역으로 셀을 나누기. <br>
 * {기본값 true}
 * 
 * @param splitCellAtPageSize {Boolean}
 * @method ExcelOption
 */
ExcelOption.prototype.setSplitCellAtPageSize = function(splitCellAtPageSize) {
	this.option.splitCellAtPageSize = splitCellAtPageSize;
};

/**
 * @description 열이 오른쪽에서 왼쪽으로. <br>
 * {기본값 false}
 * 
 * @param rightToLeft {Boolean}
 * @method ExcelOption
 */
ExcelOption.prototype.setRightToLeft = function(rightToLeft) {
	this.option.rightToLeft = rightToLeft;
};

/**
 * @description 출력시 페이지에 맞춤. <br>
 * {기본값 false}
 * 
 * @param fitToPageWhenPrinting {Boolean}
 * @method ExcelOption
 */
ExcelOption.prototype.setFitToPageWhenPrinting = function(fitToPageWhenPrinting) {
	this.option.fitToPageWhenPrinting = fitToPageWhenPrinting;
};

/**
 * @description 하이퍼 링크 제거. <br>
 * {기본값 false}
 * 
 * @param removeHyperlink {Boolean}
 * @method ExcelOption
 */
ExcelOption.prototype.setRemoveHyperlink = function(removeHyperlink) {
	this.option.removeHyperlink = removeHyperlink;
};

/**
 * @description 셀 크기 가로 비율. <br>
 * {기본값 100}
 * 
 * @param widthRate {Integer}
 * @method ExcelOption
 */
ExcelOption.prototype.setWidthRate = function(widthRate) {
	this.option.widthRate = widthRate;
};

/**
 * @description 셀 크기 세로 비율. <br>
 * {기본값 100}
 * 
 * @param heightRate {Integer}
 * @method ExcelOption
 */
ExcelOption.prototype.setHeightRate = function(heightRate) {
	this.option.heightRate = heightRate;
};

/**
 * @description 셀을 나눌때 사용하는 오차 범위. <br>
 * {기본값 10}
 * 
 * @param coordinateErrorLimit {Integer}
 * @method ExcelOption
 */
ExcelOption.prototype.setCoordinateErrorLimit = function(coordinateErrorLimit) {
	this.option.coordinateErrorLimit = coordinateErrorLimit;
};

/**
 * @description 일반 포멧팅을 엑셀(.xls)에서 처리하는 방법. <br> 
 * 텍스트 - 1 <br>
 * 일반 - 2 <br>
 * {기본값 텍스트}
 * 
 * @param processGeneralFormat {Integer}
 * @method ExcelOption
 */
ExcelOption.prototype.setProcessGeneralFormat = function(processGeneralFormat) {
	if (processGeneralFormat >= 1 && processGeneralFormat <= 2) {
		this.option.processGeneralFormat = processGeneralFormat;	
	}
};

/**
 * @description 프린팅 확대/축소 비율. <br>
 * {기본값 100}
 * 
 * @param printingMagnification {Integer}
 * @method ExcelOption
 */
ExcelOption.prototype.setPrintingMagnification = function(printingMagnification) {
	this.option.printingMagnification = printingMagnification;
};

/**
 * @class ExcelXOption
 * @description 엑셀(.xlsx) 저장 옵션 객체입니다. 엑셀(.xlsx) 저장옵션에 대한 설정을 합니다.<br>
 * (저장범위, 저장경로, 파일이름, 알림창, 저장완료후 실행여부 등을 설정 등은 ExportOption 객체를 보시기 바랍니다.)
 *
 * @param totalPageNumber 총 페이지
 * @param currentPageNumber 현재 페이지
 */
function ExcelXOption(totalPageNumber, currentPageNumber) {
	ExportOption.call(this, totalPageNumber, currentPageNumber);

	this.option = {
		exportType : "2",

		exportMethod : 1,
		mergeCell : true,
		mergeEmptyCell : false,
		splitCellAtPageSize : true,
		rightToLeft : false,
		fitToPageWhenPrinting : false,
		removeHyperlink : false,

		widthRate : 100,
		heightRate : 100,

		coordinateErrorLimit : 10,
		processGeneralFormat : 1,
		printingMagnification : 100,

		insertBackgroundImage : false
	};
};

ExcelXOption.prototype = Object.create(ExportOption.prototype);
ExcelXOption.prototype.constructor = ExportOption;

/**
 * @description 엑셀(.xlsx) 저장 방법을 지정합니다. <br>
 * 페이지 마다 - 1 <br>
 * 하나의 시트 - 2 <br>
 * 하나의 시트 페이지 영역 무시 - 3 <br> 
 * 리포트 마다 - 4 <br>
 * 리포트 마다 페이지 영역 무시 - 5 <br>
 * {기본값 페이지마다}
 * 
 * @param exportMethod {Integer}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setExportMethod = function(exportMethod) {
	if (exportMethod >= 1 && exportMethod <= 5) {
		this.option.exportMethod = exportMethod;	
	}
};

/**
 * @description 셀 합치기. <br> 
 * {기본값 true}
 * 
 * @param mergeCell {Boolean}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setMergeCell = function(mergeCell) {
	this.option.mergeCell = mergeCell;
};

/**
 * @description 빈 셀 합치기. <br>
 * {기본값 false}
 * 
 * @param mergeEmptyCell {Boolean}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setMergeEmptyCell = function(mergeEmptyCell) {
	this.option.mergeEmptyCell = mergeEmptyCell;
};

/**
 * @description 페이지 영역으로 셀을 나누기. <br>
 * {기본값 true}
 * 
 * @param splitCellAtPageSize {Boolean}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setSplitCellAtPageSize = function(splitCellAtPageSize) {
	this.option.splitCellAtPageSize = splitCellAtPageSize;
};

/**
 * @description 열이 오른쪽에서 왼쪽으로. <br>
 * {기본값 false}
 * 
 * @param rightToLeft {Boolean}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setRightToLeft = function(rightToLeft) {
	this.option.rightToLeft = rightToLeft;
};

/**
 * @description 출력시 페이지에 맞춤. <br>
 * {기본값 false}
 * 
 * @param fitToPageWhenPrinting {Boolean}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setFitToPageWhenPrinting = function(fitToPageWhenPrinting) {
	this.option.fitToPageWhenPrinting = fitToPageWhenPrinting;
};

/**
 * @description 하이퍼 링크 제거. <br>
 * {기본값 false}
 * 
 * @param removeHyperlink {Boolean}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setRemoveHyperlink = function(removeHyperlink) {
	this.option.removeHyperlink = removeHyperlink;
};

/**
 * @description 셀 크기 가로 비율. <br>
 * {기본값 100}
 * 
 * @param widthRate {Integer}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setWidthRate = function(widthRate) {
	this.option.widthRate = widthRate;
};

/**
 * @description 셀 크기 세로 비율. <br>
 * {기본값 100}
 * 
 * @param heightRate {Integer}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setHeightRate = function(heightRate) {
	this.option.heightRate = heightRate;
};

/**
 * @description 셀을 나눌때 사용하는 오차 범위. <br>
 * {기본값 10}
 * 
 * @param coordinateErrorLimit {Integer}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setCoordinateErrorLimit = function(coordinateErrorLimit) {
	this.option.coordinateErrorLimit = coordinateErrorLimit;
};

/**
 * @description 일반 포멧팅을 엑셀(.xlsx)에서 처리하는 방법. <br> 
 * 텍스트 - 1 <br>
 * 일반 - 2 <br>
 * {기본값 텍스트}
 * 
 * @param processGeneralFormat {Integer}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setProcessGeneralFormat = function(processGeneralFormat) {
	if (processGeneralFormat >= 1 && processGeneralFormat <= 2) {
		this.option.processGeneralFormat = processGeneralFormat;	
	}
};

/**
 * @description 프린팅 확대/축소 비율. <br>
 * {기본값 100}
 * 
 * @param printingMagnification {Integer}
 * @method ExcelXOption
 */
ExcelXOption.prototype.setPrintingMagnification = function(printingMagnification) {
	this.option.printingMagnification = printingMagnification;
};

/**
 * @class PdfOption
 * 
 * @description PDF(.pdf) 저장 옵션 객체입니다. PDF 저장옵션에 대한 설정을 합니다.<br>
 * (저장범위, 저장경로, 파일이름, 알림창, 저장완료후 실행여부 등을 설정 등은 ExportOption 객체를 보시기 바랍니다.)
 * 
 * @param totalPageNumber 총 페이지
 * @param currentPageNumber 현재 페이지
 */
function PdfOption(totalPageNumber, currentPageNumber) {
	ExportOption.call(this, totalPageNumber, currentPageNumber);

	this.option = {
		exportType : "3",

		userPassword : "",
		textToImage : false,
		importOriginImage : false,
	};
};

PdfOption.prototype = Object.create(ExportOption.prototype);
PdfOption.prototype.constructor = ExportOption;

/**
 * @description PDF 문서에 암호를 설정합니다. 파일 실행시 설정된 암호를 알아여 문서열람이 가능합니다.
 * {기본값 false}
 * 
 * @param userPassword {String}
 * @method PdfOption
 */
PdfOption.prototype.setUserPassword = function(userPassword) {
	this.option.userPassword = userPassword;
};
/**
 * @description 텍스트를 이미지로 표현합니다. <br>
 * { 기본값 false }
 * 
 * @param textToImage {Boolean}
 * @method PdfOption
 */
PdfOption.prototype.setTextToImage = function(textToImage) {
	this.option.textToImage = textToImage;
};

/**
 * @description 원본 이미지를 삽입합니다. <br>
 * { 기본값 false }
 * 
 * @param importOriginImage {Boolean}
 * @method PdfOption
 */
PdfOption.prototype.setImportOriginImage = function(importOriginImage) {
	this.option.importOriginImage = importOriginImage;
};

/**
 * @class HwpOption
 * 
 * @description Hwp(.hwp) 옵션 객체입니다. Hwp 저장옵션에 대한 설정을 합니다.<br> 
 * (저장범위, 저장경로, 파일이름, 알림창, 저장완료후 실행여부 등을 설정 등은 ExportOption 객체를 보시기 바랍니다.)
 * 
 * @param totalPageNumber
 * @param currentPageNumber
 */
function HwpOption(totalPageNumber, currentPageNumber) {
	ExportOption.call(this, totalPageNumber, currentPageNumber);
	
	this.option = {
		exportType : "4",

		exportMethod : 1,
		splitPageOnListTableOnly : true,
		fixSize : true,
		positionRelTo : 1,
		allowOverlay : true,
		mergeTable : false,
		setPageBottomMarginToZero : true,
		pageBottomMarginApplicationRate : 100,
		outputLikeWord : false,
		tableSplitMethod : 2,
		defaultCharGap : -8,
		charRatio : 100,
		lineSpaceRate : 100,
		putCheckboxIntoCell : true,
		splitTextByLine : true
	};
};

HwpOption.prototype = Object.create(ExportOption.prototype);
HwpOption.prototype.constructor = ExportOption;

/**
 *@description 한글 Hwp(.hwp) 저장 방식. <br> 
 * 일반 - 1 <br>
 * 표만 나열하기 - 2 <br>
 * {기본값 일반}
 * 
 * @param exportMethod {Integer}
 * @method HwpOption
 */
HwpOption.prototype.setExportMethod = function(exportMethod) {
	if (exportMethod >= 1 && exportMethod <= 2) {
		this.option.exportMethod = exportMethod;
	}
};

/**
 * @description 표만 나열하기 일때 페이지 나눔 여부. <br>
 * {기본값 true}
 * 
 * @param splitPageOnListTableOnly {Boolean}
 * @method HwpOption
 */
HwpOption.prototype.setSplitPageOnListTableOnly = function(splitPageOnListTableOnly) {
	this.option.splitPageOnListTableOnly = splitPageOnListTableOnly;
};

/**
 * @description 컨트롤 크기 고정. <br>
 * {기본값 true}
 * 
 * @param fixSize {Boolean}
 * @method HwpOption
 */
HwpOption.prototype.setFixSize = function(fixSize) {
	this.option.fixSize = fixSize;
};

/**
 * @description 좌표 기준. <br>
 * 종이 - 1 <br>
 * 문단 - 2 <br>
 * {기본값 종이}
 * 
 * @param positionRelTo {Integer}
 * @method HwpOption
 */
HwpOption.prototype.setPositionRelTo = function(positionRelTo) {
	if (positionRelTo >= 1 && positionRelTo <= 2) {
		this.option.positionRelTo = positionRelTo;
	}
};

/**
 * @description 겸침 허용. <br>
 * {기본값 true}
 * 
 * @param allowOverlay {Boolean}
 * @method HwpOption
 */
HwpOption.prototype.setAllowOverlay = function(allowOverlay) {
	this.option.allowOverlay = allowOverlay;
};

/**
 * @description 이웃한 표 병합. <br>
 * {기본값 false}
 * 
 * @param mergeTable {Boolean}
 * @method HwpOption
 */
HwpOption.prototype.setMergeTable = function(mergeTable) {
	this.option.mergeTable = mergeTable;
};

/**
 * @description 페이지 바닥 여백 0으로 설정. <br>
 * {기본값 true}
 * 
 * @param setPageBottomMarginToZero {Boolean}
 * @method HwpOption
 */
HwpOption.prototype.setSetPageBottomMarginToZero = function(setPageBottomMarginToZero) {
	this.option.setPageBottomMarginToZero = setPageBottomMarginToZero;
};

/**
 * @description 페이지 바닥 여백 적용 비율. <br>
 * {기본값 100}
 * 
 * @param pageBottomMarginApplicationRate {Integer}
 * @method HwpOption
 */
HwpOption.prototype.setPageBottomMarginApplicationRate = function(pageBottomMarginApplicationRate) {
	this.option.pageBottomMarginApplicationRate = pageBottomMarginApplicationRate;
};

/**
 * @description 글자 처럼 출력. <br>
 * {기본값 false}
 * 
 * @param outputLikeWord {Boolean}
 * @method HwpOption
 */
HwpOption.prototype.setOutputLikeWord = function(outputLikeWord) {
	this.option.outputLikeWord = outputLikeWord;
};

/**
 * @description 표 페이지 영역에서 분할 방법. <br>
 * 나눔 - 1 <br>
 * 셀 단위로 나눔 - 2 <br>
 * 나누지 않음 - 3 <br>
 * {기본값 셀 단위로 나눔}
 * 
 * @param tableSplitMethod {Integer}
 * @method HwpOption
 */
HwpOption.prototype.setTableSplitMethod = function(tableSplitMethod) {
	this.option.tableSplitMethod = tableSplitMethod;
};

/**
 * @description 기본 자간. <br>
 * {기본값 -8}
 * 
 * @param defaultCharGap {Integer}
 * @method HwpOption
 */
HwpOption.prototype.setDefaultCharGap = function(defaultCharGap) {
	this.option.defaultCharGap = defaultCharGap;
};

/**
 * @description 자평. <br>
 * {기본값 100}
 * 
 * @param charRatio {Integer}
 * @method HwpOption
 */
HwpOption.prototype.setCharRatio = function(charRatio) {
	this.option.charRatio = charRatio;
};

/**
 * @description 행간 비율. <br>
 * {기본값 100}
 * 
 * @param lineSpaceRate {Integer}
 * @method HwpOption
 */
HwpOption.prototype.setLineSpaceRate = function(lineSpaceRate) {
	this.option.lineSpaceRate = lineSpaceRate;
};

/**
 * @description 셀 안에 체크박스 이미지로 넣기. <br>
 * {기본값 true}
 * 
 * @param putCheckboxIntoCell {Boolean}
 * @method HwpOption
 */
HwpOption.prototype.setPutCheckboxIntoCell = function(putCheckboxIntoCell) {
	this.option.putCheckboxIntoCell = putCheckboxIntoCell;
};

/**
 * @description 문자열을 여러줄로 나눠서 표현. <br>
 * {기본값 true}
 * 
 * @param splitTextByLine {Boolean}
 * @method HwpOption
 */
HwpOption.prototype.setSplitTextByLine = function(splitTextByLine) {
	this.option.splitTextByLine = splitTextByLine;
};

/**
 * @class DocOption
 * 
 * @description Doc(.doc) 옵션 객체입니다. doc 저장옵션에 대한 설정을 합니다.<br> 
 * (저장범위, 저장경로, 파일이름, 알림창, 저장완료후 실행여부 등을 설정 등은 ExportOption 객체를 보시기 바랍니다.)
 * 
 * @param totalPageNumber
 * @param currentPageNumber
 */
function DocOption(totalPageNumber, currentPageNumber) {
	ExportOption.call(this, totalPageNumber, currentPageNumber);

	this.option = {
		exportType : "5",

		splitTextLine : true,
		defaultCharSpace : -0.5,
		mergeTable : false,
		fitShapeToText : false,
		insertTableWrapper : true,
		tableWrapperBottomGap : 0,
		tableRowHeightSort : 1,
		
		processEqualAlign : 4,
		processAsUnicode : true
	};
};

DocOption.prototype = Object.create(ExportOption.prototype);
DocOption.prototype.constructor = ExportOption;

/**
 * @description 문자열을 여러줄로 나누어 표현. <br>
 * {기본값 true}
 * 
 * @param splitTextLine {Boolean}
 * @method DocOption
 */
DocOption.prototype.setSplitTextLine = function(splitTextLine) {
	this.option.splitTextLine = splitTextLine;
};

/**
 * @description 기본 자간. <br>
 * {기본값 -0.5}
 * 
 * @param defaultCharSpace {Float}
 * @method DocOption
 */
DocOption.prototype.setDefaultCharSpace = function(defaultCharSpace) {
	this.option.defaultCharSpace = defaultCharSpace;
};

/**
 * @description 표 병합. <br>
 * {기본값 false}
 * 
 * @param mergeTable {Boolean}
 * @method DocOption
 */
DocOption.prototype.setMergeTable = function(mergeTable) {
	this.option.mergeTable = mergeTable;
};

/**
 * @description 도형을 텍스트 크기에 맞춤. <br>
 * {기본값 false}
 * 
 * @param fitShapeToText {Boolean}
 * @method DocOption
 */
DocOption.prototype.setFitShapeToText = function(fitShapeToText) {
	this.option.fitShapeToText = fitShapeToText;
};

/**
 * @description 표를 감싸는 객체 삽입. <br>
 * {기본값 true}
 * 
 * @param insertTableWrapper {Boolean}
 * @method DocOption
 */
DocOption.prototype.setInsertTableWrapper = function(insertTableWrapper) {
	this.option.insertTableWrapper = insertTableWrapper;
};

/**
 * @description 표를 감싸는 객체의 아래쪽 여백. <br>
 * {기본값 0}
 * 
 * @param tableWrapperBottomGap {Integer}
 * @method DocOption
 */
DocOption.prototype.setTableWrapperBottomGap = function(tableWrapperBottomGap) {
	this.option.tableWrapperBottomGap = tableWrapperBottomGap;
};

/**
 * @description 표 행 높이. <br>
 * Fix - 1 <br>
 *	Minimum - 2 <br>
 * {기본값 Fix}
 * 
 * @param tableRowHeightSort
 * @method DocOption
 */
DocOption.prototype.setTableRowHeightSort = function(tableRowHeightSort) {
	this.option.tableRowHeightSort = tableRowHeightSort;
};

/**
 * @class DocXOption
 * 
 * @description Docx(.docx) 옵션 객체입니다. docx 저장옵션에 대한 설정을 합니다.<br> 
 * (저장범위, 저장경로, 파일이름, 알림창, 저장완료후 실행여부 등을 설정 등은 ExportOption 객체를 보시기 바랍니다.)
 * 
 * @param totalPageNumber
 * @param currentPageNumber
 */
function DocXOption(totalPageNumber, currentPageNumber) {
	ExportOption.call(this, totalPageNumber, currentPageNumber);

	this.option = {
		exportType : "6",

		splitTextLine : true,
		defaultCharSpace : -0.5,
		mergeTable : false,
		fitShapeToText : false,
		insertTableWrapper : true,
		tableWrapperBottomGap : 0,
		tableRowHeightSort : 1,
		
		processEqualAlign : 4,
		processAsUnicode : true,
	};
};

DocXOption.prototype = Object.create(ExportOption.prototype);
DocXOption.prototype.constructor = ExportOption;

/**
 * @description 문자열을 여러줄로 나누어 표현. <br>
 * {기본값 true}
 * 
 * @param splitTextLine {Boolean}
 * @method DocXOption
 */
DocXOption.prototype.setSplitTextLine = function(splitTextLine) {
	this.option.splitTextLine = splitTextLine;
};

/**
 * @description 기본 자간. <br>
 * {기본값 -0.5}
 * 
 * @param defaultCharSpace {Float}
 * @method DocXOption
 */
DocXOption.prototype.setDefaultCharSpace = function(defaultCharSpace) {
	this.option.defaultCharSpace = defaultCharSpace;
};

/**
 * @description 표 병합. <br>
 * {기본값 false}
 * 
 * @param mergeTable {Boolean}
 * @method DocXOption
 */
DocXOption.prototype.setMergeTable = function(mergeTable) {
	this.option.mergeTable = mergeTable;
};

/**
 * @description 도형을 텍스트 크기에 맞춤. <br>
 * {기본값 false}
 * 
 * @param fitShapeToText {Boolean}
 * @method DocXOption
 */
DocXOption.prototype.setFitShapeToText = function(fitShapeToText) {
	this.option.fitShapeToText = fitShapeToText;
};

/**
 * @description 표를 감싸는 객체 삽입. <br>
 * {기본값 true}
 * 
 * @param insertTableWrapper {Boolean}
 * @method DocXOption
 */
DocXOption.prototype.setInsertTableWrapper = function(insertTableWrapper) {
	this.option.insertTableWrapper = insertTableWrapper;
};

/**
 * @description 표를 감싸는 객체의 아래쪽 여백. <br>
 * {기본값 0}
 * 
 * @param tableWrapperBottomGap {Integer}
 * @method DocXOption
 */
DocXOption.prototype.setTableWrapperBottomGap = function(tableWrapperBottomGap) {
	this.option.tableWrapperBottomGap = tableWrapperBottomGap;
};

/**
 * @description 표 행 높이. <br>
 * Fix - 1 <br>
 *	Minimum - 2 <br>
 * {기본값 Fix}
 * 
 * @param tableRowHeightSort
 * @method DocXOption
 */
DocXOption.prototype.setTableRowHeightSort = function(tableRowHeightSort) {
	this.option.tableRowHeightSort = tableRowHeightSort;
};

/**
 * @class PptOption
 * 
 * @description Ppt(.ppt) 옵션 객체입니다. ppt 저장옵션에 대한 설정을 합니다.<br> 
 * (저장범위, 저장경로, 파일이름, 알림창, 저장완료후 실행여부 등을 설정 등은 ExportOption 객체를 보시기 바랍니다.)
 * 
 * @param totalPageNumber
 * @param currentPageNumber
 */
function PptOption(totalPageNumber, currentPageNumber) {
	ExportOption.call(this, totalPageNumber, currentPageNumber);
	
	this.option = {
		exportType : "7",

		mergeTable : false,
		ignoreLineSpace : true
	};
};

PptOption.prototype = Object.create(ExportOption.prototype);
PptOption.prototype.constructor = ExportOption;

/**
 * @description 이웃한 표 병합. <br>
 * {기본값 false}
 * 
 * @param mergeTable {Boolean}
 * @method PptOption
 */
PptOption.prototype.setMergeTable = function(mergeTable) {
	this.option.mergeTable = mergeTable;
};

/**
 * @description 행간 무시. <br>
 * {기본값 true}
 * 
 * @param ignoreLineSpace {Boolean}
 * @method PptOption
 */
PptOption.prototype.setIgnoreLineSpace = function(ignoreLineSpace) {
	this.option.ignoreLineSpace = ignoreLineSpace;
};

/**
 * @class HtmlOption
 * 
 * @description Html5(.html) 옵션 객체입니다. Html5 저장옵션에 대한 설정을 합니다.<br> 
 * (저장범위, 저장경로, 파일이름, 알림창, 저장완료후 실행여부 등을 설정 등은 ExportOption 객체를 보시기 바랍니다.)
 * 
 * @param totalPageNumber
 * @param currentPageNumber
 */
function HtmlOption(totalPageNumber, currentPageNumber) {
	ExportOption.call(this, totalPageNumber, currentPageNumber);

	this.option = {
		exportType : "8",

		pageLine : true,
		dpi : 96
	};
};

HtmlOption.prototype = Object.create(ExportOption.prototype);
HtmlOption.prototype.constructor = ExportOption;

/**
 * @description 페이지 마다 라인을 표시 합니다. <br>
 * { 기본값 false }
 * 
 * @param pageLine {Boolean}
 * @method HtmlOption
 */
HtmlOption.prototype.setPageLine = function(pageLine) {
	this.option.pageLine = pageLine;
};

/** 
 * @description Dpi를 설정합니다. <br>
 * { 기본값 96 }
 * 
 * @param dpi {Integer}
 * @method HtmlOption
 */
HtmlOption.prototype.setDpi = function(dpi) {
	this.option.dpi = dpi;
};

/**
 * @class HanCellOption
 * @description 한셀(.cell) 저장 옵션 객체입니다. 한셀(.cell) 저장옵션에 대한 설정을 합니다.<br>
 * (저장범위, 저장경로, 파일이름, 알림창, 저장완료후 실행여부 등을 설정 등은 ExportOption 객체를 보시기 바랍니다.)
 *
 * @param totalPageNumber 총 페이지
 * @param currentPageNumber 현재 페이지
 */
function HanCellOption(totalPageNumber, currentPageNumber) {
	ExportOption.call(this, totalPageNumber, currentPageNumber);

	this.option = {
		exportType : "1",

		exportMethod : 1,
		mergeCell : true,
		mergeEmptyCell : false,
		splitCellAtPageSize : true,
		rightToLeft : false,
		fitToPageWhenPrinting : false,
		removeHyperlink : false,

		widthRate : 100,
		heightRate : 100,

		coordinateErrorLimit : 10,
		processGeneralFormat : 1,
		printingMagnification : 100,

		insertBackgroundImage : false
	};
};

HanCellOption.prototype = Object.create(ExportOption.prototype);
HanCellOption.prototype.constructor = ExportOption;

/**
 * @description 엑셀(.xls) 저장 방법을 지정합니다. <br>
 * 페이지 마다 - 1 <br>
 * 하나의 시트 - 2 <br>
 * 하나의 시트 페이지 영역 무시 - 3 <br> 
 * 리포트 마다 - 4 <br>
 * 리포트 마다 페이지 영역 무시 - 5 <br>
 * {기본값 페이지마다}
 * 
 * @param exportMethod {Integer}
 * @method HanCellOption
 */
HanCellOption.prototype.setExportMethod = function(exportMethod) {
	if (exportMethod >= 1 && exportMethod <= 5) {
		this.option.exportMethod = exportMethod;	
	}
};

/**
 * @description 셀 합치기. <br> 
 * {기본값 true}
 * 
 * @param mergeCell {Boolean}
 * @method HanCellOption
 */
HanCellOption.prototype.setMergeCell = function(mergeCell) {
	this.option.mergeCell = mergeCell;
};

/**
 * @description 빈 셀 합치기. <br>
 * {기본값 false}
 * 
 * @param mergeEmptyCell {Boolean}
 * @method HanCellOption
 */
HanCellOption.prototype.setMergeEmptyCell = function(mergeEmptyCell) {
	this.option.mergeEmptyCell = mergeEmptyCell;
};

/**
 * @description 페이지 영역으로 셀을 나누기. <br>
 * {기본값 true}
 * 
 * @param splitCellAtPageSize {Boolean}
 * @method HanCellOption
 */
HanCellOption.prototype.setSplitCellAtPageSize = function(splitCellAtPageSize) {
	this.option.splitCellAtPageSize = splitCellAtPageSize;
};

/**
 * @description 열이 오른쪽에서 왼쪽으로. <br>
 * {기본값 false}
 * 
 * @param rightToLeft {Boolean}
 * @method HanCellOption
 */
HanCellOption.prototype.setRightToLeft = function(rightToLeft) {
	this.option.rightToLeft = rightToLeft;
};

/**
 * @description 출력시 페이지에 맞춤. <br>
 * {기본값 false}
 * 
 * @param fitToPageWhenPrinting {Boolean}
 * @method HanCellOption
 */
HanCellOption.prototype.setFitToPageWhenPrinting = function(fitToPageWhenPrinting) {
	this.option.fitToPageWhenPrinting = fitToPageWhenPrinting;
};

/**
 * @description 하이퍼 링크 제거. <br>
 * {기본값 false}
 * 
 * @param removeHyperlink {Boolean}
 * @method HanCellOption
 */
HanCellOption.prototype.setRemoveHyperlink = function(removeHyperlink) {
	this.option.removeHyperlink = removeHyperlink;
};

/**
 * @description 셀 크기 가로 비율. <br>
 * {기본값 100}
 * 
 * @param widthRate {Integer}
 * @method HanCellOption
 */
HanCellOption.prototype.setWidthRate = function(widthRate) {
	this.option.widthRate = widthRate;
};

/**
 * @description 셀 크기 세로 비율. <br>
 * {기본값 100}
 * 
 * @param heightRate {Integer}
 * @method HanCellOption
 */
HanCellOption.prototype.setHeightRate = function(heightRate) {
	this.option.heightRate = heightRate;
};

/**
 * @description 셀을 나눌때 사용하는 오차 범위. <br>
 * {기본값 10}
 * 
 * @param coordinateErrorLimit {Integer}
 * @method HanCellOption
 */
HanCellOption.prototype.setCoordinateErrorLimit = function(coordinateErrorLimit) {
	this.option.coordinateErrorLimit = coordinateErrorLimit;
};

/**
 * @description 일반 포멧팅을 엑셀(.xls)에서 처리하는 방법. <br> 
 * 텍스트 - 1 <br>
 * 일반 - 2 <br>
 * {기본값 텍스트}
 * 
 * @param processGeneralFormat {Integer}
 * @method HanCellOption
 */
HanCellOption.prototype.setProcessGeneralFormat = function(processGeneralFormat) {
	if (processGeneralFormat >= 1 && processGeneralFormat <= 2) {
		this.option.processGeneralFormat = processGeneralFormat;	
	}
};

/**
 * @description 프린팅 확대/축소 비율. <br>
 * {기본값 100}
 * 
 * @param printingMagnification {Integer}
 * @method HanCellOption
 */
HanCellOption.prototype.setPrintingMagnification = function(printingMagnification) {
	this.option.printingMagnification = printingMagnification;
};

/**
 * @class createOption
 * @description Export 옵션 객체를 생성합니다. 해당 리포트의 총페이지와 현재 페이지설정이 유효해야 합니다. 
 * 
 * @example 총페이지가 10페이지이고, 현재페이지가 5페이지이고 PDF 옵션을 바로(옵션 팝업창X) 저장하고 싶을경우 <br>
 * var option = createOption("PDF", false, 10, 5) <br>
 * reportExeViewer.directExport(option);
 *  
 * @param type {String} 저장 타입 { Excel, ExcelX, Pdf, Hwp, Doc, Docx, Html, HanCell }
 * @param totalPageNumber {Integer} 총 페이지
 * @param currentPageNumber {Integer} 현재 페이지
 */
function createOption(type, totalPageNumber, currentPageNumber) {
	var option = null;
	type = type.toUpperCase();
	
	switch (type) {
	case "EXCEL":
	case "XLS":
		option = new ExcelOption(totalPageNumber, currentPageNumber);
		break;
	case "EXCELX":
	case "XLSX":
		option = new ExcelXOption(totalPageNumber, currentPageNumber);
		break;
	case "PDF":
		option = new PdfOption(totalPageNumber, currentPageNumber);
		break;
	case "HWP":
		option = new HwpOption(totalPageNumber, currentPageNumber);
		break;
	case "DOC":
		option = new DocOption(totalPageNumber, currentPageNumber);
		break;
	case "DOCX":
		option = new DocXOption(totalPageNumber, currentPageNumber);
		break;
	case "PPT":
		option = new PptOption(totalPageNumber, currentPageNumber);
		break;
	case "HTML":
		option = new HtmlOption(totalPageNumber, currentPageNumber);
		break;
	case "HANCELL":
		option = new HanCellOption(showDialog, totalPageNumber, currentPageNumber);
		break;
	}
	
	return option;
}