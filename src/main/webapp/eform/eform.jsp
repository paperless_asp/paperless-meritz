﻿<%@page import="com.clipsoft.clipreport.oof.OOFFile"%>
<%@page import="com.clipsoft.clipreport.oof.OOFDocument"%>
<%@page import="com.clipsoft.clipreport.oof.connection.*"%>
<%@page import="java.io.File"%>
<%@page import="com.clipsoft.clipreport.server.service.ReportUtil"%>
<%@page import="java.util.Enumeration"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
 response.setHeader("Cache-Control","no-cache");
 response.setHeader("Pragma","no-cache");
 response.setDateHeader("Expires",0);
%>
<% 
	String progress_id = request.getParameter("progress_id");
	String vfnYn = request.getParameter("vfnYn");
	String vfnInfo = request.getParameter("vfnInfo");
	String param3 = request.getParameter("param3");
	
	String csinfyn01 = request.getParameter("csinfyn01");
	String csinfyn02 = request.getParameter("csinfyn02");
	String csinfyn03 = request.getParameter("csinfyn03");
	String csinfyn04 = request.getParameter("csinfyn04");
	
	String usmktmd02 = request.getParameter("usmktmd02");
	String usmktmd03 = request.getParameter("usmktmd03");
	String usmktmd04 = request.getParameter("usmktmd04");
	String usmktmd05 = request.getParameter("usmktmd05");
	
	String ct_code = request.getParameter("ct_code");
	String eform_id = request.getParameter("eform_id");
	String url = "/ajax/eformUserData?progress_id=" +progress_id;
%>
<%
String requestMethod = request.getMethod();
Enumeration enu = request.getParameterNames();
String sNames = "";

while (enu.hasMoreElements()) {
	sNames = (String)enu.nextElement();
	//System.out.println(sNames);
	if (request.getParameter(sNames).indexOf("script") > -1 ||
		request.getParameter(sNames).indexOf("<") > -1 ||
		request.getParameter(sNames).indexOf(">") > -1 ) {
		
		response.sendRedirect("/error.jsp");
	}
}

String crfeUrl = "";
if("01".equals(eform_id)){
	crfeUrl = "01_usedAutoLoan.crfe";
}else if("02".equals(eform_id)){
	crfeUrl = "02_newCarLease.crfe";
}else if("03".equals(eform_id)){
	crfeUrl = "03_newCarAutoLoan.crfe";
}else if("04".equals(eform_id)){
	crfeUrl = "04_rentaCar.crfe";
}else if("05".equals(eform_id)){
	crfeUrl = "05_usedLease.crfe";
}

OOFDocument oof = OOFDocument.newOOF();
OOFFile file = oof.addFile("crfe.root", "%root%/crf/"+crfeUrl);
// file = oof.addFile("crfe.root", "%root%/crf/00_commonAgreement.crfe");

String str = request.getParameter("userdata");

//System.out.println("##str : "+str);

OOFConnectionMemo connectionMemo = oof.addConnectionMemo("*",str);
connectionMemo.addContentParamJSON("*","utf-8","{%dataset.json.root%}");


// file.addField("name","jspark");
// file.addField("tel","01066376497");
// //System.out.println("getFieldListType : "+file.getFieldListType());
// //System.out.println("getFieldList : "+file.getFieldList());


%><%@include file="Property.jsp"%><%
String resultKey =  ReportUtil.createEForm(request, oof, "false", "false", request.getRemoteAddr(), propertyPath);

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>메리츠캐피탈</title>
<meta name="viewport" content="width=800, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="./css/clipreport.css">
<link rel="stylesheet" type="text/css" href="./css/eform.css?ver=2.0">
<link rel="stylesheet" type="text/css" href="./css/UserConfig.css">
<link rel="stylesheet" type="text/css" href="./css/font.css">
<link rel="stylesheet" type="text/css" href="/static/include/css/sweetalert.css">

<style type="text/css">

span{
line-height:normal !important;
}

/* modal popup */
.blocker {overflow:auto; position:fixed; top:0; right:0; bottom:0; left:0;width:100%; height:100%; padding:0 26px; text-align:center; background-color:rgb(0,0,0); background-color:rgba(0,0,0,0.75); z-index:1;  box-sizing:border-box;}
.blocker:before{display:inline-block; content:""; height:100%; vertical-align:middle;}
.blocker.behind {background-color:transparent;}
.modal {display:inline-block; width:100%; position:relative; border-radius:19px; vertical-align:middle; background:#fff; z-index:9999; box-sizing:border-box;}
.modal .inner {position:relative; padding:17px 13px; text-align:left;}
.modal  .h2_tit {padding-bottom:4px; border-bottom:2px solid #cdd2d7;}
.modal .btn_wrap {padding-top:14px;}
.modal .txt_alert {padding:26px 0 12px; text-align:center;}
.modal a.close-modal {display:block;  position:absolute; top:17px; right:13px; width:15px; height:15px; text-indent:-9999px; background:url('../static/img/common/sp_btn_icon.png') no-repeat -155px 0; background-size:200px 250px;}

.modal.pop_loading a.close-modal {display:none;}
.modal.pop_loading {background-color:#fafafa;}
.pop_loading .inner {padding:26px 0; text-align:center;}
.pop_loading .gif_wrap {width:100%; height:100%;}
.pop_loading .gif_wrap img {width:68px; height:68px;}
.pop_loading .txt {margin-top:16px; font-family:'ex_bold'; font-weight:bold; font-size:18px; color:#323232;}
.pop_loading .desc {margin-top:13px; font-size:13px; color:#6e6e6e; line-height:19px;}
</style>

<script type='text/javascript' src='./js/jquery-1.11.1.js'></script>
<script type='text/javascript' src='./js/clipreport.js?ver=2.0'></script>
<script type='text/javascript' src='./js/UserConfig.js'></script>
<script type="text/javascript" src="/static/include/js/sweetalert-dev.js"></script>
<script type="text/javascript" src="/static/js/lib/jquery.modal.js"></script>
<script type="text/javascript" src="/static/js/jQueryRotate.js"></script>
<script type="text/javascript" src="/static/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="/static/js/page-common.js"></script>

<script type='text/javascript'>
var urlPath = document.location.protocol + "//" + document.location.host;
	
var userHashCode = hashCode(JSON.stringify(<%=str%>));

function chkData() {
	var vfnYn = '<%=vfnYn%>';
	var vfnInfo = '<%=vfnInfo%>';
	var param3 = '<%=param3%>';
	
	if(param3 == 'Y'){
		var csinfyn01 = '<%=csinfyn01%>';
		var csinfyn02 = '<%=csinfyn02%>';
		var csinfyn03 = '<%=csinfyn03%>';
		var csinfyn04 = '<%=csinfyn04%>';
		                             
		var usmktmd02 = '<%=usmktmd02%>';
		var usmktmd03 = '<%=usmktmd03%>';
		var usmktmd04 = '<%=usmktmd04%>';
		var usmktmd05 = '<%=usmktmd05%>';
		
		$.ajax({
			url: "/ajax/eformUserData?progress_id=<%=progress_id%>",
			global: false,
			type: "GET",
			data: {vfnYn : vfnYn, vfnInfo : vfnInfo, param3 : param3,
				csinfyn01 : csinfyn01, csinfyn02 : csinfyn02, csinfyn03 : csinfyn03, csinfyn04 : csinfyn04, 
				usmktmd02 : usmktmd02, usmktmd03 : usmktmd03, usmktmd04 : usmktmd04, usmktmd05 : usmktmd05
				},
				async: false,
			success: function (msg) {
				if (!msg) {
					alert("상담자료가 존재하지 않습니다.");
					window.close();
					return false;				
				} else {
					var getUserHashCode = hashCode(JSON.stringify(msg));
					if (getUserHashCode != userHashCode) {
						alert("상담내용과 자료가 일치하지 않습니다.");
						window.close();
					} else {
						html2xml('targetDiv1');
					}
					return ;
				}
			}
		});
	}else{
		
		$.ajax({
			url: "/ajax/eformUserData?progress_id=<%=progress_id%>",
			global: false,
			type: "GET",
			data: {vfnYn : vfnYn, vfnInfo : vfnInfo},
			async: false,
			success: function (msg) {
				if (!msg) {
					alert("상담자료가 존재하지 않습니다.");
					window.close();
					return false;				
				} else {
					var getUserHashCode = hashCode(JSON.stringify(msg));
					if (getUserHashCode != userHashCode) {
						alert("상담내용과 자료가 일치하지 않습니다.");
						window.close();
					} else {
						html2xml('targetDiv1');
					}
					return ;
				}
			}
		});
	}
}


function html2xml(divPath){
	
    var eformkey = "<%=resultKey%>";
	var eform = createImportJSPEForm(urlPath + "/eform/Clip.jsp", eformkey, document.getElementById(divPath));
	var file_name = "<%=progress_id%>";
	var ct_code = "<%=ct_code%>";
	
	//필수 서명 사항 체크
	var pageCheckFn = function(){
		//페이지 별 체크
		var isCheck = eform.getNecessaryCheckPage(true);
		//페이지가 체크가 다 됐다면 true
		return isCheck;
	}
	
	//버튼 이벤트 추가
	eform.setStartFirstPageButtonEvent(pageCheckFn);
	eform.setStartLastPageButtonEvent(pageCheckFn);
	eform.setStartNextPageButtonEvent(pageCheckFn);
	eform.setStartPreviousPageButtonEvent(pageCheckFn);
	
	//화면에 스와이프 잠금
	eform.setEFormMobileSwipeDisabled(true);

	//eform 속도 빠르게
	eform.setNextPageLoading(true);

	//서버에 PDF 저장하기
	eform.setEndSaveButtonEvent(function (){
		if (confirm("신청서를 저장하시겠습니까?") == true){    //확인
			var eform_id = '<%=eform_id%>';
			var param = "report_key=" + eform.getReportKey();
			param +="&file_name=" + file_name + "&ct_code="+ ct_code + "&eform_id" + eform_id;
			
			// PDF 생성
			objHttpClient = new HttpClient();
			var strResult_pdf = objHttpClient.send(urlPath + '/eform/exportForPartPDF.jsp', param, false, null);	
			// cms파일 jpg 생성
			objHttpClient_jpg = new HttpClient();
			var strResult_jpg = objHttpClient.send(urlPath + '/eform/exportForPartJPG.jsp', param, false, null);
			//PDF파일 및 CMS.JPG 생성 실패
			if(strResult_pdf != "0"){
				alert("신청서 작성에 실패했습니다. 잠시후 다시 시도해 주세요.");		
				return false;
			}
			if(strResult_jpg != "0"){
				alert("신청서 작성에 실패했습니다. 잠시후 다시 시도해 주세요.");		
				return false;
			}		
			//PDF파일 및 CMS.JPG 생성 성공
//	 		alert("신청서 작성이 완료되었습니다.");
			
			var tsaUrl = urlPath + '/tsa?progress_id=' + file_name + "&ct_code="+ ct_code;
		
			showLoading();
			$('#targetDiv1').hide();
			setTimeout(function(){
				$.ajax({
					url: tsaUrl,
					global: false,
					type: "GET",
					success: function (msg) {
						console.log(msg);
						if(msg.response == 'FAIL'){
							alert("파일 업로드에 실패하였습니다. 잠시후 다시 시도해 주세요.");
							return false;
						}
						window.opener.location.href = opener.location.href.substring(0, opener.location.href.lastIndexOf("/")+1) + "acceptCertification?progress_id="+ file_name;
		 				window.close();
					},beforeSend : function(){
						$('#targetDiv1').hide();
						showLoading();
					},complete:function(){
						closeLoading();
					},error : function(request, status, error ) {
						console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
					}
				});
			},10);
			
			// TIF 생성
			/* showLoading();
			$('#targetDiv1').hide();
			setTimeout(function(){
			  $.ajax({
			    url: urlPath + '/eform/exportForPartTIF.jsp',
			    async: false,
			    data : param,
			    type: "GET",
			    beforeSend: function () {
			    	showLoading();
			    },
			    success: function(data, result) {
			    	console.log(data);
			    	if(data == "0"){
						var tsaUrl = urlPath + '/tsa?progress_id=' + file_name + "&ct_code="+ ct_code;
					
						$.ajax({
							url: tsaUrl,
							global: false,
							type: "GET",
							success: function (msg) {
								console.log(msg);
								if(msg.response == 'FAIL'){
									alert("파일 업로드에 실패하였습니다. 잠시후 다시 시도해 주세요.");
									return false;
								}
								window.opener.location.href = opener.location.href.substring(0, opener.location.href.lastIndexOf("/")+1) + "acceptCertification?progress_id="+ file_name;
				 				window.close();
							},beforeSend : function(){
								$('#targetDiv1').hide();
								showLoading();
							},complete:function(){
								closeLoading();
							},error : function(request, status, error ) {
								console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
							}
						});
			    	}else{
			    		alert("신청서 작성에 실패했습니다. 잠시후 다시 시도해 주세요.");		
						return false;
			    	}
			    },complete:function(){
			    	
				}
			  });
			},10);
			console.log(1);   */
		}else{   //취소
		    return;
		}
	});
	eform.view();	
}

function userEFormEvent(eform){
	var inputGroup = eform.findGroup("Inputbox1");
	var checkGroup = eform.findGroup("inputcheck1");
	var radioGroup = eform.findGroup("inputradio1");
	var signGroup = eform.findGroup("clipsign1");
	
	var checkControl = checkGroup.getControlList()[0];
	var inputControl = inputGroup.getControlList()[0];
	
	/* checkControl.onCheckedEvent(function(){
		inputControl.setValue("checked");
	});
	checkControl.onUnCheckedEvent(function(){
		inputControl.setValue("unChecked");
	});		 */
}

function getUserData() {

}

function hashCode (str){
    var hash = 0;
    if (str.length == 0) return hash;
    for (i = 0; i < str.length; i++) {
        hash = ((hash<<5)-hash)+ str.charCodeAt(i);
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}

var isLoading = false;
function showLoading()
{
	if(isLoading == false)
	{
		var angle = 0;
		setInterval(function(){
		      angle+=35;
		     $(".lotate_img").rotate(angle);
		},100);
		isLoading = true;
	}
	
	$('#pop_loading').modal({
		clickClose: false
	});
	
// 	setTimeout(closeLoading, 30000); 
}

function closeLoading()
{
	$(".close-modal").click();
}
</script>
</head>
<body onload="chkData()">
<!-- 로딩 팝업 -->
<div id="pop_loading" class="modal pop_loading" style="display:none;">
	<div class="inner">
		<div class="gif_wrap">
			<img src="/static/img/common/img_loading.gif" class="lotate_img" alt="진행 중">
		</div>
		<p class="txt">처리 중입니다.(최장 3분 소요)</p>
		<p class="desc">* 절대 화면을 닫거나 이동하지 마세요.<br>(자동으로 닫힙니다)</p>
	</div>
</div>
<div id='targetDiv1' style='position:absolute;top:5px;left:5px;right:5px;bottom:5px;'></div>
</body>
</html>
