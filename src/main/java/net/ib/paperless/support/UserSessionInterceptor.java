package net.ib.paperless.support;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import net.ib.paperless.domain.User;

@Component
public class UserSessionInterceptor extends HandlerInterceptorAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger(UserSessionInterceptor.class);
	
	//컨트롤러 이벤트 호출전
	@Override
	public boolean preHandle( HttpServletRequest request, HttpServletResponse response, Object handler ) throws Exception {
		logger.info("### --------------------------------");
		String remoteAddr = request.getHeader("REMOTE_ADDR");
		logger.info("### " + remoteAddr + "/" + request.getRemoteHost() + "/" + request.getRemoteUser() + "/" + request.getRequestedSessionId());
		logger.info("### --------------------------------");		

		// HTTP 요청 처리 전 수행할 로직 작성 
		//session 체크
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		String reqUri = request.getRequestURI().toString();
		String progress_id = request.getParameter("progress_id");
		logger.info("### user : " + user);
		logger.info("### reqUri : " + reqUri);
//		//System.out.println("reqUri : " + reqUri.indexOf("/api/"));
		logger.info("### progress_id : " + progress_id);
//		Map<?, ?> pathVariables = (Map<?, ?>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
//		//if(user == null && !reqUri.equals("/null/main")){
//		if(pathVariables.get("loanId") != null){
//			//System.out.println("loanInfoSelect : " + pathVariables.get("loanId").toString());	
		//		}
		String referrer = request.getHeader("Referer");
		//System.out.println("referrer : " + referrer);

		try {
			//세션이 없으면(null) 세션이 없는데 해당 url로 접근했을 경우 Exception 처리
			if(user == null){
				if(reqUri.indexOf("/acceptCertification") >=0){ //전자청약
					String url = getHttpsUrl(request, "/certification?progress_id="+ progress_id);
					response.sendRedirect(url);
				}else if(reqUri.indexOf("/file_upload_info") >=0) { //파일업로드
					String url = getHttpsUrl(request, "/error");
					response.sendRedirect(url);
				}else if(reqUri.indexOf("/error") >=0 && referrer != null) { //에러페이지 처리
					String url = getHttpsUrl(request, "/error");
					response.sendRedirect(url);
				}else{
					//response.sendRedirect("/certification?progress_id="+ progress_id);	
				}	
				return false;
			}else {//세션이 존재하고 비교하는 분기처리
				//해당 세션 progress_id 와 url progress_id 같은지 비교
				if(progress_id != null && progress_id.equals(user.getLoanId())) {
					//해당 세션 userKey(휴대폰번호)가 동일한지 비교
					//if(){
					//}
					return true;
				}else {
					String url = getHttpsUrl(request, "/error");
					response.sendRedirect(url);
					return false;
				}
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		//admin 세션key 존재시 main 페이지 이동
		return true;
	}

	//컨트롤러 호출후 view 페이지 출력전
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		//System.out.println("postHandle call");
		super.postHandle(request, response, handler, modelAndView);
	}
	
	//컨트롤러 + view 페이지 모두 출력 후
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}
	
	/**
	 * sendRedirect 메소드 실행할 경우 https -> http로 바뀌는 이슈가 있어 강제로 url을 만드는 함수
	 * @param url
	 * @return fullPath
	 */
	private String getHttpsUrl(HttpServletRequest request, String context) {
		String url = "";
		String httpUrl = request.getRequestURL().toString();
		//http:// 로 시작하고, localhost가 아닐경우 https를 붙임
		if(httpUrl.startsWith("http://") && httpUrl.indexOf("localhost") < 0) {
			String hostUrl = request.getHeader("host");
			url = "https://" + hostUrl + context;
		}else {
			String hostUrl = request.getHeader("host");
			url = "http://" + hostUrl + context;
		}
		return url;
	}
}
