package net.ib.paperless.support;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
* session이 끊어졌을때를 처리하기 위해 사용
* static메소드에서는 static만사용 하므로static으로 선언한다.
**/
@Component
public class SessionManager implements HttpSessionBindingListener {
	
	private static final Logger logger = LoggerFactory.getLogger(SessionManager.class);

	private static SessionManager sessionManager = null;
	//로그인한 접속자를 담기위한 해시테이블
	private static Hashtable<String, HttpSession> loginUsers = new Hashtable<String, HttpSession>(); 
	
	private SessionManager(){
		super();
	}
	
	/**
     * 싱글톤 패턴 사용
     **/
	public static synchronized SessionManager getInstance() {
		if (sessionManager == null) {
			sessionManager = new SessionManager();
		}
		return sessionManager;
	}
	
	public boolean isValid(String userId, String remoteAddr) {
		return true;
	}
	
	/**
	 * 기존 로그인 여부 확인
	 * true 이면 
	 * @param sessionId
	 * @param remoteAddr
	 * @return boolean
	 */
	public boolean chkValidLogin(String req_session_id, String remoteAddr) {
		boolean isLogin = true;             
		
		Enumeration<String> e = loginUsers.keys();  
		String session_id = "";
		boolean isExist = false;
		while(e.hasMoreElements()) {                                   
		    session_id = (String)e.nextElement();		    
//		    HttpSession session = (HttpSession)loginUsers.get(key);
		    HttpSession session = getHttpSession(session_id);
		    try {
		    	logger.info("### MAP KEY : " + session_id + " SESSIONID : " + req_session_id);
		    	logger.info("### MAP ADDR : " + (String)session.getAttribute("remoteaddr") + " SESSIONADDR : " + remoteAddr);
		    	
		    	if (req_session_id.equals(session_id)) {
		    		isExist = true;
		    		if (!session.getAttribute("remoteaddr").toString().equals(remoteAddr)) {
		    			isLogin = false;
		    		}
		    	}
		    } catch (IllegalStateException ex) {
		    	logger.info("### First Login : " + req_session_id);
		    	loginUsers.remove(session_id);
		    	return true;
		    }
		}
		logger.info("### CHECK LOGIN : " + isLogin);
		return isLogin;
	}
	public HttpSession getHttpSession (String sessionId) {
		if (!loginUsers.containsKey(sessionId)) {
			return null;
		} else { 
			return (HttpSession)loginUsers.get(sessionId);
		}
	}
	
	public void setHttpSession (String sessionId, HttpSession session, String remoteAddr) {
		if (!chkValidLogin(sessionId, remoteAddr)) {
			removeSession(sessionId);
		}
		loginUsers.put(sessionId, session);
	}
	
	// 세션종료
	public void removeSession (String sessionId) {
		try {
			HttpSession session = (HttpSession) loginUsers.get(sessionId);
			if (session != null) {
				session.invalidate();
				loginUsers.remove(sessionId);
			}
		} catch (IllegalStateException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	 /**
     * 현재 접속한 총 사용자 수
     * @return int  현재 접속자 수
     **/
    public int getUserCount(){
        return loginUsers.size();
    }
	
	/**
     * 이 메소드는 세션이 연결되을때 호출된다.(session.setAttribute("login", this))
     * Hashtable에 세션과 접속자 아이디를 저장한다.
     **/
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		//session값을 put한다.
		loginUsers.put(event.getName(), event.getSession());
        //System.out.println(event.getName() + "님이 로그인 하셨습니다.");
        //System.out.println("현재 접속자 수 : " +  getUserCount());
	}

	/**
     * 이 메소드는 세션이 끊겼을때 호출된다.(invalidate)
     * Hashtable에 저장된 로그인한 정보를 제거해 준다.
     **/
	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		// TODO Auto-generated method stub
		try {
			//session값을 찾아서 없애준다.
			HttpSession session = loginUsers.get(event.getSession().getId());
			session.invalidate();
			loginUsers.remove(event.getSession().getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}