package net.ib.paperless.omap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OMAP_0110 extends OMAP{
	
	private static final Logger logger = LoggerFactory.getLogger(OMAP_0110.class);
	
	byte[]  PARAM01	=	new byte[20];	// 파라메터_01
	byte[]  PARAM02	=	new byte[100];	// 파라메터_02
	byte[]  PARAM03	=	new byte[100];	// 파라메터_03
	byte[]  PARAM04	=	new byte[100];	// 파라메터_04
	
	byte[]  RECFLDCNT = new byte[3];	// 반복건수
	ArrayList<byte[]> REQFILCDES = new ArrayList<byte[]>(); //파일코드
	ArrayList<byte[]> REQFILNAMS = new ArrayList<byte[]>(); //파일명
		
    public OMAP_0110(){}

    public OMAP_0110(String SVCFLDCDE,	String TRDFLDCDE,	String TRDSEQNBR,
			String TELFLDTYP,	String TRDFLDDAT,	String TRDFLDDTM,	String RSTFLDCDE,
			String RSTFLDNAM,	String PARAM01,		String PARAM02,
			String PARAM03,		String PARAM04, 	String RECFLDCNT, 	ArrayList<String> REQFILCDES,
			ArrayList<String> REQFILNAMS)throws UnsupportedEncodingException{
    	
		super(SVCFLDCDE,	TRDFLDCDE,	TRDSEQNBR, 	TELFLDTYP,	TRDFLDDAT,	TRDFLDDTM,	RSTFLDCDE, 	RSTFLDNAM);
		setData(this.PARAM01, PARAM01);		
		setData(this.PARAM02, PARAM02);
		setData(this.PARAM03, PARAM03);
		setData(this.PARAM04, PARAM04);
		setData(this.RECFLDCNT, RECFLDCNT);
		
		int listLength = Integer.valueOf(RECFLDCNT);
		
		for(int i = 0; i< listLength; i++){
			byte[]  REQFILCDE	=	new byte[9];	// 파일코드
			setData(REQFILCDE, REQFILCDES.get(i));
			this.REQFILCDES.add(REQFILCDE);
		}
		
		for(int i = 0; i< listLength; i++){
			byte[]  REQFILNAM	=	new byte[200];	// 파일명
			setData(REQFILNAM, REQFILNAMS.get(i));
			this.REQFILNAMS.add(REQFILNAM);
		}
		
	}

	

    public void writeDataExternal(java.io.DataOutputStream stream)throws IOException{
		super.writeDataExternal(stream);
		stream.write(PARAM01);			
		stream.write(PARAM02);		
		stream.write(PARAM03);		
		stream.write(PARAM04);			
		stream.write(RECFLDCNT);
		
		for(int i =0;i<Integer.valueOf(getData(RECFLDCNT).trim());i++) {
			stream.write(REQFILCDES.get(i));
			stream.write(REQFILNAMS.get(i));
		}
    }
	 
    public void readDataExternal(java.io.DataInputStream stream)throws IOException{
    	super.readDataExternal(stream);
    	stream.read(PARAM01, 0, PARAM01.length);			
    	stream.read(PARAM02, 0, PARAM02.length); 		
    	stream.read(PARAM03, 0, PARAM03.length);
    	stream.read(PARAM04, 0, PARAM04.length); 			
    	stream.read(RECFLDCNT, 0, RECFLDCNT.length);
    	
    	int cnt = Integer.valueOf(getData(TRDFLDLEN)) - 623;
    	
    	byte[] messageByte = new byte[cnt];
    	stream.readFully(messageByte);	
    	
    	int index = 0;
    	for(int i = 0; i< Integer.valueOf(getData(RECFLDCNT).trim()); i++){
    		byte[]  REQFILCDE	=	new byte[9];	// 파일코드
    		byte[]  REQFILNAM	=	new byte[200];	// 파일명

    		System.arraycopy(messageByte, index, REQFILCDE, 0, REQFILCDE.length);
    		String cde = new String(REQFILCDE,"EUC-KR").trim();
    		index += REQFILCDE.length;
			setData(REQFILCDE, cde);
			
			System.arraycopy(messageByte, index, REQFILNAM, 0, REQFILNAM.length);
			String val = new String(REQFILNAM,"EUC-KR").trim();
			index += REQFILNAM.length;
			setData(REQFILNAM, val);
						
			this.REQFILCDES.add(REQFILCDE);
			this.REQFILNAMS.add(REQFILNAM);			
		}
    	
    }
    public void print() throws IOException {	
    	super.print();
		logger.info("PARAM01: " + getData(PARAM01) + "\tSize:" + PARAM01.length	
					+"\n"+"PARAM02: " + getData(PARAM02) + "\tSize:" + PARAM02.length	
					+"\n"+"PARAM03: " + getData(PARAM03) + "\tSize:" + PARAM03.length
					+"\n"+"PARAM04: " + getData(PARAM04) + "\tSize:" + PARAM04.length	
					+"\n"+"RECFLDCNT: " + getData(RECFLDCNT) + "\tSize:" + RECFLDCNT.length);			
		
		for(int i = 0; i< REQFILCDES.size(); i++){
			logger.info("REQFILCDE: " + getData(REQFILCDES.get(i)) + "Size:" + REQFILCDES.get(i).length + " String Size:" 
    						+ new String(REQFILCDES.get(i)).length()
						+"\n"+"REQFILNAM: " + getData(REQFILNAMS.get(i)) + "Size:" + REQFILNAMS.get(i).length + " String Size:" 
								+ new String(REQFILNAMS.get(i)).length());
		}
    }
    public HashMap<String, String> getResult() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();

    	result.put("SVCFLDCDE", getData(SVCFLDCDE));
    	result.put("TRDFLDLEN", getData(TRDFLDLEN));
    	result.put("SVCFLDCDE", getData(SVCFLDCDE));
    	result.put("TRDFLDCDE", getData(TRDFLDCDE));
    	result.put("TRDSEQNBR", getData(TRDSEQNBR));
    	result.put("TELFLDTYP", getData(TELFLDTYP));
    	result.put("TRDFLDDAT", getData(TRDFLDDAT));
    	result.put("TRDFLDDTM", getData(TRDFLDDTM));
    	result.put("RSTFLDCDE", getData(RSTFLDCDE));
    	result.put("RSTFLDNAM", getData(RSTFLDNAM));
    	result.put("RECFLDCNT", getData(RECFLDCNT));
    
    	result.put("PARAM01", getData(PARAM01));
    	result.put("PARAM02", getData(PARAM02));
    	result.put("PARAM03", getData(PARAM03));
    	result.put("PARAM04", getData(PARAM04));
    	
    	return result;
   
    }
    
    public Map<String, String> getReqfilListData() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();
    	for(int i = 0; i< REQFILCDES.size(); i++){
    		result.put(getData(REQFILCDES.get(i)).trim(), getData(REQFILNAMS.get(i)).trim());
		}
    	return result;
    }
}
