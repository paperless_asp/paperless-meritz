package net.ib.paperless.omap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OMAP_0003 extends OMAP{
	
	private static final Logger logger = LoggerFactory.getLogger(OMAP_0003.class);

	byte[]  CONFLDNBR	=	new byte[20];	// 계약번호
	byte[]	CUSTMRCDE	=	new byte[20];	// 거래처코드
	byte[]  KUMFLDYON	=	new byte[1];    // 취약금융소비자확인서-동의여부
	byte[]  KUMFLDCDE	=	new byte[10];    // 취약금융소비자확인서-정보
	
	byte[]  CSINFYN01	=	new byte[1];    // 개인신용정보필수수집이용-동의여부
	byte[]  CSINFYN02	=	new byte[1];    // 개인신용정보필수조회-동의여부
	byte[]  CSINFYN03	=	new byte[1];    // 개인신용정보필수제공-동의여부
	byte[]  CSINFYN04	=	new byte[1];    // 개인신용정보선택수집이용-동의여부
	byte[]  USMKTMD01	=	new byte[1];    // 이용권유방법(전부동의)
	byte[]  USMKTMD02	=	new byte[1];    // 이용권유방법(전화)
	byte[]  USMKTMD03	=	new byte[1];    // 이용권유방법(SMS)
	byte[]  USMKTMD04	=	new byte[1];    // 이용권유방법(서면)
	byte[]  USMKTMD05	=	new byte[1];    // 이용권유방법(이메일 )
	
    public OMAP_0003(){}

    public OMAP_0003(String SVCFLDCDE,	String TRDFLDCDE,	String TRDSEQNBR,
					String TELFLDTYP,	String TRDFLDDAT,	String TRDFLDDTM,	String RSTFLDCDE,
					String RSTFLDNAM,	String CONFLDNBR,	String CUSTMRCDE, String KUMFLDYON, String KUMFLDCDE,
					Map<String, String> agreeCheckMap
					)throws UnsupportedEncodingException{

		super(SVCFLDCDE,	TRDFLDCDE,	TRDSEQNBR, 	TELFLDTYP,	TRDFLDDAT,	TRDFLDDTM,	RSTFLDCDE, 	RSTFLDNAM);
		
		setData(this.CONFLDNBR, CONFLDNBR);		
		setData(this.CUSTMRCDE, CUSTMRCDE);
		setData(this.KUMFLDYON, KUMFLDYON);		
		setData(this.KUMFLDCDE, KUMFLDCDE);
		
		setData(this.CSINFYN01, agreeCheckMap.get("csinfyn01"));
		setData(this.CSINFYN02, agreeCheckMap.get("csinfyn02"));
		setData(this.CSINFYN03, agreeCheckMap.get("csinfyn03"));
		setData(this.CSINFYN04, agreeCheckMap.get("csinfyn04"));
		setData(this.USMKTMD01, agreeCheckMap.get("usmktmd01"));
		setData(this.USMKTMD02, agreeCheckMap.get("usmktmd02"));
		setData(this.USMKTMD03, agreeCheckMap.get("usmktmd03"));
		setData(this.USMKTMD04, agreeCheckMap.get("usmktmd04"));
		setData(this.USMKTMD05, agreeCheckMap.get("usmktmd05"));
    }

	public void writeDataExternal(java.io.DataOutputStream stream)throws IOException{
		super.writeDataExternal(stream);
    	stream.write(CONFLDNBR);		
    	stream.write(CUSTMRCDE);
    	stream.write(KUMFLDYON);		
    	stream.write(KUMFLDCDE);
    	
    	stream.write(CSINFYN01);
    	stream.write(CSINFYN02);
    	stream.write(CSINFYN03);
    	stream.write(CSINFYN04);
    	stream.write(USMKTMD01);
    	stream.write(USMKTMD02);
    	stream.write(USMKTMD03);
    	stream.write(USMKTMD04);
    	stream.write(USMKTMD05);
    }
	
	//수신전문이 따로 있어 이 function은 사용하지 않음
    public void readDataExternal(java.io.DataInputStream stream)throws IOException{
    	super.readDataExternal(stream);
    	stream.read(CONFLDNBR, 0, CONFLDNBR.length);		
    	stream.read(CUSTMRCDE, 0, CUSTMRCDE.length);		
    	stream.read(KUMFLDYON, 0, KUMFLDYON.length);		
    	stream.read(KUMFLDCDE, 0, KUMFLDCDE.length);	
    	
    	stream.read(CSINFYN01, 0, CSINFYN01.length);
    	stream.read(CSINFYN02, 0, CSINFYN02.length);
    	stream.read(CSINFYN03, 0, CSINFYN03.length);
    	stream.read(CSINFYN04, 0, CSINFYN04.length);
    	stream.read(USMKTMD01, 0, USMKTMD01.length);
    	stream.read(USMKTMD02, 0, USMKTMD02.length);
    	stream.read(USMKTMD03, 0, USMKTMD03.length);
    	stream.read(USMKTMD04, 0, USMKTMD04.length);
    	stream.read(USMKTMD05, 0, USMKTMD05.length);
    }
    public void print() throws IOException {	
    	super.print();
		logger.info("CONFLDNBR: " + getData(CONFLDNBR) + "\tSize:" + CONFLDNBR.length	
				+"\n"+"CUSTMRCDE: " + getData(CUSTMRCDE) + "\tSize:" + CUSTMRCDE.length	
				+"\n"+"KUMFLDYON: " + getData(KUMFLDYON) + "\tSize:" + KUMFLDYON.length	
				+"\n"+"KUMFLDCDE: " + getData(KUMFLDCDE) + "\tSize:" + KUMFLDCDE.length
				+"\n"+"CSINFYN01: " + getData(CSINFYN01) + "\tSize:" + CSINFYN01.length
				+"\n"+"CSINFYN02: " + getData(CSINFYN02) + "\tSize:" + CSINFYN02.length
				+"\n"+"CSINFYN03: " + getData(CSINFYN03) + "\tSize:" + CSINFYN03.length
				+"\n"+"CSINFYN04: " + getData(CSINFYN04) + "\tSize:" + CSINFYN04.length
				+"\n"+"USMKTMD01: " + getData(USMKTMD01) + "\tSize:" + USMKTMD01.length
				+"\n"+"USMKTMD02: " + getData(USMKTMD02) + "\tSize:" + USMKTMD02.length
				+"\n"+"USMKTMD03: " + getData(USMKTMD03) + "\tSize:" + USMKTMD03.length
				+"\n"+"USMKTMD04: " + getData(USMKTMD04) + "\tSize:" + USMKTMD04.length
				+"\n"+"USMKTMD05: " + getData(USMKTMD05) + "\tSize:" + USMKTMD05.length);	
		
    }
    public HashMap<String, String> getResult() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();

    	result.put("TRDFLDLEN", getData(TRDFLDLEN));
    	result.put("SVCFLDCDE", getData(SVCFLDCDE));
    	result.put("TRDFLDCDE", getData(TRDFLDCDE));
    	result.put("TRDSEQNBR", getData(TRDSEQNBR));
    	result.put("TELFLDTYP", getData(TELFLDTYP));
    	result.put("TRDFLDDAT", getData(TRDFLDDAT));
    	result.put("TRDFLDDTM", getData(TRDFLDDTM));
    	result.put("RSTFLDCDE", getData(RSTFLDCDE));
    	result.put("RSTFLDNAM", getData(RSTFLDNAM));
    	result.put("CONFLDNBR", getData(CONFLDNBR));
    	result.put("CUSTMRCDE", getData(CUSTMRCDE));
    	result.put("KUMFLDYON", getData(KUMFLDYON));
    	result.put("KUMFLDCDE", getData(KUMFLDCDE));
    	result.put("CSINFYN01", getData(CSINFYN01));
    	result.put("CSINFYN02", getData(CSINFYN02));
    	result.put("CSINFYN03", getData(CSINFYN03));
    	result.put("CSINFYN04", getData(CSINFYN04));
    	result.put("USMKTMD01", getData(USMKTMD01));
    	result.put("USMKTMD02", getData(USMKTMD02));
    	result.put("USMKTMD03", getData(USMKTMD03));
    	result.put("USMKTMD04", getData(USMKTMD04));
    	result.put("USMKTMD05", getData(USMKTMD05));
    	return result;
   
    }
}
