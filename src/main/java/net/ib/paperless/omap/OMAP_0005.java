package net.ib.paperless.omap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OMAP_0005 extends OMAP{
	
	private static final Logger logger = LoggerFactory.getLogger(OMAP_0005.class);
	
	byte[]  CONFLDNBR	=	new byte[20];	// 계약번호
	byte[]	CUSTMRCDE	=	new byte[20];	// 거래처코드
	byte[]  AUTFLDDTM	=	new byte[17];	// 인증일시
	byte[]  CUSTMRNAM	=	new byte[200];	// 거래처명
	byte[]  SEXFLDMOW	=	new byte[1];	// 성별
	byte[]  BIRFLDTXT	=	new byte[6];	// 생년월일
	byte[]  BNKFLDCDE	=	new byte[3];	// 은행코드
	byte[]  BNKFLDNAM	=	new byte[40];	// 은행명
	byte[]  KUJFLDNBR	=	new byte[16];	// 계좌번호
	byte[]  KUJCERRCD	=	new byte[8];	// 계좌인증결과코드
	
    public OMAP_0005(){}

    public OMAP_0005(String SVCFLDCDE,	String TRDFLDCDE,	String TRDSEQNBR,
					String TELFLDTYP,	String TRDFLDDAT,	String TRDFLDDTM,	String RSTFLDCDE,
					String RSTFLDNAM,	String CONFLDNBR,	String CUSTMRCDE,	String AUTFLDDTM,
					String CUSTMRNAM,	String SEXFLDMOW,	String BIRFLDTXT,	String BNKFLDCDE,
					String BNKFLDNAM,	String KUJFLDNBR,	String KUJCERRCD
					)throws UnsupportedEncodingException{

		super(SVCFLDCDE,	TRDFLDCDE,	TRDSEQNBR, 	TELFLDTYP,	TRDFLDDAT,	TRDFLDDTM,	RSTFLDCDE, 	RSTFLDNAM);

		setData(this.CONFLDNBR, CONFLDNBR);		
		setData(this.CUSTMRCDE, CUSTMRCDE);
		setData(this.CUSTMRNAM, CUSTMRNAM);
		setData(this.AUTFLDDTM, AUTFLDDTM);
		setData(this.SEXFLDMOW, SEXFLDMOW);
		setData(this.BIRFLDTXT, BIRFLDTXT);		
		setData(this.BNKFLDCDE, BNKFLDCDE);
		setData(this.BNKFLDNAM, BNKFLDNAM);
		setData(this.KUJFLDNBR, KUJFLDNBR);
		setData(this.KUJCERRCD, KUJCERRCD);
    }

	 public void writeDataExternal(java.io.DataOutputStream stream)throws IOException{
		super.writeDataExternal(stream);
    	stream.write(CONFLDNBR);		
    	stream.write(CUSTMRCDE);
    	stream.write(AUTFLDDTM);	
    	stream.write(CUSTMRNAM);	
    	stream.write(SEXFLDMOW);		
    	stream.write(BIRFLDTXT);
    	stream.write(BNKFLDCDE);	
    	stream.write(BNKFLDNAM);		
    	stream.write(KUJFLDNBR);
    	stream.write(KUJCERRCD);
    }
    public void readDataExternal(java.io.DataInputStream stream)throws IOException{
    	super.readDataExternal(stream);
    	stream.read(CONFLDNBR, 0, CONFLDNBR.length);		
    	stream.read(CUSTMRCDE, 0, CUSTMRCDE.length);
    	stream.read(AUTFLDDTM, 0, AUTFLDDTM.length);
    	stream.read(CUSTMRNAM, 0, CUSTMRNAM.length);	
    	stream.read(SEXFLDMOW, 0, SEXFLDMOW.length); 		
    	stream.read(BIRFLDTXT, 0, BIRFLDTXT.length);	
    	stream.read(BNKFLDCDE, 0, BNKFLDCDE.length);	
    	stream.read(BNKFLDNAM, 0, BNKFLDNAM.length);		
    	stream.read(KUJFLDNBR, 0, KUJFLDNBR.length);
    	stream.read(KUJCERRCD, 0, KUJCERRCD.length);
    }
    public void print() throws IOException {	
    	super.print();
//    	String bankNum = getData(KUJFLDNBR).trim();
		logger.info("CONFLDNBR: " + getData(CONFLDNBR) + "\tSize:" + CONFLDNBR.length
			+"\n"+"CUSTMRCDE: " + getData(CUSTMRCDE) + "\tSize:" + CUSTMRCDE.length	
			+"\n"+"AUTFLDDTM: " + getData(AUTFLDDTM) + "\tSize:" + AUTFLDDTM.length	
			+"\n"+"CUSTMRNAM: " + getData(CUSTMRNAM) + "\tSize:" + CUSTMRNAM.length
			+"\n"+"SEXFLDMOW: " + getData(SEXFLDMOW) + "\tSize:" + SEXFLDMOW.length		
			+"\n"+"BIRFLDTXT: " + getData(BIRFLDTXT) + "\tSize:" + BIRFLDTXT.length		
			+"\n"+"BNKFLDCDE: " + getData(BNKFLDCDE) + "\tSize:" + BNKFLDCDE.length		
			+"\n"+"BNKFLDNAM: " + getData(BNKFLDNAM) + "\tSize:" + BNKFLDNAM.length		
			//계좌번호
			+"\n"+"KUJFLDNBR: " + "고객계좌" + "\tSize:" + KUJFLDNBR.length		
			+"\n"+"KUJCERRCD: " + getData(KUJCERRCD) + "\tSize:" + KUJCERRCD.length);		
    }
    public HashMap<String, String> getResult() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();

    	result.put("TRDFLDLEN", getData(TRDFLDLEN));
    	result.put("SVCFLDCDE", getData(SVCFLDCDE));
    	result.put("TRDFLDCDE", getData(TRDFLDCDE));
    	result.put("TRDSEQNBR", getData(TRDSEQNBR));
    	result.put("TELFLDTYP", getData(TELFLDTYP));
    	result.put("TRDFLDDAT", getData(TRDFLDDAT));
    	result.put("TRDFLDDTM", getData(TRDFLDDTM));
    	result.put("RSTFLDCDE", getData(RSTFLDCDE));
    	result.put("RSTFLDNAM", getData(RSTFLDNAM));
    	result.put("CONFLDNBR", getData(CONFLDNBR));
    	result.put("CUSTMRCDE", getData(CUSTMRCDE));
    	
    	return result;
   
    }

}
