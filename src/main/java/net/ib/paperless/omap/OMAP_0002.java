package net.ib.paperless.omap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OMAP_0002 extends OMAP{
	
	private static final Logger logger = LoggerFactory.getLogger(OMAP_0002.class);

	byte[]  CONFLDNBR	=	new byte[20];	// 계약번호
	byte[]	CUSTMRCDE	=	new byte[20];	// 거래처코드
	byte[]  PIDFLDCLL	=	new byte[20];	// 휴대폰번호
	byte[]  AUTFLDDTM	=	new byte[14];	// 인증일시
	byte[]  AUTFLDCDE	=	new byte[1];	// 인증결과코드
	byte[]  AUTFLDNAM	=	new byte[200];	// 인증결과명
	
    public OMAP_0002(){}

    public OMAP_0002(String SVCFLDCDE,	String TRDFLDCDE,	String TRDSEQNBR,
					String TELFLDTYP,	String TRDFLDDAT,	String TRDFLDDTM,	String RSTFLDCDE,
					String RSTFLDNAM,	String CONFLDNBR,	String CUSTMRCDE,
					String PIDFLDCLL, 	String AUTFLDDTM,	String AUTFLDCDE, 	String AUTFLDNAM
					)throws UnsupportedEncodingException{

		super(SVCFLDCDE,	TRDFLDCDE,	TRDSEQNBR, 	TELFLDTYP,	TRDFLDDAT,	TRDFLDDTM,	RSTFLDCDE, 	RSTFLDNAM);
		setData(this.CONFLDNBR, CONFLDNBR);		
		setData(this.CUSTMRCDE, CUSTMRCDE);
		setData(this.PIDFLDCLL, PIDFLDCLL);
		setData(this.AUTFLDDTM, AUTFLDDTM);
		setData(this.AUTFLDCDE, AUTFLDCDE);
		setData(this.AUTFLDNAM, AUTFLDNAM);
    }

	 public void writeDataExternal(java.io.DataOutputStream stream)throws IOException{
		super.writeDataExternal(stream);
    	stream.write(CONFLDNBR);		
    	stream.write(CUSTMRCDE);
    	stream.write(PIDFLDCLL); 	
    	stream.write(AUTFLDDTM);		
    	stream.write(AUTFLDCDE);		
    	stream.write(AUTFLDNAM);
    }
    public void readDataExternal(java.io.DataInputStream stream)throws IOException{
    	super.readDataExternal(stream);
    	stream.read(CONFLDNBR, 0, CONFLDNBR.length);		
    	stream.read(CUSTMRCDE, 0, CUSTMRCDE.length);
    	stream.read(PIDFLDCLL, 0, PIDFLDCLL.length); 	
    	stream.read(AUTFLDDTM, 0, AUTFLDDTM.length);		
    	stream.read(AUTFLDCDE, 0, AUTFLDCDE.length);
    	stream.read(AUTFLDNAM, 0, AUTFLDNAM.length);			
    }
    public void print() throws IOException {	
    	super.print();
    	String phoneNum = getData(PIDFLDCLL).trim();
    	if(!"".equals(phoneNum) && phoneNum.length() > 4) {
    		phoneNum = phoneNum.substring(phoneNum.length()-4, phoneNum.length());
    	}
		logger.info("CONFLDNBR: " + getData(CONFLDNBR) + "\tSize:" + CONFLDNBR.length	
				+"\n"+"CUSTMRCDE: " + getData(CUSTMRCDE) + "\tSize:" + CUSTMRCDE.length	
				+"\n"+"PIDFLDCLL: " + "고객 휴대폰 뒷번호("+ phoneNum +")" + "\tSize:" + PIDFLDCLL.length
				+"\n"+"AUTFLDDTM: " + getData(AUTFLDDTM) + "\tSize:" + AUTFLDDTM.length	
				+"\n"+"AUTFLDCDE: " + getData(AUTFLDCDE) + "\tSize:" + AUTFLDCDE.length	
				+"\n"+"AUTFLDNAM: " + getData(AUTFLDNAM) + "\tSize:" + AUTFLDNAM.length);	
    }
    
    public HashMap<String, String> getResult() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();

    	result.put("TRDFLDLEN", getData(TRDFLDLEN));
    	result.put("SVCFLDCDE", getData(SVCFLDCDE));
    	result.put("TRDFLDCDE", getData(TRDFLDCDE));
    	result.put("TRDSEQNBR", getData(TRDSEQNBR));
    	result.put("TELFLDTYP", getData(TELFLDTYP));
    	result.put("TRDFLDDAT", getData(TRDFLDDAT));
    	result.put("TRDFLDDTM", getData(TRDFLDDTM));
    	result.put("RSTFLDCDE", getData(RSTFLDCDE));
    	result.put("RSTFLDNAM", getData(RSTFLDNAM));
    	result.put("CONFLDNBR", getData(CONFLDNBR));
    	result.put("CUSTMRCDE", getData(CUSTMRCDE));
    	
    	return result;
   
    }
}
