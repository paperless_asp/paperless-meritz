package net.ib.paperless.omap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OMAP_0007 extends OMAP{
	
	private static final Logger logger = LoggerFactory.getLogger(OMAP_0007.class);
	
	byte[]  CONFLDNBR	=	new byte[20];	// 계약번호
	byte[]	CUSTMRCDE	=	new byte[20];	// 거래처코드
	byte[]	RECFLDCNT	=	new byte[3];	// 반복건
	
	ArrayList<byte[]> REQFILCDES = new ArrayList<byte[]>(); //파일코드
	ArrayList<byte[]> REQFILNAMS = new ArrayList<byte[]>(); //파일명
		
	public OMAP_0007(){}

	public OMAP_0007(String SVCFLDCDE,	String TRDFLDCDE,	String TRDSEQNBR,
				String TELFLDTYP,	String TRDFLDDAT,	String TRDFLDDTM,	String RSTFLDCDE,
				String RSTFLDNAM,	String CONFLDNBR,		String CUSTMRCDE,
				String RECFLDCNT, 	ArrayList<String> REQFILCDES, 	ArrayList<String> REQFILNAMS)throws UnsupportedEncodingException{
    	
		super(SVCFLDCDE,	TRDFLDCDE,	TRDSEQNBR, 	TELFLDTYP,	TRDFLDDAT,	TRDFLDDTM,	RSTFLDCDE, 	RSTFLDNAM);
		setData(this.CONFLDNBR, CONFLDNBR);		
		setData(this.CUSTMRCDE, CUSTMRCDE);
		setData(this.RECFLDCNT, RECFLDCNT);
		
		int listLength = Integer.valueOf(RECFLDCNT);
		
		for(int i = 0; i< listLength; i++){
			byte[]  REQFILCDE	=	new byte[9];	// 파일코드
			setData(REQFILCDE, REQFILCDES.get(i));
			this.REQFILCDES.add(REQFILCDE);
		}
		
		for(int i = 0; i< listLength; i++){
			byte[]  REQFILNAM	=	new byte[200];	// 파일명
			setData(REQFILNAM, REQFILNAMS.get(i));
			this.REQFILNAMS.add(REQFILNAM);
		}
		
	}

	

	 public void writeDataExternal(java.io.DataOutputStream stream)throws IOException{
		super.writeDataExternal(stream);
		stream.write(CONFLDNBR);			
		stream.write(CUSTMRCDE);				
		stream.write(RECFLDCNT);
		
		for(int i =0;i<Integer.valueOf(getData(RECFLDCNT).trim());i++) {
			stream.write(REQFILCDES.get(i));
			stream.write(REQFILNAMS.get(i));
		}
		 
		//write전 총 byte를 전문길이에 입력한다.
		/*setData(this.TRDFLDLEN, Integer.toString(length));	
		
		byte [] writeTotalByte = new byte[length];
		int index = 0;
		
		System.arraycopy(TRDFLDLEN, index, writeTotalByte, 0, TRDFLDLEN.length);
		index += TRDFLDLEN.length;
		System.arraycopy(SVCFLDCDE, index, writeTotalByte, 0, SVCFLDCDE.length);
		index += SVCFLDCDE.length;
		System.arraycopy(TRDFLDCDE, index, writeTotalByte, 0, TRDFLDCDE.length);
		index += TRDFLDCDE.length;
		System.arraycopy(TRDSEQNBR, index, writeTotalByte, 0, TRDSEQNBR.length);
		index += TRDSEQNBR.length;
		System.arraycopy(TELFLDTYP, index, writeTotalByte, 0, TELFLDTYP.length);
		index += TELFLDTYP.length;
		System.arraycopy(TRDFLDDAT, index, writeTotalByte, 0, TRDFLDDAT.length);
		index += TRDFLDDAT.length;
		System.arraycopy(TRDFLDDTM, index, writeTotalByte, 0, TRDFLDDTM.length);
		index += TRDFLDDTM.length;
		System.arraycopy(RSTFLDCDE, index, writeTotalByte, 0, RSTFLDCDE.length);
		index += RSTFLDCDE.length;
		System.arraycopy(RSTFLDNAM, index, writeTotalByte, 0, RSTFLDNAM.length);
		index += RSTFLDNAM.length;
		System.arraycopy(FILLER01, index, writeTotalByte, 0, FILLER01.length);
		index += FILLER01.length;
		
		System.arraycopy(CONFLDNBR, index, writeTotalByte, 0, CONFLDNBR.length);
		index += CONFLDNBR.length;
		System.arraycopy(CUSTMRCDE, index, writeTotalByte, 0, CUSTMRCDE.length);
		index += CUSTMRCDE.length;
    	
		for(int i =0;i<Integer.valueOf(getData(RECFLDCNT).trim());i++) {
			System.arraycopy(REQFILCDES.get(i), index, writeTotalByte, 0, REQFILCDES.get(i).length);
			index += REQFILCDES.get(i).length;
			System.arraycopy(REQFILNAMS.get(i), index, writeTotalByte, 0, REQFILNAMS.get(i).length);
			index += REQFILNAMS.get(i).length;
		}
		
		//System.out.println("### totalByte : "+new String(writeTotalByte,"EUC-KR"));
		//System.out.println("### size : "+writeTotalByte.length);*/
    }
	 
    public void readDataExternal(java.io.DataInputStream stream)throws IOException{
    	super.readDataExternal(stream);
    	stream.read(CONFLDNBR, 0, CONFLDNBR.length);			
    	stream.read(CUSTMRCDE, 0, CUSTMRCDE.length); 				
    	stream.read(RECFLDCNT, 0, RECFLDCNT.length);
    	
    	int cnt = Integer.valueOf(getData(TRDFLDLEN)) - 343;
    	
    	byte[] messageByte = new byte[cnt];
    	stream.readFully(messageByte);	
    	
    	int index = 0;
    	for(int i = 0; i< Integer.valueOf(getData(RECFLDCNT).trim()); i++){
    		byte[]  REQFILCDE	=	new byte[9];	// 파일코드
    		byte[]  REQFILNAM	=	new byte[200];	// 파일명

    		System.arraycopy(messageByte, index, REQFILCDE, 0, REQFILCDE.length);
    		String cde = new String(REQFILCDE,"EUC-KR").trim();
    		index += REQFILCDE.length;
			setData(REQFILCDE, cde);
			
			System.arraycopy(messageByte, index, REQFILNAM, 0, REQFILNAM.length);
			String val = new String(REQFILNAM,"EUC-KR").trim();
			index += REQFILNAM.length;
			setData(REQFILNAM, val);
						
			this.REQFILCDES.add(REQFILCDE);
			this.REQFILNAMS.add(REQFILNAM);				
		}
    	
    }
    public void print() throws IOException {	
    	super.print();
		logger.info("PARAM01: " + getData(CONFLDNBR) + "\tSize:" + CONFLDNBR.length
		+"\n"+"PARAM02: " + getData(CUSTMRCDE) + "\tSize:" + CUSTMRCDE.length
		+"\n"+"RECFLDCNT: " + getData(RECFLDCNT) + "\tSize:" + RECFLDCNT.length);			
		
		for(int i = 0; i< REQFILCDES.size(); i++){
			logger.info("REQFILCDE: " + getData(REQFILCDES.get(i)) + "Size:" + REQFILCDES.get(i).length + " String Size:" 
    						+ new String(REQFILCDES.get(i)).length()
						+"\n"+"REQFILNAM: " + getData(REQFILNAMS.get(i)) + "Size:" + REQFILNAMS.get(i).length + " String Size:" 
								+ new String(REQFILNAMS.get(i)).length());
		}
    }
    public HashMap<String, String> getResult() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();

    	result.put("TRDFLDLEN", getData(TRDFLDLEN));
    	result.put("SVCFLDCDE", getData(SVCFLDCDE));
    	result.put("TRDFLDCDE", getData(TRDFLDCDE));
    	result.put("TRDSEQNBR", getData(TRDSEQNBR));
    	result.put("TELFLDTYP", getData(TELFLDTYP));
    	result.put("TRDFLDDAT", getData(TRDFLDDAT));
    	result.put("TRDFLDDTM", getData(TRDFLDDTM));
    	result.put("RSTFLDCDE", getData(RSTFLDCDE));
    	result.put("RSTFLDNAM", getData(RSTFLDNAM));
    	result.put("CONFLDNBR", getData(CONFLDNBR));
    	result.put("CUSTMRCDE", getData(CUSTMRCDE));
    	
    	return result;
   
    }
    
    public Map<String, String> getReqfilListData() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();
    	for(int i = 0; i< REQFILCDES.size(); i++){
    		result.put(getData(REQFILCDES.get(i)).trim(), getData(REQFILNAMS.get(i)).trim());
		}
    	return result;
    }
}
