package net.ib.paperless.omap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OMAP_0006 extends OMAP{
	
	private static final Logger logger = LoggerFactory.getLogger(OMAP_0006.class);
	
	byte[]  SNDFLDNBR    =	new byte[18];      //발송번호
	byte[]  CONFLDNBR    =	new byte[20];      //계약번호
	byte[]  CUSTMRCDE    =	new byte[20];      //거래처코드
	byte[]  PARAM1       =	new byte[20];      //파라미터1
	byte[]  PARAM2       =	new byte[20];      //파라미터2
	byte[]  PARAM3       =	new byte[20];      //파라미터3
	byte[]  PARAM4       =	new byte[20];      //파라미터4
	byte[]  PIDFLDCLL    =	new byte[20];      //휴대폰번호
	byte[]  CALLBACK     =	new byte[20];      //콜백번호
	byte[]  SNDFLDDTM    =	new byte[14];      //발송일시
	byte[]  SNDFLDMSG    =	new byte[2000];    //발송메시지
	byte[]  SNDFLDSTA    =	new byte[2];       //발송결과
	byte[]  SNDSTARSN    =	new byte[1000];    //발송결과사유
	byte[]  SNDMSGKND    =	new byte[1];       //발송메시지종류
	byte[]  SNDFLDRSN    =	new byte[2];       //발송사유
	
	
    public OMAP_0006(){}

    public OMAP_0006(String SVCFLDCDE, String TRDFLDCDE, String TRDSEQNBR, String TELFLDTYP, String TRDFLDDAT, String TRDFLDDTM, 
    		String RSTFLDCDE, String RSTFLDNAM, String SNDFLDNBR, String CONFLDNBR, String CUSTMRCDE, String PARAM1, String PARAM2, 
    		String PARAM3, String PARAM4, String PIDFLDCLL, String CALLBACK, String SNDFLDDTM, String SNDFLDMSG, String SNDFLDSTA,
    		String SNDSTARSN, String SNDMSGKND, String SNDFLDRSN
    		)throws UnsupportedEncodingException{

    	super(SVCFLDCDE, TRDFLDCDE,	TRDSEQNBR, TELFLDTYP, TRDFLDDAT, TRDFLDDTM, RSTFLDCDE, RSTFLDNAM);

		setData(this.SNDFLDNBR, SNDFLDNBR);
		setData(this.CONFLDNBR, CONFLDNBR);		
		setData(this.CUSTMRCDE, CUSTMRCDE);
		setData(this.PARAM1   , PARAM1   );
		setData(this.PARAM2   , PARAM2   );
		setData(this.PARAM3   , PARAM3   );
		setData(this.PARAM4   , PARAM4   );
		setData(this.PIDFLDCLL, PIDFLDCLL);
		setData(this.CALLBACK , CALLBACK );
		setData(this.SNDFLDDTM, SNDFLDDTM);
		setData(this.SNDFLDMSG, SNDFLDMSG);
		setData(this.SNDFLDSTA, SNDFLDSTA);
		setData(this.SNDSTARSN, SNDSTARSN);
		setData(this.SNDMSGKND, SNDMSGKND);
		setData(this.SNDFLDRSN, SNDFLDRSN);
    }

	 public void writeDataExternal(java.io.DataOutputStream stream)throws IOException{
		super.writeDataExternal(stream);
		
    	stream.write(SNDFLDNBR);	
    	stream.write(CONFLDNBR);		
    	stream.write(CUSTMRCDE);
    	stream.write(PARAM1   );	
    	stream.write(PARAM2   );
    	stream.write(PARAM3   );
    	stream.write(PARAM4   );
    	stream.write(PIDFLDCLL);
    	stream.write(CALLBACK );
    	stream.write(SNDFLDDTM);
    	stream.write(SNDFLDMSG);
    	stream.write(SNDFLDSTA);
    	stream.write(SNDSTARSN);
    	stream.write(SNDMSGKND);
    	stream.write(SNDFLDRSN);
    	
    }
    public void readDataExternal(java.io.DataInputStream stream)throws IOException{
    	super.readDataExternal(stream);
    	stream.read(SNDFLDNBR, 0, SNDFLDNBR.length);
    	stream.read(CONFLDNBR, 0, CONFLDNBR.length);		
    	stream.read(CUSTMRCDE, 0, CUSTMRCDE.length);
    	stream.read(PARAM1   , 0, PARAM1   .length);	
    	stream.read(PARAM2   , 0, PARAM2   .length);	
    	stream.read(PARAM3   , 0, PARAM3   .length);	
    	stream.read(PARAM4   , 0, PARAM4   .length);	
    	stream.read(PIDFLDCLL, 0, PIDFLDCLL.length);	
    	stream.read(CALLBACK , 0, CALLBACK .length);	
    	stream.read(SNDFLDDTM, 0, SNDFLDDTM.length);	
    	stream.read(SNDFLDMSG, 0, SNDFLDMSG.length);	
    	stream.read(SNDFLDSTA, 0, SNDFLDSTA.length);	
    	stream.read(SNDSTARSN, 0, SNDSTARSN.length);	
    	stream.read(SNDMSGKND, 0, SNDMSGKND.length);	
        stream.read(SNDFLDRSN, 0, SNDFLDRSN.length);
    }
    
    public void print() throws IOException {	
    	super.print();
    	String phoneNum = getData(PIDFLDCLL).trim();
    	if(!"".equals(phoneNum) && phoneNum.length() > 4) {
    		phoneNum = phoneNum.substring(phoneNum.length()-4, phoneNum.length());
    	}
		logger.info("SNDFLDNBR: " + getData(SNDFLDNBR) + "\tSize:" + SNDFLDNBR.length	
		+"\n"+"CONFLDNBR: " + getData(CONFLDNBR) + "\tSize:" + CONFLDNBR.length	
		+"\n"+"CUSTMRCDE: " + getData(CUSTMRCDE) + "\tSize:" + CUSTMRCDE.length	
		+"\n"+"PARAM1   : " + getData(PARAM1   ) + "\tSize:" + PARAM1   .length
		+"\n"+"PARAM2   : " + getData(PARAM2   ) + "\tSize:" + PARAM2   .length
		+"\n"+"PARAM3   : " + getData(PARAM3   ) + "\tSize:" + PARAM3   .length
		+"\n"+"PARAM4   : " + getData(PARAM4   ) + "\tSize:" + PARAM4   .length
		+"\n"+"PIDFLDCLL: " + "고객 휴대폰 뒷번호("+ phoneNum +")" + "\tSize:" + PIDFLDCLL.length
		+"\n"+"CALLBACK : " + getData(CALLBACK ) + "\tSize:" + CALLBACK .length
		+"\n"+"SNDFLDDTM: " + getData(SNDFLDDTM) + "\tSize:" + SNDFLDDTM.length
		+"\n"+"SNDFLDMSG: " + getData(SNDFLDMSG) + "\tSize:" + SNDFLDMSG.length
		+"\n"+"SNDFLDSTA: " + getData(SNDFLDSTA) + "\tSize:" + SNDFLDSTA.length
		+"\n"+"SNDSTARSN: " + getData(SNDSTARSN) + "\tSize:" + SNDSTARSN.length
		+"\n"+"SNDMSGKND: " + getData(SNDMSGKND) + "\tSize:" + SNDMSGKND.length
		+"\n"+"SNDFLDRSN: " + getData(SNDFLDRSN) + "\tSize:" + SNDFLDRSN.length);
		
    }
    public HashMap<String, String> getResult() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();

    	result.put("TRDFLDLEN", getData(TRDFLDLEN));
    	result.put("SVCFLDCDE", getData(SVCFLDCDE));
    	result.put("TRDFLDCDE", getData(TRDFLDCDE));
    	result.put("TRDSEQNBR", getData(TRDSEQNBR));
    	result.put("TELFLDTYP", getData(TELFLDTYP));
    	result.put("TRDFLDDAT", getData(TRDFLDDAT));
    	result.put("TRDFLDDTM", getData(TRDFLDDTM));
    	result.put("RSTFLDCDE", getData(RSTFLDCDE));
    	result.put("RSTFLDNAM", getData(RSTFLDNAM));
    	result.put("CONFLDNBR", getData(CONFLDNBR));
    	result.put("CUSTMRCDE", getData(CUSTMRCDE));
    	//필요한만큼리턴
    	/*result.put("SNDFLDNBR", getData(SNDFLDNBR));
    	result.put("PARAM1   ", getData(PARAM1   ));
    	result.put("PARAM2   ", getData(PARAM2   ));
    	result.put("PARAM3   ", getData(PARAM3   ));
    	result.put("PARAM4   ", getData(PARAM4   ));
    	result.put("PIDFLDCLL", getData(PIDFLDCLL));
    	result.put("CALLBACK ", getData(CALLBACK ));
    	result.put("SNDFLDDTM", getData(SNDFLDDTM)); 
    	result.put("SNDFLDMSG", getData(SNDFLDMSG)); 
    	result.put("SNDFLDSTA", getData(SNDFLDSTA)); 
    	result.put("SNDSTARSN", getData(SNDSTARSN)); 
    	result.put("SNDMSGKND", getData(SNDMSGKND)); 
    	result.put("SNDFLDRSN", getData(SNDFLDRSN)); */
    	
    	return result;
    }

}
