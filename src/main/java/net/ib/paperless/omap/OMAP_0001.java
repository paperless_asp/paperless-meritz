package net.ib.paperless.omap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OMAP_0001 extends OMAP{
	
	private static final Logger logger = LoggerFactory.getLogger(OMAP_0001.class);
	
	byte[]  CONFLDNBR	=	new byte[20];	// 계약번호
	byte[]	CUSTMRCDE	=	new byte[20];	// 거래처코드
	byte[]  CUSTMRNAM	=	new byte[200];	// 거래처명
	byte[]  BIRFLDTXT	=	new byte[8];	// 생년월일
	byte[]  SEXFLDMOW	=	new byte[1];	// 성별
	byte[]  DOMFORKND	=	new byte[1];	// 내외국인구분
	byte[]  PIDFLDKND	=	new byte[3];	// 통신사구분
	byte[]  PIDFLDCLL	=	new byte[20];	// 휴대폰번호
	byte[]  AUTFLDDTM	=	new byte[14];	// 인증일시
	byte[]  AUTFLDCDE	=	new byte[1];	// 인증결과코드
	byte[]  AUTFLDNAM	=	new byte[200];	// 인증결과명
	byte[]  DUPFLDTXT	=	new byte[64];	// 중복정보
	byte[]  CONINFTXT	=	new byte[88];	// 연계정보
	
    public OMAP_0001(){}

    public OMAP_0001(String SVCFLDCDE,	String TRDFLDCDE,	String TRDSEQNBR,
					String TELFLDTYP,	String TRDFLDDAT,	String TRDFLDDTM,	String RSTFLDCDE,
					String RSTFLDNAM,	String CONFLDNBR,	String CUSTMRCDE,
					String CUSTMRNAM,	String BIRFLDTXT,	String SEXFLDMOW,	String DOMFORKND,
					String PIDFLDKND,	String PIDFLDCLL,	String AUTFLDDTM,	String AUTFLDCDE,
					String AUTFLDNAM,	String DUPFLDTXT,	String CONINFTXT
					)throws UnsupportedEncodingException{

		super(SVCFLDCDE,	TRDFLDCDE,	TRDSEQNBR, 	TELFLDTYP,	TRDFLDDAT,	TRDFLDDTM,	RSTFLDCDE, 	RSTFLDNAM);
		
		setData(this.CONFLDNBR, CONFLDNBR);		
		setData(this.CUSTMRCDE, CUSTMRCDE);
		setData(this.CUSTMRNAM, CUSTMRNAM);
		setData(this.BIRFLDTXT, BIRFLDTXT);
		setData(this.SEXFLDMOW, SEXFLDMOW);
		setData(this.DOMFORKND, DOMFORKND);
		setData(this.PIDFLDKND, PIDFLDKND);
		setData(this.PIDFLDCLL, PIDFLDCLL);
		setData(this.AUTFLDDTM, AUTFLDDTM);
		setData(this.AUTFLDCDE, AUTFLDCDE);
		setData(this.AUTFLDNAM, AUTFLDNAM);
		setData(this.DUPFLDTXT, DUPFLDTXT);
		setData(this.CONINFTXT, CONINFTXT);
    }
	 public void writeDataExternal(java.io.DataOutputStream stream)throws IOException{
		super.writeDataExternal(stream);
		stream.write(CONFLDNBR);		
		stream.write(CUSTMRCDE);
    	stream.write(CUSTMRNAM);	
    	stream.write(BIRFLDTXT);		
    	stream.write(SEXFLDMOW);
    	stream.write(DOMFORKND);	
    	stream.write(PIDFLDKND);		
    	stream.write(PIDFLDCLL);
    	stream.write(AUTFLDDTM);	
    	stream.write(AUTFLDCDE);		
    	stream.write(AUTFLDNAM);
    	stream.write(DUPFLDTXT);	
    	stream.write(CONINFTXT);
    }
    public void readDataExternal(java.io.DataInputStream stream)throws IOException{
    	super.readDataExternal(stream);
    	stream.read(CONFLDNBR, 0, CONFLDNBR.length);		
    	stream.read(CUSTMRCDE, 0, CUSTMRCDE.length);
    	stream.read(CUSTMRNAM, 0, CUSTMRNAM.length);	
    	stream.read(BIRFLDTXT, 0, BIRFLDTXT.length);		
    	stream.read(SEXFLDMOW, 0, SEXFLDMOW.length);
    	stream.read(DOMFORKND, 0, DOMFORKND.length);	
    	stream.read(PIDFLDKND, 0, PIDFLDKND.length);		
    	stream.read(PIDFLDCLL, 0, PIDFLDCLL.length);
    	stream.read(AUTFLDDTM, 0, AUTFLDDTM.length);	
    	stream.read(AUTFLDCDE, 0, AUTFLDCDE.length);		
    	stream.read(AUTFLDNAM, 0, AUTFLDNAM.length);
    	stream.read(DUPFLDTXT, 0, DUPFLDTXT.length);	
    	stream.read(CONINFTXT, 0, CONINFTXT.length);			
    }

    public void print() throws IOException {	
    	super.print();
    	String phoneNum = getData(PIDFLDCLL).trim();
    	if(!"".equals(phoneNum) && phoneNum.length() > 4) {
    		phoneNum = phoneNum.substring(phoneNum.length()-4, phoneNum.length());
    	}
    	
		logger.info("CONFLDNBR: " + getData(CONFLDNBR) + "\tSize:" + CONFLDNBR.length	
				+"\n"+"CUSTMRCDE: " + getData(CUSTMRCDE) + "\tSize:" + CUSTMRCDE.length	
				+"\n"+"CUSTMRNAM: " + getData(CUSTMRNAM) + "\tSize:" + CUSTMRNAM.length	
				+"\n"+"BIRFLDTXT: " + getData(BIRFLDTXT) + "\tSize:" + BIRFLDTXT.length	
				+"\n"+"SEXFLDMOW: " + getData(SEXFLDMOW) + "\tSize:" + SEXFLDMOW.length	
				+"\n"+"DOMFORKND: " + getData(DOMFORKND) + "\tSize:" + DOMFORKND.length	
				+"\n"+"PIDFLDKND: " + getData(PIDFLDKND) + "\tSize:" + PIDFLDKND.length	
				//핸드폰번호
				+"\n"+"PIDFLDCLL: " + "고객 휴대폰 뒷번호("+ phoneNum +")" + "\tSize:" + PIDFLDCLL.length
				+"\n"+"AUTFLDDTM: " + getData(AUTFLDDTM) + "\tSize:" + AUTFLDDTM.length	
				+"\n"+"AUTFLDCDE: " + getData(AUTFLDCDE) + "\tSize:" + AUTFLDCDE.length	
				+"\n"+"AUTFLDNAM: " + getData(AUTFLDNAM) + "\tSize:" + AUTFLDNAM.length	
				+"\n"+"DUPFLDTXT: " + getData(DUPFLDTXT) + "\tSize:" + DUPFLDTXT.length	
				+"\n"+"CONINFTXT: " + getData(CONINFTXT) + "\tSize:" + CONINFTXT.length);
    }
    
    public HashMap<String, String> getResult() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();

    	result.put("SVCFLDCDE", getData(SVCFLDCDE));
    	result.put("TRDFLDCDE", getData(TRDFLDCDE));
    	result.put("TRDSEQNBR", getData(TRDSEQNBR));
    	result.put("TELFLDTYP", getData(TELFLDTYP));
    	result.put("TRDFLDDAT", getData(TRDFLDDAT));
    	result.put("TRDFLDDTM", getData(TRDFLDDTM));
    	result.put("RSTFLDCDE", getData(RSTFLDCDE));
    	result.put("RSTFLDNAM", getData(RSTFLDNAM));
    	result.put("CONFLDNBR", getData(CONFLDNBR));
    	result.put("CUSTMRCDE", getData(CUSTMRCDE));
    	
    	return result;
    	
    }

}
