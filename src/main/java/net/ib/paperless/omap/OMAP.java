package net.ib.paperless.omap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OMAP {
	
	private static final Logger logger = LoggerFactory.getLogger(OMAP.class);

	int 	length 		= 	10;
	
	byte[]	TRDFLDLEN	=	new byte[10];	// 전문길이
    byte[]	SVCFLDCDE	=	new byte[4];	// 서비스코드
	byte[]	TRDFLDCDE	=	new byte[4];	// 거래코드
	byte[]	TRDSEQNBR	=	new byte[11];	// 거래일련번호
	byte[]  TELFLDTYP	=	new byte[2];	// 전문타입
	byte[]  TRDFLDDAT	=	new byte[8];	// 거래일자
	byte[]  TRDFLDDTM	=	new byte[6];	// 거래일시
	byte[]  RSTFLDCDE	=	new byte[2];	// 결과코드
	byte[]  RSTFLDNAM	=	new byte[200];	// 결과명
	byte[]  FILLER01	=	new byte[53];	// 필러
	
	public OMAP(){}

    public OMAP(String SVCFLDCDE,	String TRDFLDCDE,	String TRDSEQNBR,
					String TELFLDTYP,	String TRDFLDDAT,	String TRDFLDDTM,	String RSTFLDCDE,
					String RSTFLDNAM)throws UnsupportedEncodingException{
    		
		setData(this.SVCFLDCDE, SVCFLDCDE);		
		setData(this.TRDFLDCDE, TRDFLDCDE);
		setData(this.TRDSEQNBR, TRDSEQNBR);	
		setData(this.TELFLDTYP, TELFLDTYP);		
		setData(this.TRDFLDDAT, TRDFLDDAT);
		setData(this.TRDFLDDTM, TRDFLDDTM);	
		setData(this.RSTFLDCDE, RSTFLDCDE);		
		setData(this.RSTFLDNAM, RSTFLDNAM);
		setData(this.FILLER01, "");
    }
    
    public void setData(byte[] arryByte, String str)throws UnsupportedEncodingException{
		
		//문자열 값이 널이면 전부 공백으로 채우기 위해
		if(str == null){
			str ="";
		}

		//필드에 채울 문자열을 바이트배열로 변환
		byte[] bytes = str.getBytes("EUC_KR");

		//실제데이터 값의 바이트배열길이
		int endIdx = 0;
		
		//실제데이터 바이트배열 설정 
		if(arryByte.length >= bytes.length){ 
			endIdx = bytes.length;				//필드의 배열이 실제데이터배열보다 크거나 같을 경우
		}else{								 
			endIdx = arryByte.length;			//필드의 배열이 실제데이터배열보다 작을 경우
		}
		
		//필드배열에 실제데이터배열값으로 채운다.
		for(int i=0;i<endIdx;i++){		
			arryByte[i] = bytes[i];
		}
		//실제데이터값이 채워지지 않은 배열은 공백으로 채운다.
		for(int j=endIdx;j<arryByte.length;j++){
			arryByte[j] = ' ';
		}
		
		//총 byte 계산
		if(arryByte != TRDFLDLEN)
			length = length + arryByte.length;
	}
	public String getData(byte[] arryByte)throws UnsupportedEncodingException{

		if(arryByte==null){
			return "";
		}

		return new String(arryByte, "EUC-KR");

	}
	
	public void writeDataExternal(java.io.DataOutputStream stream)throws IOException{

		//write전 총 byte를 전문길이에 입력한다.
		setData(this.TRDFLDLEN, Integer.toString(length));	
		
    	stream.write(TRDFLDLEN);	
    	stream.write(SVCFLDCDE);		
    	stream.write(TRDFLDCDE);
    	stream.write(TRDSEQNBR);	
    	stream.write(TELFLDTYP);		
    	stream.write(TRDFLDDAT);
    	stream.write(TRDFLDDTM);	
    	stream.write(RSTFLDCDE);		
    	stream.write(RSTFLDNAM);
    	stream.write(FILLER01);		
    }
	 
    public void readDataExternal(java.io.DataInputStream stream)throws IOException{

    	stream.read(TRDFLDLEN, 0, TRDFLDLEN.length);	
    	stream.read(SVCFLDCDE, 0, SVCFLDCDE.length);		
    	stream.read(TRDFLDCDE, 0, TRDFLDCDE.length);
    	stream.read(TRDSEQNBR, 0, TRDSEQNBR.length);	
    	stream.read(TELFLDTYP, 0, TELFLDTYP.length);		
    	stream.read(TRDFLDDAT, 0, TRDFLDDAT.length);
    	stream.read(TRDFLDDTM, 0, TRDFLDDTM.length);	
    	stream.read(RSTFLDCDE, 0, RSTFLDCDE.length);		
    	stream.read(RSTFLDNAM, 0, RSTFLDNAM.length);
    	stream.read(FILLER01, 0, FILLER01.length);		
    	
    }
    
    public void print() throws IOException {	

    	logger.info("TRDFLDLEN: " + getData(TRDFLDLEN) + "\tSize:" + TRDFLDLEN.length
			+"\n"+"TRDFLDLEN: " + getData(TRDFLDLEN) + "\tSize:" + TRDFLDLEN.length	
			+"\n"+"SVCFLDCDE: " + getData(SVCFLDCDE) + "\tSize:" + SVCFLDCDE.length	
			+"\n"+"TRDFLDCDE: " + getData(TRDFLDCDE) + "\tSize:" + TRDFLDCDE.length	
			+"\n"+"TRDSEQNBR: " + getData(TRDSEQNBR) + "\tSize:" + TRDSEQNBR.length	
			+"\n"+"TELFLDTYP: " + getData(TELFLDTYP) + "\tSize:" + TELFLDTYP.length	
			+"\n"+"TRDFLDDAT: " + getData(TRDFLDDAT) + "\tSize:" + TRDFLDDAT.length	
			+"\n"+"TRDFLDDTM: " + getData(TRDFLDDTM) + "\tSize:" + TRDFLDDTM.length	
			+"\n"+"RSTFLDCDE: " + getData(RSTFLDCDE) + "\tSize:" + RSTFLDCDE.length	
			+"\n"+"RSTFLDNAM: " + getData(RSTFLDNAM) + "\tSize:" + RSTFLDNAM.length
			+"\n"+"FILLER01: " + getData(FILLER01) + "\tSize:" + FILLER01.length);			
		
    }
}
