package net.ib.paperless.omap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.clipsoft.org.json.simple.JSONValue;

public class OMAP_0003_rcv extends OMAP{
	
	private static final Logger logger = LoggerFactory.getLogger(OMAP_0003_rcv.class);

	byte[]  CONFLDNBR	=	new byte[20];	// 계약번호
	byte[]	CUSTMRCDE	=	new byte[20];	// 거래처코드
	byte[]	DOCFLDDIV	=	new byte[20];	// 약정서식분류
	byte[]	RECFLDCNT	=	new byte[3];	// 반복건
	
	ArrayList<byte[]> TYPCOLCDES = new ArrayList<byte[]>(); //서식항목코드
	ArrayList<byte[]> TYPCOLVALS = new ArrayList<byte[]>(); //서식항목코드값
	
    public OMAP_0003_rcv(){}

    public OMAP_0003_rcv(String SVCFLDCDE,	String TRDFLDCDE,	String TRDSEQNBR,
					String TELFLDTYP,	String TRDFLDDAT,	String TRDFLDDTM,	String RSTFLDCDE,
					String RSTFLDNAM,	String CONFLDNBR,	String CUSTMRCDE, String DOCFLDDIV, String RECFLDCNT, 	ArrayList<String> TYPCOLCDES,
					ArrayList<String> TYPCOLVALS
					)throws UnsupportedEncodingException{

		super(SVCFLDCDE,	TRDFLDCDE,	TRDSEQNBR, 	TELFLDTYP,	TRDFLDDAT,	TRDFLDDTM,	RSTFLDCDE, 	RSTFLDNAM);
		
		setData(this.CONFLDNBR, CONFLDNBR);		
		setData(this.CUSTMRCDE, CUSTMRCDE);
		setData(this.DOCFLDDIV, DOCFLDDIV);
		setData(this.RECFLDCNT, RECFLDCNT);	
		
		int listLength = Integer.valueOf(RECFLDCNT);
		
		for(int i = 0; i< listLength; i++){
			byte[]  TYPCOLCDE	=	new byte[20];	// 파일코드
			setData(TYPCOLCDE, TYPCOLCDES.get(i));
			this.TYPCOLCDES.add(TYPCOLCDE);
		}
		
		for(int i = 0; i< listLength; i++){
			byte[]  TYPCOLVAL	=	new byte[200];	// 파일명
			setData(TYPCOLVAL, TYPCOLVALS.get(i));
			this.TYPCOLVALS.add(TYPCOLVAL);
		}
		
    }

    //수신전용이라 사용안함
	public void writeDataExternal(java.io.DataOutputStream stream)throws IOException{
		super.writeDataExternal(stream);
    	stream.write(CONFLDNBR);		
    	stream.write(CUSTMRCDE);
    	
    }
    public void readDataExternal(java.io.DataInputStream stream)throws IOException{    	
    	super.readDataExternal(stream);
    	
    	stream.read(CONFLDNBR, 0, CONFLDNBR.length);		
    	stream.read(CUSTMRCDE, 0, CUSTMRCDE.length);	
    	stream.read(DOCFLDDIV, 0, DOCFLDDIV.length);	
    	stream.read(RECFLDCNT, 0, RECFLDCNT.length);
    	
    	int cnt = Integer.valueOf(getData(TRDFLDLEN)) - 363;
    	
    	byte[] messageByte = new byte[cnt];
    	stream.readFully(messageByte);	
		
    	int index = 0;
    	for(int i = 0; i< Integer.valueOf(getData(RECFLDCNT).trim()); i++){
    		byte[]  TYPCOLCDE	=	new byte[20];	// 서식항목코드
    		byte[]  TYPCOLVAL	=	new byte[200];	// 서식항목코드값
    		
    		System.arraycopy(messageByte, index, TYPCOLCDE, 0, TYPCOLCDE.length);
    		String cde = new String(TYPCOLCDE,"EUC-KR").trim();
    		index += TYPCOLCDE.length;
    		
    		/*if(cde.isEmpty()) {
    			//System.out.println("#### empty cde : " + new String(TYPCOLCDE,"EUC-KR"));
    			listLength --;
    			continue;
    		}*/
			setData(TYPCOLCDE, cde);
			
			System.arraycopy(messageByte, index, TYPCOLVAL, 0, TYPCOLVAL.length);
			String val = new String(TYPCOLVAL,"EUC-KR").trim();
			index += TYPCOLVAL.length;
			setData(TYPCOLVAL, val);
						
			this.TYPCOLCDES.add(TYPCOLCDE);
			this.TYPCOLVALS.add(TYPCOLVAL);			
		}
    }
    public void print() throws IOException {	
    	super.print();
		logger.info("CONFLDNBR: " + getData(CONFLDNBR) + "\tSize:" + CONFLDNBR.length	
				+"\n"+"CUSTMRCDE: " + getData(CUSTMRCDE) + "\tSize:" + CUSTMRCDE.length
				+"\n"+"DOCFLDDIV: " + getData(DOCFLDDIV) + "\tSize:" + DOCFLDDIV.length
				+"\n"+"RECFLDCNT: " + getData(RECFLDCNT) + "\tSize:" + RECFLDCNT.length);	
//		for(int i = 0; i< TYPCOLCDES.size(); i++){
//    		System.out.println("TYPCOLCDE: " + getData(TYPCOLCDES.get(i)) + "Size:" + TYPCOLCDES.get(i).length + " String Size:" 
//    						+ new String(TYPCOLCDES.get(i)).length());
//    		System.out.println("TYPCOLVAL: " + getData(TYPCOLVALS.get(i)) + "Size:" + TYPCOLVALS.get(i).length + " String Size:" 
//					+ new String(TYPCOLVALS.get(i)).length());
//		}
    }
    public HashMap<String, String> getResult() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();

    	result.put("TRDFLDLEN", getData(TRDFLDLEN));
    	result.put("SVCFLDCDE", getData(SVCFLDCDE));
    	result.put("TRDFLDCDE", getData(TRDFLDCDE));
    	result.put("TRDSEQNBR", getData(TRDSEQNBR));
    	result.put("TELFLDTYP", getData(TELFLDTYP));
    	result.put("TRDFLDDAT", getData(TRDFLDDAT));
    	result.put("TRDFLDDTM", getData(TRDFLDDTM));
    	result.put("RSTFLDCDE", getData(RSTFLDCDE));
    	result.put("RSTFLDNAM", getData(RSTFLDNAM));
    	result.put("CONFLDNBR", getData(CONFLDNBR));
    	result.put("CUSTMRCDE", getData(CUSTMRCDE));
    	result.put("DOCFLDDIV", getData(DOCFLDDIV));
    	result.put("RECFLDCNT", getData(RECFLDCNT));
    	
    	return result;
   
    }
    
    public Map<String, String> getTypcolcdesListData() throws UnsupportedEncodingException{
    	Map<String,String> result = new LinkedHashMap<String,String>();
    	for(int i = 0; i< TYPCOLCDES.size(); i++){
    		result.put(getData(TYPCOLCDES.get(i)).trim(), getData(TYPCOLVALS.get(i)).trim());
		}
    	return result;
    }
    
    public String getTypcolcdesListDataOrder() throws UnsupportedEncodingException  {
    	return JSONValue.toJSONString(getTypcolcdesListData());
    }
}
