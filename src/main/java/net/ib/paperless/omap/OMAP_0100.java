package net.ib.paperless.omap;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OMAP_0100 extends OMAP{
	
	private static final Logger logger = LoggerFactory.getLogger(OMAP_0100.class);
	
	byte[]  PARAM01	=	new byte[20];	// 파라메터_01
	byte[]  PARAM02	=	new byte[100];	// 파라메터_02
	byte[]  PARAM03	=	new byte[100];	// 파라메터_03
	byte[]  PARAM04	=	new byte[100];	// 파라메터_04
	
    public OMAP_0100(){}

    public OMAP_0100(String SVCFLDCDE,	String TRDFLDCDE,	String TRDSEQNBR,
					String TELFLDTYP,	String TRDFLDDAT,	String TRDFLDDTM,	String RSTFLDCDE,
					String RSTFLDNAM,	String PARAM01,		String PARAM02,
					String PARAM03,		String PARAM04
					)throws UnsupportedEncodingException{

		super(SVCFLDCDE,	TRDFLDCDE,	TRDSEQNBR, 	TELFLDTYP,	TRDFLDDAT,	TRDFLDDTM,	RSTFLDCDE, 	RSTFLDNAM);
		setData(this.PARAM01, PARAM01);		
		setData(this.PARAM02, PARAM02);
		setData(this.PARAM03, PARAM03);
		setData(this.PARAM04, PARAM04);
    }

	public void writeDataExternal(java.io.DataOutputStream stream)throws IOException{
		super.writeDataExternal(stream);
    	stream.write(PARAM01);			
    	stream.write(PARAM02);
    	stream.write(PARAM03);		
    	stream.write(PARAM04);		
    }
    public void readDataExternal(java.io.DataInputStream stream)throws IOException{
    	super.readDataExternal(stream);
    	stream.read(PARAM01, 0, PARAM01.length);			
    	stream.read(PARAM02, 0, PARAM02.length);
    	stream.read(PARAM03, 0, PARAM03.length);		
    	stream.read(PARAM04, 0, PARAM04.length); 		
    }
    public void print() throws IOException {	
    	super.print();
    	logger.info("PARAM01: " + getData(PARAM01) + "\tSize:" + PARAM01.length
			+"\n"+"PARAM02: " + getData(PARAM02) + "\tSize:" + PARAM02.length	
			+"\n"+"PARAM03: " + getData(PARAM03) + "\tSize:" + PARAM03.length
			+"\n"+"PARAM04: " + getData(PARAM04) + "\tSize:" + PARAM04.length);			
		
    }
    
    public HashMap<String, String> getResult() throws UnsupportedEncodingException{
    	HashMap<String,String> result = new HashMap<String,String>();

    	result.put("SVCFLDCDE", getData(SVCFLDCDE));
    	result.put("TRDFLDCDE", getData(TRDFLDCDE));
    	result.put("TRDSEQNBR", getData(TRDSEQNBR));
    	result.put("TELFLDTYP", getData(TELFLDTYP));
    	result.put("TRDFLDDAT", getData(TRDFLDDAT));
    	result.put("TRDFLDDTM", getData(TRDFLDDTM));
    	result.put("RSTFLDCDE", getData(RSTFLDCDE));
    	result.put("RSTFLDNAM", getData(RSTFLDNAM));
    	result.put("PARAM01", getData(PARAM01));
    	result.put("PARAM02", getData(PARAM02));
    	result.put("PARAM03", getData(PARAM03));
    	result.put("PARAM04", getData(PARAM04));
    	
    	return result;
    	
    }


}
