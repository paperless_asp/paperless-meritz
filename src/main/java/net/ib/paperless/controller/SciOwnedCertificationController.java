package net.ib.paperless.controller;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import net.ib.paperless.common.ApiResponse;
import net.ib.paperless.common.UUIDs;
import net.ib.paperless.domain.Billing;
import net.ib.paperless.domain.PhoneAuthCode;
import net.ib.paperless.domain.Progress;
import net.ib.paperless.domain.User;
import net.ib.paperless.omap.OMAP_0001;
import net.ib.paperless.omap.OMAP_0002;
import net.ib.paperless.omap.OMAP_0006;
import net.ib.paperless.scoket.SocketManager;
import net.ib.paperless.service.BillingLogService;
import net.ib.paperless.service.OmapLogService;
import net.ib.paperless.service.PhoneAuthService;
import net.ib.paperless.service.ProgressService;
import net.ib.paperless.supersms.SupersmsApi;
import net.ib.paperless.support.SessionManager;


@RequestMapping("/ajax/**")
@Controller
public class SciOwnedCertificationController {
	
	private static final Logger logger = LoggerFactory.getLogger(SciOwnedCertificationController.class);


	@Autowired
	ProgressService progressService;
	@Autowired
	PhoneAuthService phoneAuthService;
	@Autowired
	OmapLogService omapLogService;
	@Autowired
	BillingLogService billingLogService;
	@Autowired
	SessionManager sessionManager;
	
	//SCI인증 후 결과 송신
	@RequestMapping(value="/setIdentityAuthInfo",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public ApiResponse<Map<String,Object>> setIdentityAuthInfo(@RequestBody Map<String,Object> params , HttpServletRequest request) throws IOException{
		
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();

		String progress_id 	= params.get("progress_id").toString();
		
		//ProgressInfo 가져오기
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);

		Date date = new Date();
		
		//전문 데이터 생성
		String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
		String BIRFLDTXT 	= params.get("BIRFLDTXT").toString();
		String SEXFLDMOW 	= params.get("SEXFLDMOW").toString();
		String DOMFORKND 	= params.get("DOMFORKND").toString();
		String PIDFLDKND 	= params.get("PIDFLDKND").toString();
		String PIDFLDCLL 	= params.get("PIDFLDCLL").toString();
		String AUTFLDDTM 	= params.get("AUTFLDDTM").toString();
		String AUTFLDCDE 	= params.get("AUTFLDCDE").toString();
		String AUTFLDNAM 	= params.get("AUTFLDNAM").toString();
		String DUPFLDTXT 	= params.get("DUPFLDTXT").toString();
		String CONINFTXT 	= params.get("CONINFTXT").toString();
		String CONFLDNBR	= resProgress.getContract_code();	
		String CUSTMRCDE	= resProgress.getCustomer_code();
		String CUSTMRNAM	= "인포뱅크";	// 거래처명
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
		
		//전문 데이터 등록
		OMAP_0001 td1 = new OMAP_0001("OMAP", 
				"0001", 
				TRDSEQNBR, 
				resProgress.getService_type(), 
				dateFormat.format(date), 
				timeFormat.format(date), 
				"",
				"", 
				CONFLDNBR,
				CUSTMRCDE,
				CUSTMRNAM, 
				BIRFLDTXT, 
				SEXFLDMOW, 
				DOMFORKND, 
				PIDFLDKND, 
				PIDFLDCLL, 
				AUTFLDDTM, 
				AUTFLDCDE, 
				AUTFLDNAM, 
				DUPFLDTXT, 
				CONINFTXT);
		
		//전송 전문 저장
		HashMap<String, String> sendLog = td1.getResult();
		logger.info("## OMAP_0001 Log Save(/setIdentityAuthInfo) - seq : "+resProgress.getSeq());
		logger.info("## OMAP_0001 Log Save(/setIdentityAuthInfo) [{}]"+ sendLog);
		omapLogService.omapLogInsert(sendLog);
		
		//수신 전문 생성
		OMAP_0001 td2 = new OMAP_0001();
		
		//전문  전송
		SocketManager.connection(td1, td2);
		td2.print();
		
		//수신 전문 저장
		HashMap<String, String> receiveLog = td2.getResult();
		omapLogService.omapLogUpdate(receiveLog);
		
		responseJson.setResult(false);
		
		//sci 인증 결과코드
		//00:성공 , 01:등록실패, 03:계약정보없음, 04:고객정보없음, 05:휴대전화번호상이, 06:유효기간만료
		String resultCode = receiveLog.get("RSTFLDCDE").trim();
		if("00".equals(resultCode)){
			//Billing Insert
			Billing bdata = new Billing();
			bdata.setProgress_id(progress_id);
			bdata.setType(1);//1 SCI인증 성공
			bdata.setSuccess_yn("y");
			billingLogService.billingLogInsert(bdata);
			logger.info("### SCI인증 Billing Log Insert ### Seq : "+ bdata.getSeq()+", Type : " + bdata.getType()
			+ ", Success_Yn : " + bdata.getSuccess_yn()+ ", Progress_id : " + bdata.getProgress_id());
			
			String uuids = UUIDs.createNameUUID(PIDFLDCLL.getBytes()).toString();
			
			HashMap<String, String> updateInfo = new HashMap<String, String>();
			updateInfo.put("uuids", uuids);
			updateInfo.put("progress_id", progress_id);
			updateInfo.put("progress_status", "2");
			if(progressService.uuidsPhonenumberInsert(updateInfo) == 1){
				progressService.identityChkUpdate(progress_id);
				responseJson.setResult(true);
				//세션 셋팅
				setLoginSession(request, progress_id, uuids);
			}
		}else {
			responseJson.setResultType(resultCode);
			responseJson.setMessage(receiveLog.get("RSTFLDNAM").trim());
		}

		return responseJson;
	}
	
	private void setLoginSession(HttpServletRequest request, String progress_id, String uuids) {			
			HttpSession session = request.getSession();
			User user = new User();
			user.setLoanId(progress_id);
			user.setLoanName(progress_id);
			user.setUserKey(uuids);
			session.setAttribute("user", user);
			
//			String sessionId = request.changeSessionId();
			String sessionId = request.getRequestedSessionId();
			logger.info("## LOGIN SESSION-ID : " + sessionId);
			String remoteAddr = request.getHeader("REMOTE_ADDR");
			session.setAttribute("remoteaddr", remoteAddr);
			sessionManager.setHttpSession(sessionId, session, remoteAddr);
	}

	//핸드폰소유인증 인증번호요청 ajax
	@RequestMapping(value="/setHavePhoneAuth",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public Map<String, Object> setHavePhoneAuth(@RequestBody Map<String,Object> params , HttpServletRequest request) throws IOException{
		Map<String, Object> resultMap = new HashMap<String, Object>();
		//관리번호, 핸드폰 번호
		String progress_id 	= params.get("progress_id").toString();
		String phone_num 	= params.get("phone_num").toString();
		
		//휴대폰 번호 uuid 로직 비교
		String uuids = UUIDs.createNameUUID(phone_num.getBytes()).toString();
		String target_uuids = progressService.uuidsPhonenumberSelectByUuid(progress_id);
		
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
		
		//일반적인 전자청약의 소유인증일 경우
		if(resProgress.getProgress_type().equals("1")){
			if(!uuids.equals(target_uuids)) {
				resultMap.put("code", "F");
				resultMap.put("text", "계약한 휴대폰 번호와 다릅니다.");
				return resultMap;
			}			
//			resultMap = sendMessageAndOmap0006(progress_id, phone_num, resProgress);
			
		//파일업로드 ProgressType = 2이고 ServiceType =10, 11인 소유인증 
		//0006전문으로 휴대폰번호를 비교하여 성공여부를 넘겨주기 때문에 일단 uuids insert
		}else if(resProgress.getProgress_type().equals("2") && resProgress.getService_type().startsWith("1")) {
			HashMap<String, String> updateInfo = new HashMap<String, String>();
			updateInfo.put("uuids", uuids);
			updateInfo.put("progress_id", progress_id);
			updateInfo.put("progress_status", "1");
			//uuids 휴대폰 번호 업데이트
			progressService.uuidsPhonenumberInsert(updateInfo);
		}						
		
		PhoneAuthCode pac = new PhoneAuthCode();
		
		pac.setProgress_id(progress_id);
		pac.setTel_numer(phone_num);
		pac.setAuth_code("");
		
		//휴대폰번호 본인여부 전문 송신 OMAP0006
		OMAP_0006 resultOmap_0006 = sendOmap0006(resProgress, pac, "", "", "");
		
		Map<String, String> receiveLog = resultOmap_0006.getResult();
		//결과코드 00 성공
		String resultCode = receiveLog.get("RSTFLDCDE").trim();
		String resultMessage = receiveLog.get("RSTFLDNAM").trim();
		if(!"00".equals(resultCode)){
			resultMap.put("code", "F");
			resultMap.put("text", resultMessage != null ? resultMessage : "인증이 실패하였습니다. 잠시후 다시 시도해 주세요.");
			return resultMap;
		}
		
		resultMap = sendMessageAndOmap0006(progress_id, phone_num, resProgress);
		return resultMap;
	}

	/**
	 * 메시지 송신과 0006전문 송신
	 * @param progress_id
	 * @param phone_num
	 * @param resProgress
	 * @return
	 * @throws IOException
	 */
	private Map<String, Object> sendMessageAndOmap0006(String progress_id, String phone_num, Progress resProgress)
			throws IOException {
		Map<String, Object> resultMap;
		//인증번호 생성 및 supersmsApi 호출
		Random generator = new Random();   
		String random_code = String.valueOf(1000+generator.nextInt(9000)); 
		String text = "GoodPaper 인증번호는 [" + random_code + "]입니다.";
//		resultMap = SupersmsApi.sendMessage("0316281586", text, phone_num);
		resultMap = SupersmsApi.sendMessage("15889666", text, phone_num);
		
		PhoneAuthCode pac = new PhoneAuthCode();
		
		pac.setProgress_id(progress_id);
		pac.setTel_numer(phone_num);

		pac.setAuth_code(random_code);
		phoneAuthService.phoneAuthInfoInsert(pac);
		
		//인증번호 전문 송신 OMAP0006
		List<Map<String, Object>> destinations = (List) resultMap.get("destinations");
		String smsResult = "";
		smsResult = (String) destinations.get(0).get("status");
		String errorText = "";
		errorText = (String) destinations.get(0).get("errorText");
		
		//Billing Insert
		Billing bdata = new Billing();
		if("R000".equals(smsResult)) {
			bdata.setProgress_id(progress_id);
			bdata.setType(2);//2 소유인증 관련 문자성공하면 이력 insert
			bdata.setSuccess_yn("y");
			billingLogService.billingLogInsert(bdata);
		}else {
			bdata.setSuccess_yn("n");
			billingLogService.billingLogInsert(bdata);
		}
		logger.info("### 소유인증 관련 문자 Billing Log Insert ### Seq : "+ bdata.getSeq()+", Type : " + bdata.getType()
		+ ", Success_Yn : " + bdata.getSuccess_yn()+ ", Progress_id : " + bdata.getProgress_id());
		
		sendOmap0006(resProgress, pac, text, smsResult, errorText);
		return resultMap;
	}
	
	private OMAP_0006 sendOmap0006(Progress resProgress, PhoneAuthCode pac, String text, String smsResult, String errorText) throws IOException {		
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

		String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
		String SNDFLDNBR = dateFormat.format(date) + getZeroPlusNumber(omapLogService.omapLog0006NumberSelect() + 1); //발송번호
		String CONFLDNBR = ""; //계약번호
		String CUSTMRCDE = ""; //거래처코드
		String PARAM1 = "";
		String PARAM2 = "";
		String PARAM3 = "";
		String PARAM4 = "";
		String PIDFLDCLL = pac.getTel_numer(); //휴대폰 번호
//		String CALLBACK = "0316281586";//발신번호
		String CALLBACK = "0263093700";//발신번호
		String SNDFLDDTM = dateFormat.format(date) + timeFormat.format(date);  //발송일시
		String SNDFLDMSG = text;  //발송메시지
		String SNDFLDSTA = "R000".equals(smsResult) ? "00" : "01";  //발송결과 R000가 들어오면 성공이므로 00, 그외에는 실패
		String SNDSTARSN = errorText;  //발송결과사유
		String SNDMSGKND = "0";  //발송메시지종류
		String SNDFLDRSN = "01";	//발송사유
		
		//progress_type으로 분기처리
		String progress_type = resProgress.getProgress_type();		
		if("1".equals(progress_type)) {
			CONFLDNBR = resProgress.getContract_code();	//계약번호
			CUSTMRCDE = resProgress.getCustomer_code();	//거래처코드
		}else {
			CONFLDNBR = resProgress.getParam1();	//계약번호
			PARAM1 = resProgress.getParam1();
			PARAM2 = resProgress.getParam2();
			PARAM3 = resProgress.getParam3();
			PARAM4 = resProgress.getParam4();
		}		
		
		OMAP_0006 td1 = new OMAP_0006("OMAP", 
				"0006", 
				TRDSEQNBR, 
				resProgress.getService_type(), 
				dateFormat.format(date), 
				timeFormat.format(date), 
				"",
				"",
				SNDFLDNBR,
				CONFLDNBR,
				CUSTMRCDE,
				PARAM1,
				PARAM2,
				PARAM3,
				PARAM4,
				PIDFLDCLL,
				CALLBACK,
				SNDFLDDTM,
				SNDFLDMSG,
				SNDFLDSTA,
				SNDSTARSN,
				SNDMSGKND,
				SNDFLDRSN);
		
		//전송 전문 저장
		HashMap<String, String> sendLog = td1.getResult();
		logger.info("## OMAP_0006 Log Save(/setHavePhoneAuth) - seq : "+resProgress.getSeq());
		logger.info("## OMAP_0006 (/setHavePhoneAuth) [{}]"+ sendLog);
		omapLogService.omapLogInsert(sendLog);
		
		OMAP_0006 td2 = new OMAP_0006();
		SocketManager.connection(td1, td2);
		td2.print();

		//수신 전문 저장
		HashMap<String, String> receiveLog = td2.getResult();
		omapLogService.omapLogUpdate(receiveLog);
		
		return td2;
	}

	/**
	 * 0006번에서 YYYYMMDD###### (년월일+일련번호 '000001') 서식을 맞추기위해 만든 메소드
	 * @param omapLog0006NumberSelect
	 * @return
	 */
	private String getZeroPlusNumber(int omapLog0006NumberSelect) {		
		return String.format("%06d", omapLog0006NumberSelect);
	}

	//핸드폰소유인증 확인 ajax
	@RequestMapping(value="/isPhoneAuthNum",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public Map<String, Object> isPhoneAuthNum(@RequestBody Map<String,Object> params , HttpServletRequest request) throws IOException{
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		//관리 번호, 휴대폰 번호, 인증 번호
		String progress_id 	= params.get("progress_id").toString();
		String phone_num 	= params.get("phone_num").toString();
		String auth_code 	= params.get("auth_num").toString();
		
		PhoneAuthCode pac = new PhoneAuthCode();
		pac.setTel_numer(phone_num);
		pac.setAuth_code(auth_code);

		PhoneAuthCode isPac = phoneAuthService.phoneAuthInfoSelectByTelAuth(pac);
		
		String code = null;
		String text = null;
		
		if(isPac != null) {
			if(auth_code.equals(isPac.getAuth_code())) {
				//인증 성공
				text = "인증이 완료 되었습니다.";
				code = "Y";
				//인증 성공 후 삭제
				phoneAuthService.phoneAuthInfoDeleteByTelAuth(isPac);
				
				//세션 셋팅
				String uuids = progressService.uuidsPhonenumberSelectByUuid(progress_id);
				setLoginSession(request, progress_id, uuids);
			}else {
				//인증 실패 처리
				if(isPac.getFail_count() < 3) {
					isPac.setFail_count(isPac.getFail_count() + 1);
					text = "인증번호가 올바르지 않습니다.";
					code = "N";
					//failcount 증가 update
					phoneAuthService.phoneAuthInfoUpdateFailCountByTelAuth(isPac);
				}else {
					text =  "인증번호를 3회이상 잘못 입력 하였습니다.\n인증번호 재요청 후 이용해 주시기 바랍니다.";
					code = "F";
					//인증횟수초과 삭제 초기화
					phoneAuthService.phoneAuthInfoDeleteByTelAuth(isPac);
				}
			}
			
			Progress progress = new Progress();
			progress.setSeq(Integer.parseInt(progress_id));
			Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
			if(resProgress.getProgress_type().equals("1")){
				//인증 후 전문 송신
				boolean sendOmap0002Result = sendOmap0002(isPac, progress_id, code, text);
//				resultMap.put("sendOmap0002Result", sendOmap0002Result);
				if(!sendOmap0002Result) {
					//전문이 성공하지 못할 경우
					text = "인증번호 확인 중 오류가 발생하였습니다.";
					code = "F";
				}
			}
		}else {
			//그 외 에러
			text = "인증번호 확인 중 오류가 발생하였습니다.";
			code = "F";
		}
		
		resultMap.put("code", code);
		resultMap.put("text", text);
		return resultMap;
	}	
	
	@RequestMapping(value="/isProgressStatus",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public ApiResponse<Map<String,Object>> isProgressStatus(@RequestParam(value="progress_id", required=true) String progress_id, HttpServletRequest request) throws IOException{
		
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		
		//ProgressInfo 가져오기
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
		responseJson.setMessage(resProgress.getProgress_status());
		//서비스 타입 넘김
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("serviceType", resProgress.getService_type().trim());
		responseJson.setMap(map);
		
		logger.info("## isProgressStatus (/isProgressStatus) Progress_status : " + resProgress.getProgress_status());

		return responseJson;
	}
	
	private boolean sendOmap0002(PhoneAuthCode isPac, String progress_id, String authCode, String authResult) throws IOException {
		
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
		
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

		String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
		String CONFLDNBR = resProgress.getContract_code();	
		String CUSTMRCDE = resProgress.getCustomer_code();
		String PIDFLDCLL = isPac.getTel_numer();
		String AUTFLDDTM = dateFormat.format(date) + timeFormat.format(date);
		String AUTFLDCDE = authCode;
		String AUTFLDNAM = authResult;
		
		OMAP_0002 td1 = new OMAP_0002("OMAP", 
				"0002", 
				TRDSEQNBR, 
				resProgress.getService_type(), 
				dateFormat.format(date), 
				timeFormat.format(date), 
				"",
				"",
				CONFLDNBR,
				CUSTMRCDE,
				PIDFLDCLL,
				AUTFLDDTM,
				AUTFLDCDE,
				AUTFLDNAM);
		
		//전송 전문 저장
		HashMap<String, String> sendLog = td1.getResult();
		logger.info("## OMAP_0002 Log Save(/isPhoneAuthNum) - seq : "+resProgress.getSeq());
		logger.info("## OMAP_0002 (/isPhoneAuthNum) [{}]"+ sendLog);
		omapLogService.omapLogInsert(sendLog);
		
		OMAP_0002 td2 = new OMAP_0002();
		SocketManager.connection(td1, td2);
		td2.print();

		//수신 전문 저장
		HashMap<String, String> receiveLog = td2.getResult();
		omapLogService.omapLogUpdate(receiveLog);
		
		boolean sendOmap0002Result = false;

		//결과값 성공인지 아닌지 구분필요??
		if("00".equals(receiveLog.get("RSTFLDCDE"))){
			sendOmap0002Result = true;
		}
		return sendOmap0002Result;
	}

}
