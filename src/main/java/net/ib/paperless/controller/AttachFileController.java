package net.ib.paperless.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import net.ib.paperless.common.ApiResponse;
import net.ib.paperless.domain.EformAttach;
import net.ib.paperless.domain.Progress;
import net.ib.paperless.service.AttachService;
import net.ib.paperless.service.ProgressService;

@RestController
@RequestMapping("/attach/**")
public class AttachFileController {
	
	private static final Logger logger = LoggerFactory.getLogger(AttachFileController.class);
	
	@Autowired
	AttachService attachService;
	@Autowired
	ProgressService progressService;
	
	// 첨부 업로드
	@RequestMapping(value="/upload", method=RequestMethod.POST)
	public ApiResponse<Map<String,Object>> attachFile(@RequestParam("progress_id") String progress_id, 
			@RequestParam("attach_id") int attach_id,
			@RequestParam("attach") MultipartFile attach,
			@RequestParam("attach_cnt") int attach_cnt) throws IOException {

		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		
		Integer file_cnt = attach_cnt;
		
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));

		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
		
		//성공처리가 되었는데도 2번실행되어 임시 처리
		if(!"5".equals(resProgress.getProgress_status())){
			if(resProgress.getProgress_type().equals("2") && (resProgress.getService_type().equals("01") || resProgress.getService_type().equals("11"))){
				file_cnt = resProgress.getFile_cnt();
			}
			logger.info("## attachFile (/upload) - progress_id [{}]", progress_id);
			logger.info("## attachFile (/upload) - attach_id [{}]", attach_id);
			logger.info("## attachFile (/upload) - file name [{}]", attach.getOriginalFilename());
			logger.info("## attachFile (/upload) - resProgress.getFile_cnt() [{}]", resProgress.getFile_cnt());
			logger.info("## attachFile (/upload) - file_cnt [{}]", file_cnt);
			String param1 = null;
			if(resProgress.getParam1() != null) {
				param1 = resProgress.getParam1().trim();
			}else {
				param1 = resProgress.getContract_code().trim();
			}
			boolean result = attachService.eFormAttachFileSave(progress_id, attach_id, attach, file_cnt, resProgress.getService_type().trim(), resProgress.getProgress_type(), param1);
			logger.info("## attachFile (/upload) - FileUploadResult [{}]", result);
			responseJson.setResult(result);
		}else {
			responseJson.setResult(true);
		}
		
		responseJson.setMessage("");
		return responseJson;
	}

	// type 00, 10 파일개수 제한 없을때 완료버튼으로 파일업로드 수행할때
	@RequestMapping(value="/completeUpload", method=RequestMethod.POST)
	public ApiResponse<Map<String,Object>> completeUpload(@RequestParam("progress_id") String progress_id) throws IOException {

		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();

		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));

		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);

		//5 - 처리완료 상태가 아니고, Progress_type이 2인 파일업로드이면서 파일갯수제한이 없을 때
		if(!"5".equals(resProgress.getProgress_status())){
			if(resProgress.getProgress_type().equals("2") && (resProgress.getService_type().equals("00") || resProgress.getService_type().equals("10"))){
				logger.info("## completeUpload (/completeUpload) - progress_id [{}]", progress_id);
				String param1 = null;
				if(resProgress.getParam1() != null) {
					param1 = resProgress.getParam1().trim();
				}else {
					param1 = resProgress.getContract_code().trim();
				}
				boolean result = attachService.attachCompleteFileSave(progress_id, param1);
				
				logger.info("## completeUpload (/completeUpload) - FileUploadResult [{}]", result);
				responseJson.setResult(result);
			}
		}else {
			responseJson.setResult(false);
		}

		responseJson.setMessage("");
		return responseJson;
	}


	// 필수 첨부 리스트 조회
	@RequestMapping(value="/getAttachList", method={RequestMethod.GET , RequestMethod.POST})
	public ApiResponse<Map<String,Object>> getRequiredAttachList(@RequestParam("progress_id") String progress_id) {
		
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));

		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
		
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		responseJson.setResult(true);
		responseJson.setType(resProgress.getService_type());
		
		if(resProgress.getProgress_type().equals("1")){
			List<EformAttach> eFormList = attachService.eFormAttachSelectByProgressId1(progress_id);
			responseJson.setTotalItems(eFormList.size());
			responseJson.setList(eFormList);
		}
		else{
			Integer file_cnt = 10;
			
			if(resProgress.getService_type().equals("01") || resProgress.getService_type().equals("11")){
				file_cnt = resProgress.getFile_cnt();
			}
			
			List<EformAttach> eFormList = new ArrayList<EformAttach>();
			for(int i = 0; i<file_cnt; i++){
				//주민등록증은 무조건 필수처리
				String file_name = "이미지".concat(Integer.toString(i));
				if(i == 0){
					file_name = "주민등록증";
				}
				EformAttach eformAttach = new EformAttach();
				eformAttach.setSeq(i);
				eformAttach.setProgress_id(progress_id);
				eformAttach.setEform_attach_id(Integer.toString(i+1));
				eformAttach.setUpload_yn(0);
				eformAttach.setTransfer_yn(0);
				eformAttach.setAdmin_confirm_yn(0);
				eformAttach.setId(Integer.toString(i));
				eformAttach.setName(file_name);
				eformAttach.setEform_id(Integer.toString(i));
				
				eFormList.add(eformAttach);
			}
			
			responseJson.setTotalItems(11);
			responseJson.setList(eFormList);
		}

		return responseJson;
	}
}
