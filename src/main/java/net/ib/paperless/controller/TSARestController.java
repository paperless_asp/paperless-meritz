package net.ib.paperless.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.ib.paperless.domain.Progress;
import net.ib.paperless.domain.Response;
import net.ib.paperless.omap.OMAP_0004;
import net.ib.paperless.scoket.SocketManager;
import net.ib.paperless.service.OmapLogService;
import net.ib.paperless.service.ProgressService;
import net.ib.paperless.service.TsaService;



@RestController
public class TSARestController {
	
	private static final Logger logger = LoggerFactory.getLogger(TSARestController.class);
	
	@Autowired
	TsaService tsaService;
	
	@Autowired
	ProgressService progressService;
	
	@Autowired
	OmapLogService omapLogService;
		
	@RequestMapping({"/tsa"})
	public Response tsa(@RequestParam(value="progress_id", required=true) String progress_id, 
						@RequestParam(value="ct_code", required=true) String ct_code) throws IOException {
		Response response = new Response();
		
		logger.info("## progress_id (/tsa) : " + progress_id);
		logger.info("## ct_code (/tsa) : " + ct_code);
		
		//파일명 정의를 위한 유일날짜 정의
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
		String timeStampNow = dateFormat.format(date) + timeFormat.format(date);
		
		//파일 이름을 계약번호로 함
		boolean ret = tsaService.timestampCertificate(ct_code, progress_id, timeStampNow);
		if (ret) {
			response.setResponse("OK");
			response.setNote("완료");	
			
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("eform_yn", 1);
			map.put("progressId", progress_id);

			//완료 전문 송신
			Map<String, Object> omapResult = sendOmap0004(progress_id, ct_code, timeStampNow);
			boolean sendOmap0004Result = (boolean) omapResult.get("sendOmap0004Result");
			OMAP_0004 omap0004rcv = (OMAP_0004) omapResult.get("omap0004rcv");

			if(sendOmap0004Result) {
				//수신된 파일코드, 파일명
				Map<String, String> filecdenamList = omap0004rcv.getReqfilListData();
				logger.info("## 수신된 파일코드 , 파일명 JsonData : {}" + new JSONObject(filecdenamList));		
				//tb_progress_status 상태값 업데이트
				tsaService.eformCompletUpdate(progress_id);
				
				//tb_progress progress_status 상태값 업데이트
				Progress progress = new Progress();
				progress.setSeq(Integer.parseInt(progress_id));
				progress.setProgress_status("4");
				progressService.progressStatusUpdate(progress);
			}else {
				response.setResponse("FAIL");
				response.setNote("실패");
				logger.info("## Status Update Fail [{}]", progress_id);
			}
		} else {			
			response.setResponse("FAIL");
			response.setNote("실패");
			logger.info("## TSA Certifacate Fail [{}]", progress_id);
		}
		return response;
	}
	
	private Map<String, Object> sendOmap0004(String progress_id, String ct_code, String timeStampNow) throws IOException {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		//사인된 전자문서파일명(PDF)
		String saveFileName = ct_code + "_" + progress_id + "_" + timeStampNow + "_signed.pdf";
		
		//전자문서파일명(tif)
		//String saveTifFileName = ct_code + "_" + progress_id + "_" + timeStampNow + "_tif.tif";
		
		//전자문서파일명(PDF)
		String saveOriginalPdfFileName = ct_code + "_" + progress_id + "_" + timeStampNow + ".pdf";
		
		//CMS출금이체파일명(jpg)
		String saveCmsJpgFileName = ct_code + "_" + progress_id + "_" + timeStampNow + "_cms.jpg";
		
		//ProgressInfo 가져오기
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);

		Date date = new Date();

		//전문 데이터 생성
		String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
		String CONFLDNBR	= resProgress.getContract_code();	
		String CUSTMRCDE	= resProgress.getCustomer_code();
		String EDFPDFNAM 	= saveFileName; //전자문서파일명(PDF)
		String CMSJPGNAM	= saveCmsJpgFileName;//CMS출금이체파일명		
		
		
		ArrayList<String> codeList	= new ArrayList<String>();
		ArrayList<String> fileList	= new ArrayList<String>();
		//tif 파일명 넣기
		codeList.add("ED010001");				
		fileList.add(saveOriginalPdfFileName);
		
		//String format 앞에 자릿수만큼 0으로 채움
		String file_cnt = String.format("%03d", codeList.size());	
		
		
		String RECFLDCNT	= file_cnt;//반복건수
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
		
		OMAP_0004 td1 = new OMAP_0004("OMAP",
				"0004",
				TRDSEQNBR,
				resProgress.getService_type(),
				dateFormat.format(date), 
				timeFormat.format(date), 
				"",
				"",
				CONFLDNBR,
				CUSTMRCDE,
				EDFPDFNAM,
				CMSJPGNAM,
				RECFLDCNT,
				codeList,
				fileList);
		//전송 전문 저장
		HashMap<String, String> sendLog = td1.getResult();
		logger.info("## OMAP_0004 Log Save(/tsa) - seq : "+resProgress.getSeq());
		logger.info("## OMAP_0004 (/tsa) [{}]"+ sendLog);
		omapLogService.omapLogInsert(sendLog);

		//수신 전문 생성
		OMAP_0004 td2 = new OMAP_0004();

		//전문  전송
		SocketManager.connection(td1, td2);
		td2.print();

		//수신 전문 저장
		HashMap<String, String> receiveLog = td2.getResult();
		omapLogService.omapLogUpdate(receiveLog);		

		boolean sendOmap0004Result = false;
		
		if("00".equals(receiveLog.get("RSTFLDCDE"))){
			sendOmap0004Result = true;
		}
			
		returnMap.put("sendOmap0004Result", sendOmap0004Result);
		returnMap.put("omap0004rcv", td2);
		return returnMap;
	}
}
