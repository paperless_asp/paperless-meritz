package net.ib.paperless.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

//import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.ib.paperless.domain.AttachInfo;
import net.ib.paperless.domain.BankInfo;
import net.ib.paperless.domain.EformAttach;
import net.ib.paperless.domain.Progress;
import net.ib.paperless.domain.ProgressStatus;
import net.ib.paperless.openplatform.OpenPlatformAPI;
import net.ib.paperless.service.AccountService;
import net.ib.paperless.service.AttachService;
import net.ib.paperless.service.ProgressService;

//import net.ib.paperless.config.SftpConfig.UploadGateway;


@Controller
public class WebController {
	
	private static final Logger logger = LoggerFactory.getLogger(WebController.class);

	@Autowired
	ProgressService progressService;
	@Autowired
	AttachService attachService;
	@Autowired
	AccountService accountService;
	 	
	/**
	 * 일반 전자 청약 url 파람 형식 : /edocument?param01=PA14090358&param02=1017658
	 * 파라메터 : PARAM01(계약번호), PARAM02(거래처코드)
	 * @return url : /cert_user/request_content_info (기본정보 화면)
	 * @throws Exception
	 */
	@RequestMapping(value = {"/edocument"}, method={RequestMethod.GET , RequestMethod.POST})
	public String document_type(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String noticeValue = request.getParameter("notice");
		if(noticeValue == null) {
			return "/common/start_edocument";
		}
		
		String contract_code = request.getParameter("PARAM01");
		String customer_code = request.getParameter("PARAM02");
		String param3 = request.getParameter("PARAM03") == null ? "" : request.getParameter("PARAM03");
		String type = "11";
		
		//파라메터가 없으면 Exception 처리
		if(contract_code == null || customer_code == null) {
			return "/error";
		}
		
		//파라메터 정규식 검사
		String param1Pattern = "^[A-Z]{1,2}[0-9]{1,8}";
		boolean param1Boo = Pattern.matches(param1Pattern, contract_code);
		String param2Pattern = "[0-9]{1,7}";
		boolean param2Boo = Pattern.matches(param2Pattern, customer_code);
		boolean param3Boo = param3.length() < 2 ? true : false;
		if(!param1Boo || !param2Boo || !param3Boo) {	
			logger.info("Parameter Validation Fail contract_code : {}, customer_code : {}, param3 : {}", contract_code,customer_code,param3);
			return "/error";
		}
		
		
		Progress progress = new Progress();
		progress.setContract_code(contract_code);
		progress.setCustomer_code(customer_code);
		progress.setProgress_type("1");
		progress.setParam3(param3);
		progress.setService_type(type);
		
		Progress resProgress =  progressService.progressInfoSelect(progress);
		
		int insertValue = 1;
		//데이터가 없으면 insert
		if(resProgress == null){
			insertValue = progressService.progressInfoInsert(progress);
			resProgress =  progressService.progressInfoSelect(progress);
		}
		
		//progress_id를 insert하다가 데이터 에러가 나면 에러페이지 리턴 
		if(insertValue == 0) {
			return "/error";
		}
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("seq", resProgress.getSeq());
		map.put("contract_code", resProgress.getContract_code());
		map.put("customer_code", resProgress.getCustomer_code());
		map.put("progress_status", resProgress.getProgress_status());
		map.put("status_name", resProgress.getStatus_name());
		map.put("progress_type", resProgress.getProgress_type());
		map.put("service_type", resProgress.getService_type());
		model.addAttribute("map", map);
		
		logger.info("edocument Success contract_code : {}, customer_code : {}, param3 : {}", contract_code,customer_code,param3);
		
		return "/cert_user/request_content_info";
	}
	
	/**
	 * 파일업로드 url 파람 형식 : /fileupload?TELFLDTYP=10&TELFLDCNT=&PARAM01=S000000000000079&PARAM02=1&PARAM03=2&PARAM04=3
	 * service_type 별로 분기가 나눠짐
	 * 10 : 소유인증 거치고 파일업로드 개수 무제한
	 * 11 : 소유인증 거치고 파일업로드 개수 TELFLDCNT 파라메터 제한
	 * 00 : 소유인증 거치지 않고 파일업로드 개수 무제한
	 * 01 : 소유인증 거치지 않고 파일업로드 TELFLDCNT 파라메터 제한
	 * 파라메터 : TELFLDTYP(서비스타입), TELFLDCNT(업로드파일 개수), PARAM01(계약번호), PARAM02, PARAM03, PARAM04
	 * @return url : /phone_certification?progress_id (phone 여부)
	 * @throws Exception
	 */
	@RequestMapping(value = {"/fileupload"}, method={RequestMethod.GET , RequestMethod.POST})
	public void fileUploadType(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String cnt = "0";
		String param2 = "";
		String param3 = "";
		String param4 = "";
		String type = request.getParameter("TELFLDTYP");
		String param1 = request.getParameter("PARAM01");
		
		if(request.getParameter("PARAM02") != null && !request.getParameter("PARAM02").equals("")){
			param2 = request.getParameter("PARAM02");
		}
		
		if(request.getParameter("PARAM03") != null && !request.getParameter("PARAM03").equals("")){
			param3 = request.getParameter("PARAM03");
		}
		
		if(request.getParameter("PARAM04") != null && !request.getParameter("PARAM04").equals("")){
			param4 = request.getParameter("PARAM04");
		}
		
		if(request.getParameter("TELFLDCNT") != null && !request.getParameter("TELFLDCNT").equals("")){
			cnt = request.getParameter("TELFLDCNT");
		}
	
		Progress progress = new Progress();
		progress.setParam1(param1);
		progress.setParam2(param2);
		progress.setParam3(param3);
		progress.setParam4(param4);
		progress.setProgress_type("2");
		progress.setService_type(type);
		progress.setFile_cnt(Integer.parseInt(cnt));
		
		Progress resProgress =  progressService.progressInfoSelect(progress);
		
		if(resProgress == null){
			progressService.progressInfoInsert(progress);
			resProgress =  progressService.progressInfoSelect(progress);
		}
		
		String url = getHttpsUrl(request, "/phone_certification?progress_id="+resProgress.getSeq());
		response.sendRedirect(url);
//		response.sendRedirect("https://dev.goodpaper.co.kr:445/phone_certification?progress_id="+resProgress.getSeq());
			
	}
	
	
	/** 전자청약일 경우 본인인증 또는 소유 인증 분기처리
	 *  PARAMETER : progress_id
	 * @return Progress_status에 따라 /cert_user/phone_certitication, /cert_user/phone_have_certification
	 * @throws Exception
	 */
	@RequestMapping(value = {"/certification"}, method={RequestMethod.GET , RequestMethod.POST})
	public String certification(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String progress_id = request.getParameter("progress_id");
		//progress id 안들어왔을경우
		if (progress_id == null || "".equals(progress_id)) {
			return "/error";
		}
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
		
		//DB에 맞는 progress_id가  없으면 에러
		if(resProgress == null) {
			return "/error";
		}
		
		//상태값이 5이면 완료
		if("5".equals(resProgress.getProgress_status())) {
			return "/common/completion";
		}
		
		//progressType이 2일 경우 파일업로드 이므로 error
		if("2".equals(resProgress.getProgress_type())) {
			return "/error";
		}
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("seq", resProgress.getSeq());
		map.put("contract_code", resProgress.getContract_code());
		map.put("customer_code", resProgress.getCustomer_code());
		map.put("progress_status", resProgress.getProgress_status());
		map.put("status_name", resProgress.getStatus_name());
		model.addAttribute("map", map);
		
		if(resProgress.getProgress_status().equals("1"))
			return "/cert_user/phone_certitication";
		
		return "/cert_user/phone_have_certification";
		
		
		
	}
	
	/** 본인인증 또는 소유 인증 완료 및 세션 생성 후 단계별 진행 시 호출
	 * 
	 * PARAMETER : progress_id
	 * @return 각 상태값에 맞는 url 리턴
	 * @throws Exception
	 */
	@RequestMapping(value = {"/acceptCertification"}, method={RequestMethod.GET , RequestMethod.POST})
	public String acceptCertification(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String url;
		String progress_id = request.getParameter("progress_id");
	
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("seq", resProgress.getSeq());
		map.put("type", resProgress.getService_type());
		map.put("contract_code", resProgress.getContract_code());
		map.put("customer_code", resProgress.getCustomer_code());
		map.put("progress_status", resProgress.getProgress_status());
		map.put("status_name", resProgress.getStatus_name());
		model.addAttribute("map", map);
		
		if(resProgress.getProgress_status().equals("2")){
			HashMap<String, Object> bankList = (HashMap<String, Object>) OpenPlatformAPI.getBankStatus();
			map.put("bankList", bankList.get("res_list"));
			/**
			 * 농협 은행 다 불러오기 시작
			 */
			List<BankInfo> bankList_nh = accountService.getBankInfoList();	// 은행LIST 조회
			List<BankInfo> majorBankList = new ArrayList<BankInfo>();
			List<BankInfo> securityList = new ArrayList<BankInfo>();
			for (BankInfo bankInfo : bankList_nh) {	// 은행/증권 구분
//				System.out.println(bankInfo.toString());
				if (bankInfo.getBank_type().equals("b")) {
					majorBankList.add(bankInfo);
				} else {
					securityList.add(bankInfo);
				}
			}
			map.put("majorBankList", majorBankList);
			map.put("otherBankList", securityList);
			/**
			 * 농협 은행 다 불러오기 끝
			 */
			map.put("step", "1");
			ProgressStatus progressStatus = progressService.getProgressStatusInfo(progress_id);
			if(progressStatus.getAccount_transfer_yn() == 1)
				map.put("step", "2");
			url = "/cert_user/account_certitication";
		}else if(resProgress.getProgress_status().equals("3")) {
			map.put("param3", resProgress.getParam3());
			url = "/cert_user/electronic_signature";
		}else if(resProgress.getProgress_status().equals("4")) {
			List<AttachInfo> attachList = attachService.getAttachList(progress_id);
			map.put("attachList", attachList);
			url =  "/cert_user/attach_document";
		}else {
			url = "/common/completion";
		}
		
		model.addAttribute("map", map);
		
		return url;
	}

	/** fileupload 로 들어왔을 경우 sendRedirect로 들어와 service 체크후  Service_type별로 URL 분기처리
	 * 
	 * PARAMETER : progress_id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = {"/phone_certification"}, method={RequestMethod.GET , RequestMethod.POST})
	public String phoneCertification(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String progress_id = request.getParameter("progress_id");
		
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
		
		Boolean checkService = accountService.isServiceCheck(progress_id);
		
		if(!checkService){
			//TODO 서비스 불가능 코드에 대한 페이지로 이동
			String url = getHttpsUrl(request, "/notice");
			response.sendRedirect(url);
//			response.sendRedirect("/notice");
			return null;
		}
		
		//서비스타입이 0으로 시작하여 소유인증을 거치지 않는 분기
		if("00".equals(resProgress.getService_type().trim()) || "01".equals(resProgress.getService_type().trim())){
			String url = getHttpsUrl(request, "/uncert_file_upload_info?progress_id="+progress_id);
			response.sendRedirect(url);
//			response.sendRedirect("/file_upload_info?progress_id="+progress_id);
			return null;
		}
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("seq", resProgress.getSeq());
		map.put("progress_status", resProgress.getProgress_status());
		map.put("status_name", resProgress.getStatus_name());
		map.put("service_type", resProgress.getService_type());
		map.put("param1", resProgress.getParam1());
		map.put("param2", resProgress.getParam2());
		map.put("param3", resProgress.getParam3());
		map.put("param4", resProgress.getParam4());
		model.addAttribute("map", map);
		
		return "/uncert_user/phone_have_certification";
		
		
	}
	
	/**
	 * 서비스 타입이 0으로 시작하여 소유인증을 거치지 않는 타입에 대한 url
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = {"/uncert_file_upload_info"}, method={RequestMethod.GET , RequestMethod.POST})
	public String unCertFileUploadInfo(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String progress_id = request.getParameter("progress_id");
		
		//progress id 안들어왔을경우
		if (progress_id == null || "".equals(progress_id)) {
			return "/error";
		}
	
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
		
		//DB에 맞는 progress_id가 없고 progressType이 1일 경우 기본전자청약 이므로 error
		if(resProgress == null || "1".equals(resProgress.getProgress_type())) {
			return "/error";
		}

		//서비스타입이 1으로 시작하면 에러처리
		if("10".equals(resProgress.getService_type().trim()) || "11".equals(resProgress.getService_type().trim())){
			return "/error";
		}
		
		//완료 됐으면
		if("5".equals(resProgress.getProgress_status())) {
			return "/common/completion";
		}
			
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("seq", resProgress.getSeq());
		map.put("type", resProgress.getService_type());
		map.put("progress_status", resProgress.getProgress_status());
		map.put("status_name", resProgress.getStatus_name());
		map.put("param1", resProgress.getParam1());
		map.put("param2", resProgress.getParam2());
		map.put("param3", resProgress.getParam3());
		map.put("param4", resProgress.getParam4());
	
		List<EformAttach> attachList = attachService.getFileUploadAttachList(progress_id);
		map.put("attachList", attachList);

		model.addAttribute("map", map);
		return "/uncert_user/photo_upload_info";
	}
	
	/**
	 * 서비스 타입이 1으로 시작하여 소유인증을 거치는 url
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = {"/file_upload_info"}, method={RequestMethod.GET , RequestMethod.POST})
	public String fileUploadInfo(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		String progress_id = request.getParameter("progress_id");
	
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
			
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("seq", resProgress.getSeq());
		map.put("type", resProgress.getService_type());
		map.put("progress_status", resProgress.getProgress_status());
		map.put("status_name", resProgress.getStatus_name());
		map.put("param1", resProgress.getParam1());
		map.put("param2", resProgress.getParam2());
		map.put("param3", resProgress.getParam3());
		map.put("param4", resProgress.getParam4());
	
		List<EformAttach> attachList = attachService.getFileUploadAttachList(progress_id);
		map.put("attachList", attachList);

		model.addAttribute("map", map);
		return "/uncert_user/photo_upload_info";
	}
	
	@RequestMapping(value = {"/notice"}, method={RequestMethod.GET , RequestMethod.POST})
	public String notice(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		
		return "/common/notice";
	}
	
	@RequestMapping(value = {"/error"}, method={RequestMethod.GET , RequestMethod.POST})
	public String error(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		
		return "/error";
	}
	
	@RequestMapping(value = {"/completion"}, method={RequestMethod.GET , RequestMethod.POST})
	public String completion(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		
		return "/common/completion";
	}

	/**
	 * sendRedirect 메소드 실행할 경우 https -> http로 바뀌는 이슈가 있어 강제로 url을 만드는 함수
	 * @param url
	 * @return fullPath
	 */
	private String getHttpsUrl(HttpServletRequest request, String context) {
		String url = "";
		String httpUrl = request.getRequestURL().toString();
		//http:// 로 시작하고, localhost가 아닐경우 https를 붙임
		if(httpUrl.startsWith("http://") && httpUrl.indexOf("localhost") < 0) {
			String hostUrl = request.getHeader("host");
			url = "https://" + hostUrl + context;
		}else {
			String hostUrl = request.getHeader("host");
			url = "http://" + hostUrl + context;
		}
		return url;
	}
}
