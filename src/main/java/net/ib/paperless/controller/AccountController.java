package net.ib.paperless.controller;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.extrus.common.json.simple.JSONObject;

import net.ib.paperless.common.DateUtil;
import net.ib.paperless.domain.AccountHistoryInfo;
import net.ib.paperless.domain.AccountVerifyInfo;
import net.ib.paperless.domain.Billing;
import net.ib.paperless.domain.Progress;
import net.ib.paperless.omap.OMAP_0005;
import net.ib.paperless.openplatform.NhOpenPlatform;
import net.ib.paperless.openplatform.OpenPlatformAPI;
import net.ib.paperless.scoket.SocketManager;
import net.ib.paperless.service.AccountService;
import net.ib.paperless.service.BillingLogService;
import net.ib.paperless.service.OmapLogService;
import net.ib.paperless.service.ProgressService;


@RequestMapping("/ajax/**")
@Controller
public class AccountController {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	ProgressService progressService;
	@Autowired
	OmapLogService omapLogService;
	@Autowired
	AccountService accountService;
	@Autowired
	BillingLogService billingLogService;
	
	//농협 API
	@Autowired
	NhOpenPlatform nhapi;
	
	@Value("${deploy.server}")
	private String deploy_server;
	
	//계좌 실명인증 ajax
	@RequestMapping(value="/accountCheck",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public Map<String, Object> accountCheck(@RequestBody Map<String,Object> params , HttpServletRequest request) throws IOException{
		Map<String,Object> map = new HashMap<String,Object>();
		String progress_id = params.get("progress_id").toString();
		String request_holder_name = params.get("username").toString();					// 요청 실명
		String account_holder_number = params.get("account_holder_number").toString();	// 계좌번호
		String account_holder_code = params.get("account_holder_code").toString();		// 은행코드
		String ssn_number = params.get("ssn_number").toString();						// 생년월일
		String gender	= params.get("gender").toString();								// 성별 (M,F)
		String account_bank_name = params.get("account_bank_name").toString();			// 은행이름
		logger.info("## BankCode(/accountCheck) = "+account_holder_code);
		
		// 시도 횟수 체크
		Map<String,Object> accountCntParams = new HashMap<String, Object>();
		
		accountCntParams.put("account_holder_number", account_holder_number);
		accountCntParams.put("account_bank_code", account_holder_code);
		int cnt =  accountService.getHistoryCount(accountCntParams);
		logger.info("## AccountCheck Count = " + cnt +", RealServer = " + deploy_server);
		if("initech_real".equals(deploy_server)) {
//		if("initech_dev".equals(deploy_server)) {
			if (cnt > 9) {
				map.put("rsp_code", "4");
				map.put("rsp_msg", "서비스 제한(일일 제한 횟수 초과 10회)");
				map.put("rsp_error_msg", "일일 제한 횟수 초과 시도 (10회)");
				return map;
			}
		}
		
		// 계좌실명 인증 
		HashMap<String, Object> realNameResult = (HashMap<String, Object>) OpenPlatformAPI.getRealName(account_holder_code, account_holder_number, ssn_number+gender);
		
		//billing 4 - 계좌 실명인증, 5 - 계좌 입금이체
		Billing bdataType4 = new Billing();
		Billing bdataType5 = new Billing();
		
		//계좌인증 tba_history 정보
		AccountHistoryInfo historyInfo = new AccountHistoryInfo();
		historyInfo.setProgress_id(progress_id);
		historyInfo.setAccount_holder_name(request_holder_name);
		historyInfo.setAccount_holder_number(account_holder_number);
		historyInfo.setAccount_bank_code(account_holder_code);
		historyInfo.setAccount_bank_name(account_bank_name);
		historyInfo.setAccount_ssn_number(ssn_number);
		historyInfo.setAccount_sex(gender);
		
		if(realNameResult.get("rsp_code").equals("A0000"))
		{
			//계좌 실명 인증 성공 billing insert
			bdataType4.setProgress_id(progress_id);
			bdataType4.setType(4);//4  계좌 실명인증
			bdataType4.setSuccess_yn("y");
			billingLogService.billingLogInsert(bdataType4);
			logger.info("### Billing Log Insert ### Seq : "+ bdataType4.getSeq()+", Type : " + bdataType4.getType()
					+ ", Success_Yn : " + bdataType4.getSuccess_yn()+ ", Progress_id : " + bdataType4.getProgress_id());
			
			historyInfo.setInquiry_realname_yn("y");
			
			String random_code = "1234";	//임시
			if("initech_real".equals(deploy_server)) {
				Random generator = new Random();   
				random_code = String.valueOf(1000+generator.nextInt(9000));
			}
	
			// 1원 이체
			HashMap<String, Object> result = (HashMap<String, Object>) OpenPlatformAPI.setTransderDeposit(account_bank_name, account_holder_code, account_holder_number, account_bank_name, request_holder_name,  random_code);
			String rsp_code = (String)result.get("rsp_code");
			
			if(rsp_code.equals("A0000")) {
				//계좌 입금이체 성공 billing insert
				bdataType5.setProgress_id(progress_id);
				bdataType5.setType(5);//5  계좌 입금이체
				bdataType5.setSuccess_yn("y");
				billingLogService.billingLogInsert(bdataType5);
				logger.info("### Billing Log Insert 계좌 입금이체 성공 ### Seq : "+ bdataType5.getSeq()+", Type : " + bdataType5.getType()
						+ ", Success_Yn : " + bdataType5.getSuccess_yn()+ ", Progress_id : " + bdataType5.getProgress_id());
				
				historyInfo.setInquiry_transfer_deposit_yn("y");
				accountService.insertAccountHistoryInfo(historyInfo);
				
				//ProgressInfo 가져오기
				Progress progress = new Progress();
				progress.setSeq(Integer.parseInt(progress_id));
				Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);

				Date date = new Date();
				
				//전문 데이터 생성
				String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
				String CONFLDNBR	= resProgress.getContract_code();	
				String CUSTMRCDE	= resProgress.getCustomer_code();
				String CUSTMRNAM	= "인포뱅크";	// 거래처명
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
				
				OMAP_0005 td1 = new OMAP_0005("OMAP", 
						"0005", 
						TRDSEQNBR, 
						resProgress.getService_type(), 
						dateFormat.format(date), 
						timeFormat.format(date), 
						"", 
						"", 
						CONFLDNBR, 
						CUSTMRCDE, 
						"", 
						CUSTMRNAM, 
						gender.equals("1") ? "M" : "F", 
						ssn_number, 
						account_holder_code, 
						account_bank_name, 
						account_holder_number, 
						"");
				
				//전송 전문 저장
				HashMap<String, String> sendLog = td1.getResult();
				logger.info("## OMAP_0005 Log Save(/accountCheck) - seq : "+resProgress.getSeq());
				logger.info("## OMAP_0005 (/accountCheck) [{}]"+ sendLog);
				omapLogService.omapLogInsert(sendLog);
				
				//수신 전문 생성
				OMAP_0005 td2 = new OMAP_0005();
				
				//전문  전송
				SocketManager.connection(td1, td2);
				td2.print();
				
				//수신 전문 저장
				HashMap<String, String> receiveLog = td2.getResult();
				omapLogService.omapLogUpdate(receiveLog);
				
				if("00".equals(receiveLog.get("RSTFLDCDE"))){
					AccountVerifyInfo codeInfo = new AccountVerifyInfo();
					codeInfo.setProgress_id(progress_id);
					codeInfo.setAuth_code(random_code);
					accountService.setAccountVerifyInfo(codeInfo);

					map.put("rsp_code", "1");	
					map.put("rsp_msg", "요청하신 계좌의 실명인증이 완료되었습니다. <br/>입금자 명의에 기재된 4자리 인증코드를 확인하시고<br/> 계좌인증을 완료해 주세요.");
					return map;
				}else{
					map.put("rsp_code", "2");
//					map.put("rsp_msg", "입금이체 중 오류가 발생하였습니다.<br/>잠시 후 다시 이용해 주시기 바랍니다.");
					map.put("rsp_msg", receiveLog.get("RSTFLDNAM"));
					String error_msg = (String)result.get("rsp_message");
					map.put("rsp_error_msg", error_msg);
					return map;
				}
			} else {
				//계좌 입금이체 실패 billing insert
				bdataType5.setProgress_id(progress_id);
				bdataType5.setType(5);//5  계좌 입금이체
				bdataType5.setSuccess_yn("n");
				billingLogService.billingLogInsert(bdataType5);
				logger.info("### Billing Log Insert 계좌 입금이체 실패 ### Seq : "+ bdataType5.getSeq()+", Type : " + bdataType5.getType()
						+ ", Success_Yn : " + bdataType5.getSuccess_yn()+ ", Progress_id : " + bdataType5.getProgress_id());
				
				historyInfo.setInquiry_transfer_deposit_yn("n");
				accountService.insertAccountHistoryInfo(historyInfo);
				
				//ProgressInfo 가져오기
				Progress progress = new Progress();
				progress.setSeq(Integer.parseInt(progress_id));
				Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);

				Date date = new Date();
				
				//전문 데이터 생성
				String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
				String CONFLDNBR	= resProgress.getContract_code();	
				String CUSTMRCDE	= resProgress.getCustomer_code();
				String CUSTMRNAM	= "인포뱅크";	// 거래처명
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
				
				OMAP_0005 td1 = new OMAP_0005("OMAP", 
						"0005", 
						TRDSEQNBR, 
						resProgress.getService_type(), 
						dateFormat.format(date), 
						timeFormat.format(date), 
						"", 
						"", 
						CONFLDNBR, 
						CUSTMRCDE, 
						"", 
						CUSTMRNAM, 
						gender.equals("1") ? "M" : "F", 
						ssn_number, 
						account_holder_code, 
						account_bank_name, 
						account_holder_number, 
						"A0000002");
				
				//전송 전문 저장
				HashMap<String, String> sendLog = td1.getResult();
				logger.info("## OMAP_0005 Log Save(/accountCheck) - seq : "+resProgress.getSeq());
				logger.info("## OMAP_0005 (/accountCheck) [{}]"+ sendLog);
				omapLogService.omapLogInsert(sendLog);
				
				//수신 전문 생성
				OMAP_0005 td2 = new OMAP_0005();
				
				//전문  전송
				SocketManager.connection(td1, td2);
				td2.print();
				
				//수신 전문 저장
				HashMap<String, String> receiveLog = td2.getResult();
				omapLogService.omapLogUpdate(receiveLog);
				
				map.put("rsp_code", "2");
				map.put("rsp_msg", "해당 계좌는 이체가 불가합니다.<br/>다른 계좌번호를 입력해주시기 바랍니다.");
				String error_msg = (String)result.get("rsp_message");
				map.put("rsp_error_msg", error_msg);
				
				// 실패정보 insert
				List<Map<String, Object>> res_list = (List<Map<String, Object>>) result.get("res_list");
				if(res_list != null && !res_list.get(0).isEmpty()) {
					result.put("bank_code_tran", res_list.get(0).get("bank_code_tran"));
					result.put("bank_rsp_code", res_list.get(0).get("bank_rsp_code"));
					result.put("bank_rsp_message", res_list.get(0).get("bank_rsp_message"));
					result.put("bank_code_std", res_list.get(0).get("bank_code_std"));
					result.put("bank_code_sub", res_list.get(0).get("bank_code_sub"));
					result.put("bank_name", res_list.get(0).get("bank_name"));
					result.put("bank_tran_id", res_list.get(0).get("bank_tran_id"));
				}
				result.put("progress_id", progress_id);
				accountService.accountFailInfoInsert(result);
				
				return map;
			}
			
		}
		
		//계좌 실명 인증 실패 billing insert
		bdataType4.setProgress_id(progress_id);
		bdataType4.setType(4);//4  계좌 실명인증
		bdataType4.setSuccess_yn("n");
		billingLogService.billingLogInsert(bdataType4);
		logger.info("### Billing Log Insert 계좌 실명 인증 실패 ### Seq : "+ bdataType4.getSeq()+", Type : " + bdataType4.getType()
				+ ", Success_Yn : " + bdataType4.getSuccess_yn()+ ", Progress_id : " + bdataType4.getProgress_id());
		
		historyInfo.setInquiry_realname_yn("n");
		accountService.insertAccountHistoryInfo(historyInfo);
		
		map.put("rsp_code", "3");
		map.put("rsp_msg", "입력하신 계좌정보가 올바르지 않습니다.<br/>다시 확인 후 이용해 주시기 바랍니다.");
		String error_msg = (String)realNameResult.get("rsp_message");
		map.put("rsp_error_msg", error_msg);
		
		// 실패정보 insert
		realNameResult.put("progress_id", progress_id);
		accountService.accountFailInfoInsert(realNameResult);
		
		return map;
	}
	
    //계좌 실명인증 ajax
	@RequestMapping(value="/accountCheck_Nh",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public Map<String, Object> accountCheckNh(@RequestBody Map<String,Object> params , HttpServletRequest request) throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		String progress_id = params.get("progress_id").toString();
		String request_holder_name = params.get("username").toString();					// 요청 실명
		String account_holder_number = params.get("account_holder_number").toString();	// 계좌번호
		String account_holder_code = params.get("account_holder_code").toString();		// 은행코드
//		String ssn_number = params.get("ssn_number").toString();						// 생년월일
//		String gender	= params.get("gender").toString();								// 성별 (M,F)
		String account_bank_name = params.get("account_bank_name").toString();			// 은행이름
		logger.info("## BankCode(/accountCheck) = "+account_holder_code);
		
		// 시도 횟수 체크
		Map<String,Object> accountCntParams = new HashMap<String, Object>();
		
		accountCntParams.put("account_holder_number", account_holder_number);
		accountCntParams.put("account_bank_code", account_holder_code);
		int cnt =  accountService.getHistoryCount(accountCntParams);
		logger.info("## AccountCheck Count = " + cnt +", RealServer = " + deploy_server);
		if("initech_real".equals(deploy_server)) {
//			if("initech_dev".equals(deploy_server)) {
			if (cnt > 9) {
				map.put("rsp_code", "4");
				map.put("rsp_msg", "서비스 제한(일일 제한 횟수 초과 10회)");
				map.put("rsp_error_msg", "일일 제한 횟수 초과 시도 (10회)");
				return map;
			}
		}
		
		String timestamp = DateUtil.getCurrentDateString("yyyyMMddHHmmss");
		String dateStr = timestamp.substring(0, 8);
		String timeStr = timestamp.substring(8, timestamp.length());
		
		// 예금주 확인 (농협 당행/타행 구분)
		nhapi.init("realname", account_holder_code);
		
		nhapi.addHeader("Tsymd", dateStr)
			.addHeader("Trtm", timeStr)
			.addHeader("IsTuno", timestamp + "000001");
		
		nhapi.addBody("Bncd", account_holder_code)
			.addBody("Acno", account_holder_number);
		
		JSONObject nhNameChkResult = nhapi.callAPI();

		logger.info("NH Name Check RESULT : [{}]", nhNameChkResult.toString());
		
		
		// 계좌실명 인증 
//			HashMap<String, Object> realNameResult = (HashMap<String, Object>) OpenPlatformAPI.getRealName(account_holder_code, account_holder_number, ssn_number+gender);
		
		//billing 4 - 계좌 실명인증, 5 - 계좌 입금이체
		Billing bdataType4 = new Billing();
		Billing bdataType5 = new Billing();
		
		//계좌인증 tba_history 정보
		AccountHistoryInfo historyInfo = new AccountHistoryInfo();
		historyInfo.setProgress_id(progress_id);
		historyInfo.setAccount_holder_name(request_holder_name);
		historyInfo.setAccount_holder_number(account_holder_number);
		historyInfo.setAccount_bank_code(account_holder_code);
		historyInfo.setAccount_bank_name(account_bank_name);
		historyInfo.setAccount_ssn_number("");
		historyInfo.setAccount_sex("");
		
		int historyRet = 0;
//			if(realNameResult.get("rsp_code").equals("A0000"))
		if(nhapi.isSuccess() && request_holder_name.equals(nhapi.getResultBody("Dpnm")))
		{
			//계좌 실명 인증 성공 billing insert
			bdataType4.setProgress_id(progress_id);
			bdataType4.setType(4);//4  계좌 실명인증
			bdataType4.setSuccess_yn("y");
			billingLogService.billingLogInsert(bdataType4);
			logger.info("### Billing Log Insert ### Seq : "+ bdataType4.getSeq()+", Type : " + bdataType4.getType()
					+ ", Success_Yn : " + bdataType4.getSuccess_yn()+ ", Progress_id : " + bdataType4.getProgress_id());
			
			historyInfo.setInquiry_realname_yn("y");
			
			String random_code = "1234";	//임시
			if("initech_real".equals(deploy_server)) {
				Random generator = new Random();   
				random_code = String.valueOf(1000+generator.nextInt(9000));
			}
			logger.info("random_code : {}", random_code);
			
			// 1원 이체
			nhapi.init("transfer", account_holder_code);
			
			nhapi.addHeader("Tsymd", dateStr)
				.addHeader("Trtm", timeStr)
				.addHeader("IsTuno", timestamp + "000002");
			
			nhapi.addBody("Bncd", account_holder_code)
				.addBody("Acno", account_holder_number)
				.addBody("Tram", "1")
				.addBody("DractOtlt", "굿페이퍼")
				.addBody("MractOtlt", random_code);					
			
			JSONObject nhNameTransferResult = nhapi.callAPI();
			logger.info("NH Transfer RESULT : [{}]", nhNameTransferResult.toString());
			
			//이체 성공시
			if(nhapi.isSuccess()) {
				//계좌 입금이체 성공 billing insert
				bdataType5.setProgress_id(progress_id);
				bdataType5.setType(5);//5  계좌 입금이체
				bdataType5.setSuccess_yn("y");
				billingLogService.billingLogInsert(bdataType5);
				logger.info("### Billing Log Insert 계좌 입금이체 성공 ### Seq : "+ bdataType5.getSeq()+", Type : " + bdataType5.getType()
						+ ", Success_Yn : " + bdataType5.getSuccess_yn()+ ", Progress_id : " + bdataType5.getProgress_id());
				
				historyInfo.setInquiry_transfer_deposit_yn("y");
				accountService.insertAccountHistoryInfo(historyInfo);
				
				//ProgressInfo 가져오기
				Progress progress = new Progress();
				progress.setSeq(Integer.parseInt(progress_id));
				Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);

				Date date = new Date();
				
				//전문 데이터 생성
				String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
				String CONFLDNBR	= resProgress.getContract_code();	
				String CUSTMRCDE	= resProgress.getCustomer_code();
				String CUSTMRNAM	= "인포뱅크";	// 거래처명
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
				
				OMAP_0005 td1 = new OMAP_0005("OMAP", 
						"0005", 
						TRDSEQNBR, 
						resProgress.getService_type(), 
						dateFormat.format(date), 
						timeFormat.format(date), 
						"", 
						"", 
						CONFLDNBR, 
						CUSTMRCDE, 
						"", 
						CUSTMRNAM, 
//						gender.equals("1") ? "M" : "F", 
//						ssn_number, 
						"",
						"",
						account_holder_code, 
						account_bank_name, 
						account_holder_number, 
						"");
				
				//전송 전문 저장
				HashMap<String, String> sendLog = td1.getResult();
				logger.info("## OMAP_0005 Log Save(/accountCheck) - seq : "+resProgress.getSeq());
				logger.info("## OMAP_0005 (/accountCheck) [{}]"+ sendLog);
				omapLogService.omapLogInsert(sendLog);
				
				//수신 전문 생성
				OMAP_0005 td2 = new OMAP_0005();
				
				//전문  전송
				SocketManager.connection(td1, td2);
				td2.print();
				
				//수신 전문 저장
				HashMap<String, String> receiveLog = td2.getResult();
				omapLogService.omapLogUpdate(receiveLog);
				
				if("00".equals(receiveLog.get("RSTFLDCDE"))){
					AccountVerifyInfo codeInfo = new AccountVerifyInfo();
					codeInfo.setProgress_id(progress_id);
					codeInfo.setAuth_code(random_code);
					accountService.setAccountVerifyInfo(codeInfo);

					map.put("rsp_code", "1");	
					map.put("rsp_msg", "요청하신 계좌의 실명인증이 완료되었습니다. <br/>입금자 명의에 기재된 4자리 인증코드를 확인하시고<br/> 계좌인증을 완료해 주세요.");
					
					return map;
				}else{
					map.put("rsp_code", "2");
//						map.put("rsp_msg", "입금이체 중 오류가 발생하였습니다.<br/>잠시 후 다시 이용해 주시기 바랍니다.");
					map.put("rsp_msg", receiveLog.get("RSTFLDNAM"));
					String error_msg = null;
					if(nhapi.getResultHeader("Rsms") != null) {
						error_msg = nhapi.getResultHeader("Rsms");	
					}
					map.put("rsp_error_msg", error_msg);
					return map;
				}
			} else {
				//계좌 입금이체 실패 billing insert
				bdataType5.setProgress_id(progress_id);
				bdataType5.setType(5);//5  계좌 입금이체
				bdataType5.setSuccess_yn("n");
				billingLogService.billingLogInsert(bdataType5);
				logger.info("### Billing Log Insert 계좌 입금이체 실패 ### Seq : "+ bdataType5.getSeq()+", Type : " + bdataType5.getType()
						+ ", Success_Yn : " + bdataType5.getSuccess_yn()+ ", Progress_id : " + bdataType5.getProgress_id());
				
				historyInfo.setInquiry_transfer_deposit_yn("n");
				accountService.insertAccountHistoryInfo(historyInfo);
				
				//ProgressInfo 가져오기
				Progress progress = new Progress();
				progress.setSeq(Integer.parseInt(progress_id));
				Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);

				Date date = new Date();
				
				//전문 데이터 생성
				String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
				String CONFLDNBR	= resProgress.getContract_code();	
				String CUSTMRCDE	= resProgress.getCustomer_code();
				String CUSTMRNAM	= "인포뱅크";	// 거래처명
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
				
				OMAP_0005 td1 = new OMAP_0005("OMAP", 
						"0005", 
						TRDSEQNBR, 
						resProgress.getService_type(), 
						dateFormat.format(date), 
						timeFormat.format(date), 
						"", 
						"", 
						CONFLDNBR, 
						CUSTMRCDE, 
						"", 
						CUSTMRNAM, 
//						gender.equals("1") ? "M" : "F", 
//						ssn_number, 
						"",
						"",
						account_holder_code, 
						account_bank_name, 
						account_holder_number, 
						"A0000002");
				
				//전송 전문 저장
				HashMap<String, String> sendLog = td1.getResult();
				logger.info("## OMAP_0005 Log Save(/accountCheck) - seq : "+resProgress.getSeq());
				logger.info("## OMAP_0005 (/accountCheck) [{}]"+ sendLog);
				omapLogService.omapLogInsert(sendLog);
				
				//수신 전문 생성
				OMAP_0005 td2 = new OMAP_0005();
				
				//전문  전송
				SocketManager.connection(td1, td2);
				td2.print();
				
				//수신 전문 저장
				HashMap<String, String> receiveLog = td2.getResult();
				omapLogService.omapLogUpdate(receiveLog);
				
				map.put("rsp_code", "2");
				map.put("rsp_msg", "해당 계좌는 이체가 불가합니다.<br/>다른 계좌번호를 입력해주시기 바랍니다.");
				String error_msg = null;
				if(nhapi.getResultHeader("Rsms") != null) {
					error_msg = nhapi.getResultHeader("Rsms");	
				}
				map.put("rsp_error_msg", error_msg);
				
				// 실패정보 ins
				HashMap<String, Object> failMap = new HashMap<>(nhapi.getResultHeader());
				failMap.putAll(nhapi.getResultBody());
				failMap.put("progress_id", progress_id);
				failMap.put("RspJson", nhNameTransferResult.toString());
				int failInfoCnt = accountService.accountNHPublicFailInfoInsert(failMap);
				
				return map;
			}
			
		}
		
		//계좌 실명 인증 실패 billing insert
		bdataType4.setProgress_id(progress_id);
		bdataType4.setType(4);//4  계좌 실명인증
		bdataType4.setSuccess_yn("n");
		billingLogService.billingLogInsert(bdataType4);
		logger.info("### Billing Log Insert 계좌 실명 인증 실패 ### Seq : "+ bdataType4.getSeq()+", Type : " + bdataType4.getType()
				+ ", Success_Yn : " + bdataType4.getSuccess_yn()+ ", Progress_id : " + bdataType4.getProgress_id());
		
		historyInfo.setInquiry_realname_yn("n");
		accountService.insertAccountHistoryInfo(historyInfo);
		
		map.put("rsp_code", "3");
		map.put("rsp_msg", "입력하신 계좌정보가 올바르지 않습니다.<br/>다시 확인 후 이용해 주시기 바랍니다.");
//		String error_msg = (String)realNameResult.get("rsp_message");
		String error_msg = null;
		if(nhapi.getResultHeader("Rsms") != null) {
			error_msg = nhapi.getResultHeader("Rsms");	
		}
		
		map.put("rsp_error_msg", error_msg);
		
		// 실패정보 insert
//		realNameResult.put("progress_id", progress_id);
//		accountService.accountFailInfoInsert(realNameResult);
		
		// 실패정보 ins
		HashMap<String, Object> failMap = new HashMap<>(nhapi.getResultHeader());
		failMap.putAll(nhapi.getResultBody());
		failMap.put("progress_id", progress_id);
		failMap.put("RspJson", nhNameChkResult.toString());
		int failInfoCnt = accountService.accountNHPublicFailInfoInsert(failMap);
		return map;
	}
	
	@RequestMapping(value="/account_authcode_check",method={RequestMethod.GET , RequestMethod.POST},produces="application/json")
	@ResponseBody
	public Map<String, Object> accountAuthCodeCheck(@RequestBody Map<String,Object> params , HttpServletRequest request) throws IOException{
		Map<String,Object> map = new HashMap<String,Object>();
		String progress_id = params.get("progress_id").toString();
		String auth_code = params.get("auth_code").toString();					// 인증번호
		
		AccountVerifyInfo accountVerifyInfo = accountService.getAuthCodeInfo(progress_id);
		
		if(accountVerifyInfo != null){
			
			//우선 response만 생성
			if(accountVerifyInfo.getFail_count() < 3){
				if(accountVerifyInfo.getAuth_code().equals(auth_code)){
					map.put("rsp_code", "1");
					map.put("rsp_msg", "인증이 완료 되었습니다.");
					
				}
				else{
					if(accountVerifyInfo.getFail_count() == 2){
						map.put("rsp_code", "3");
						map.put("rsp_msg", "인증번호 3회 실패로 재인증이 필요합니다.");
					}
					else{
						map.put("rsp_code", "2");
						map.put("rsp_msg", "인증번호를 잘못 입력하였습니다.");
					}
				}
			}else{
				map.put("rsp_code", "3");
				map.put("rsp_msg", "인증번호 3회 실패로 재인증이 필요합니다.");
				
			}
			
			//전문 생성 및 전송
			Date date = new Date();
			SimpleDateFormat authDateFormat = new SimpleDateFormat("yyyyMMddhhmmssSSS");
			
			//ProgressInfo 가져오기
			Progress progress = new Progress();
			progress.setSeq(Integer.parseInt(progress_id));
			Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
	
			HashMap<String, String> data = new HashMap<String,String>();
			data.put("TRDFLDCDE", "0005");
			data.put("CONFLDNBR", resProgress.getContract_code());
			data.put("CUSTMRCDE", resProgress.getCustomer_code());
			
			//전문 데이터 생성
			String TRDSEQNBR	= Integer.toString(omapLogService.omapLogSeqSelect(data));
			String CONFLDNBR	= resProgress.getContract_code();	
			String CUSTMRCDE	= resProgress.getCustomer_code();
			String CUSTMRNAM	= "인포뱅크";	// 거래처명
			String KUJCERRCD	= map.get("rsp_code").equals("1") ? "A0000000" : "A0000001";
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
			
			OMAP_0005 td1 = new OMAP_0005("OMAP", 
					"0005", 
					TRDSEQNBR, 
					resProgress.getService_type(), 
					dateFormat.format(date), 
					timeFormat.format(date), 
					"", 
					"", 
					CONFLDNBR, 
					CUSTMRCDE, 
					authDateFormat.format(date), 
					CUSTMRNAM, 
					"", 
					"", 
					"", 
					"", 
					"", 
					KUJCERRCD);
			
			//전송 전문 저장
			HashMap<String, String> sendLog = td1.getResult();
			logger.info("## OMAP_0005 Log Save(/account_authcode_check) - seq : "+resProgress.getSeq());
			logger.info("## OMAP_0005 (/account_authcode_check) [{}]"+ sendLog);
			omapLogService.omapLogUpdate(sendLog);
			
			//수신 전문 생성
			OMAP_0005 td2 = new OMAP_0005();
			
			//전문  전송
			SocketManager.connection(td1, td2);
			td2.print();
			
			//수신 전문 저장
			HashMap<String, String> receiveLog = td2.getResult();
			omapLogService.omapLogUpdate(receiveLog);
			
			Billing bdataType6 =new Billing();
			//전문 송수신완료 후 db처리 
			//전문결과 성공&인증번호 일치 시에만 성공처리
			//나머지의 경우 전문결과와 상관 없이 처리
			if(accountVerifyInfo.getFail_count() < 3){
				if(accountVerifyInfo.getAuth_code().equals(auth_code)){
					if("00".equals(receiveLog.get("RSTFLDCDE"))){
						//계좌 인증코드 컨펌 성공 billing insert
						bdataType6.setProgress_id(progress_id);
						bdataType6.setType(6);//6  계좌 인증코드 컨펌
						bdataType6.setSuccess_yn("y");
						billingLogService.billingLogInsert(bdataType6);
						logger.info("### Billing Log Insert 계좌 인증코드 컨펌 ### Seq : "+ bdataType6.getSeq()+", Type : " + bdataType6.getType()
								+ ", Success_Yn : " + bdataType6.getSuccess_yn()+ ", Progress_id : " + bdataType6.getProgress_id());
						
						//인증코드 테이블 삭제
						accountService.accountVerifyCodeDelete(progress_id);
						//progress_status 테이블 업데이트
						accountService.accountVerifyUpdate(progress_id);
						//tb_progress progress_status 상태값 업데이트
						Progress progressUpdate = new Progress();
						progressUpdate.setSeq(Integer.parseInt(progress_id));
						progressUpdate.setProgress_status("3");
						progressService.progressStatusUpdate(progressUpdate);
					}else{
						//계좌 인증코드 컨펌 실패 billing insert
						bdataType6.setProgress_id(progress_id);
						bdataType6.setType(6);//6  계좌 인증코드 컨펌
						bdataType6.setSuccess_yn("n");
						billingLogService.billingLogInsert(bdataType6);
						logger.info("### Billing Log Insert 계좌 인증코드 컨펌 ### Seq : "+ bdataType6.getSeq()+", Type : " + bdataType6.getType()
								+ ", Success_Yn : " + bdataType6.getSuccess_yn()+ ", Progress_id : " + bdataType6.getProgress_id());
						map.put("rsp_code", "4");
						map.put("rsp_msg", "기타오류");
					}
					
				}
				else{
					if(accountVerifyInfo.getFail_count() == 2){
						//인증코드 삭제
						accountService.accountVerifyCodeDelete(progress_id);
						//실명인증 시간 삭제
						accountService.accountTransferFailUpdate(progress_id);
					}
					else{
						//인증 실패1회 증가
						accountService.accountVerifyCntUpdate(progress_id);
					}
				}
			}else{
				//인증코드 삭제
				accountService.accountVerifyCodeDelete(progress_id);
				//실명인증 시간 삭제
				accountService.accountTransferFailUpdate(progress_id);
			}
			
		}else{
			map.put("rsp_code", "4");
			map.put("rsp_msg", "기타오류");
		}
		
		
		/*if(!map.get("rsp_code").equals("4")){
			
			Date date = new Date();
			SimpleDateFormat authDateFormat = new SimpleDateFormat("yyyyMMddhhmmssSSS");
			
			//ProgressInfo 가져오기
			Progress progress = new Progress();
			progress.setSeq(Integer.parseInt(progress_id));
			Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);
	
			HashMap<String, String> data = new HashMap<String,String>();
			data.put("TRDFLDCDE", "0005");
			data.put("CONFLDNBR", resProgress.getContract_code());
			data.put("CUSTMRCDE", resProgress.getCustomer_code());
			
			//전문 데이터 생성
			String TRDSEQNBR	= Integer.toString(omapLogService.omapLogSeqSelect(data));
			String CONFLDNBR	= resProgress.getContract_code();	
			String CUSTMRCDE	= resProgress.getCustomer_code();
			String CUSTMRNAM	= "인포뱅크";	// 거래처명
			String KUJCERRCD	= map.get("rsp_code").equals("1") ? "A0000000" : "A0000001";
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
			
			OMAP_0005 td1 = new OMAP_0005("OMAP", 
					"0005", 
					TRDSEQNBR, 
					resProgress.getService_type(), 
					dateFormat.format(date), 
					timeFormat.format(date), 
					"", 
					"", 
					CONFLDNBR, 
					CUSTMRCDE, 
					authDateFormat.format(date), 
					CUSTMRNAM, 
					"", 
					"", 
					"", 
					"", 
					"", 
					KUJCERRCD);
			
			//전송 전문 저장
			HashMap<String, String> sendLog = td1.getResult();
			logger.info("## OMAP_0005 Log Save(/accountCheck) - seq : "+resProgress.getSeq());
			logger.info("## OMAP_0005 (/accountCheck) [{}]"+ sendLog);			
			omapLogService.omapLogUpdate(sendLog);
			
			//수신 전문 생성
			OMAP_0005 td2 = new OMAP_0005();
			
			//전문  전송
			SocketManager.connection(td1, td2);
			td2.print();
			
			//수신 전문 저장
			HashMap<String, String> receiveLog = td2.getResult();
			omapLogService.omapLogUpdate(receiveLog);
		}*/
		
		return map;
		
	}
}
