package net.ib.paperless.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import net.ib.paperless.common.ApiResponse;
import net.ib.paperless.service.AppApiService;
import net.ib.paperless.service.ProgressService;

@Controller
@RequestMapping("/appapi/**")
public class AppApiController{

	@Autowired
	AppApiService appApiService;
	@Autowired
    ProgressService progressService;
	
	@RequestMapping(value="/appInfoCheck",method={RequestMethod.GET , RequestMethod.POST})
	@ResponseBody
	public ApiResponse<Map<String,Object>> appInfoCheck(@RequestParam("os") String os,
			@RequestParam("version") String version,
			@RequestParam("package_name") String package_name,
			@RequestParam("hash") String hash){

		Map<String,String> params = new HashMap<String, String>();
		
		params.put("os", os);
		params.put("package_name", package_name);
		params.put("version", version);
		params.put("hash", hash);
		
		int cnt =  appApiService.appInfoCheckSelectOne(params);
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		responseJson.setResult(cnt>0?true:false);
		return responseJson;
	}
}