package net.ib.paperless.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import net.ib.paperless.domain.Progress;
import net.ib.paperless.omap.OMAP_0003;
import net.ib.paperless.omap.OMAP_0003_rcv;
import net.ib.paperless.scoket.SocketManager;
import net.ib.paperless.service.OmapLogService;
import net.ib.paperless.service.ProgressService;

@RequestMapping("/ajax/**")
@Controller
public class eFormController {	
	private static final Logger logger = LoggerFactory.getLogger(eFormController.class);
	
	@Autowired
	ProgressService progressService;
		
	@Autowired
	OmapLogService omapLogService;
	
	// 서식 노출을 위한 사용자 데이터 조회
	@RequestMapping({"/eformUserData"})
	@ResponseBody
	public Map<String, Object> getEformUserData(@RequestParam(value="progress_id", required=true) String progress_id, HttpServletRequest request) throws IOException {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		//취약 금융 소비자 동의여부 및 정보
		String vfnYn = request.getParameter("vfnYn");
		String vfnInfo = request.getParameter("vfnInfo");
		
		//개인 신용정보 동의 여부
		Map<String, String> agreeCheckMap = null;
		String param3 = getNullCheckString(request.getParameter("param3"), "");
		String csinfyn01 = getNullCheckString(request.getParameter("csinfyn01"), "");
		String csinfyn02 = getNullCheckString(request.getParameter("csinfyn02"), "");
		String csinfyn03 = getNullCheckString(request.getParameter("csinfyn03"), "");
		String csinfyn04 = getNullCheckString(request.getParameter("csinfyn04"), "");
		String usmktmd02 = getNullCheckString(request.getParameter("usmktmd02"), "");
		String usmktmd03 = getNullCheckString(request.getParameter("usmktmd03"), "");
		String usmktmd04 = getNullCheckString(request.getParameter("usmktmd04"), "");
		String usmktmd05 = getNullCheckString(request.getParameter("usmktmd05"), "");
		agreeCheckMap = getAgreeCheckMap(param3, csinfyn01, csinfyn02, csinfyn03 ,csinfyn04, usmktmd02, usmktmd03, usmktmd04, usmktmd05);
		
		//전문송신 및 수신
		Map<String, Object> omap3_rcv_map = sendOmap0003(progress_id, vfnYn, vfnInfo, agreeCheckMap); 
		
		OMAP_0003_rcv omap3_rcv = (OMAP_0003_rcv) omap3_rcv_map.get("td2");
		boolean sendOmap0003Result = (boolean) omap3_rcv_map.get("sendOmap0003Result");
		
		//개발 		
//		OMAP_0003_rcv omap3_rcv = new OMAP_0003_rcv();
//		boolean sendOmap0003Result = true;

		//jsonData		
//		String jsnoOrderList = omap3_rcv.getTypcolcdesListDataOrder();
//		logger.info("###Response JsonOrderData : " + jsnoOrderList);
		logger.info("## OMAP_0003_rcv (/eformUserData) - sendOmap0003Result : " + sendOmap0003Result);
		
		//tb_progress 테이블에 약정서식종류 구분하여 efrom_id update
		
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		String eform_id = getEformIdByDocflddiv(omap3_rcv.getResult().get("DOCFLDDIV").trim());
		progress.setEform_id(eform_id);
		if(sendOmap0003Result) {
			progressService.progressEformIdUpdate(progress);	
		}
		
		//eForm의 서식에 맞추기 위해 item : [] 리스트 형태로 서식 맞춤
		Map<String,String> jMap = omap3_rcv.getTypcolcdesListData();
		List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		list.add(jMap);
		resultMap.put("item", list);
		resultMap.put("ct_code", omap3_rcv.getResult().get("CONFLDNBR").trim());
		resultMap.put("eform_id", eform_id);
		resultMap.put("sendOmap0003Result", sendOmap0003Result);
		return resultMap;
	}

	/**
	 * 개인신용정보 동의 관련 HashMap으로 변환
	 * @param csinfyn01
	 * @param csinfyn02
	 * @param csinfyn03
	 * @param csinfyn04
	 * @param usmktmd02
	 * @param usmktmd03
	 * @param usmktmd04
	 * @param usmktmd05
	 * @return
	 */
	private Map<String, String> getAgreeCheckMap(String param3, String csinfyn01, String csinfyn02, String csinfyn03, String csinfyn04,
			String usmktmd02, String usmktmd03, String usmktmd04, String usmktmd05) {
		Map<String, String> map = new HashMap<String, String>();
		                    
		if("Y".equals(param3)) {
			if("Y".equals(usmktmd02) && "Y".equals(usmktmd03) && "Y".equals(usmktmd04) && "Y".equals(usmktmd05)) {
				map.put("usmktmd01", "Y");
			}else {
				map.put("usmktmd01", "N");
			}
		}else {
			map.put("usmktmd01", "");
		}
		
		map.put("csinfyn01", csinfyn01);
		map.put("csinfyn02", csinfyn02);
		map.put("csinfyn03", csinfyn03);
		map.put("csinfyn04", csinfyn04);
		map.put("usmktmd02", usmktmd02);
		map.put("usmktmd03", usmktmd03);
		map.put("usmktmd04", usmktmd04);
		map.put("usmktmd05", usmktmd05);
		
		return map;
	}
	
	private String getNullCheckString(String value1, String value2) {
		if(value1 != null && !"".equals(value1)) {
			return value1;
		}
		return value2;
	}

	/**
	 *  ED020001 중고오토론 (return 1)
	 *	ED020002 신차리스 (return 2)
	 *	ED020003 신차오토론 (return 3)
	 *	ED020004 렌터카 (return 4)
	 *	ED020005 중고리스 (return 5)
	 * @param string
	 * @return
	 */
	private String getEformIdByDocflddiv(String string) {
		if("ED020001".equals(string)){
			return "01";
		}else if("ED020002".equals(string)){
			return "02";
		}else if("ED020003".equals(string)){
			return "03";
		}else if("ED020004".equals(string)){
			return "04";
		}else if("ED020005".equals(string)){
			return "05";
		}else {
			return "F";
		}
	}

	private Map<String, Object> sendOmap0003(String progress_id, String vfnYn, String vfnInfo, Map<String, String> agreeCheckMap) throws IOException {
		Map<String, Object> resultMap = new HashMap<String, Object>();

		//ProgressInfo 가져오기
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);

		Date date = new Date();

		//전문 데이터 생성
		String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
		String CONFLDNBR	= resProgress.getContract_code();	
		String CUSTMRCDE	= resProgress.getCustomer_code();
		String KUMFLDYON 	= vfnYn;
		String KUMFLDCDE 	= vfnInfo;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

		//전문 데이터 등록
		OMAP_0003 td1 = new OMAP_0003("OMAP",
				"0003", 
				TRDSEQNBR, 
				resProgress.getService_type(), 
				dateFormat.format(date), 
				timeFormat.format(date), 
				"",
				"",
				CONFLDNBR,
				CUSTMRCDE,
				KUMFLDYON,
				KUMFLDCDE,
				agreeCheckMap
				);

		//전송 전문 저장
		HashMap<String, String> sendLog = td1.getResult();
		logger.info("## OMAP_0003 Log Save(/eformUserData) - seq : "+resProgress.getSeq());
		logger.info("## OMAP_0003 (/eformUserData) [{}]"+ sendLog);
		omapLogService.omapLogInsert(sendLog);

		//수신 전문 생성
		OMAP_0003_rcv td2 = new OMAP_0003_rcv();

		//전문  전송
		SocketManager.connection(td1, td2);
		td2.print();

		//수신 전문 저장
		HashMap<String, String> receiveLog = td2.getResult();
		omapLogService.omapLogUpdate(receiveLog);

		boolean sendOmap0003Result = false;
		
		if("00".equals(receiveLog.get("RSTFLDCDE").trim())){
			sendOmap0003Result = true;
		}
		resultMap.put("sendOmap0003Result", sendOmap0003Result);
		resultMap.put("td2", td2);
		
		return resultMap;
	}
}
