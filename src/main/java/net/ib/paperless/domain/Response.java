package net.ib.paperless.domain;

import lombok.Data;

@Data
public class Response {
	private String response;
	private String note;
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
}
