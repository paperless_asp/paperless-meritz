package net.ib.paperless.domain;

public class ProgressStatus {
	private Integer seq;
	private String progress_id;
	private Integer eform_complet_yn;
	private String eform_complet_date;
	private Integer attach_transfer_yn;
	private String attach_transfer_date;
	private Integer attach_yn;
	private String attach_date;
	private Integer account_identity_yn;
	private String account_identity_date;
	private Integer account_transfer_yn;
	private String account_transfer_date;
	private Integer account_verify_yn;
	private String account_verify_date;
	private Integer identity_chk_yn;
	private String identity_chk_date;
	private String reg_date;
	
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getProgress_id() {
		return progress_id;
	}
	public void setProgress_id(String progress_id) {
		this.progress_id = progress_id;
	}
	public Integer getEform_complet_yn() {
		return eform_complet_yn;
	}
	public void setEform_complet_yn(Integer eform_complet_yn) {
		this.eform_complet_yn = eform_complet_yn;
	}
	public String getEform_complet_date() {
		return eform_complet_date;
	}
	public void setEform_complet_date(String eform_complet_date) {
		this.eform_complet_date = eform_complet_date;
	}
	public Integer getAttach_transfer_yn() {
		return attach_transfer_yn;
	}
	public void setAttach_transfer_yn(Integer attach_transfer_yn) {
		this.attach_transfer_yn = attach_transfer_yn;
	}
	public String getAttach_transfer_date() {
		return attach_transfer_date;
	}
	public void setAttach_transfer_date(String attach_transfer_date) {
		this.attach_transfer_date = attach_transfer_date;
	}
	public Integer getAttach_yn() {
		return attach_yn;
	}
	public void setAttach_yn(Integer attach_yn) {
		this.attach_yn = attach_yn;
	}
	public String getAttach_date() {
		return attach_date;
	}
	public void setAttach_date(String attach_date) {
		this.attach_date = attach_date;
	}
	public Integer getAccount_identity_yn() {
		return account_identity_yn;
	}
	public void setAccount_identity_yn(Integer account_identity_yn) {
		this.account_identity_yn = account_identity_yn;
	}
	public String getAccount_identity_date() {
		return account_identity_date;
	}
	public void setAccount_identity_date(String account_identity_date) {
		this.account_identity_date = account_identity_date;
	}
	public Integer getAccount_transfer_yn() {
		return account_transfer_yn;
	}
	public void setAccount_transfer_yn(Integer account_transfer_yn) {
		this.account_transfer_yn = account_transfer_yn;
	}
	public String getAccount_transfer_date() {
		return account_transfer_date;
	}
	public void setAccount_transfer_date(String account_transfer_date) {
		this.account_transfer_date = account_transfer_date;
	}
	public Integer getAccount_verify_yn() {
		return account_verify_yn;
	}
	public void setAccount_verify_yn(Integer account_verify_yn) {
		this.account_verify_yn = account_verify_yn;
	}
	public String getAccount_verify_date() {
		return account_verify_date;
	}
	public void setAccount_verify_date(String account_verify_date) {
		this.account_verify_date = account_verify_date;
	}
	public Integer getIdentity_chk_yn() {
		return identity_chk_yn;
	}
	public void setIdentity_chk_yn(Integer identity_chk_yn) {
		this.identity_chk_yn = identity_chk_yn;
	}
	public String getIdentity_chk_date() {
		return identity_chk_date;
	}
	public void setIdentity_chk_date(String identity_chk_date) {
		this.identity_chk_date = identity_chk_date;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	
}
