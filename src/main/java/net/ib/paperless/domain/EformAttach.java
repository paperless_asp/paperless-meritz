package net.ib.paperless.domain;

//@Data
public class EformAttach{
	private int seq;
	private String progress_id;
	private String eform_attach_id;
	private String path;
	private int upload_yn;
	private String upload_date;
	private int transfer_yn;
	private String transfer_date;
	private int admin_confirm_yn;
	private String admin_confirm_date;
	private String note;
	
	private String eform_id;
	private String id;
	private String name;
	private String eform_path;
	private int required_yn;
	private String reg_date;
	private String user_key;
	
	private String loan_id;
	
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getProgress_id() {
		return progress_id;
	}
	public void setProgress_id(String progress_id) {
		this.progress_id = progress_id;
	}
	public String getEform_attach_id() {
		return eform_attach_id;
	}
	public void setEform_attach_id(String eform_attach_id) {
		this.eform_attach_id = eform_attach_id;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getUpload_yn() {
		return upload_yn;
	}
	public void setUpload_yn(int upload_yn) {
		this.upload_yn = upload_yn;
	}
	public String getUpload_date() {
		return upload_date;
	}
	public void setUpload_date(String upload_date) {
		this.upload_date = upload_date;
	}
	public int getTransfer_yn() {
		return transfer_yn;
	}
	public void setTransfer_yn(int transfer_yn) {
		this.transfer_yn = transfer_yn;
	}
	public String getTransfer_date() {
		return transfer_date;
	}
	public void setTransfer_date(String transfer_date) {
		this.transfer_date = transfer_date;
	}
	public int getAdmin_confirm_yn() {
		return admin_confirm_yn;
	}
	public void setAdmin_confirm_yn(int admin_confirm_yn) {
		this.admin_confirm_yn = admin_confirm_yn;
	}
	public String getAdmin_confirm_date() {
		return admin_confirm_date;
	}
	public void setAdmin_confirm_date(String admin_confirm_date) {
		this.admin_confirm_date = admin_confirm_date;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getEform_id() {
		return eform_id;
	}
	public void setEform_id(String eform_id) {
		this.eform_id = eform_id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEform_path() {
		return eform_path;
	}
	public void setEform_path(String eform_path) {
		this.eform_path = eform_path;
	}
	public int getRequired_yn() {
		return required_yn;
	}
	public void setRequired_yn(int required_yn) {
		this.required_yn = required_yn;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getUser_key() {
		return user_key;
	}
	public void setUser_key(String user_key) {
		this.user_key = user_key;
	}
	public String getLoan_id() {
		return loan_id;
	}
	public void setLoan_id(String loan_id) {
		this.loan_id = loan_id;
	}
	@Override
	public String toString() {
		return "EformAttach [seq=" + seq + ", progress_id=" + progress_id + ", eform_attach_id=" + eform_attach_id
				+ ", path=" + path + ", upload_yn=" + upload_yn + ", upload_date=" + upload_date + ", transfer_yn="
				+ transfer_yn + ", transfer_date=" + transfer_date + ", admin_confirm_yn=" + admin_confirm_yn
				+ ", admin_confirm_date=" + admin_confirm_date + ", note=" + note + ", eform_id=" + eform_id + ", id="
				+ id + ", name=" + name + ", eform_path=" + eform_path + ", required_yn=" + required_yn + ", reg_date="
				+ reg_date + ", user_key=" + user_key + ", loan_id=" + loan_id + "]";
	}
	
	
}