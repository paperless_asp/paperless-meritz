package net.ib.paperless.domain;

public class AccountHistoryInfo {
	private String seq;
	private String progress_id;
	private String account_holder_name;
	private String account_holder_number;
	private String account_bank_code;
	private String account_bank_name;
	private String account_ssn_number;
	private String account_sex;
	private String inquiry_realname_yn;
	private String inquiry_realname_date;
	private String inquiry_transfer_deposit_yn;
	private String inquiry_transfer_deposit_date;
	private String authcode_confirm_yn;
	private String authcode_confirm_date;
	private String req_code;
	
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getProgress_id() {
		return progress_id;
	}
	public void setProgress_id(String progress_id) {
		this.progress_id = progress_id;
	}
	public String getAccount_holder_name() {
		return account_holder_name;
	}
	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}
	public String getAccount_holder_number() {
		return account_holder_number;
	}
	public void setAccount_holder_number(String account_holder_number) {
		this.account_holder_number = account_holder_number;
	}
	public String getAccount_bank_code() {
		return account_bank_code;
	}
	public void setAccount_bank_code(String account_bank_code) {
		this.account_bank_code = account_bank_code;
	}
	public String getAccount_bank_name() {
		return account_bank_name;
	}
	public void setAccount_bank_name(String account_bank_name) {
		this.account_bank_name = account_bank_name;
	}
	public String getAccount_ssn_number() {
		return account_ssn_number;
	}
	public void setAccount_ssn_number(String account_ssn_number) {
		this.account_ssn_number = account_ssn_number;
	}
	public String getAccount_sex() {
		return account_sex;
	}
	public void setAccount_sex(String account_sex) {
		this.account_sex = account_sex;
	}
	public String getInquiry_realname_yn() {
		return inquiry_realname_yn;
	}
	public void setInquiry_realname_yn(String inquiry_realname_yn) {
		this.inquiry_realname_yn = inquiry_realname_yn;
	}
	public String getInquiry_realname_date() {
		return inquiry_realname_date;
	}
	public void setInquiry_realname_date(String inquiry_realname_date) {
		this.inquiry_realname_date = inquiry_realname_date;
	}
	public String getInquiry_transfer_deposit_yn() {
		return inquiry_transfer_deposit_yn;
	}
	public void setInquiry_transfer_deposit_yn(String inquiry_transfer_deposit_yn) {
		this.inquiry_transfer_deposit_yn = inquiry_transfer_deposit_yn;
	}
	public String getInquiry_transfer_deposit_date() {
		return inquiry_transfer_deposit_date;
	}
	public void setInquiry_transfer_deposit_date(String inquiry_transfer_deposit_date) {
		this.inquiry_transfer_deposit_date = inquiry_transfer_deposit_date;
	}
	public String getAuthcode_confirm_yn() {
		return authcode_confirm_yn;
	}
	public void setAuthcode_confirm_yn(String authcode_confirm_yn) {
		this.authcode_confirm_yn = authcode_confirm_yn;
	}
	public String getAuthcode_confirm_date() {
		return authcode_confirm_date;
	}
	public void setAuthcode_confirm_date(String authcode_confirm_date) {
		this.authcode_confirm_date = authcode_confirm_date;
	}
	public String getReq_code() {
		return req_code;
	}
	public void setReq_code(String req_code) {
		this.req_code = req_code;
	}
	
	public String toJsonString() {
		return 
				"{"
				+ "\"progress_id\":\"" +progress_id+  "\", "
				+ "\"account_ssn_number\":\""+account_ssn_number+"\", "
				+ "\"account_holder_name\":\""+account_holder_name+"\", "
				+ "\"account_sex\":\""+account_sex+"\", "
				+ "\"inquiry_transfer_deposit_date\":\""+inquiry_transfer_deposit_date+"\", "
				+ "\"inquiry_realname_date\":\""+inquiry_realname_date+"\", "
				+ "\"inquiry_realname_yn\":\""+inquiry_realname_yn+"\", "
				+ "\"authcode_confirm_date\":\""+authcode_confirm_date+"\", "
				+ "\"req_code\":\""+req_code+"\", "
				+ "\"account_bank_code\":\""+account_bank_code+"\", "
				+ "\"account_holder_number\":\""+account_holder_number+"\", "
				+ "\"inquiry_transfer_deposit_yn\":\""+inquiry_transfer_deposit_yn+"\", "
				+ "\"account_bank_name\":\""+account_bank_name+"\", "
				+ "\"authcode_confirm_yn\":\""+authcode_confirm_yn+"\" "
				+ "}";
	}
	
	
	
}
