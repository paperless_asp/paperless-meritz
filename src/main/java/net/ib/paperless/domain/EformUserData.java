package net.ib.paperless.domain;

public class EformUserData {

	private int seq;
	private String loan_name;
	private String amount;
	private String interest_per;
	private String interest_day;
	private String repayment_method;
	private String repayment_commission;
	private String account;
	private String start_date;
	private String expire_date;
	private String loan_method;
	private String user_name;
	private String registration_number;
	private String reg_date;
	
	
	//------
	private String progress_id;
	private String admin_id;
	private String start_date_memo;
	private String expire_date_memo;
	private String interest_per_memo;
	private String note;
	private String eform_id;

	public String getEform_id() {
		return eform_id;
	}
	public void setEform_id(String eform_id) {
		this.eform_id = eform_id;
	}
	public String getProgress_id() {
		return progress_id;
	}
	public void setProgress_id(String progress_id) {
		this.progress_id = progress_id;
	}
	public String getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
	public String getStart_date_memo() {
		return start_date_memo;
	}
	public void setStart_date_memo(String start_date_memo) {
		this.start_date_memo = start_date_memo;
	}
	public String getExpire_date_memo() {
		return expire_date_memo;
	}
	public void setExpire_date_memo(String expire_date_memo) {
		this.expire_date_memo = expire_date_memo;
	}
	public String getInterest_per_memo() {
		return interest_per_memo;
	}
	public void setInterest_per_memo(String interest_per_memo) {
		this.interest_per_memo = interest_per_memo;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getLoan_name() {
		return loan_name;
	}
	public void setLoan_name(String loan_name) {
		this.loan_name = loan_name;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getInterest_per() {
		return interest_per;
	}
	public void setInterest_per(String interest_per) {
		this.interest_per = interest_per;
	}
	public String getInterest_day() {
		return interest_day;
	}
	public void setInterest_day(String interest_day) {
		this.interest_day = interest_day;
	}
	public String getRepayment_method() {
		return repayment_method;
	}
	public void setRepayment_method(String repayment_method) {
		this.repayment_method = repayment_method;
	}
	public String getRepayment_commission() {
		return repayment_commission;
	}
	public void setRepayment_commission(String repayment_commission) {
		this.repayment_commission = repayment_commission;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getExpire_date() {
		return expire_date;
	}
	public void setExpire_date(String expire_date) {
		this.expire_date = expire_date;
	}
	public String getLoan_method() {
		return loan_method;
	}
	public void setLoan_method(String loan_method) {
		this.loan_method = loan_method;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getRegistration_number() {
		return registration_number;
	}
	public void setRegistration_number(String registration_number) {
		this.registration_number = registration_number;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
}
