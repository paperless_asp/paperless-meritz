package net.ib.paperless.domain;

public class PhoneAuthCode {
	private Integer seq;
	private String progress_id;
	private String tel_numer;
	private String auth_code;
	private Integer fail_count = 1;
	private String reg_date;
	
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getProgress_id() {
		return progress_id;
	}
	public void setProgress_id(String progress_id) {
		this.progress_id = progress_id;
	}
	public String getTel_numer() {
		return tel_numer;
	}
	public void setTel_numer(String tel_numer) {
		this.tel_numer = tel_numer;
	}
	public String getAuth_code() {
		return auth_code;
	}
	public void setAuth_code(String auth_code) {
		this.auth_code = auth_code;
	}
	public Integer getFail_count() {
		return fail_count;
	}
	public void setFail_count(Integer fail_count) {
		this.fail_count = fail_count;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
}
