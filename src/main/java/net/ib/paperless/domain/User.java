package net.ib.paperless.domain;


public class User {
	private String loanId;
	private String userKey;
	private String userName;
	private String userPhone;
	private String requireAmount;
	private String loanName;
	private String jumin;
	private String authKey;
	private String remoteAddr;
	
	public String getAuthKey() {
		return authKey;
	}
	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	public String getJumin() {
		return jumin;
	}
	public void setJumin(String jumin) {
		this.jumin = jumin;
	}
	public String getLoanName() {
		return loanName;
	}
	public void setLoanName(String loanName) {
		this.loanName = loanName;
	}
	public String getLoanId() {
		return loanId;
	}
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}
	public String getUserKey() {
		return userKey;
	}
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getRequireAmount() {
		return requireAmount;
	}
	public void setRequireAmount(String requireAmount) {
		this.requireAmount = requireAmount;
	}
	public String getRemoteAddr() {
		return remoteAddr;
	}
	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}
	@Override
	public String toString() {
		return "User [loanId=" + loanId + ", userKey=" + userKey + ", userName=" + userName + ", userPhone=" + userPhone
				+ ", requireAmount=" + requireAmount + ", loanName=" + loanName + ", jumin=" + jumin + ", authKey="
				+ authKey + ", remoteAddr=" + remoteAddr + "]";
	}
	
}