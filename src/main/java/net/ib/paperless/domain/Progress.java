package net.ib.paperless.domain;


public class Progress {
	private Integer seq;
	private String progress_type;
	private String service_type;
	private String contract_code;
	private String customer_code;
	private String progress_status;
	private String status_name;
	private String uuids;
	private String eform_id;
	private String param1;
	private String param2;
	private String param3;
	private String param4;
	private Integer file_cnt;
	private String update_date;
	private String reg_date;
	

	public Integer getFile_cnt() {
		return file_cnt;
	}
	public void setFile_cnt(Integer file_cnt) {
		this.file_cnt = file_cnt;
	}
	public String getProgress_type() {
		return progress_type;
	}
	public void setProgress_type(String progress_type) {
		this.progress_type = progress_type;
	}
	public String getService_type() {
		return service_type;
	}
	public void setService_type(String service_type) {
		this.service_type = service_type;
	}
	public String getEform_id() {
		return eform_id;
	}
	public void setEform_id(String eform_id) {
		this.eform_id = eform_id;
	}
	public String getUuids() {
		return uuids;
	}
	public void setUuids(String uuids) {
		this.uuids = uuids;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getContract_code() {
		return contract_code;
	}
	public void setContract_code(String contract_code) {
		this.contract_code = contract_code;
	}
	public String getCustomer_code() {
		return customer_code;
	}
	public void setCustomer_code(String customer_code) {
		this.customer_code = customer_code;
	}
	public String getProgress_status() {
		return progress_status;
	}
	public void setProgress_status(String progress_status) {
		this.progress_status = progress_status;
	}
	public String getStatus_name() {
		return status_name;
	}
	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}
	public String getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getParam1() {
		return param1;
	}
	public void setParam1(String param1) {
		this.param1 = param1;
	}
	public String getParam2() {
		return param2;
	}
	public void setParam2(String param2) {
		this.param2 = param2;
	}
	public String getParam3() {
		return param3;
	}
	public void setParam3(String param3) {
		this.param3 = param3;
	}
	public String getParam4() {
		return param4;
	}
	public void setParam4(String param4) {
		this.param4 = param4;
	}
}
