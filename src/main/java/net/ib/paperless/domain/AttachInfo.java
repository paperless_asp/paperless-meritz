package net.ib.paperless.domain;

public class AttachInfo {
	private int seq;
	private String eform_id;
	private String id;
	private String name;
	private String eform_path;
	private int required_yn;
	private String reg_date;
	private int upload_yn;
	
	public int getUpload_yn() {
		return upload_yn;
	}
	public void setUpload_yn(int upload_yn) {
		this.upload_yn = upload_yn;
	}
	
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getEform_id() {
		return eform_id;
	}
	public void setEform_id(String eform_id) {
		this.eform_id = eform_id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEform_path() {
		return eform_path;
	}
	public void setEform_path(String eform_path) {
		this.eform_path = eform_path;
	}
	public int getRequired_yn() {
		return required_yn;
	}
	public void setRequired_yn(int required_yn) {
		this.required_yn = required_yn;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
}
