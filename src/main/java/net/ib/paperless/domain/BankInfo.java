package net.ib.paperless.domain;

public class BankInfo {

	private String bank_code;
	private String bank_name;
	private String major_yn;
	private String available_yn;
	private String bank_type;
	private String available_op;
	
	public String getBank_code() {
		return bank_code;
	}
	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getMajor_yn() {
		return major_yn;
	}
	public void setMajor_yn(String major_yn) {
		this.major_yn = major_yn;
	}
	public String getAvailable_yn() {
		return available_yn;
	}
	public void setAvailable_yn(String available_yn) {
		this.available_yn = available_yn;
	}
	public String getBank_type() {
		return bank_type;
	}
	public void setBank_type(String bank_type) {
		this.bank_type = bank_type;
	}
	public String getAvailable_op() {
		return available_op;
	}
	public void setAvailable_op(String available_op) {
		this.available_op = available_op;
	}
	@Override
	public String toString() {
		return "BankInfo [bank_code=" + bank_code + ", bank_name=" + bank_name + ", major_yn=" + major_yn
				+ ", available_yn=" + available_yn + ", bank_type=" + bank_type + ", available_op=" + available_op
				+ "]";
	}
}
