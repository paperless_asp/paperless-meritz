package net.ib.paperless.common;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ApiResponse<T> extends LinkedHashMap<String, Object> {
	
	private static final long serialVersionUID = 1L;


    public ApiResponse() {

    }
    
    public ApiResponse(boolean result, String message , int totalItems , String resultType ) {
    	this.put("result", result);
    	this.put("message", message);
    	this.put("totalItems", totalItems);
    	this.put("resultType", resultType);
    }

    public void setResult(boolean result) {
    	this.put("result", result);
    }

    public void setMessage(String message){
    	this.put("message", message);
    }
    
    public void setTotalItems(int totalItems){
    	this.put("totalItems", totalItems);
    }
    
    public void setResultType(String resultType){
    	this.put("resultType", resultType);
    }
    public void setType(String type){
    	this.put("Type", type);
    }
    
    /*public void setList(List<Object> list ) {
    	this.put("list", list);
    }*/
    
    public void setList(List<?> list ) {
    	this.put("list", list);
    }

    public void setMap(Map<String, Object> map ) {
    	this.put("map", map);
    }

    public void setString(String str) {
    	this.put("string", str);
    }
    
    public void setPageNo(int pageNo) {
    	this.put("pageNo", pageNo);
    }
    
    public void setItemSize(int itemSize) {
    	this.put("itemSize", itemSize);
    }
	
	public void setUrl(String url) {
    	this.put("url", url);
	}
}
