package net.ib.paperless.common;

import java.util.Map;

public class Function {
	public ApiResponse<Map<String, Object>> getResultStatus(String code){
		
		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		
		//00:성공 , 01:등록실패, 03:계약정보없음, 04:고객정보없음, 05:휴대전화번호상이, 06:유효기간만료
		if(code.equals("00")){
			responseJson.setResult(true);
			responseJson.setMessage("성공");
		}else if(code.equals("01")){
			responseJson.setResult(false);
			responseJson.setMessage("등록에 실패 하였습니다.");
		}else if(code.equals("03")){
			responseJson.setResult(false);
			responseJson.setMessage("계약정보가 일치하지 않습니다.");
		}else if(code.equals("04")){
			responseJson.setResult(false);
			responseJson.setMessage("고객정보가 일치하지 않습니다.");
		}else if(code.equals("05")){
			responseJson.setResult(false);
			responseJson.setMessage("휴대폰번호가 일치하지 않습니다.");
		}else if(code.equals("06")){
			responseJson.setResult(false);
			responseJson.setMessage("유효기간이 만료 되었습니다.");
		}
		
		return responseJson;
	}
}
