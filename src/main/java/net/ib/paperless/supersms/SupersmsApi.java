package net.ib.paperless.supersms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import net.minidev.json.JSONObject;

public class SupersmsApi {
	
	private static final Logger logger = LoggerFactory.getLogger(SupersmsApi.class);

	/******************************************************************************************
	 ** Static 변수 정의
	 ******************************************************************************************/
//	static String id = "goodpaper_int";
//	static String pw = "KIC429Q5198IHFF02LAY";
	static String id = "meritzcapital_int";
	static String pw = "16106ZABL850QZNQ103X";
	
	static String getAccessToken = "https://auth.supersms.co:7000/auth/v3/token";
	static String sendMessage = "https://sms.supersms.co:7020/sms/v3/multiple-destinations";
	
	static String postMethod = "POST";
	static String getMethod = "GET";
	
	public enum httpType { FormType, JsonType}
	
	/**
	 * sms를 발송한다.
	 * @return	조회결과(jsonString)
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, Object> sendMessage(String from, String text, String to){		
		
		HashMap<String, Object> destinations = new HashMap<>();
		destinations.put("to", "82"+to);
		
		ArrayList<Object> list = new ArrayList<>();
		list.add(destinations);
		
		HashMap<String, Object> params = new HashMap<>();
		params.put("from",from);
		params.put("text",text);
		params.put("destinations",list);
		params.put("ttl", "86400");
		
		String result = httpUrlConnection(httpType.JsonType, true, sendMessage, postMethod, params);
		
		Gson gson = new Gson();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map = (HashMap<String, Object>)gson.fromJson(result, map.getClass());
		
		return map;
	}
	
	
	/**
	 * accessToken을 조회한다.
	 * @return	조회결과(jsonString)
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, Object> getAccessToken(){

		HashMap<String, Object> params = new HashMap<>();
		params.put("X-IB-Client-Id",id);
		params.put("X-IB-Client-Passwd",pw);
		
		String result = httpUrlConnection(httpType.FormType, false, getAccessToken, postMethod, null);
		
		Gson gson = new Gson();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map = (HashMap<String, Object>)gson.fromJson(result, map.getClass());
		
		return map;
	}
	
	/**
	 * httpRequest function
	 * @param type 		content-type (formType or jsonType)
	 * @param isHeader	accessToke 포함 여부
	 * @param url		host를 제외한 url
	 * @param method	http method(post or get)
	 * @param body		hashMap<string,string> parameter
	 * @return			resopnse to jsonString
	 */
	public static String httpUrlConnection(httpType type, Boolean isHeader, String url, String method, HashMap<String, Object> body)
	{
		
		String accessToken = "";
		
		if(isHeader)
		{
			HashMap<String, Object> result = getAccessToken();
			accessToken = (String)result.get("accessToken");
			logger.info("### accessToken =" + accessToken);
		}
		
		HttpsURLConnection httpsURLConnection = null;
		StringBuffer param = new StringBuffer();
		String result = "";
		
		if(isHeader)
		{
			if(body != null)
			{
				JSONObject jsonObject = new JSONObject(body);
				param.append(jsonObject.toJSONString());
			}
		}
		
			

		logger.info("### requestUrl  = "+url);
		logger.info("### method = " + method);
		//System.out.println("param = "+param);
		
        try {
			URL httpUrl = new URL(url);
			httpsURLConnection = (HttpsURLConnection) httpUrl.openConnection();
			httpsURLConnection.setRequestMethod(method);

			httpsURLConnection.setRequestProperty("Accept","application/json;");
			if(isHeader) httpsURLConnection.setRequestProperty("Content-type","application/json; charset=UTF-8");
			httpsURLConnection.setUseCaches(false);
			httpsURLConnection.setDoOutput(true);
			httpsURLConnection.setDoInput(true);
			if(isHeader) {
				httpsURLConnection.setRequestProperty("Authorization", "Basic " + accessToken);
				logger.info("### Authorization"+"Basic " + accessToken);
			}
			else{
				httpsURLConnection.setRequestProperty("X-IB-Client-Id", id);
				httpsURLConnection.setRequestProperty("X-IB-Client-Passwd", pw);
			}
			if(body != null)
			{
				OutputStream outputStream = httpsURLConnection.getOutputStream();
				outputStream.write(param.toString().getBytes("UTF-8") );
				outputStream.flush();
				outputStream.close();
			}
			httpsURLConnection.connect();  
			httpsURLConnection.setInstanceFollowRedirects(true);  
			   
			String temp = null;
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			while((temp = bufferedReader.readLine()) != null){
				result+= temp + "\n";
			}
			
//			logger.info("### result = "+result);


        } catch (Exception e) {

             e.printStackTrace();

        } finally {
             if (httpsURLConnection != null) {
            	 httpsURLConnection.disconnect();
             }
        }
        
		return result;
        
	}
	
	
	
}