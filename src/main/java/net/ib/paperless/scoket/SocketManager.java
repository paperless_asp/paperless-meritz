package net.ib.paperless.scoket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import net.ib.paperless.omap.OMAP;

@ConfigurationProperties
@Component
public class SocketManager {
	private static final Logger logger = LoggerFactory.getLogger(SocketManager.class);
	private static String meritz_server_ip;
	private static int meritz_server_port;
	
	public SocketManager(@Value("${meritz_server_ip}") String meritz_ip, @Value("${meritz_server_port}") int meritz_port){
		meritz_server_ip = meritz_ip;
		meritz_server_port = meritz_port;
	}

	public static boolean connection(OMAP td1, OMAP td2) throws IOException{
		boolean returnValue = true;
		
		Socket socket = null;
		try {
			socket = new Socket(meritz_server_ip, meritz_server_port);
		} catch (IOException ie) {
			logger.info("cannot establish socket connection to " 
							   + meritz_server_ip + ":" + meritz_server_port + " - " + ie);
			return false;
			//System.exit(2);
		}
		
		if(socket != null && socket.isConnected()){
			
			logger.info("Socket Connection Success");

			DataInputStream in = new DataInputStream(socket.getInputStream());
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());

			try {
				// first write to server socket
				logger.info("--------------------------------------------------------------------------");
				td1.writeDataExternal(out);
				td1.print();
				out.flush();
				logger.info("write to socket server ends");

				// later, read from server socket
				logger.info("--------------------------------------------------------------------------");
				td2.readDataExternal(in);
				logger.info("read from socket server ends");
				
				return returnValue;
				
			}catch(Exception e) {
				return false;
			}finally {
				out.close();
				in.close();
				socket.close();
			}
		}
		
		return returnValue;
	}
	
}
