package net.ib.paperless.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ib.paperless.domain.PhoneAuthCode;
import net.ib.paperless.repository.PhoneAuthRepository;

@Service
public class PhoneAuthService {
	
	@Autowired
	PhoneAuthRepository phoneAuthRepository;
	
	public int phoneAuthInfoInsert(PhoneAuthCode pac){
		int isPhoneAuthCode = phoneAuthRepository.phoneAuthInfoInsert(pac);
		if(isPhoneAuthCode == 1){
			
		}
		return isPhoneAuthCode;
	}
	
	public PhoneAuthCode phoneAuthInfoSelectByTelAuth(PhoneAuthCode pac){
		PhoneAuthCode returnPac = phoneAuthRepository.phoneAuthInfoSelectByTelAuth(pac);
		if(returnPac != null){
			return returnPac;
		}
		return null;
	}
	
	public int phoneAuthInfoDeleteByTelAuth(PhoneAuthCode pac){
		int isDeleteAuthInfo = phoneAuthRepository.phoneAuthInfoDeleteByTelAuth(pac);
		if(isDeleteAuthInfo == 1){
			
		}
		return isDeleteAuthInfo;
	}
	
	public int phoneAuthInfoUpdateFailCountByTelAuth(PhoneAuthCode pac){
		int isUpdate = phoneAuthRepository.phoneAuthInfoUpdateFailCountByTelAuth(pac);
		if(isUpdate == 1){
			
		}
		return isUpdate;
	}
	
}
