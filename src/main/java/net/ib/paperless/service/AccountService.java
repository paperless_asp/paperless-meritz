package net.ib.paperless.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ib.paperless.domain.AccountHistoryInfo;
import net.ib.paperless.domain.AccountVerifyInfo;
import net.ib.paperless.domain.BankInfo;
import net.ib.paperless.domain.Progress;
import net.ib.paperless.omap.OMAP_0100;
import net.ib.paperless.repository.AccountRepository;
import net.ib.paperless.scoket.SocketManager;

@Service
public class AccountService {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

	@Autowired
	AccountRepository accountRepository;
	@Autowired
	ProgressService progressService;
	@Autowired
	OmapLogService omapLogService;
	
	public Map<String,Object> setAccountVerifyInfo(AccountVerifyInfo codeInfo){
		
		Map<String, Object> retMap = new HashMap<String, Object>();
	
		if(accountRepository.accountVerifyInfoInsert(codeInfo) == 0){
			retMap.put("result",false);
			retMap.put("msg"," failed insert AccountVerifyInfo");
			return retMap;
		}
		
		if(accountRepository.accountTransferUpdate(codeInfo) == 0){
			retMap.put("result",false);
			retMap.put("msg"," failed insert AccountVerifyInfo");
			return retMap;
		}
		
		retMap.put("result",true);
		retMap.put("msg"," success insert AccountVerifyInfo");
		return retMap;
	}
	
	public AccountVerifyInfo getAuthCodeInfo(String progress_id){
		
		AccountVerifyInfo accountVerifyInfo = accountRepository.getAuthCodeInfo(progress_id);
		
		if(accountVerifyInfo != null){
			return accountVerifyInfo;
		}
		
		return null;
		
	}
	public Boolean isServiceCheck(String progress_id) throws IOException{
		
		//ProgressInfo 가져오기
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));
		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);

		Date date = new Date();
		
		//전문 데이터 생성
		String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
		
		OMAP_0100 td1 = new OMAP_0100("OMAP", 
				"0100", 
				TRDSEQNBR, 
				resProgress.getService_type(), 
				dateFormat.format(date), 
				timeFormat.format(date), 
				"", 
				"", 
				resProgress.getParam1(),
				resProgress.getParam2(),
				resProgress.getParam3(),
				resProgress.getParam4());
		
		//전송 전문 저장
		HashMap<String, String> sendLog = td1.getResult();
		logger.info("## OMAP_0100 Log Save(isServiceCheck) - seq : "+resProgress.getSeq());
		logger.info("## OMAP_0100 (isServiceCheck) [{}]"+ sendLog);
		omapLogService.omapLogInsert2(sendLog);
		
		//수신 전문 생성
		OMAP_0100 td2 = new OMAP_0100();
		
		//전문  전송
		SocketManager.connection(td1, td2);
		td2.print();
		
		//수신 전문 저장
		HashMap<String, String> receiveLog = td2.getResult();
		omapLogService.omapLogUpdate2(receiveLog);
		
		if(receiveLog.get("RSTFLDCDE").equals("00")){
			return true;
		}

		return false;
	}
	
	public int accountTransferFailUpdate(String progress_id){
		return accountRepository.accountTransferFailUpdate(progress_id);
	}
	
	public int accountVerifyUpdate(String progress_id){
		return accountRepository.accountVerifyUpdate(progress_id);
	}
	
	public int accountVerifyCntUpdate(String progress_id){
		return accountRepository.accountVerifyCntUpdate(progress_id);
	}
	
	public int accountVerifyCodeDelete(String progress_id){
		return accountRepository.accountVerifyCodeDelete(progress_id);
	}
	
	public int accountFailInfoInsert(HashMap<String, Object> map){
		return accountRepository.accountFailInfoInsert(map);
	}
	/**
	 * History 관련
	 */
	public int insertAccountHistoryInfo(AccountHistoryInfo codeInfo){
		return accountRepository.accountHistoryInfoInsert(codeInfo);
	}
	public int getHistoryCount(Map<String,Object> params) {
		return accountRepository.getHistoryCount(params);
	}
	/**
	 * NH계좌인증 관련
	 */
	public List<BankInfo> getBankInfoList(){
		return accountRepository.selectBankInfoList();
	}
	public int accountNHPublicFailInfoInsert(HashMap<String, Object> map){
		return accountRepository.accountNHPublicFailInfoInsert(map);
	}
	
}
