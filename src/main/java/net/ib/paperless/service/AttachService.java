package net.ib.paperless.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import net.ib.paperless.common.ApiResponse;
import net.ib.paperless.common.SFTPHandler;
import net.ib.paperless.domain.AttachInfo;
import net.ib.paperless.domain.EformAttach;
import net.ib.paperless.domain.Progress;
import net.ib.paperless.domain.ProgressAttach;
import net.ib.paperless.omap.OMAP_0007;
import net.ib.paperless.omap.OMAP_0110;
import net.ib.paperless.repository.AttachRepository;
import net.ib.paperless.repository.ProgressRepository;
import net.ib.paperless.scoket.SocketManager;

@Service
public class AttachService {
	
	private static final Logger logger = LoggerFactory.getLogger(AttachService.class);
	
	@Value("${deploy.server}")
	private String deployServer;
	
	@Value("${sftp_ip}")
	private String sftp_ip;
	
	@Value("${sftp_id}")
	private String sftp_id;
	
	@Value("${sftp_pw}")
	private String sftp_pw;
	
	@Value("${sftp_port}")
	private String sftp_port;
	
//	@Autowired
//    UploadGateway gateway;
	
	@Autowired
	AttachRepository attachRepository;
	@Autowired
	ProgressRepository progressRepository;
	@Autowired
	OmapLogService omapLogService;
	@Autowired
	ProgressService progressService;
	
	public List<AttachInfo> getAttachList(String progress_id){
		
		return attachRepository.getAttachList(progress_id);
	}
	
	//progress_type = 2 , 업로드파일개수가 정해진 것이라면 where조건으로 uploadyn 1인것만 뽑아옴
	public List<EformAttach> eFormAttachSelectByProgressId3(String progressId){
		return attachRepository.eFormAttachSelectByProgressId3(progressId);
	}
	
	public List<EformAttach> eFormAttachSelectByProgressId2(String progressId){
		return attachRepository.eFormAttachSelectByProgressId2(progressId);
	}
	
	public List<EformAttach> eFormAttachSelectByProgressId1(String progressId){
		return attachRepository.eFormAttachSelectByProgressId1(progressId);
	}
	
	public Boolean eFormAttachFileSave(String progressId , int attachId , MultipartFile attach, int attach_cnt, String type, String progress_type, String param1) throws IOException{
		
		boolean ret = false;
		boolean attachYnBoo = true;
		
		//파일명 정의를 위한 유일날짜 정의
		Date date_file = new Date();
		SimpleDateFormat dateFormat_file = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat timeFormat_file = new SimpleDateFormat("HHmmss");
		String timeStampNow = dateFormat_file.format(date_file) + timeFormat_file.format(date_file);
				
		String filepath = "";
		String fileName = param1+ "_" + progressId + "_" + timeStampNow + "_" + attachId + ".jpg";
		filepath = putFileUpload(attach,fileName, param1);
//		filepath = "/root/test";
		
		logger.info("### fileAttachLog Start..");
		logger.info("### fileAttachLog filepath : " + filepath);
		logger.info("### fileAttachLog progress_type : " + progress_type);
		logger.info("### fileAttachLog type : " + type);
		logger.info("### fileAttachLog attach_cnt: " + attach_cnt);
		logger.info("### fileAttachLog attachId: " + attachId);
		
		
		if (filepath != null && !"".equals(filepath)) {
			
			int seq = attachRepository.getAttachProgressSeq(progressId, String.valueOf(attachId));
			//해당 파일이 업로드 되있는지 확인후 update or insert
			ProgressAttach progressAttach = new ProgressAttach();
			if(seq > 0) {
				// update 업로드 상태 반영			
				progressAttach.setSeq(seq);
				progressAttach.setPath(filepath+fileName);
				progressAttach.setUpload_yn(1);
				progressAttach.setTransfer_yn(1);
				
				attachRepository.updateAttachFile(progressAttach);
			}else {
				// insert 업로드 상태 반영				
				progressAttach.setProgress_id(progressId);
				progressAttach.setEform_attach_id(String.valueOf(attachId));
				progressAttach.setPath(filepath+fileName);
				progressAttach.setUpload_yn(1);
				progressAttach.setTransfer_yn(1);
				progressAttach.setAdmin_confirm_yn(1);	
				
				attachRepository.progressAttachInsert(progressAttach);
			}		
			
			ret = true;
			
			//서식 서비스는 파일 갯수가 무조건 정해짐
			if(progress_type.trim().equals("1")){
				List<EformAttach> list = attachRepository.eFormAttachSelectByProgressId1(progressId);
				if(!list.isEmpty()){
					for(EformAttach entity : list){
						logger.info("### fileAttachLog progress_type.equals(1): " + entity.toString());
						if(entity.getUpload_yn() != 1)
							attachYnBoo = false;
					}
				}
			}else{
				List<EformAttach> list = null;
				if(type.endsWith("1")) {
					list = eFormAttachSelectByProgressId3(progressId);
				}else {
					list = eFormAttachSelectByProgressId2(progressId);
				}				
				logger.info("### fileAttachLog list.size(): " + list.size());
				logger.info("### fileAttachLog attach_cnt: " + attach_cnt);
				if(list.size() != attach_cnt)
					attachYnBoo = false;
			}

		}else {
			// 파일업로드 실패
			return false;
		}
		if(attachYnBoo){
			//전자서식
			if(progress_type.equals("1")){
				//progress 불러오기
				Progress progress = new Progress();
				progress.setSeq(Integer.parseInt(progressId));				
				
				Progress resProgress = progressRepository.progressInfoSelectByProgressId(progress);
				
				Date date = new Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
				
				//전문 데이터 생성
				String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
				String CONFLDNBR	= resProgress.getContract_code();	
				String CUSTMRCDE	= resProgress.getCustomer_code();
				
				ArrayList<String> codeList	= new ArrayList<String>();
				ArrayList<String> fileList	= new ArrayList<String>();
				
				List<EformAttach> list = attachRepository.eFormAttachSelectByProgressId1(progressId);
				for(EformAttach entity : list){
					//현재 전자청약에서는 신분증만 찍어보내므로 고정
					codeList.add("ED010002");
					//path가 아닌 파일명이므로 substring 고유번호 필요
					String path = entity.getPath();					
					fileList.add(path.substring(path.lastIndexOf("/")+1));
					logger.info("file = "+path.substring(path.lastIndexOf("/")+1));
				}
				
				//String format 앞에 자릿수만큼 0으로 채움
				String file_cnt = String.format("%03d", codeList.size());	
				
				//전문 데이터 등록
				OMAP_0007 td1 = new OMAP_0007("OMAP", 
						"0007", 
						TRDSEQNBR, 
						resProgress.getService_type(), 
						dateFormat.format(date), 
						timeFormat.format(date), 
						"",
						"", 
						CONFLDNBR,
						CUSTMRCDE,
						file_cnt, 
						codeList,
						fileList);
				
				//전송 전문 저장
				HashMap<String, String> sendLog = td1.getResult();
				logger.info("## OMAP_0007 Log Save(eFormAttachFileSave) - seq : "+resProgress.getSeq());
				logger.info("## OMAP_0007 (eFormAttachFileSave) [{}]"+ sendLog);
				omapLogService.omapLogInsert(sendLog);
				
				//수신 전문 생성
				OMAP_0007 td2 = new OMAP_0007();
				//전문  전송
				boolean isConnect = SocketManager.connection(td1, td2);
				if(!isConnect) {
					logger.info("### Connection Error");
					return false;
				}			
				td2.print();
				
				//수신 전문 저장
				HashMap<String, String> receiveLog = td2.getResult();
				omapLogService.omapLogUpdate(receiveLog);
				
				//상태 완료 업데이트
				progress.setProgress_status("5");
				progressRepository.progressStatusUpdate(progress);
				progressRepository.attachStatusUpdate(progressId);
			}
			//파일업로드
			else{
				//progress 상태 변경
				Progress progress = new Progress();
				progress.setSeq(Integer.parseInt(progressId));
				
				Progress resProgress = progressRepository.progressInfoSelectByProgressId(progress);
				
				Date date = new Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
				
				//전문 데이터 생성
				String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
				String PARAM1	= resProgress.getParam1();	
				String PARAM2	= resProgress.getParam2();	
				String PARAM3	= resProgress.getParam3();	
				String PARAM4	= resProgress.getParam4();	
				
				ArrayList<String> codeList	= new ArrayList<String>();
				ArrayList<String> fileList	= new ArrayList<String>();
				
				List<EformAttach> list = attachRepository.eFormAttachSelectByProgressId2(progressId);
				for(EformAttach entity : list){
					String attach_id = entity.getEform_attach_id().trim();
					if("0".equals(attach_id)) {
						codeList.add("ED010002");
					}else {
						codeList.add("ED019999");
					}
					logger.info("code = "+entity.getEform_attach_id());
					//path가 아닌 파일명이므로 substring 고유번호 필요
					String path = entity.getPath();					
					fileList.add(path.substring(path.lastIndexOf("/")+1));
					logger.info("file = "+path.substring(path.lastIndexOf("/")+1));
				}
				
				//String format 앞에 자릿수만큼 0으로 채움
				String file_cnt = String.format("%03d", codeList.size());				
				
				//전문 데이터 등록
				OMAP_0110 td1 = new OMAP_0110("OMAP", 
						"0110", 
						TRDSEQNBR, 
						resProgress.getService_type(), 
						dateFormat.format(date), 
						timeFormat.format(date), 
						"",
						"", 
						PARAM1,
						PARAM2,
						PARAM3,
						PARAM4,
						file_cnt, 
						codeList,
						fileList);
				
				//전송 전문 저장
				HashMap<String, String> sendLog = td1.getResult();
				logger.info("## OMAP_0110 Log Save(eFormAttachFileSave) - seq : "+resProgress.getSeq());
				logger.info("## OMAP_0110 (eFormAttachFileSave) [{}]"+ sendLog);
				omapLogService.omapLogInsert2(sendLog);
				
				//수신 전문 생성
				OMAP_0110 td2 = new OMAP_0110();
				
				//전문  전송
				boolean isConnect = SocketManager.connection(td1, td2);
				if(!isConnect) {
					logger.info("### Connection Error");
					return false;
				}
				
				td2.print();
				
				//수신 전문 저장
				HashMap<String, String> receiveLog = td2.getResult();
				omapLogService.omapLogUpdate2(receiveLog);

				//상태 완료 업데이트
				progress.setProgress_status("5");
				progressRepository.progressStatusUpdate(progress);
				progressRepository.attachStatusUpdate(progressId);
			}
		}
		
		logger.info("### fileAttachLog End..");
		return ret;
	}
	
public Boolean attachCompleteFileSave(String progressId , String param1) throws IOException{
		
		boolean ret = true;
		
		logger.info("### fileAttachLog Start..");
		logger.info("### fileAttachLog progressId :" + progressId);
		logger.info("### fileAttachLog param1 : " + param1);

		//파일업로드
		//progress 상태 변경
		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progressId));

		Progress resProgress = progressRepository.progressInfoSelectByProgressId(progress);

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

		//전문 데이터 생성
		String TRDSEQNBR	= Integer.toString(omapLogService.omapLogNewSeqSelect());
		String PARAM1	= resProgress.getParam1();	
		String PARAM2	= resProgress.getParam2();	
		String PARAM3	= resProgress.getParam3();	
		String PARAM4	= resProgress.getParam4();	

		ArrayList<String> codeList	= new ArrayList<String>();
		ArrayList<String> fileList	= new ArrayList<String>();

		List<EformAttach> list = attachRepository.eFormAttachSelectByProgressId2(progressId);
		for(EformAttach entity : list){
			String attach_id = entity.getEform_attach_id().trim();
			if("0".equals(attach_id)) {
				codeList.add("ED010002");
			}else {
				codeList.add("ED019999");
			}
			logger.info("### code = "+entity.getEform_attach_id());
			//path가 아닌 파일명이므로 substring 고유번호 필요
			String path = entity.getPath();					
			fileList.add(path.substring(path.lastIndexOf("/")+1));
			logger.info("### file = "+path.substring(path.lastIndexOf("/")+1));
		}

		//String format 앞에 자릿수만큼 0으로 채움
		String file_cnt = String.format("%03d", codeList.size());				

		//전문 데이터 등록
		OMAP_0110 td1 = new OMAP_0110("OMAP", 
				"0110", 
				TRDSEQNBR, 
				resProgress.getService_type(), 
				dateFormat.format(date), 
				timeFormat.format(date), 
				"",
				"", 
				PARAM1,
				PARAM2,
				PARAM3,
				PARAM4,
				file_cnt, 
				codeList,
				fileList);

		//전송 전문 저장
		HashMap<String, String> sendLog = td1.getResult();
		logger.info("## OMAP_0110 Log Save(attachCompleteFileSave) - seq : "+resProgress.getSeq());
		logger.info("## OMAP_0110 (attachCompleteFileSave) [{}]"+ sendLog);
		omapLogService.omapLogInsert2(sendLog);

		//수신 전문 생성
		OMAP_0110 td2 = new OMAP_0110();

		//전문  전송
		boolean isConnect = SocketManager.connection(td1, td2);
		if(!isConnect) {
			logger.info("### Connection Error");
			return false;
		}
		
		td2.print();

		//수신 전문 저장
		HashMap<String, String> receiveLog = td2.getResult();
		omapLogService.omapLogUpdate2(receiveLog);

		//상태 완료 업데이트
		progress.setProgress_status("5");
		progressRepository.progressStatusUpdate(progress);
		progressRepository.attachStatusUpdate(progressId);

		logger.info("### fileAttachLog End..");
		return ret;
	}
	
	/**
	 * 첨부이미지 SFTP 파일 업로드
	 * @param file
	 * @param file_name
	 * @return
	 */
	private String putFileUpload(MultipartFile file, String file_name, String param1) {
		boolean result = false;
		String fullPath = "";
		SFTPHandler sftphandler = new SFTPHandler();
		try {
			//sftp 초기화
			sftphandler.init(sftp_ip, sftp_id, sftp_pw, Integer.valueOf(sftp_port));
			
			Calendar cal = Calendar.getInstance();
			String year = String.format("%04d", cal.get(Calendar.YEAR));
			String dateString = String.format("%04d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
			
			//디렉토리 생성 및 fullpath 리턴
			fullPath = sftphandler.mkdirDir(year + "/" + dateString + "/" + param1);
			//sftp업로드     파일, 파일이름, 디렉토리(서버)
			result = sftphandler.uploadFile(file,file_name, fullPath);
			
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}finally {
			sftphandler.disconnection();
		}
		if(result){
			return fullPath;
		}
		
		return null;
	}
	
	// 파일업로드 관련 조회
	public List<EformAttach> getFileUploadAttachList(String progress_id) {
		List<EformAttach> eFormList = new ArrayList<EformAttach>();

		Progress progress = new Progress();
		progress.setSeq(Integer.parseInt(progress_id));

		Progress resProgress =  progressService.progressInfoSelectByProgressId(progress);

		ApiResponse<Map<String , Object>> responseJson = new ApiResponse<Map<String,Object>>();
		responseJson.setResult(true);
		responseJson.setType(resProgress.getService_type());

		if(resProgress.getProgress_type().equals("1")){
			eFormList = eFormAttachSelectByProgressId1(progress_id);
			responseJson.setTotalItems(eFormList.size());
			responseJson.setList(eFormList);
		}
		else{
			Integer file_cnt = 10;

			if(resProgress.getService_type().equals("01") || resProgress.getService_type().equals("11")){
				file_cnt = resProgress.getFile_cnt();
			}

			for(int i = 0; i<file_cnt; i++){
				//주민등록증은 무조건 필수처리
				String file_name = "이미지".concat(Integer.toString(i));
				if(i == 0){
					file_name = "신분증";
				}
				EformAttach eformAttach = new EformAttach();
				eformAttach.setSeq(i);
				eformAttach.setProgress_id(progress_id);
				eformAttach.setEform_attach_id(Integer.toString(i+1));
				eformAttach.setUpload_yn(0);
				eformAttach.setTransfer_yn(0);
				eformAttach.setAdmin_confirm_yn(0);
				eformAttach.setId(Integer.toString(i));
				eformAttach.setName(file_name);
				eformAttach.setEform_id(Integer.toString(i));

				eFormList.add(eformAttach);
			}
		}
		return eFormList;
	}

	/*private String putFileUpload2(MultipartFile file, String file_name, String param1){		
		boolean result = false;
		String fullPath = "";
		Calendar cal = Calendar.getInstance();
		String year = String.format("%04d", cal.get(Calendar.YEAR));
		String dateString = String.format("%04d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
		
		try {
			fullPath = year + "/" + dateString + "/" + param1;
			// Prepare phase
			
	        Path tempFile = convert(file, fullPath, file_name).toPath();
	        
	        
	         * 경로 출력
	         
	        // 경로 루트
			System.out.printf("Root   : %s \n" , tempFile.getRoot());
			// 경로의 부모
			System.out.printf("Parent : %s \n", tempFile.getParent());
			// 경로의 요소
			for (int i = 0; i < tempFile.getNameCount(); i++) {
				System.out.printf("getNameCount %d : %s \n"  ,i, tempFile.getName(i));	
			}
			// 서브경로
			System.out.printf("subu path : %s \n", tempFile.subpath(0, tempFile.getNameCount()));
			//System.out.println();
			// 경로를 실제 경로로 변환
			// path가 실제로 존재하지 않으면 에러가 발생
//			Path real_path = tempFile.toRealPath(LinkOption.NOFOLLOW_LINKS);
//			//System.out.println(real_path);
 
			// 경로를 파일로 변환
			File path_to_file = tempFile.toFile();
 
			// 파일에서 path추출
			Path file_to_path = path_to_file.toPath();
			
			fullPath = file_to_path.toString();
 
			// 파일에서 파일 이름
			System.out.printf("Path to file name : %s \n" , path_to_file.getName());
			// 파일에서 추출한 path에서의 경로
			System.out.printf("File to path      : %s \n" , file_to_path.toString());
	        
	         * 경로 출력 끝
	         

	        // test phase
	        gateway.upload(tempFile.toFile());
	        
	        result = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(result){
			return fullPath;
		}
        return null;
	}
	
	public File convert(MultipartFile file, String fullPath, String file_name) throws IOException
	{   
		FileOutputStream fos = null;
		File convFile = null;
		
		//System.out.println(System.getProperty("java.io.tmpdir") + "/" + file.getOriginalFilename());
		//System.out.println();

		try {
			convFile = new File(System.getProperty("java.io.tmpdir") + "/" + file.getOriginalFilename());
		    convFile.createNewFile(); 
		    fos = new FileOutputStream(convFile); 
		    fos.write(file.getBytes());		    
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fos.close(); 
		}
	    
	    return convFile;
	}*/
}
