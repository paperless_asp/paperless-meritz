package net.ib.paperless.service;


import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ib.paperless.repository.OmapLogRepository;

@Service
public class OmapLogService {
	

	@Autowired
	OmapLogRepository omapLogRepository;
	public int omapLogNewSeqSelect(){
		return omapLogRepository.omapLogNewSeqSelect();
	}
	
	public int omapLog0006NumberSelect(){
		return omapLogRepository.omapLog0006NumberSelect();
	}
	
	public int omapLogSeqSelect(HashMap<String, String> data){
		return omapLogRepository.omapLogSeqSelect(data);
	}
	public int omapLogInsert(HashMap<String, String> data){
		return omapLogRepository.omapLogInsert(data);
	}
	
	public int omapLogUpdate(HashMap<String, String> data){
		return omapLogRepository.omapLogUpdate(data);
	}
	
	public int omapLogInsert2(HashMap<String, String> data){
		return omapLogRepository.omapLogInsert2(data);
	}
	
	public int omapLogUpdate2(HashMap<String, String> data){
		return omapLogRepository.omapLogUpdate2(data);
	}
}
