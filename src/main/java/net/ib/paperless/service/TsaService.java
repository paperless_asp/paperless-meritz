package net.ib.paperless.service;

import java.io.File;
import java.util.Calendar;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.dreamsecurity.common.exception.DTException;
import com.dreamsecurity.pdfsigner.PDFSigner;
import com.dreamsecurity.pdfsigner.PDFSignerArg;

import net.ib.paperless.common.SFTPHandler;
import net.ib.paperless.domain.Billing;
import net.ib.paperless.repository.BillingRepository;
import net.ib.paperless.repository.efromTsaRepository;

@Service
public class TsaService {
	private static final Logger logger = LoggerFactory.getLogger(TsaService.class);
	// TSA 인증 코스콤 URL
	private final String kftc_url_test = "http://211.175.81.101/tsp.php";	// 개발
	private final String kftc_url_real = "http://tsp.signkorea.com:8008/tsp.php";	// 운영 210.207.195.131

	@Value("${deploy.server}")
	private String deployServer;
	
	@Value("${sftp_ip}")
	private String sftp_ip;
	
	@Value("${sftp_id}")
	private String sftp_id;
	
	@Value("${sftp_pw}")
	private String sftp_pw;
	
	@Value("${sftp_port}")
	private String sftp_port;
	
	@Autowired
	ServletContext servletContext;	
	
	@Autowired
	efromTsaRepository efromtsaRepository;
	
	@Autowired
	BillingRepository billingRepository; 
	
	public boolean timestampCertificate(String ct_code,String progress_id,String timeStampNow) {
		String org_pdf_base_path = null;
		boolean result = false;
		
		//export한 파일명 계약번호+progress_id
		String file_name = ct_code + "_" + progress_id;
		
		//저장될 파일명 정의
		String saveFileName = ct_code + "_" + progress_id + "_" + timeStampNow;
		
		try {
			org_pdf_base_path = servletContext.getRealPath("/org_pdf");
			logger.info("### TEMP FILE Location [{}]", org_pdf_base_path);
			PDFSigner pdfSigner = new PDFSigner();	
			//config 파일 경로  설정(was에서 사용시 절대경로)
			PDFSignerArg pdfSingerArg = new PDFSignerArg();
			
//			String propertyPath  =  File.separator +  "WEB-INF" + File.separator + "lib" + File.separator + "pdfsigner.properties";
			String propName = "pdfsigner.properties";
			if (deployServer.equals("dev")) {
				propName = "pdfsigner-dev.properties";
			} else if (deployServer.equals("initech_dev")) {
				propName = "pdfsigner-initech-dev.properties";
			} else if (deployServer.equals("initech_real")) {
				propName = "pdfsigner-initech-real.properties";
			}
			String propertyPath  =  servletContext.getRealPath("/WEB-INF/lib") + File.separator + propName;
			pdfSingerArg.setPropertyPath(propertyPath);

			
			pdfSigner.init(pdfSingerArg);
			//시점확인 토큰 서버 URL설정
			//금결원 개발
			if (deployServer.equals("dev")) {
				pdfSigner.setURL(kftc_url_test);
			} else if (deployServer.equals("initech_dev")) {
				pdfSigner.setURL(kftc_url_test);
			} else if (deployServer.equals("initech_real")) {
				pdfSigner.setURL(kftc_url_real);
			}
//			pdfSigner.setURL(kftc_url_test);
//			pdfSigner.setURL(kftc_url_real);
			
			//시도횟수 설정 : 기본 3
			pdfSigner.setTryCount(3);
			//connect timeout : 기본 10000ms
			pdfSigner.setConTimeOut(10000);
			//read timeout : 기본 15000ms
			pdfSigner.setReadTimeOut(15000);
			
			//타임스탬프 발급 실행(입력파일, 출력파일)
			String abs_org_file = org_pdf_base_path + File.separator + file_name + ".pdf";
			//System.out.println(new File(abs_org_file).getAbsolutePath());;
			logger.info("### org file : {}", abs_org_file);
			String abs_signed_file = org_pdf_base_path + File.separator + saveFileName +"_signed.pdf";
			logger.info("### signed file : {}", abs_signed_file);
			pdfSigner.Sign(abs_org_file, abs_signed_file);
		}catch (DTException e) {			
			//에러 발생시 에러 정보 출력
			logger.error("##pdf사인 실패##");
			logger.error("### 에러코드 : {}", e.getLastError());
			logger.error("### 에러메시지 : {}", e.getMessage());
			logger.error("",e);
			return false;
		} catch (Exception e) {
			logger.error("",e);
			return false;
		} 
		
		logger.info("### pdf사인 성공");
		//billing 이력 insert
		Billing bdata = new Billing();
		bdata.setProgress_id(progress_id);
		bdata.setType(3);//3 TSA인증 성공하면 이력 insert
		bdata.setSuccess_yn("y");
		billingRepository.billingInsert(bdata);
		logger.info("### pdf사인 Billing Log Insert ### Seq : "+ bdata.getSeq()+", Type : " + bdata.getType()
		+ ", Success_Yn : " + bdata.getSuccess_yn()+ ", Progress_id : " + bdata.getProgress_id());
		
		//SFTP 파일 업로드
		try {
			result = putFileUpload(org_pdf_base_path, file_name, saveFileName, ct_code);
		} catch (Exception e) {
			//파일 업로드하다가 에러나면?
			logger.error("### 에러 : {}", e.getCause());
			logger.error("### 에러메시지 : {}", e.getMessage());
			return false;
		}		
		if(!result) {
			return false;
		}
		
		//파일업로드에 성공했으니 로컬파일 삭제
		//원본 pdf 파일 삭제
		fileLocalDelete(org_pdf_base_path + "/" + file_name +".pdf");
		//pdf 사인된 파일 삭제
		fileLocalDelete(org_pdf_base_path + "/" + saveFileName +"_signed.pdf");
		//tif 파일 삭제
		fileLocalDelete(org_pdf_base_path + "/" + file_name +".tif");
		//jpg 파일 삭제
		fileLocalDelete(org_pdf_base_path + "/" + file_name +".jpg");
		
		return true;
	}

	/**
	 * 전자서식파일 SFTP 파일 업로드
	 * @param realPath
	 * @param file_name
	 * @return
	 */
	private boolean putFileUpload(String org_pdf_base_path, String file_name, String saveFileName, String ct_code) {
		boolean result = false;
//		boolean resultTif = false;
		boolean resultOriginal_pdf = false;
		boolean resultJpg = false;
		SFTPHandler sftphandler = new SFTPHandler();
		
		//로컬 사인된 pdf파일 경로
		String abs_signed_file = org_pdf_base_path + "/" + saveFileName +"_signed.pdf";
		logger.info("### org_pdf_base_path + saveFileName +_signed.pdf : {}", abs_signed_file);
		
		//로컬 원본 cmsjpg파일 경로
		String cmsjpg_file = org_pdf_base_path + "/" + file_name +".jpg";
		logger.info("### cmsjpg_file : {}", cmsjpg_file);
		
		//로컬 원본 tif파일 경로
//		String tif_file = org_pdf_base_path + "/" + file_name +".tif";
//		logger.info("### tif_file : {}", tif_file);
		
		//로컬 원본 pdf파일 경로
		String orignal_pdf_file = org_pdf_base_path + "/" + file_name +".pdf";
		logger.info("### orignal_pdf_file : {}", orignal_pdf_file);
		
		try {
			//sftp 초기화
			sftphandler.init(sftp_ip, sftp_id, sftp_pw, Integer.valueOf(sftp_port));
			
			Calendar cal = Calendar.getInstance();
			String year = String.format("%04d", cal.get(Calendar.YEAR));
			String dateString = String.format("%04d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
			
			//디렉토리 생성 및 fullpath 리턴  ex) 2017/20171208/PA17100755
			String fullPath = sftphandler.mkdirDir(year + "/" + dateString + "/" + ct_code);
			
			//sftp업로드     저장시킬 주소(서버), 업로드할 파일 경로(클라이언트)
			//_signed.pdf 업로드
			result = sftphandler.upload(fullPath, abs_signed_file);
			
			//tiff파일 업로드 (뒤에 _tif 파일구분 추가하여 업로드)
//			String saveTif_file = saveFileName +"_tif.tif";
//			logger.info("### saveTif_file : {}", saveTif_file);
//			resultTif = sftphandler.upload(fullPath, tif_file, saveTif_file);
			
			//원본 pdf파일 업로드 (뒤에 Timestamp 추가하여 업로드)
			String saveOriginalPdf_file = saveFileName +".pdf";
			logger.info("### saveOriginalPdf_file : {}", saveOriginalPdf_file);
			resultOriginal_pdf = sftphandler.upload(fullPath, orignal_pdf_file, saveOriginalPdf_file);
			
			//jpg파일 업로드 (뒤에 _cms.jpg 파일구분 추가하여 업로드)
			String saveJPG_file = saveFileName +"_cms.jpg";
			logger.info("### saveJPG_file : {}", saveJPG_file);
			resultJpg = sftphandler.upload(fullPath, cmsjpg_file, saveJPG_file);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sftphandler.disconnection();
		}
		//파일업로드에 실패했다면?
		if(!result) {
			return false;
		}
//		if(!resultTif) {
//			return false;
//		}
		if(!resultOriginal_pdf) {
			return false;
		}
		if(!resultJpg) {
			return false;
		}
		
		return true;
	}
	
	public void fileLocalDelete(String path) {
		File file = new File(path);
		if (file.exists()) {
			file.delete();
		}
	}
	
	public int eformCompletUpdate(String progress_id){
		return efromtsaRepository.eformCompletUpdate(progress_id);
	}
}
