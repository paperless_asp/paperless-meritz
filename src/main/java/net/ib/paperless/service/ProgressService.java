package net.ib.paperless.service;


import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ib.paperless.domain.Progress;
import net.ib.paperless.domain.ProgressStatus;
import net.ib.paperless.repository.ProgressRepository;

@Service
public class ProgressService {
	

	@Autowired
	ProgressRepository progressRepository;
	
	public Progress progressInfoSelect(Progress progress){
		
		Progress resProgress = null;
		
		if(progress.getProgress_type().equals("1")){
			resProgress = progressRepository.progressInfoSelect(progress);
		}
		else{
			resProgress = progressRepository.progressInfoSelect2(progress);
		}
		
		if(resProgress != null){
			
			return resProgress;
		}
		return null;
	}
	
	public Progress progressInfoSelectByProgressId(Progress progress){
		
		Progress resProgress = progressRepository.progressInfoSelectByProgressId(progress);
		if(resProgress != null){
			
			return resProgress;
		}
		return null;
	}
	
	public int progressInfoInsert(Progress progress){
		
		int isProgressInfoInsert = 0;
		
		if(progress.getProgress_type().equals("1")){
			isProgressInfoInsert = progressRepository.progressInfoInsert(progress);
		}
		
		if(progress.getProgress_type().equals("2")){
			isProgressInfoInsert = progressRepository.progressInfoInsert2(progress);
		}
		
		if(isProgressInfoInsert == 1){
			Progress progressInfo = null;
			if(progress.getProgress_type().equals("1")){
				progressInfo = progressRepository.progressInfoSelect(progress);
			}
			
			if(progress.getProgress_type().equals("2")){
				progressInfo = progressRepository.progressInfoSelect2(progress);
			}
			if(progressInfo == null) {
				return 0;
			}
			return progressRepository.progressStatusInsert(progressInfo.getSeq());
		}
		return isProgressInfoInsert;
	}
	
	public Progress certificationInfoByProgressId(Progress progress){
		
		Progress resProgress = progressRepository.progressInfoSelectByProgressId(progress);
		if(resProgress != null){
			
			return resProgress;
		}
		return null;
	}
	
	public int uuidsPhonenumberInsert(HashMap<String, String> uuidsInfo){
		
		return progressRepository.uuidsPhonenumberInsert(uuidsInfo);
		
	}
	
	public String uuidsPhonenumberSelectByUuid(String progress_id){
		
		return progressRepository.uuidsPhonenumberSelectByUuid(progress_id);
		
	}
	

	public int progressStatusUpdate(Progress progress){
		return progressRepository.progressStatusUpdate(progress);
	}
	
	public int progressEformIdUpdate(Progress progress){
		return progressRepository.progressEformIdUpdate(progress);
	}
	
	public int identityChkUpdate(String progress_id){
		return progressRepository.identityChkUpdate(progress_id);
	}
	
	public Progress selectProgressById(String progressId){
		return progressRepository.selectProgressById(progressId);

	}
	public ProgressStatus getProgressStatusInfo(String progress_id){
		return progressRepository.getProgressStatusInfo(progress_id);
	}
}
