package net.ib.paperless.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ib.paperless.repository.AppApiRepository;

@Service
public class AppApiService {
	

	@Autowired
	AppApiRepository appApiRepository;
	public int appInfoCheckSelectOne(Map map){
		return appApiRepository.appInfoCheckSelectOne(map);
	}
}
