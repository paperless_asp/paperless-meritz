package net.ib.paperless.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ib.paperless.domain.Billing;
import net.ib.paperless.repository.BillingRepository;

@Service
public class BillingLogService {
	

	@Autowired
	BillingRepository billingRepository;
	
	public int billingLogInsert(Billing bdata){
		return billingRepository.billingInsert(bdata);
	}
	
	public int billingLogUpdate(Billing bdata){
		return billingRepository.billingUpdate(bdata);
	}
}
