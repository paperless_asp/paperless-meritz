package net.ib.paperless.openplatform;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NhOpenPlatformConfig {
	
    @Value("${nh.apisvccd.namechk.nh}")
    private String apiSvcCodeRealNameNH;	// 당행 실명확인 서비스코드
    
    @Value("${nh.apisvccd.namechk.other}")
    private String apiSvcCodeRealNameOther;	// 타행 실명확인 서비스코드
    
    @Value("${nh.apisvccd.transfer.nh}")
    private String apiSvcCodeTransferNH;	// 당행 입급이체 서비스코드
    
    @Value("${nh.apisvccd.transfer.other}")
    private String apiSvcCodeTransferOther;	// 타행 입급이체 서비스코드
    
    @Value("${nh.fintech.apsno}")
    private String fintechApsNo;	// 핀테크 앱코드
    
    @Value("${nh.lscd}")
    private String lscd;	// 기관코드

	public String getApiSvcCodeRealNameNH() {
		return apiSvcCodeRealNameNH;
	} 

	public void setApiSvcCodeRealNameNH(String apiSvcCodeRealNameNH) {
		this.apiSvcCodeRealNameNH = apiSvcCodeRealNameNH;
	}

	public String getApiSvcCodeRealNameOther() {
		return apiSvcCodeRealNameOther;
	}

	public void setApiSvcCodeRealNameOther(String apiSvcCodeRealNameOther) {
		this.apiSvcCodeRealNameOther = apiSvcCodeRealNameOther;
	}

	public String getApiSvcCodeTransferNH() {
		return apiSvcCodeTransferNH;
	}

	public void setApiSvcCodeTransferNH(String apiSvcCodeTransferNH) {
		this.apiSvcCodeTransferNH = apiSvcCodeTransferNH;
	}

	public String getApiSvcCodeTransferOther() {
		return apiSvcCodeTransferOther;
	}

	public void setApiSvcCodeTransferOther(String apiSvcCodeTransferOther) {
		this.apiSvcCodeTransferOther = apiSvcCodeTransferOther;
	}

	public String getFintechApsNo() {
		return fintechApsNo;
	}

	public void setFintechApsNo(String fintechApsNo) {
		this.fintechApsNo = fintechApsNo;
	}

	public String getLscd() {
		return lscd;
	}

	public void setLscd(String lscd) {
		this.lscd = lscd;
	}

	@Override
	public String toString() {
		return "NhOpenPlatformConfig [apiSvcCodeRealNameNH=" + apiSvcCodeRealNameNH + ", apiSvcCodeRealNameOther="
				+ apiSvcCodeRealNameOther + ", apiSvcCodeTransferNH=" + apiSvcCodeTransferNH
				+ ", apiSvcCodeTransferOther=" + apiSvcCodeTransferOther + ", fintechApsNo=" + fintechApsNo + ", lscd="
				+ lscd + "]";
	}
}
