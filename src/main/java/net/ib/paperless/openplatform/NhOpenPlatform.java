package net.ib.paperless.openplatform;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.extrus.common.json.simple.JSONObject;
import com.extrus.common.json.simple.parser.JSONParser;
import com.extrus.common.json.simple.parser.ParseException;
import com.extrus.exafe.ota.client.common.exception.OTAClientException;

import fintech.client.net.NetClient;
import fintech.client.net.NetManager;

@Service
public class NhOpenPlatform {

	private static final Logger logger = LoggerFactory.getLogger(NhOpenPlatform.class);
	
	private final String apiNm = "001";
	
	NetClient client;
	
	@Autowired
	private NhOpenPlatformConfig nhConfig;
	
	private Map<String, Object> sendHeader;	// json header
	private Map<String, Object> sendBody;	// json body
	
	private static Map<String, Object> resultHeader;
	private static Map<String, Object> resultBody;
	
	private static boolean isSuccess;
	
	private static String bankCode;
	private static String step;	// realname or transfer
	
	public void init(String reqStep, String code) throws OTAClientException {
		sendHeader = new LinkedHashMap<String, Object>();
		sendBody = new LinkedHashMap<String, Object>();
		resultHeader = new LinkedHashMap<String, Object>();
		resultBody = new LinkedHashMap<String, Object>();
		client = NetManager.getInstance().getNetClient(apiNm);
		bankCode = code;
		step = reqStep;
	}
	
	public NhOpenPlatform addHeader(String key, String value) {
		sendHeader.put(key, value);
		return this;
	}
	
	public NhOpenPlatform addBody(String key, String value) {
		sendBody.put(key, value);
		return this;
	}
	
	
//	@SuppressWarnings("unchecked")
//	public JSONObject getJSONObject() {
//		JSONObject jsonObject = new JSONObject();
//		sendBody.put("Header", this.sendHeader);
//		jsonObject.putAll(this.sendBody);
//		return jsonObject;
//	} 
	
	@SuppressWarnings("unchecked")
	public JSONObject callAPI() {
		
		JSONObject jsonObject = new JSONObject();
		logger.info("NH Config : {}", nhConfig.toString());		
		// 예금주 확인 / 입급이체 구분
		if (step.equals("realname")) {	 
			if (bankCode.equals("011")) {	// 당행/타행 구분
				addHeader("ApiNm", "InquireDepositorAccountNumber").addHeader("ApiSvcCd", nhConfig.getApiSvcCodeRealNameNH());
			} else {
				addHeader("ApiNm", "InquireDepositorOtherBank").addHeader("ApiSvcCd", nhConfig.getApiSvcCodeRealNameOther());
			}
		} else {
			if (bankCode.equals("011")) {
				addHeader("ApiNm", "ReceivedTransferAccountNumber").addHeader("ApiSvcCd", nhConfig.getApiSvcCodeTransferNH());
			} else {
				addHeader("ApiNm", "ReceivedTransferOtherBank").addHeader("ApiSvcCd", nhConfig.getApiSvcCodeTransferOther());
			}			
		}
		addHeader("FintechApsno", nhConfig.getFintechApsNo()).addHeader("Iscd", nhConfig.getLscd());
		sendBody.put("Header", this.sendHeader);
		
		jsonObject.putAll(this.sendBody);		
		logger.info("REQ : {}", jsonObject.toString());
		
		isSuccess = false;
		Map<String, Object> result = null;
		JSONObject jsonResult = new JSONObject();
		try {
			result = client.call(jsonObject);
			jsonResult.putAll(result);	
			resultBody = result;
			JSONParser parser = new JSONParser();			
			resultHeader = toMap((JSONObject) parser.parse(result.get("Header").toString()));
			if (resultHeader != null && resultHeader.containsKey("Rpcd")) {
				if (resultHeader.get("Rpcd").equals("00000")) {
					isSuccess = true;
				}
			}
        } catch(OTAClientException e) {
        	logger.error("{}", e.getMessage());
        } catch (JSONException e) {
        	logger.error("{}", e.getMessage());
		} catch (ParseException e) {
			logger.error("{}", e.getMessage());
		}       
		return jsonResult;
	}
	
	
	@SuppressWarnings("unchecked")
	public static Map<String, Object> toMap(JSONObject object) throws JSONException {
	    Map<String, Object> map = new HashMap<String, Object>();

	    Iterator<String> keysItr = object.keySet().iterator();
	    while(keysItr.hasNext()) {
	        String key = keysItr.next();
	        Object value = object.get(key);
	        map.put(key, value);
	    }
	    return map;
	}
	
	public Map<String, Object> getResultHeader() {
		return resultHeader;
	}
	
	public Map<String, Object> getResultBody() {
		return resultBody;
	}
	
	public String getResultHeader(String key) {
		return resultHeader.get(key).toString();
	}
	
	public String getResultBody(String key) {
		return resultBody.get(key).toString();
	}
	
	public boolean isSuccess() {
		return isSuccess;
	}
}
