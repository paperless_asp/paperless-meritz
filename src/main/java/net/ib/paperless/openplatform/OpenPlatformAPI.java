package net.ib.paperless.openplatform;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import net.minidev.json.JSONObject;

@ConfigurationProperties
@Component
public class OpenPlatformAPI {
	
	private static final Logger logger = LoggerFactory.getLogger(OpenPlatformAPI.class);

	/******************************************************************************************
	 ** Static 변수 정의
	 ******************************************************************************************/
	private static String apiKey;
	private static String apiSecret;
	private static String host;
	private static String passPhrase;
	private static String bankTranId;
	private static String ownAccountNum;
	
	
	static String postMethod = "POST";
	static String getMethod = "GET";
	
//	static String getAccessTokenUrl = "/oauth/2.0/token";
//	static String getRealNameUrl = "/v1.0/inquiry/real_name";
//	static String getBankStatusUrl = "/bank/status";
//	static String setTransderDepositUrl= "/transfer/deposit2";
	static String getAccessTokenUrl = "/oauth/2.0/token";	
	static String getRealNameUrl = "/v2.0/inquiry/real_name";
	static String getBankStatusUrl = "/v2.0/bank/status";
	static String setTransderDepositUrl= "/v2.0/transfer/deposit/acnt_num";
	
    public enum httpType { FormType, JsonType}
    
    public OpenPlatformAPI(@Value("${deploy.server}") String deployServer) {
    	if (deployServer.equals("initech_real")) {	// 상용
//	    	apiKey = "l7xxe7c58d9258e44ba990341a1dc42acc3a";
//	    	apiSecret = "dc4badc2428649f7bca5c204b6ccb3aa";
//	    	host = "https://openapi.open-platform.or.kr";
//	    	passPhrase = "acef79e5d6f06e603e11d8baadbfe736428aae13100ea17d1fd5a70be754d1e2c97135426c738dd33e03f8c9cdb042ffdc704c245da3632711ed5ff91aa658de";
	    	apiKey = "l7xxe7c58d9258e44ba990341a1dc42acc3a";
	    	apiSecret = "dc4badc2428649f7bca5c204b6ccb3aa";
	    	host = "https://openapi.open-platform.or.kr";
	    	passPhrase = "acef79e5d6f06e603e11d8baadbfe736428aae13100ea17d1fd5a70be754d1e2c97135426c738dd33e03f8c9cdb042ffdc704c245da3632711ed5ff91aa658de"; 
	    	bankTranId = "F110253744";
	    	ownAccountNum = "43340101395669";
    	} else {	// 개발
//        	apiKey = "l7xx4ec42a5d79934d218f707a0f69a8fd12";
//        	apiSecret = "1157dea490e144498bc14380e7b95a03";
//        	host = "https://testapi.open-platform.or.kr";
//        	passPhrase = "NONE";    
        	apiKey = "UCx4Q5tRn4pGV3FyFw93klnpHibh4SxRZBTectSf";
        	apiSecret = "2gaC4d5UKaC78U20zGnhBej7zmpLtc2IGFM0T0WF";
        	host = "https://testapi.openbanking.or.kr";
        	passPhrase = "NONE";
        	bankTranId = "T991591730";
        	ownAccountNum = "9606996451";
    	}
    }
    
	/**
	 * 현재 시간을 조회한다.
	 * @return 현재시간(yyyymmddhhmmss)
	 */
	public static String getDateTime(){
		long time = System.currentTimeMillis(); 

		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMddHHmmss");

		String dateTime = dayTime.format(new Date(time));

		return dateTime;

	}
	
	/**
	 * 해당 계좌로 송금한다.
	 * @param outputText			출금계좌 인자내역(출금내역에 나오는 이름?)
	 * @param bankCode				은행코드
	 * @param accountNumber			입금계좌번호
	 * @param accountHolderName		입금계좌 예금주명
	 * @param inputText				입금계좌 인자내역(입금내역에 나오는 이름?)
	 * @return						처리결과(jsonString)
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, Object> setTransderDeposit(String outputText, String bankCode, String accountNumber, String accountBankName, String accountHolderName, String inputText)
	{
		HashMap<String, String> req = new HashMap<>();
		
		
//		req.put("tran_no","1");
//		req.put("bank_code_std",bankCode);
//		req.put("account_num",accountNumber);
//		req.put("account_bank_name",accountBankName);
//		req.put("account_holder_name",accountHolderName);
//		req.put("print_content",inputText);
//		req.put("tran_amt","1");
		String timeStr = getDateTime();
		String tranId = bankTranId + "U" + timeStr.substring(6, timeStr.length()) + "2";	// 개발
		
		req.put("tran_no","1");
		req.put("bank_tran_id",tranId);
		req.put("bank_code_std",bankCode);
		req.put("account_num",accountNumber);
//		req.put("account_bank_name",accountBankName);
		req.put("account_holder_name",accountHolderName);
		req.put("print_content",inputText);
		req.put("tran_amt","1");
		req.put("req_client_name","티소프트");
		req.put("req_client_bank_code",bankCode);
		req.put("req_client_account_num",accountNumber);
		req.put("req_client_num",accountNumber);		// 고객에 회원번호 부여
		req.put("transfer_purpose","AU");		
		
		Object[] reqList = new Object[]{req};
		
		HashMap<String, Object> param = new HashMap<>();
		
//		param.put("wd_pass_phrase",passPhrase);
//		param.put("wd_print_content",outputText);
//		param.put("tran_dtime",getDateTime());
//		param.put("req_cnt","1");
//		param.put("req_list",reqList);

		param.put("cntr_account_type","N");
		param.put("cntr_account_num",ownAccountNum);
		param.put("wd_pass_phrase",passPhrase);
		param.put("wd_print_content",outputText);
		param.put("name_check_option","on");
		param.put("tran_dtime",timeStr);
		param.put("req_cnt","1");
		param.put("req_list",reqList);		
		
		
		String result = httpUrlConnection(httpType.JsonType, true, setTransderDepositUrl,postMethod, param);
		
		Gson gson = new Gson();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map = (HashMap<String, Object>)gson.fromJson(result, map.getClass());
		//System.out.println(map);
		logger.info("setTransderDeposit(계좌 송금API)");
		
		return map;
		
	}
	
	/**
	 * 실효 은행 리스트를 조회한다.
	 * @return	조회결과(jsonString)
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, Object> getBankStatus(){

		String result = httpUrlConnection(httpType.JsonType, true,  getBankStatusUrl, getMethod, null);
		
		Gson gson = new Gson();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map = (HashMap<String, Object>)gson.fromJson(result, map.getClass());
		
		return map;
		
	}
	
	/**
	 * 해당 계좌의 실명인증 사실여부를 조회한다.
	 * @param bankCode		은행코드
	 * @param accountNumber	계좌번호
	 * @param birthDay		생년월일(yyyymmdd)
	 * @return				조회결과(jsonString)
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, Object> getRealName(String bankCode, String accountNumber, String birthDay){
		
		HashMap<String, Object> params = new HashMap<>();
		
//		params.put("bank_code_std",bankCode);
//		params.put("account_num",accountNumber);
//		params.put("account_holder_info",birthDay);
//		params.put("tran_dtime",getDateTime());

		String timeStr = getDateTime();
		String tranId = bankTranId + "U" + timeStr.substring(6, timeStr.length()) + "1";	// 개발
		logger.info("bank_tran_id : {}", tranId);
		params.put("bank_tran_id",tranId);
		params.put("bank_code_std",bankCode);
		params.put("account_num",accountNumber);
		params.put("account_holder_info",birthDay);
		params.put("account_holder_info_type"," ");
		params.put("tran_dtime",timeStr);		
		
		logger.info("Account Info : {}/{}/{}", bankCode, accountNumber, birthDay);
		
		String result = httpUrlConnection(httpType.JsonType, true,  getRealNameUrl,postMethod, params);
		
		Gson gson = new Gson();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map = (HashMap<String, Object>)gson.fromJson(result, map.getClass());
		return map;
	}
	
	/**
	 * accessToken을 조회한다.
	 * @return	조회결과(jsonString)
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, Object> getAccessToken(){

		HashMap<String, Object> params = new HashMap<>();
		
		params.put("grant_type","client_credentials");
		params.put("scope","oob");
		params.put("client_id",apiKey);
		params.put("client_secret",apiSecret);
		
		String result = httpUrlConnection(httpType.FormType, false, getAccessTokenUrl,postMethod, params);
		
		Gson gson = new Gson();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map = (HashMap<String, Object>)gson.fromJson(result, map.getClass());
		
		return map;
	}
	
	/**
	 * httpRequest function
	 * @param type 		content-type (formType or jsonType)
	 * @param isHeader	accessToke 포함 여부
	 * @param url		host를 제외한 url
	 * @param method	http method(post or get)
	 * @param body		hashMap<string,string> parameter
	 * @return			resopnse to jsonString
	 */
	public static String httpUrlConnection(httpType type, Boolean isHeader, String url, String method, HashMap<String, Object> body)
	{
		
		String accessToken = "";
		
		if(isHeader)
		{
			HashMap<String, Object> result = getAccessToken();
			accessToken = (String)result.get("access_token");
			logger.info("## accessToken =" + accessToken);
		}
		
		HttpsURLConnection httpsURLConnection = null;
		StringBuffer param = new StringBuffer();
		String result = "";
		String contentType = "";
		
	
		if(type == httpType.FormType)
		{
			if(body != null){
				int i = 1;
				Set<String> set = body.keySet();
				Iterator<String> iterator = set.iterator();
				while(iterator.hasNext())
				{
					String key = iterator.next();
					param.append(key).append("=").append(body.get(key));
					if(i != body.size()) {
						param.append("&");
						i++;
					}
				}
			}
			contentType = "application/x-www-form-urlencoded; charset=UTF-8";
		}
		else if(type == httpType.JsonType)
		{
			if(body != null)
			{
				JSONObject jsonObject = new JSONObject(body);
				param.append(jsonObject.toJSONString());
			}
			contentType = "application/json; charset=UTF-8";
		}
			
		
        try {
			URL httpUrl = new URL(host+url);
			httpsURLConnection = (HttpsURLConnection) httpUrl.openConnection();
			httpsURLConnection.setRequestMethod(method);
			
			httpsURLConnection.setRequestProperty("Content-type",contentType);
			httpsURLConnection.setUseCaches(false);
			if(method == postMethod) httpsURLConnection.setDoOutput(true);
			httpsURLConnection.setDoInput(true);
			if(isHeader) httpsURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
			
			if(body != null)
			{
				OutputStream outputStream = httpsURLConnection.getOutputStream();
				outputStream.write(param.toString().getBytes("UTF-8") );
				outputStream.flush();
				outputStream.close();
			}
			httpsURLConnection.connect();  
			httpsURLConnection.setInstanceFollowRedirects(true);  
			   
			String temp = null;
			   
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			while((temp = bufferedReader.readLine()) != null){
				result+= temp + "\n";
			}
			logger.info("requestUrl  = "+host+url);
			logger.info("method = " + method);
			logger.info("contentType = "+contentType);
			//System.out.println("param = "+param);
//			logger.info("result = "+result);
			logger.info("httpUrlConnection(계좌인증API)");


        } catch (Exception e) {
             e.printStackTrace();
        } finally {
             if (httpsURLConnection != null) {
            	 httpsURLConnection.disconnect();
             }
        }
        
		return result;
        
	}
	
	

}
