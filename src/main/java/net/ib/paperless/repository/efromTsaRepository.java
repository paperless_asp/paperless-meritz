package net.ib.paperless.repository;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class efromTsaRepository {
	@Autowired
	private SqlSession sqlSession;
	
	public int eformCompletUpdate(String progress_id){
		return sqlSession.update("progressMapper.electronicSignatureProgressStatusUpdate", progress_id);
	}
}
