package net.ib.paperless.repository;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.ib.paperless.domain.PhoneAuthCode;

@Repository
public class PhoneAuthRepository {

	@Autowired
	private SqlSession sqlSession;
	
	public int phoneAuthInfoInsert(PhoneAuthCode pac) {
		return sqlSession.insert("PhoneAuthMapper.phoneAuthStatusInsert", pac);
	}
	
	public PhoneAuthCode phoneAuthInfoSelectByTelAuth(PhoneAuthCode pac) {
		List<Object> list = sqlSession.selectList("PhoneAuthMapper.phoneAuthInfoSelectByTelAuth", pac);
		if(list !=null && !list.isEmpty()) {
			return (PhoneAuthCode) list.get(0);
		}
		
		return null;
	}
	
	public int phoneAuthInfoDeleteByTelAuth(PhoneAuthCode pac) {
		return sqlSession.delete("PhoneAuthMapper.phoneAuthInfoDeleteByTelAuth", pac);
	}
	
	public int phoneAuthInfoUpdateFailCountByTelAuth(PhoneAuthCode pac) {
		return sqlSession.update("PhoneAuthMapper.phoneAuthInfoUpdateFailCountByTelAuth", pac);
	}
	
	
}
