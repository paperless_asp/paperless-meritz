package net.ib.paperless.repository;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.ib.paperless.domain.PhoneAuthCode;
import net.ib.paperless.domain.Progress;
import net.ib.paperless.domain.ProgressStatus;

@Repository
public class ProgressRepository {

	@Autowired
	private SqlSession sqlSession;
	
	
	public Progress progressInfoSelect(Progress progress){
		return sqlSession.selectOne("progressMapper.progressInfoSelect",progress);
	}
	
	public Progress progressInfoSelect2(Progress progress){
		return sqlSession.selectOne("progressMapper.progressInfoSelect2",progress);
	}
	
	public Progress progressInfoSelectByProgressId(Progress progress){
		return sqlSession.selectOne("progressMapper.progressInfoSelectByProgressId",progress);
	}
	
	public int progressInfoInsert(Progress progress){
		return sqlSession.insert("progressMapper.progressInfoInsert", progress);
	}
	
	public int progressInfoInsert2(Progress progress){
		return sqlSession.insert("progressMapper.progressInfoInsert2", progress);
	}
	
	public int progressStatusInsert(int seq){
		return sqlSession.insert("progressMapper.progressStatusInsert", seq);
	}
	
	public int phoneAuthInfoInsert(PhoneAuthCode pac) {
		return sqlSession.insert("progressMapper.phoneCertificationStatusInsert", pac);
	}
	
	public int uuidsPhonenumberInsert(HashMap<String, String> uuidsInfo){
		return sqlSession.update("progressMapper.uuidsPhonenumberInsert", uuidsInfo);
	}
	
	public String uuidsPhonenumberSelectByUuid(String progress_id){
		return sqlSession.selectOne("progressMapper.uuidsPhonenumberSelectByUuid", progress_id);
	}
	public int progressStatusUpdate(Progress progress){
		return sqlSession.update("progressMapper.progressStatusUpdate", progress);
	}
	
	public int progressEformIdUpdate(Progress progress){
		return sqlSession.update("progressMapper.progressEformIdUpdate", progress);
	}
	
	public int identityChkUpdate(String progress_id){
		return sqlSession.update("progressMapper.identityChkUpdate",progress_id);
	}
	
	public int attachStatusUpdate(String progress_id){
		return sqlSession.update("progressMapper.attachStatusUpdate",progress_id);
	}
	
	public Progress selectProgressById(String progressId) {
		return sqlSession.selectOne("progressMapper.selectProgressById", progressId);
	}
	
	public ProgressStatus getProgressStatusInfo(String progress_id) {
		return sqlSession.selectOne("progressMapper.getProgressStatusInfo", progress_id);
	}
}
