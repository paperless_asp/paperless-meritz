package net.ib.paperless.repository;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OmapLogRepository {

	@Autowired
	private SqlSession sqlSession;
	public int omapLogNewSeqSelect(){
		int seq;
		try{
			seq = sqlSession.selectOne("omapLogMapper.omapLogNewSeqSelect");
		}catch (Exception e) {
			seq = 1;
		}
		return seq;
	}
	
	public int omapLog0006NumberSelect(){
		int seq;
		try{
			seq = sqlSession.selectOne("omapLogMapper.omapLog0006NumberSelect");
		}catch (Exception e) {
			seq = 1;
		}
		return seq;
	}
	
	public int omapLogSeqSelect(HashMap<String, String> data){
		return sqlSession.selectOne("omapLogMapper.omapLogSeqSelect", data);
	}
	
	public int omapLogInsert(HashMap<String, String> data){
		return sqlSession.insert("omapLogMapper.omapLogInsert",data);
	}
	
	public int omapLogUpdate(HashMap<String, String> data){
		return sqlSession.update("omapLogMapper.omapLogUpdate",data);
	}
	
	public int omapLogInsert2(HashMap<String, String> data){
		return sqlSession.insert("omapLogMapper.omapLogInsert2",data);
	}
	
	public int omapLogUpdate2(HashMap<String, String> data){
		return sqlSession.update("omapLogMapper.omapLogUpdate2",data);
	}
	
}
