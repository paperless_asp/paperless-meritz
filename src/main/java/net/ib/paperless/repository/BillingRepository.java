package net.ib.paperless.repository;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.ib.paperless.domain.Billing;

@Repository
public class BillingRepository {
		@Autowired
		private SqlSession sqlSession;
		
		public int billingInsert(Billing billing){
			return sqlSession.insert("billing.billingInsert",billing);
		}
		
		public int billingUpdate(Billing billing){
			return sqlSession.update("billing.billingUpdate",billing);
		}
}
