package net.ib.paperless.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.ib.paperless.domain.AttachInfo;
import net.ib.paperless.domain.EformAttach;
import net.ib.paperless.domain.ProgressAttach;

@Repository
public class AttachRepository {
	@Autowired
	private SqlSession sqlSession;
	
	public List<AttachInfo> getAttachList(String progress_id){
		return sqlSession.selectList("attchMapper.getAttachList",progress_id);
	}

	public List<EformAttach> eFormAttachSelectByProgressId1(String progressId) {
		return sqlSession.selectList("attchMapper.EFormAttachSelectByProgressId1", progressId);
	}
	
	public List<EformAttach> eFormAttachSelectByProgressId2(String progressId) {
		return sqlSession.selectList("attchMapper.EFormAttachSelectByProgressId2", progressId);
	}
	
	public List<EformAttach> eFormAttachSelectByProgressId3(String progressId) {
		return sqlSession.selectList("attchMapper.EFormAttachSelectByProgressId3", progressId);
	}
	
	public int progressAttachInsert(ProgressAttach progressAttach){
		return sqlSession.insert("attchMapper.progressAttachInsert",progressAttach);
	}
	
	public int getAttachProgressSeq(String progress_id, String eform_attach_id){
		Map<String,String> map = new HashMap<String,String>();
		map.put("progress_id", progress_id);
		map.put("eform_attach_id", eform_attach_id);
		List<Integer> list = sqlSession.selectList("attchMapper.getAttachProgressSeq",map);
		if(list.isEmpty()) {
			return 0;
		}
		return list.get(0);
	}
	
	public int updateAttachFile(ProgressAttach progressAttach){
		return sqlSession.update("attchMapper.updateAttachFile",progressAttach);
	}
}
