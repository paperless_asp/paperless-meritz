package net.ib.paperless.repository;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AppApiRepository {
	
	@Autowired
	private SqlSession sqlSession;
	
	public int appInfoCheckSelectOne(Map map){
		return sqlSession.selectOne("appApiMapper.appInfoCheckSelectOne",map);
	}
}
