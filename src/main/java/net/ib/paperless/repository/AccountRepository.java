package net.ib.paperless.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.ib.paperless.domain.AccountHistoryInfo;
import net.ib.paperless.domain.AccountVerifyInfo;
import net.ib.paperless.domain.BankInfo;

@Repository
public class AccountRepository {
	@Autowired
	private SqlSession sqlSession;
	public int accountVerifyInfoInsert(AccountVerifyInfo codeInfo){
//		Integer insertSmsCode = sqlSession.insert("accountMapper.smsAuthCodeInsert",codeInfo);
		return sqlSession.insert("accountMapper.accountVerifyInfoInsert",codeInfo);
	}
	
	public int accountTransferUpdate(AccountVerifyInfo codeInfo){
		return sqlSession.update("accountMapper.accountTransferUpdate",codeInfo);
	}
	
	public AccountVerifyInfo getAuthCodeInfo(String progress_id){
		return sqlSession.selectOne("accountMapper.getAuthCodeInfo",progress_id);
	}
	
	public int accountTransferFailUpdate(String progress_id){
		return sqlSession.update("accountMapper.accountTransferFailUpdate", progress_id);
	}
	
	public int accountVerifyUpdate(String progress_id){
		return sqlSession.update("accountMapper.accountVerifyUpdate", progress_id);
	}
	
	public int accountVerifyCntUpdate(String progress_id){
		return sqlSession.update("accountMapper.accountVerifyCntUpdate", progress_id);
	}
	
	public int accountVerifyCodeDelete(String progress_id){
		return sqlSession.delete("accountMapper.accountVerifyCodeDelete",progress_id);
	}
	
	public int accountFailInfoInsert(HashMap<String, Object> map){
		return sqlSession.insert("accountMapper.accountFailInfoInsert",map);
	}
	
	public int accountHistoryInfoInsert(AccountHistoryInfo codeInfo){
		return sqlSession.insert("accountMapper.accountHistoryInfoInsert",codeInfo);
	}
	
	public int getHistoryCount(Map<String,Object> params){
		return sqlSession.selectOne("accountMapper.getHistoryCount",params);
	}
	public List<BankInfo> selectBankInfoList() {
		return sqlSession.selectList("accountMapper.getAvailableBankList");
	}
	
	public int accountNHPublicFailInfoInsert(HashMap<String, Object> map){
		return sqlSession.insert("accountMapper.accountNHPublicFailInfoInsert",map);
	}
}
