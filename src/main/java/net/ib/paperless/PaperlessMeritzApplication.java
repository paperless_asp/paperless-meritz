package net.ib.paperless;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
//@IntegrationComponentScan
//@EnableIntegration
public class PaperlessMeritzApplication {
	public static void main(String[] args) throws IOException {
		SpringApplication.run(PaperlessMeritzApplication.class, args);
    }
	
}
